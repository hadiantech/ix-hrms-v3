<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeAsset extends Model
{
    protected $guarded = [];
    protected $table = 'asset_assigns';

    public function assign(){
        return $this->belongsTo(Assign::class,'assign_type');
    }




}
