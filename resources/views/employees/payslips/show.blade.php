<div class="modal fade" id="view-summary-payslip">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <span>VIEW SUMMARY</span>
                <h1>APRIL 2018</h1>
                <hr>
                 <div class="payslip-summary-info">
                    <h3>Staff Overview</h3>
                    <p>Employee ID : xxxxx<br>
                        Employee Name : Tajul Tarmizi bin Takisahlah<br>
                        Payslip No. : #123<br>
                        Department : Sales Solution<br>
                    Designation : Account Manager</p>
                    <br>
                    <h3>Salary Overview</h3>
                    <p>Basic Salary : xxxxx<br>
                        Overtime : Tajul Tarmizi bin Takisahlah<br>
                        Allowance : #123<br>
                        Bonus : Sales Solution<br>
                    Deduction : Account Manager</p>
                    <br>
                    <h3>Total Earning</h3>
                    <p>Total :<br></p>
                    <hr>
                 </div>
                <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Print Payslip</button>
                            <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
 </div>
