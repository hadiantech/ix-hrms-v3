<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetBrand extends Model
{
    public $table = "asset_brands";

    protected $fillable = ['brand_name','brand_asset','status'];

    public function type(){
        return $this->belongsTo('App\AssetType', 'brand_asset');
    }

    public function assign(){
        return $this->hasMany('App\Assign','assign_brand');
    }

    public function employee(){
        return $this->belongsTo(Employee::class);
    }

}
