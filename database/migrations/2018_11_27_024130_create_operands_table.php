<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_operands', function (Blueprint $table) {
            $table->increments('id');
            $table->string('operand_name')->nullable();
            $table->timestamps();
        });

        // Insert some stuff
        DB::table('payroll_operands')->insert([
            ['operand_name' => 'Addition'],
            ['operand_name' => 'Deduction'],
        ]);
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_operands');
    }
}
