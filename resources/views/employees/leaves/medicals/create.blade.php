<div id="medical-leave-form" class="modal fade in" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <p class="text-uppercase">Apply Leave</p>
                <h1>Medical Leave </h1>
                @javascript('key',$dates)
                {!! Form::open(['url' => 'medics','id'=>'medical_leave_create','files'=>true]) !!}
                {{  Form::hidden('ltype_id','3') }}
                   <div class="row form-group">

                    <div class="col-md-4">
                        {!! Form::label('Start Date') !!}
                        <div class="input-group">
                                <input type="text" name="started_at" id="leave_from_medical" class="form-control datepicker"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('End Date') !!}
                        <div class="input-group">
                                <input type="text" name="ended_at" id="leave_to_medical" class="form-control datepicker"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('Half Day?') !!}
                        <select name="half_day" class="form-control">
                            <option value="">No</option>
                            <option value="0.5">Halfday AM on Start Date</option>
                            <option value="0.5">Halfday PM on Start Date</option>
                            <option value="0.5">Halfday AM on End Date</option>
                            <option value="0.5">Halfday PM on End Date</option>
                        </select>
                    </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                                {!! Form::label('Leave Reason') !!}
                                {{Form::textarea('reason', null , ['required','class' => 'form-control','placeholder' => 'Leave Reason'])}}
                        </div>
                    </div>
                    <div class="col-md-6">
                            {!! Form::label('attachments','Attachment') !!}
                            <div class="input-group">
                            {{ Form::file('file', null, array('required','class' => 'form-control', 'placeholder' => 'Attach Document')) }}
                            </div>
                    </div>
                    <div class="row form-group ">
                        <div class="col-md-6">
                            <p class="text-muted m-b-0 text-uppercase">Total Leave : <span id="mleave_days">0.0</span> days</p>
                        </div>
                        <div class="col-md-6">
                            <p class="text-muted m-b-0 text-uppercase">Balance: <span id="mbalance_days">{{ $medical_leave_quota - $medical_leave_taken }}</span> days</p>
                        </div>
                        <div class="col-md-6">
                            <p class="text-muted m-b-0 text-uppercase">Total Unpaid Leave : <span id="munpaid_days">0.0</span> days</p>
                        </div>
                        {{-- <div class="col-md-12">
                            <p class="m-b-0 text-uppercase">This application will proceed as <em>Unpaid Leave</em>.</p>
                        </div> --}}
                    </div>
                {{Form::submit('Submit Application' , ['class' => 'm-r-5 text-uppercase btn btn-info'])}}
                {!! Form::close() !!}
            </div>

        </div>
    </div>
</div>


@section('js')
    @parent

<script>

        $(function () {
            $("#leave_from_medical").datepicker({
                daysOfWeekDisabled: [0, 6],
                format: 'yyyy-mm-dd',
                datesDisabled:key,
                autoclose: true,
                todayHighlight: true

            });

        });

</script>
<script>

            $(function () {
                $("#leave_to_medical").datepicker({
                    daysOfWeekDisabled: [0, 6],
                    format: 'yyyy-mm-dd',
                    startDate: '+0d',
                    datesDisabled:key,
                    autoclose: true,
                    todayHighlight: true

                });
            });
</script>
    <script>
        $(document).ready(function(){
        });
        unpaid = 0;

        $('#medical-leave-form').on('hidden.bs.modal', function(e) {
            $(this).find('#medical_leave_create')[0].reset();
            $("#mleave_days").text(0.0);
            $("#munpaid_days").text(0.0);
            unpaid = 0;
        });

        $('#leave_from_medical').change(function() {
            var date = $("#leave_from_medical").val();
            $("#leave_to_medical")[0].setAttribute('min', date);
            $("#leave_to_medical").val('');
            $("#mleave_days").text(0.0);
        });

        $('#leave_to_medical').change(function() {
            a = moment($("#leave_to_medical").val());
            b = moment($("#leave_from_medical").val());
            days = a.diff(b, 'days');
            days = days + 1;
            $("#mleave_days").text(days);

            balance = $("#mbalance_days").text();
            unpaid = balance - days;

            if(unpaid < 0){
                unpaid_plus = Math.abs(unpaid);
                $("#munpaid_days").text(unpaid_plus);
            }
        });

        $( "#medical_leave_create" ).on( "submit", function( event ) {
            event.preventDefault();
            if(unpaid < 0){
                swal({
                    title: 'Unpaid Leave',
                    text: "This application will proceed as <em>Unpaid Leave</em>",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Proceed'
                }).then((result) => {
                    if (result.value) {
                        medical_submit_now();
                    }
                });
            }else{
                medical_submit_now();
            }
        });

        function medical_submit_now(){
            swal({
            title: 'Saving',
            onOpen: () => {
                swal.showLoading()
                return new Promise(function(resolve, reject) {
                    var form = document.getElementById('medical_leave_create');
                    var formData = new FormData(form);

                    $.ajax({
                        type: "POST",
                        url: "{{ route('leaves.store.medical') }}",
                        data:
                            formData,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            swal.close();
                            $('#medical-leave-form').modal('toggle');
                            swal('Success', 'Please wait for approval', 'success');
                            $('#leave_table').DataTable().ajax.reload();
                        },
                        error: function(data) {
                            errormsg = '';
                            if(data.status == 422 ){
                                $.each(data.responseJSON.errors, function (key, value) {
                                    errormsg = errormsg + value[0] + "<br />";
                                });
                            }else{
                                errormsg = "Server error. Please try again";
                            }
                            swal.showValidationMessage(errormsg);
                            swal.hideLoading();
                        },
                    });
                });
            }})
        }

    </script>
@endsection
