@extends('layouts.default.master')
@section('content')


<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Admin Organisation Dashboard</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{ url('/organisations') }}">Dashboard</a></li>
                    <li class="active">Organization Dashboard</li>
                </ol>
            </div>
        </div>

        <!--company -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title text-uppercase">Companies
                    <a href="#add-company" data-toggle="modal" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Add Company</a></h3>
                    <div class="table-responsive">
                        <table id="org_table" class="">
                            <thead>
                                <tr>
                                    <th style="width: 5%">#</th>
                                    <th>Company</th>
                                    <th>Company Reg. No.</th>
                                    <th style="width: 20%">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!--departments -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title text-uppercase">Departments
                    <a href="#add-department" data-toggle="modal" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Add Department</a></h3>
                    <div class="table-responsive">
                        <table id="dept_table" class="table">
                            <thead>
                                <tr>
                                    <th style="width: 5%">#</th>
                                    <th>Department</th>
                                    <th style="width: 20%">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!--designation -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title text-uppercase">Designations
                        <a href="#add-designation" data-toggle="modal" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Add Designation</a></h3>
                    <div class="table-responsive">
                        <table id="designation_table" class="table">
                            <thead>
                                <tr>
                                    <th style="width: 5%">#</th>
                                    <th>Designation</th>
                                    <th>Department</th>
                                    <th style="width: 20%">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!--hierarchy -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box"><h3 class="box-title text-uppercase">Hierarchy
                <a href="#add-hierarchy" data-toggle="modal" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Add Hierarchy</a></h3>
                <div class="table-responsive">
                    <table id="hierarchy_table" class="table">
                        <thead>
                            <tr>
                                <th style="width: 5%">#</th>
                                <th>Hierarchy</th>
                                <th>Rank</th>
                                <th style="width: 20%">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                </div>
            </div>
        </div>

    </div>

<!--modal add company-->
     @include('admins.organisations.show')
    @include('admins.organisations.create')
    @include('admins.organisations.edit')

<!--modal dept-->
    @include('admins.organisations.departments.create')
    @include('admins.organisations.departments.show')
    @include('admins.organisations.departments.edit')

<!--modal add designation-->
    @include('admins.organisations.designations.create')
    @include('admins.organisations.designations.edit')

<!--modal add hierarchy-->
    @include('admins.organisations.hierarchies.create')
    @include('admins.organisations.hierarchies.edit')

</div>
<!-- /.container-fluid -->
    </div>


</div>
    <!-- /.container-fluid -->
</div>


@endsection

@section('js')
@parent

<script>
    $(function () {
        $('#org_table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/organisations/list',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false},
                {data: 'company_name'},
                {data: 'reg_num'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });

        $('#dept_table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/departments/list',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false},
                {data: 'department_name'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });

        $('#designation_table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/designations/list',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false},
                {data: 'designation_name'},
                {data: 'department', name: 'department.department_name'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });

        $('#hierarchy_table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/hierarchies/list',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false},
                {data: 'hierarchy_name'},
                {data: 'hierarchy_ranking'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });
    });

    $(document).off('click', '.open-modal').on("click", ".open-modal", function () {

        var data = $(this).data('modal_data');
        if($(this).data('logo') ){
            var logo = $(this).data('logo');
        }

        //company
        $('#modal_organisations_show').on('shown.bs.modal', function(e){
            $("#company_image").attr("src", logo);
            $(".modal-body #company_name").text(data.company_name);
            $(".modal-body #reg_num").text(data.reg_num);
            $(".modal-body #address1").text(data.address1);
            $(".modal-body #address2").text(data.address2);
            $(".modal-body #city").text(data.city);
            $(".modal-body #state").text(data.state);
            $(".modal-body #country").text(data.country);
            $(".modal-body #number").text(data.number);
            $(".modal-body #fax").text(data.fax);
            $(".modal-body #organisation_id").text(data.id);
            $(this).off('shown.bs.modal');
        });

        $('#modal_organisations_show').on('hide.bs.modal', function(e){
            $("#company_image").attr("src", "{{ asset('images/Image.png') }}");
            $(this).off('hide.bs.modal');
        });

        $('#modal_organisations_edit').on('shown.bs.modal', function(e){
            $("#company_image_edit").attr("src", logo);
            $(".modal-body #company_name").val(data.company_name);
            $(".modal-body #reg_num").val(data.reg_num);
            $(".modal-body #address1").val(data.address1);
            $(".modal-body #address2").val(data.address2);
            $(".modal-body #city").val(data.city);
            $(".modal-body #state").val(data.state);
            $(".modal-body #country").val(data.country);
            $(".modal-body #number").val(data.number);
            $(".modal-body #fax").val(data.fax);
            $(".modal-body #organisation_id").val(data.id);
            $(this).off('shown.bs.modal');
        });

        $('#modal_organisations_edit').on('hide.bs.modal', function(e){
            $(this).off('hide.bs.modal');
        });

        //department
        $('#modal_departments_show').on('shown.bs.modal', function(e){
            $(".modal-body #modal_department_name").text(data.department_name);
            $(this).off('shown.bs.modal');
        });

        $('#modal_departments_show').on('hide.bs.modal', function(e){
            $(".modal-body #modal_department_name").text('');
            $(this).off('hide.bs.modal');
        });

        $('#modal_departments_edit').on('shown.bs.modal', function(e){
            $(".modal-body #department_name_edit").val(data.department_name);
            $(".modal-body #department_id").val(data.id);
            $(this).off('shown.bs.modal');
        });

        $('#modal_departments_edit').on('hide.bs.modal', function(e){
            $(this).find('#department_edit')[0].reset();
            $(this).off('hide.bs.modal');
        });

        //designation
        $('#modal_designations_show').on('shown.bs.modal', function(e){
            $(this).off('shown.bs.modal');
        });

        $('#modal_designations_show').on('hide.bs.modal', function(e){
            $(this).off('hide.bs.modal');
        });

        $('#modal_designations_edit').on('shown.bs.modal', function(e){
            $("#modal_designations_edit .modal-body #designation_name").val(data.designation_name);
            $("#modal_designations_edit .modal-body #department_id").val(data.department_id).change();
            $("#modal_designations_edit .modal-body #designation_id").val(data.id);
            $(this).off('shown.bs.modal');
        });

        $('#modal_designations_edit').on('hide.bs.modal', function(e){
            $(this).find('#designation_edit')[0].reset();
            $(this).off('hide.bs.modal');
        });

        //hierarchy
        $('#modal_hierarchies_show').on('shown.bs.modal', function(e){
            $(this).off('shown.bs.modal');
        });

        $('#modal_hierarchies_show').on('hide.bs.modal', function(e){
            $(this).off('hide.bs.modal');
        });

        $('#modal_hierarchies_edit').on('shown.bs.modal', function(e){
            $("#modal_hierarchies_edit .modal-body #hierarchy_name").val(data.hierarchy_name);
            $("#modal_hierarchies_edit .modal-body #hierarchy_ranking").val(data.hierarchy_ranking).change();
            $("#modal_hierarchies_edit .modal-body #hierarchy_id").val(data.id);
            $(this).off('shown.bs.modal');
        });

        $('#modal_hierarchies_edit').on('hide.bs.modal', function(e){
            $(this).find('#hierarchy_edit')[0].reset();
            $(this).off('hide.bs.modal');
        });
    });
</script>

@endsection
