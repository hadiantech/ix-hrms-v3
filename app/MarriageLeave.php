<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;


class MarriageLeave extends Model
{
    use Notifiable;
    public $table = "leaves";

    protected $fillable = ['emp_id','ltype_id','leave_from','leave_to','leave_totalday','leave_reason'];
    protected $dates = ['date'];
    //
    public function leavetype() {

        return $this->belongsTo('App\LeaveType', 'ltype_id');
    }

    public function employee() {

        return $this->belongsTo('App\Employee', 'emp_id');
    }
}
