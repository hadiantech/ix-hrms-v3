<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assign extends Model
{
    public $table = "asset_assigns";

    protected $fillable = ['employee_id','started_at','ended_at','assign_status','assign_brand','assign_type'];

    public function employee(){
        return $this->belongsTo(Employee::class);
    }

    public function type(){
        return $this->belongsTo(AssetType::class);
    }

    public function asset()
    {
        return $this->belongsTo(Asset::class);
    }

    public function scopeType($query, $type){
        return $query->where('employee_id', $type);
    }
}
