<div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3 class="box-title">Asset Allocation</h3>
            <br />
            <br />
            <div class="table-responsive m-b-30">
                <table id="assign_table" class="table">
                    <thead>
                        <tr style="border-bottom:3px solid #eee">
                            <th>#</th>
                            <th>Asset Name</th>
                            <th>Asset Type</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>



    @section('js')
    @parent
        <script>
                $(function () {
                $('#assign_table').DataTable({
                    serverSide: true,
                    processing: true,
                    ajax: '{{ route("employees.assets.index", $employee->id) }}',
                    columns: [
                        {data: 'DT_Row_Index', orderable: false, searchable: false},
                       {data: 'assign', name:'assign.assign_type'},
                      // {data: 'assign_type'},
                     //  {data: 'type', name: 'type.type_name'},
                      {data: 'employee_id'},
                        {data: 'action', orderable: false, searchable: false}
                    ]
                });
            });
        </script>
     @endsection
