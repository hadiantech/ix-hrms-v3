<?php

namespace App\Http\Controllers;

use App\EmployeeLieu;
use App\Approval;
use App\Employee;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DataTables;
use Auth;

use Input;

use Carbon\Carbon;

class AdminLieuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function list($type)
    {
        $emp_ids = Approval::where('approver_id', Auth::id())->pluck('employee_id');

        if($type == 'unproc'){
            $query = EmployeeLieu::with(['employee'])->where(['lieu_proc_status' => 'unproc'])->whereIn('employee_id',$emp_ids);
        }
        else if($type == 'proc'){
            $query = EmployeeLieu::with(['employee'])->where(['lieu_proc_status' => 'proc'])->whereIn('employee_id',$emp_ids);
        }
        else if($type == 'approve'){
            $query = EmployeeLieu::with(['employee'])->where(['lieu_proc_status' => 'approve'])->whereIn('employee_id',$emp_ids);
        }
        else{
            return 'No lieu applied';
        }

        return Datatables::eloquent($query)
        ->addColumn('employee', function($model){
            return $model->employee->fname." ".$model->employee->lname;
        })
        ->addColumn('status', function($model){
            $status = $model->lieu_proc_status;

            if($status == 'unproc'){
                $status = 'Pending';
                $class = 'default';
            }
            elseif($status == 'proc'){
                $status = 'Reviewed';
                $class = 'warning';
            }
            elseif($status == 'approve'){
                $status = 'Approved';
                $class = 'success';
            }
            elseif($status == 'returned'){
                $status = 'Returned';
                $class = 'danger';
            }
            else{
                $class = 'primary';
            }
            return view('shared._button_color', compact('status', 'class'))->render();
        })
        ->addColumn('action', function($model){
            $url = 'adminlieus';
            // $show_hide = true;
            $edit_hide = true;
            $delete_hide = true;

            return view('shared._table_action', compact('model', 'url', 'edit_hide', 'delete_hide', 'custom_btn'))->render();
        })
        ->escapeColumns([])
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeLieu  $employeeLieu
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lieus = EmployeeLieu::find($id);
        $employees = Employee::find($id);

        $approver_level = Approval::where('employee_id',$lieus->employee_id)->where('approver_id',Auth::id())->first();

        return view('admins.leaves.lieus.show',compact('lieus','employees','approver_level'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeLieu  $employeeLieu
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeLieu $employeeLieu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeLieu  $employeeLieu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
        $lieu = EmployeeLieu::find($id);
        $lieu->lieu_proc_status = $request->lieu_proc_status;
        $lieu->status = $request->status;

        if($request->submit_val == 'reject'){
            $lieu->lieu_proc_status = 'reject';
        }
        else if($request->submit_val == 'approved'){
            $lieu->lieu_proc_status = 'approved';
            $lieu->as_lieu = $request->total_days;
            // return $request->submit_val;
        }
        else if($request->submit_val == 'submit'){
            $lieu->lieu_proc_status = $request->lieu_proc_status;
        }
        $lieu->save();
        return response()->json($lieu, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeLieu  $employeeLieu
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeLieu $employeeLieu)
    {
        //
    }
}
