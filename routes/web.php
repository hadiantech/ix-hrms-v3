<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@welcome');

Auth::routes();
Route::get('employees/{id}/accept','EmployeeController@activate_user')->name('employees.activate_user');
Route::get('employees/resetpass/{id}','EmployeeController@activate_password')->name('employees.activate_password');
Route::put('employees/{id}/passwords','EmployeeController@passwords')->name('passwords');
Route::put('employees/resetpass/{id}/reset','EmployeeController@resetpasswords')->name('resetpassword');
Route::post('employees/resets','EmployeeController@resets')->name('resets');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('adminleaves/{id}/accept','AdminLeaveController@update_email')->name('adminleaves.update_email');
Route::get('adminleaves/{id}/accept2','AdminLeaveController@approve_email')->name('adminleaves.approve_email');
Route::get('adminleaves/{id}/reject','AdminLeaveController@reject_email')->name('adminleaves.reject_email');
// Route::get('/admin', 'AdminController@index');
// Route::get('/employee', 'EmployeeController@index');

Route::group( ['middleware' => ['auth']], function() {

    Route::get('/dashboard', 'DashboardController@view')->name('dashboard');
    Route::get('/dashboard/adminleaves/list/{type}','DashboardController@leavelist');
    Route::get('/dashboard/adminclaims/list/{type}','DashboardController@claimlist');
    Route::get('/dashboard/employees/list','DashboardController@probationlist');
    Route::get('/dashboard/notices/list','DashboardController@noticelist');

    Route::get('adminlieus/list','AdminLieuController@list');
    Route::get('adminlieus/list/{type}','AdminLieuController@list');
    Route::resource('adminlieus','AdminLieuController');

    Route::get('lieus/list','LieuController@list');
    Route::resource('lieus','LieuController');

    Route::get('adminleaves/list','AdminLeaveController@list');
    Route::get('adminleaves/list/{type}','AdminLeaveController@list');

    Route::resource('adminleaves','AdminLeaveController');
    //calendar
    Route::get('events', 'EventController@index')->name('events.index');
    Route::get('events/list', 'EventController@eventlist');
    Route::delete('events/list/{id}', 'EventController@delete')->name('events.destroy');
    Route::post('events', 'EventController@addEvent')->name('events.add');
    // event for employee
    Route::get('empevents', 'EventController@employeeindex');


    Route::get('profile', 'ProfileController@my')->name('profile.my');
    Route::get('profile/documents', 'ProfileController@documents')->name('profile.documents');
    Route::post('profile/image', 'ProfileController@upload_image')->name('profile.image.upload');

    Route::get('employees/list', 'EmployeeController@list');
    Route::post('employees/image/{employee}', 'EmployeeController@upload_image')->name('employees.image.upload');
    Route::put('employees/account/update/{employee}', 'EmployeeController@account_update')->name('employees.account.update');
    Route::put('employees/info/update/{employee}', 'EmployeeController@employee_update')->name('employees.info.update');
    Route::put('employees/leave/update/{employee}', 'EmployeeController@leave_update')->name('employees.leave.update');
    Route::resource('employees', 'EmployeeController');

    Route::resource('employees.contacts', 'EmployeeContactController');
    Route::resource('employees.histories', 'EmployeeHistoryController');
    Route::resource('employees.educations', 'EmployeeEducationController');
    Route::resource('employees.certificates', 'EmployeeCertificateController');
    Route::resource('employees.medicals', 'EmployeeMedicalController');
    // Route::get('employee/assets/list', 'EmployeeAssetController@list');
    Route::resource('employees.assets', 'EmployeeAssetController');
    Route::resource('employees.documents', 'EmployeeDocumentController');

    Route::post('employees/save/approval/{employee}', 'ApprovalController@approve_update')->name('approve.save');

    Route::get('organisations/list', 'OrganisationController@list');
    Route::resource('organisations','OrganisationController');

    Route::get('departments/list', 'DepartmentController@list');
    Route::resource('departments','DepartmentController');

    Route::get('designations/list', 'DesignationController@list');
    Route::get('designations/department/{id}', 'DesignationController@list_based_department');
    Route::resource('designations','DesignationController');

    Route::get('hierarchies/list', 'HierarchyController@list');
    Route::resource('hierarchies','HierarchyController');

    // Route::resource('leaves','LeaveController');
    Route::resource('adminroles','AdminRoleController');

    Route::get('roles/list', 'RoleController@list');
    Route::resource('roles','RoleController');

    Route::resource('holiday','AdminHolidayController');

    // Assets
    Route::get('assets/list','AssetController@list');
    Route::get('assets/brand/{id}', 'AssetController@list_based_asset');
    Route::resource('assets','AssetController');

    // Asset Type
    Route::get('types/list','AssetTypeController@list');
    Route::resource('types','AssetTypeController');

    // Asset Brand
    Route::get('brands/list','AssetBrandController@list');
    Route::get('brands/type/{id}', 'AssetBrandController@list_based_type');
    Route::resource('brands','AssetBrandController');

    // Assign Asset
    Route::get('assigns/list','AssignController@list');
    Route::get('assigns/employee/{id}', 'AssignController@list_based_employee');
    // Route::get('assigns/type/{id}', 'AssignController@list_based_type');
    // Route::get('assigns/brand/{id}', 'AssignController@list_based_brand');
    Route::resource('assigns','AssignController');

    // Notices

    Route::get('notices/list','NoticeController@list');
    Route::get('notices/employee/{id}', 'NoticeController@list_based_employee');
    Route::resource('notices','NoticeController');

    //web setting
    Route::get('documents/list','AdminSettingsController@list_document')->name('settings.document.list');
    Route::post('settings/store_leave','AdminSettingsController@store_leave')->name('settings.store_leave');
    Route::post('settings/store_system','AdminSettingsController@store_system')->name('settings.store_system');
    Route::post('settings/store_document','AdminSettingsController@store_document')->name('settings.store_document');
    Route::delete('settings/delete_document/{id}', 'AdminSettingsController@delete_document')->name('default_documents.destroy');
    Route::resource('settings','AdminSettingsController');

    // Payslip
    Route::get('payslips/list','PayrollCategoryController@list');
    Route::resource('payslips','PayrollCategoryController');

    // Salaries
    Route::get('salaries/list','SalaryController@list');
    Route::get('salaries/employee/{id}', 'SalaryController@list_based_employee');
    Route::get('salaries/salary/{id}','SalaryController@salary')->name('salaries.salary');
    Route::post('salaries/set','SalaryController@set')->name('salaries.set');
    Route::get('salaries/increment/{id}','SalaryController@increment')->name('salaries.increment');
    Route::resource('salaries','SalaryController');
    // employee payroll
    Route::get('empgenerates','EmployeePayrollController@index');
    Route::get('empgenerates/{years}/{month}','EmployeePayrollController@index2');
    Route::get('empgenerates/{years}/{month}/list','EmployeePayrollController@list');
    Route::get('empgenerates/{years}/{month}/{id}/print','EmployeePayrollController@print')->name('empgenerates.print');

    // Generate
    Route::get('generates','PayrollController@index');
    Route::get('generates/{years}/{month}','PayrollController@index2');
    Route::get('generates/{years}/{month}/list','PayrollController@list');
    Route::get('generates/{years}/{month}/genlist','PayrollController@genlist');

    Route::get('generates/{years}/{month}/{id}','PayrollController@generate')->name('generates.generate');

    // Route::post('generates/{years}/{month}/store')->name('generates.store');
    Route::get('generates/{years}/{month}/addition','PayrollController@addition')->name('generates.additem');
    Route::post('generates/{years}/{month}/store_add','PayrollController@store_add')->name('generates.store_add');
    Route::get('generates/{years}/{month}/deduction','PayrollController@deduction');
    Route::post('generates/{years}/{month}/store_deduct','PayrollController@store_deduct')->name('generates.store_deduct');
    Route::post('generates/{years}/{month}/store_gen','PayrollController@store_gen')->name('generates.store_gen');
    Route::get('generates/{years}/{month}/{id}/print','PayrollController@print')->name('generates.print');
    //Route::resource('generates','PayrollController');

    //
   // Route::get('generates/addition','PayrollController@addition');
   // Route::post('generates/addition/store_add','PayrollController@store_add')->name('generates.store_add');
  //  Route::get('generates/deduction','PayrollController@deduction');
   // Route::post('generates/deduction/store_deduct','PayrollController@store_deduct')->name('generates.store_deduct');
  //  Route::get('generates/{years}/{month}/list','PayrollController@list');
    // Route::get('generates/{years}/{month}/genlist','PayrollController@genlist');
    // Route::get('generates/generate/{years}/{month}/{id}/print','PayrollController@print')->name('generates.print');
   // Route::get('generates/{years}/{month}/{id}','PayrollController@generate')->name('generates.generate');
    // Route::get('generates','PayrollController@payslip');
   // Route::delete('generates/generate/{id}/{category_id}/delete','PayrollController@destroy')->name('generates.destroy');


    // Employee Resource
    // cuti punya resorce
    Route::post('employees/leaves/emergencies', 'EmergencyController@store')->name('leaves.store.emergency');
    Route::resource('emergencies','EmergencyController');
    Route::post('employees/leaves/annuals','EmployeeLeaveController@store_annual')->name('leaves.store.annuals');
    Route::resource('annuals','EmployeeLeaveController');
    Route::post('employees/leaves/compassionates','CompassionateController@store')->name('leaves.store.compassionates');
    Route::resource('compassionates','CompassionateController');
    Route::post('employees/leaves/medics','MedicalController@store')->name('leaves.store.medical');
    Route::resource('medics','MedicalController');
    Route::post('employees/leaves/marriages','MarriageController@store')->name('leaves.store.marriage');
    Route::resource('marriages','MarriageController');
    Route::post('employees/leaves/maternity','MaternityController@store')->name('leaves.store.maternity');
    Route::resource('maternity','MaternityController');
    Route::post('employees/leaves/paternity','PaternityController@store')->name('leaves.store.paternity');
    Route::resource('paternity','PaternityController');

    Route::get('leaves/list','EmployeeLeaveController@list');
    Route::get('leaves/{id}','EmployeeLeaveController@list');
    // Route::put('leaves/{id}/update','EmployeeLeaveController@update');
    Route::resource('leaves','EmployeeLeaveController');



    // Route::name('admin.')->prefix('admin')->group(function () {
    //     Route::get('lieus/{id}/status/{status}', 'Admin\LieuController@liew_update_status');
    //     Route::resource('lieus','Admin\LieuController');
    // });

    // Claims
    Route::get('claims/create/draft', 'ClaimController@create_draft')->name('claims.monthly.create');
    Route::get('claims/list', 'ClaimController@list');
    Route::get('claims/country_list', 'ClaimController@country_list');
    Route::get('claims/list_request', 'ClaimController@list_request');
    Route::get('claims/month/{id}','ClaimController@show')->name('claims.monthly.show');
    Route::put('claims/submit/{id}', 'ClaimController@submit_advance')->name('claims.submit.advance');
    Route::resource('claims','ClaimController');

    // Claim Advances
    Route::get('claimadvances/create/draft', 'ClaimController@create_advance_draft')->name('claims.advance.create');
    Route::get('claimadvances/{id}','ClaimController@show_advance')->name('claimadvances.show');
    Route::get('claimadvances/detail/{id}','ClaimController@show_advance_detail')->name('claimadvances_detail.show');
    Route::put('claimadvances/update/{id}','ClaimController@update_advance')->name('claimadvances.update');
    Route::delete('claimadvances/delete/{id}','ClaimController@destroy_advance')->name('claimadvances.destroy');
    // Route::resource('claimadvances','AdvancesClaimController');

    // Admin Claims
    Route::get('adminclaims/list', 'AdminClaimController@list');
    Route::get('adminclaims/list/{type}', 'AdminClaimController@list');
    Route::get('adminclaims/list_request/{type}', 'AdminClaimController@list_request');
    Route::get('adminclaims/list_advance/{type}', 'AdminClaimController@list_advance');
    Route::get('adminclaims_request/{id}', 'AdminClaimController@show_request')->name('adminclaims_request.show');
    Route::put('adminclaims/update_admin/{id}', 'AdminClaimController@update_admin')->name('claim.update.admin');
    Route::put('adminclaims/update_request/{id}', 'AdminClaimController@update_request')->name('claim.update.request');
    Route::resource('adminclaims','AdminClaimController');

    // Expenses
    Route::get('expenses/list/{id}','ExpenseController@list_expense_month')->name('expense.list');
    Route::get('expenses/list_admin/{id}','ExpenseController@list_expense_month_admin');
    Route::post('expenses/update_remark/{id}','ExpenseController@update_remark');
    Route::resource('expenses','ExpenseController');

    // Travellers, Visas, Flights, Hotels
    Route::get('travellers/list/{id}','TravellerController@list')->name('traveller.list');
    Route::resource('travellers','TravellerController');
    Route::get('visas/list/{id}','VisaController@list')->name('visa.list');
    Route::resource('visas','VisaController');
    Route::get('flights/list/{id}','FlightController@list')->name('flights.list');
    Route::resource('flights','FlightController');
    Route::get('hotels/list/{id}','HotelController@list')->name('hotels.list');
    Route::resource('hotels','HotelController');

    // Route::resource('notices','NoticeController');

    // Employee Payslips
    // Route::get('payslips/print/{id}','PayrollController@print');
    // Route::resource('payslips','PayrollController');

    // Employee Profiles
    Route::resource('profiles','EmployeeProfileController');

    Route::resource('leavetype','EmployeeLeaveController');

   // Route::match(['get', 'post'], 'laravel-send-custom-email', 'EmailController@customEmail');

    Route::post('mails/send','EmailController@customEmail');

//     Route::get('sendbasicemail','MailController@basic_email');
// Route::get('sendhtmlemail','MailController@html_email');
// Route::get('sendattachmentemail','MailController@attachment_email');

});
