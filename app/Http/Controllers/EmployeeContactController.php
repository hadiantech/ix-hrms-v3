<?php

namespace App\Http\Controllers;

use App\EmployeeContact;
use App\Employee;
use Illuminate\Http\Request;
use DataTables;

class EmployeeContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Employee $employee)
    {
        return Datatables::eloquent(EmployeeContact::where('employee_id', $employee->id))
        ->addColumn('action', function($model) use ($employee){
            $model_2 = $employee;
            $modal_view_id = 'modal_contacts_show';
            $modal_edit_id = 'modal_contacts_edit';
            $url = 'employees.contacts';
            $show_hide = true;
            return view('shared._table_action', compact('model', 'url', 'modal_view_id', 'modal_edit_id', 'model_2', 'show_hide'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $contact = EmployeeContact::create($request->all());
        $employee->contacts()->save($contact);
        return response()->json($contact, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeContact  $employeeContact
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeContact $employeeContact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeContact  $employeeContact
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee, EmployeeContact $contact)
    {
        return $contact;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeContact  $employeeContact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee, EmployeeContact $contact)
    {
        $contact->update($request->all());
        return response()->json($contact, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeContact  $employeeContact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee, EmployeeContact $contact)
    {
        $contact->delete();
        return redirect()->back();
    }
}
