{{-- Modal --}}
<div class="modal fade" id="modal_hotel_create">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h1>Add Visa</h1>

                {!! Form::open(['url' => '', 'id' => 'hotel_create']) !!}

                <input name="advance_claims_id" type="hidden" value="{{ $claim->id }}" />

                <div class="form-group row">
                    <div class="col-md-12">
                        {{ Form::text('name',null,['class' => 'form-control', 'placeholder' => 'Hotel Name'])}}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        {{ Form::text('location',null,['class' => 'form-control', 'placeholder' => 'Location'])}}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        {{ Form::number('room',null,['class' => 'form-control', 'placeholder' => 'No. Of Room'])}}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        {{ Form::label('', 'Check In') }}
                        {{ Form::date('check_in',null,['class' => 'form-control', 'placeholder' => 'Check In'])}}
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        {{ Form::label('', 'Check Out') }}
                        {{ Form::date('check_out',null,['class' => 'form-control', 'placeholder' => 'Check Out'])}}
                    </div>
                </div>
                <button type="submit" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">
                    Add Hotel
                </button>
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">
                    Close
                </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('js')
@parent
<script>
    $("#hotel_create").on("submit", function (event) {
        console.log('mew');
        event.preventDefault();
        swal({
            title: "Create New Hotel",
            text: "Are you sure you want to create?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function(resolve, reject){
                    var form = document.getElementById('hotel_create');
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url:"{{ route('hotels.store') }}",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            resolve(data);
                        },
                        error: function(){
                            swal.showValidationMessage('server error');
                            swal.hideLoading();
                        }
                    });
                });
            }
        }).then((result) => {
            if (result.value) {
                $('#modal_hotel_create').modal('toggle');
                $('#hotel_create').trigger("reset");
                $('#hotel_table').DataTable().ajax.reload();
                swal('New Hotel Added!', '--', 'success');

            }

        });
    });
</script>


@endsection


