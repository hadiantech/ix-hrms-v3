<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    protected $table = 'hotel_informations';
    protected $guarded =  [];

    public function claim(){
        return $this->belongsTo(Claim::class,'advance_claims_id');
    }
}
