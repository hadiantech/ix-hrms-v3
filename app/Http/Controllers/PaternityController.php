<?php

namespace App\Http\Controllers;

use App\Paternity;
use Illuminate\Http\Request;
use App\Employee;
use App\LeaveType;
use App\Annual;
use App\EmployeeLeave;
use Auth;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Postmark\PostmarkClient;

class PaternityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employee = Employee::lists(['id']);
        $leavetype = LeaveType::lists(['id']);
        return view('employees.leaves.paternity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = new PostmarkClient("73a69c7d-8c56-4f59-877c-74d05cd5e247");


        $leaves = EmployeeLeave::where('employee_id', Auth::user()->id)->get();
        $approval = Approval::where('employee_id', Auth::user()->id)->first();
        //return $approval->approvedBy;

        $aid = Auth::user()->id;
        $employees = Employee::find($aid);

        $paternity_leave_quota = Auth::user()->leave_allocs()->type('Paternity Leave')->first()->quota;
        $paternity_leave_taken = Auth::user()->leaves()->type('Paternity Leave')->sum('total_days');
        $balance = $paternity_leave_quota - $paternity_leave_taken;


        $a = Carbon::parse($request->started_at);
        $b = Carbon::parse($request->ended_at);
        $days = $b->diffInDays($a);
        $days = $days + 1;

        $unpaid = $balance - $days;

        $pat = new EmployeeLeave;
        $pat->employee_id = Auth::user()->id;
        $pat->leave_type = 'Paternity Leave';
        $pat->started_at = $request->started_at;
        $pat->ended_at = $request->ended_at;
        $pat->reason = $request->reason;
        $pat->total_days = $days;
        $pat->as_unpaid = $unpaid;
        // $emergency->as_lieu = $request->as_lieu;
        // $emergency->remark = $request->remark;
        $pat->status = 'Pending';
        $pat->leave_proc_status = 'unproc';
        $pat->save();
        if ($request->hasFile('file')) {
            $pat->addMediaFromRequest('file')->toMediaCollection('leaves');
        }
        $leaves = $pat->id;
        $sendResult = $client->sendEmailWithTemplate(

            "hassanul@boneybone.com",
            $approval_lv1->approvedBy->email,
            10030123,
                [
                    "system_name" => "[IX HRMS] Leave Application Approval",
                    "fname" => $employees->fname,
                    "lname" => $employees->lname,
                    "uid" => $employees->employee_uid,
                    "approve" =>  $approval_lv1->approvedBy->fname,
                    "approver" =>  $approval_lv1->approvedBy->lname,
                    "leave_type" => 'Paternity Leave',
                    "started_at" => $request->started_at,
                    "ended_at" => $request->ended_at,
                    "reason" => $request->reason,
                    "accept" => route('adminleaves.update_email',$leaves),
                    "reject" => route('adminleaves.reject_email',$leaves),
                    "link" => url('http://hrms.ixtelecom.net')
                ]);

            $sendResult = $client->sendEmailWithTemplate(

                    "hassanul@boneybone.com",
                    $approval_lv2->approvedBy->email,
                    10030123,
                        [
                            "system_name" => "[IX HRMS] Leave Application Approval",
                            "fname" => $employees->fname,
                            "lname" => $employees->lname,
                            "uid" => $employees->employee_uid,
                            "approve" =>  $approval_lv2->approvedBy->fname,
                            "approver" =>  $approval_lv2->approvedBy->lname,
                            "leave_type" => 'Paternity Leave',
                            "started_at" => $request->started_at,
                            "ended_at" => $request->ended_at,
                            "reason" => $request->reason,
                            "accept" => route('adminleaves.approve_email',$leaves),
                            "reject" => route('adminleaves.reject_email',$leaves),
                            "link" => url('http://hrms.ixtelecom.net')
                        ]);
        return response()->json($pat, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
