<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpenseTitle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id')->nullable();
            $table->integer('claim_id')->nullable();
            $table->integer('project_id')->nullable();
            $table->longtext('item_type')->nullable();
            $table->longtext('item_name')->nullable();
            $table->text('description')->nullable();
            $table->text('purchase_from')->nullable();
            $table->date('purchase_date')->nullable();
            $table->integer('purchase_by')->nullable();
            $table->decimal('amount',10,2)->nullable();
            $table->text('status')->nullable();
            $table->text('receipt_no')->nullable();
            $table->text('reference_no')->nullable();
            $table->text('bill_copy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense');
    }
}
