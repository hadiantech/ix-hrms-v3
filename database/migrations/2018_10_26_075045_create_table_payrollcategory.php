<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePayrollcategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();;
            $table->string('code')->nullable();;
            $table->string('operand')->nullable();;
            $table->string('amount')->nullable();;
            $table->string('defaukt')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_categories');
    }
}
