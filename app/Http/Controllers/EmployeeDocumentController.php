<?php

namespace App\Http\Controllers;

use App\EmployeeDocument;
use App\Employee;
use Illuminate\Http\Request;
use DataTables;

class EmployeeDocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Employee $employee)
    {
        return Datatables::eloquent(EmployeeDocument::whereNull('employee_id')->orWhere('employee_id', $employee->id)->orderBy('shared', 'desc'))
        ->addColumn('url_view', function($model){
            return view('admins.settings.pages.btn_url_view', compact('model'))->render();
        })
        ->addColumn('action', function($model) use ($employee){
            $model_2 = $employee;
            $url = 'employees.documents';
            $show_hide = true;
            $edit_hide = true;
            if($model->shared != 1){
                return view('shared._table_action', compact('model', 'url', 'model_2', 'show_hide', 'edit_hide'))->render();
            }
            return 'Shared Document';
        })
        ->escapeColumns([])
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $request->validate([
            'name' => 'required',
            'url' => 'nullable|url',
            'document' => 'max:2048',
        ]);

        if(!$request->url  && !$request->hasFile('document') ){
            $returnData = array(
                'document' => array('Please upload document or insert url link')
            );
            return response()->json(['errors' => $returnData], 422);
        }

        $document = EmployeeDocument::create($request->except('document'));
        $employee->documents()->save($document);

        if ($request->hasFile('document')) {
            $document->addMediaFromRequest('document')->toMediaCollection('document');
        }

        return response()->json('success', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeDocument  $employeeDocument
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeDocument $employeeDocument)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeDocument  $employeeDocument
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeDocument $employeeDocument)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeDocument  $employeeDocument
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeDocument $employeeDocument)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeDocument  $employeeDocument
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee, EmployeeDocument $document)
    {
        $document->delete();
        return redirect()->back();
    }
}
