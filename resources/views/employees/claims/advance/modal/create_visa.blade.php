{{-- Modal --}}
<div class="modal fade" id="modal_visa_create">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h1>Add Visa</h1>

                {!! Form::open(['url' => '', 'id' => 'visa_create']) !!}

                <input name="advance_claims_id" type="hidden" value="{{ $claim->id }}" />

                <div class="form-group row">
                    <div class="col-md-6">
                        {{ Form::text('visa_owner',null,['class' => 'form-control', 'placeholder' => 'Visa Owner'])}}
                    </div>
                    <div class="col-md-6">
                        <select class="selectpicker" data-style="form-control">
                            <option selected disabled>Country</option>
                            @foreach ($country as $ct )
                            <option value="{{ $ct }}">{{ $ct }}</option>
                            @endforeach
                        </select>
                        {{-- {{ Form::text('country',null,['class' => 'form-control', 'placeholder' => 'Country Origin'])}} --}}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        {{ Form::text('type',null,['class' => 'form-control', 'placeholder' => 'Visa Type'])}}
                    </div>
                    <div class="col-md-6">
                        {{ Form::date('entry_date',null,['class' => 'form-control', 'placeholder' => 'Entry Date'])}}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        {{ Form::text('nationality',null,['class' => 'form-control', 'placeholder' => 'Nationality'])}}
                    </div>
                </div>
                <button type="submit" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">
                    Add Traveller
                </button>
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close
                </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('js')
@parent
<script>
    $("#visa_create").on("submit", function (event) {
        console.log('mew');
        event.preventDefault();
        swal({
            title: "Create New Visa",
            text: "Are you sure you want to create?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function(resolve, reject){
                    var form = document.getElementById('visa_create');
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url:"{{ route('visas.store') }}",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            resolve(data);
                        },
                        error: function(){
                            swal.showValidationMessage('server error');
                            swal.hideLoading();
                        }
                    });
                });
            }
        }).then((result) => {
            if (result.value) {
                $('#modal_visa_create').modal('toggle');
                $('#visa_create').trigger("reset");
                $('#visa_table').DataTable().ajax.reload();
                swal('New Visa Added!', '--', 'success');

            }

        });
    });
</script>


@endsection


