<?php

namespace App\Http\Controllers;
use App\Assign;
use App\Asset;
use App\AssetType;
use App\AssetBrand;
use DataTables;
use App\Employee;
use Illuminate\Http\Request;

class AssignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function list(){
        return Datatables::eloquent(Assign::with('employee','asset','type'))
        ->addColumn('employee', function($model){
            return $model->employee->username;
        })
        ->addColumn('asset', function($model){
            return $model->name;
        })
        ->addColumn('type', function($model){
            return $model->type_name;
        })
        ->addColumn('action', function($model){
            $url = 'assigns';
            $modal_view_id = 'modal_assign_show';
            $modal_edit_id = 'modal_assign_edit';
            $modal_data_customs = array('type' => $model->type, 'employee' => $model->employee, 'asset' => $model->asset);
            // $delete_datatable_id_name_refresh = 'assign_table';

            return view('shared._table_action', compact('model', 'url', 'modal_view_id', 'modal_edit_id', 'modal_data_customs'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    public function list_based_employee($id){
        return Assign::where('employee_id', $id)->get();
    }

    public function list_based_type($id){
        return Assign::where('assign_type', $id)->get();
    }

    public function list_based_brand($id){
        return Assign::where('assign_brand', $id)->get();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Employee::all();
        $assets = Asset::all();
        $types = AssetType::all();
        return view('assets.assigns.create', compact('employees','assets','types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $assigns = Assign::create($request->only('employee_id','assign_type','assign_brand','started_at','ended_at','assign_status'));

        $assigns->save();
        return response()->json($assigns, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $assigns = Assign::where('id', $id)->first();
        $types = AssetType::find($id);
        $brands = AssetBrand::find($id);
        $employees = Employee::find($id);

        return view('assets.brands.show',compact('brands','types','assigns','employees'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $assign = Assign::find($id);

        $assign->update($request->all());

        return response()->json($assign, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Assign $assign)
    {
        $assign->delete();
        return redirect()->back();
    }
}
