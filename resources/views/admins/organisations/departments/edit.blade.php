<div id="modal_departments_edit" class="modal fade in" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Edit Department</h1>
                {!! Form::open(['url' => 'departments', 'id' => 'department_edit']) !!}
                    {{ Form::hidden('department_id', null , ['id' => 'department_id']) }}
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group ">
                        {{Form::text('department_name', null , ['id' => 'department_name_edit', 'required','class' => 'form-control','placeholder' => 'Department Name'])}}
                    </div>
                    {{Form::submit('Update Department',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                    <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('js')
    @parent
    <script>

    $( "#department_edit" ).on( "submit", function( event ) {
        event.preventDefault();
        swal({
            title: "Update Department",
            text: "Are you sure you want to update this department?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                var form = document.getElementById('department_edit');
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: '/departments/'+form.department_id.value,
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(msg) {
                        return msg;
                    }
                });
            }
        }).then((result) => {
            $('#modal_departments_edit').modal('toggle');
            if(result.value){
                $('#dept_table').DataTable().ajax.reload();
                swal('Department Updated!', '--', 'success');
            }
        });
    });
    </script>
@endsection
