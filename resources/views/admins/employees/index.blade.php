@extends('layouts.default.master')

@section('content')

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>

<div id="wrapper">
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Admin Employee Dashboard</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard-adminview.html">Dashboard</a></li>
                        <li class="active">Employee Dashboard</li>
                    </ol>
                </div>
            </div>

            <!--employee -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title text-uppercase">Employees
                            <a href="#add-employee" data-toggle="modal" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Add Employee</a></h3>

                        <div class="table-responsive">
                            <table id="employee_table" class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th >Employee</th>
                                        <th class="text-center">MyKad/ <br/>Passport No.</th>
                                        <th class="text-center">Company</th>
                                        <th class="text-center">Department</th>
                                        <th class="text-center">Designation</th>
                                        <th class="text-right">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

          
        </div>
    </div>
</div>

<!-- modal add employee -->
@include('admins.employees.create')



@endsection

@section('js')
@parent

    <script>
    $(function () {
        $('#employee_table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/employees/list',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false},
                {data: 'fname'},
                {data: 'ic'},
                {data: 'organisation', name: 'organisation.company_name', searchable: false},
                {data: 'department', name: 'department_name', searchable: false},
                {data: 'designation', name: 'designation_name', searchable: false},
                {data: 'action', orderable: false, searchable: false}
            ]
        });

    });
    </script>

@endsection
