<section id="leaveallocation">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3 class="box-title">Leave Allocation</h3>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr style="border-bottom:3px solid #eee">
                            <th>Leave Type</th>
                            <th width="20%" class="text-right">Allocation</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Annual Leave</td>
                            <td>
                                <input type="number" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td>Medical Leave</td>
                            <td>
                                <input type="number" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td>Compassionate Leave</td>
                            <td>
                                <input type="number" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td>Maternity Leave</td>
                            <td>
                                <input type="number" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td>Paternity Leave</td>
                            <td>
                                <input type="number" class="form-control">
                            </td>
                        </tr>


                    </tbody>
                </table>
            </div>
            <button style="width:200px" class="m-b-20 btn-lg btn-rounded btn-info text-uppercase">Save</button>
        </div>
    </div>
</section>
