<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Hierarchy;
use App\Role;
use App\EmployeeDocument;
use Auth;
use Validator;
use DataTables;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function my(){
        $employee = Auth::user();
        $top_employees = Employee::get()->pluck('full_name', 'id');
        $hierarchies = Hierarchy::all()->pluck('hierarchy_name', 'id');
        $roles = Role::all()->pluck('name', 'id');
        return view('profiles.index', compact('employee', 'hierarchies', 'roles', 'top_employees'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function upload_image(Request $request){
        $validator = Validator::make($request->all(), [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->passes()) {
            $user = Auth::user();
            $user->clearMediaCollection('profile');
            $user->addMediaFromRequest('image')->toMediaCollection('profile');
            return response()->json(['success'=>'done']);
        }
        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function documents(){
        return Datatables::eloquent(EmployeeDocument::whereNull('employee_id')->orWhere('employee_id', Auth::id())->orderBy('shared', 'desc'))
        ->addColumn('url_view', function($model){
            return view('shared._button_file_doc', compact('model'))->render();
        })
        ->escapeColumns([])
        ->addIndexColumn()
        ->make(true);
    }
}
