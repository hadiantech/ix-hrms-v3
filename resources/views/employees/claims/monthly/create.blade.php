{{-- Modal --}}
<div class="modal fade" id="add-expense">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h1>Add Expense</h1>

                {!! Form::open(['url' => 'expenses', 'id' => 'expenses_create']) !!}

                <input name="claim_id" type="hidden" value="{{ $claim->id }}" />
                <div class="form-group row">
                    <div class="col-md-6">
                        <select name="item_type" class="selectpicker" data-style="form-control">
                            <option selected disabled>Expense Type</option>
                            @foreach ( $expenses_type as $t )
                            <option value="{{ $t->id }}">{{ $t->expense }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <input name="project_code" type="text" class="form-control" placeholder="Project Code">
                    </div>
                </div>
                <div class="form-group">
                    <input name="item_name" type="text" class="form-control" placeholder="Item Name">
                </div>
                <div class="form-group">
                    <textarea name="description" rows="4" class="form-control" placeholder="Insert description here..."></textarea>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        {{ Form::date('purchase_date',null,['class' => 'form-control', 'placeholder' => 'Item Date'])}}
                    </div>
                    <div class="col-md-6">
                        <input name="amount" type="float" class="form-control" placeholder="Amount">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <input name="receipt_no" type="text" class="form-control" placeholder="Receipt No.">
                    </div>
                    <div class="col-md-6">
                        <input name="reference_no" type="number" class="form-control" placeholder="Reference No.">
                    </div>
                </div>
                <button type="submit" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">
                    Add Expense
                </button>
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">
                    Close
                </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
