@extends('layouts.default.master')

@section('content')

<div class="preloader">
   <svg class="circular" viewBox="25 25 50 50">
       <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
   </svg>
</div>

<div id="page-wrapper">
   <div class="container-fluid">
       <div class="row bg-title">
           <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
               <h4 class="page-title">Payslip</h4>
           </div>
           <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

               <ol class="breadcrumb">
                   <li><a href="dashboard-adminview.html">Dashboard</a></li>
                   <li><a href="admin-payslip.html">Payroll</a></li>
                   <li class="active">Employee Salary</li>
               </ol>
           </div>
           <!-- /.col-lg-12 -->
       </div>

    <div class="row">
       <div class="col-sm-3">
            <div class="white-box">


                    <select class= "form-control" name="year" id="year">
                        <option value="">Select Year</option>
                    </select>
                    <br>
                    <select class="form-control" name="month" id="month">
                            <option value="">Select Month</option>
                        </select>
                    <br>
                    {{Form::submit('Check',['class' =>'btn btn-rounded btn-info text-uppercase', 'id'=>'buttonyear', 'name'=>'buttonyear', 'value'=>'buttonyear', 'onClick'=>'click(this);'])}}

            </div>
        </div>
    </div>

   </div>
</div>
    @endsection

    @section('js')
    @parent

    <script type="text/javascript">
            var start = 2019;
            var end = new Date().getFullYear();
            var options = "";
            for(var year = start ; year <=end; year++){
              options += "<option>"+ year +"</option>";
            }
            document.getElementById("year").innerHTML = options;
    </script>

    <script type="text/javascript">
        var d = new Date();
        var monthArray = new Array();
        monthArray[0] = "January";
        monthArray[1] = "February";
        monthArray[2] = "March";
        monthArray[3] = "April";
        monthArray[4] = "May";
        monthArray[5] = "June";
        monthArray[6] = "July";
        monthArray[7] = "August";
        monthArray[8] = "September";
        monthArray[9] = "October";
        monthArray[10] = "November";
        monthArray[11] = "December";
        for(m = 0; m <= 11; m++) {
            var optn = document.createElement("OPTION");
            optn.text = monthArray[m];
            // server side month start from one
            optn.value = (m+1);
            document.getElementById('month').options.add(optn);
        }
        </script>
        <script language="JavaScript">
                $('#buttonyear').on('click', function() {
                    var e = $('#year').val();
                    var ye = $('#month').val();
                  //  alert ($('#year').val());
                  //  alert ($('#month').val());
                    window.location.href = "empgenerates/"+ e +"/"+ ye;
                  })
                </script>

    @endsection
