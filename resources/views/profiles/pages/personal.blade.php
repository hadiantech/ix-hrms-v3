<div class="row">

    <div class="col-md-4 text-center m-b-40">
        @if($employee->getMedia('profile')->first())
        <img src="{{ $employee->getMedia('profile')->first()->getUrl() }}" class="profile-photo">
        @else
        <img src="{{ asset('images/Avatar.png') }}" class="profile-photo">
        @endif
        <br>
        <a onclick="upload_image()" class="m-t-30 btn btn-rounded btn-info text-uppercase">Upload Photo</a>
    </div>

    <div class="col-md-8">
        {!! Form::model($employee, ['route' => ['profiles.update', $employee->id], 'method' => 'PUT']) !!}
        <div class="form-group row">
            <div class="col-md-6">
                {!! Form::text('fname', null, array('class' => 'form-control', 'placeholder' => 'First Name')) !!}
            </div>
            <div class="col-md-6">
                {!! Form::text('lname', null, array('class' => 'form-control', 'placeholder' => 'Last Name')) !!}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-4">
                <div class="input-group">
                    {{ Form::text('dob', null,  array('class' => 'form-control  datepicker-autoclose', 'placeholder' => 'Date of Birth')) }}
                    <span class="input-group-addon">
                        <i class="icon-calender"></i>
                    </span>
                </div>
            </div>
            <div class="col-md-4">
                {{Form::select('gender', ['Male' => 'Male', 'Female' => 'Female'], null, ['class' => 'form-control', 'placeholder' => 'Gender'])}}
            </div>
            <div class="col-md-4">
                {{Form::select('marriage_status', ['Single' => 'Single', 'Married' => 'Married' , 'Widowed' => 'Widowed'], null, ['class' => 'form-control', 'placeholder' => 'Marital Status'])}}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                {{Form::select('race', ['Melayu' => 'Melayu', 'Chinese' => 'Chinese' , 'India' => 'India'], null, ['class' => 'form-control', 'placeholder' => 'Race'])}}
            </div>
            <div class="col-md-6">
                {{Form::select('religion', ['Islam' => 'Islam', 'Others' => 'Others' ], null, ['class' => 'form-control', 'placeholder' => 'Religion'])}}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                {!! Form::text('ic', null, array('class' => 'form-control', 'placeholder' => 'IC')) !!}
            </div>
            {{-- <div class="col-md-6"> --}}
                {{-- <input type="text" class="form-control" placeholder="Phone Number"> --}}
                {{-- {!! Form::text('phone_number', null, ['class' => 'form-control', 'placeholder'=>'Phone Number']) !!} --}}
            {{-- </div> --}}
        </div>
        <div class="employee-addresses m-b-30">
            <div class="form-group row">
                <div class="col-md-12">
                    <h3 class="box-title">Permanent Address</h3>
                </div>
                <div class="col-md-6">
                        {!! Form::text('pre_address_line1', null, ['class' => 'form-control', 'placeholder'=>'Address Line 1']) !!}
                </div>
                <div class="col-md-6">
                        {!! Form::text('pre_address_line2', null, ['class' => 'form-control', 'placeholder'=>'Address Line 2']) !!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                        {!! Form::text('pre_city', null, ['class' => 'form-control', 'placeholder'=>'City']) !!}
                </div>
                <div class="col-md-6">
                        {!! Form::text('pre_state', null, ['class' => 'form-control', 'placeholder'=>'State']) !!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                        {!! Form::text('pre_postcode', null, ['class' => 'form-control', 'placeholder'=>'Postcode']) !!}
                </div>
                <div class="col-md-6">
                        {!! Form::text('pre_country', null, ['class' => 'form-control', 'placeholder'=>'Country']) !!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    {!! Form::text('pre_phone_number1', null, ['class' => 'form-control', 'placeholder'=>'Phone No.']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::text('pre_phone_number2', null, ['class' => 'form-control', 'placeholder'=>'Mobile No.']) !!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <h3 class="box-title">Correspondance Address</h3>
                    <div class="m-b-15 checkbox checkbox-info">
                        <input id="checkbox4" type="checkbox">
                        <label for="checkbox4">Same as the above?</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="Address Line 1">
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="Address Line 2">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="City">
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="State">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="Postcode">
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="Country">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="Phone No.">
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" placeholder="Mobile No.">
                </div>
            </div>
        </div>
        <div class="bank-info   m-b-30">
            <div class="section-title">
                <h3 class="box-title">Banking Info</h3>

            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    {!! Form::text('bank_name', null, ['class' => 'form-control', 'placeholder'=>'Bank Name']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::text('bank_account_no', null, ['class' => 'form-control', 'placeholder'=>'Account No']) !!}
                </div>
            </div>
        </div>


        <button style="width:200px" class=" btn-lg btn-rounded btn-info text-uppercase">Save</button>
        {!! Form::close() !!}


        <hr />

        <div class="family-info  m-b-30">
            <div class="section-title">
                <h3 class="box-title">Spouse, Children & Others</h3>

            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr style="border-bottom:3px solid #eee">
                            <th>#</th>
                            <th>Name</th>
                            <th class="text-center">MyKAD/Passport No.</th>
                            <th class="text-center">Relation</th>
                            <th class="text-center">DOB</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody>


                        <tr style="border-bottom:3px solid #eee">
                            <td colspan="6">
                                <a href="#add-family" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Family</a>
                            </td>

                        </tr>
                    </tbody>
                </table>
            </div>
        </div>


        <div class="employment-info m-b-30">
            <div class="section-title">
                <h3 class="box-title">Employment Background</h3>

            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr style="border-bottom:3px solid #eee">
                            <th>#</th>
                            <th>Company</th>
                            <th class="text-center">Position</th>
                            <th class="text-center">Contact</th>
                            <th class="text-center">Period</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody>

                        <tr style="border-bottom:3px solid #eee">
                            <td colspan="6">
                                <a href="#add-employment" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Employment</a>
                            </td>

                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="education-info m-b-30">
            <div class="section-title">
                <h3 class="box-title">Education Background</h3>

            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr style="border-bottom:3px solid #eee">
                            <th>#</th>
                            <th>Field of Study</th>
                            <th class="text-center">University</th>
                            <th class="text-center">Period</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody>


                        <tr style="border-bottom:3px solid #eee">
                            <td colspan="6">
                                <a href="#add-educational" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Education</a>
                            </td>

                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="family-info  m-b-30">
            <div class="section-title">
                <h3 class="box-title">Professional Certificate</h3>

            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr style="border-bottom:3px solid #eee">
                            <th>#</th>
                            <th class="text-left">Certificate Field & Name</th>
                            <th class="text-center">Expiry</th>
                            <th class="text-center">Issued By</th>
                            <th class="text-center">Ref No.</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody>


                        <tr style="border-bottom:3px solid #eee">
                            <td colspan="6">
                                <a href="#add-cert" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Certificate</a>
                            </td>

                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>


</div>


@section('js')
    @parent

    <script>

        function upload_image(){
            swal({
                title: 'Upload your profile picture',
                input: 'file',
                inputAttributes: {
                    accept: 'image/*'
                },
                showCancelButton: true,
                confirmButtonText: 'Upload',
                showLoaderOnConfirm: true,
                allowOutsideClick: () => !swal.isLoading(),
                preConfirm: function(i) {
                    return new Promise(function() {

                        var form_data = new FormData();
                        form_data.append('image', i);

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax({
                            type:'POST',
                            url: '{{ route('profile.image.upload') }}',
                            data: form_data,
                            cache:false,
                            contentType: false,
                            processData: false,
                            dataType: "json",
                            success:function(data){
                                swal({
                                    title: 'Success!',
                                    text: 'Your profile image has been updated.',
                                    type: 'success',
                                    onClose: () => {
                                        location.reload();
                                    }
                                })
                            },
                            error: function(data){

                            }
                        });
                    });
                }
            })

        }

    </script>

@endsection
