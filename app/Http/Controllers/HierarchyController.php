<?php

namespace App\Http\Controllers;

use App\Hierarchy;
use Illuminate\Http\Request;
use DataTables;

class HierarchyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function list(){
        return Datatables::eloquent(Hierarchy::query())
        ->addColumn('action', function($model){
            $url = 'hierarchies';
            $show_hide = true;
            $modal_view_id = 'modal_hierarchies_show';
            $modal_edit_id = 'modal_hierarchies_edit';
            $delete_datatable_id_name_refresh = 'dept_table';
            return view('shared._table_action', compact('model', 'url', 'show_hide','modal_view_id', 'modal_edit_id'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.organisations.hierarchies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hierarchies = Hierarchy::create($request->only('hierarchy_name','hierarchy_ranking'));
        $hierarchies->save();
        return redirect()->route('organisations.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hierarchy  $hierarchy
     * @return \Illuminate\Http\Response
     */
    public function show(Hierarchy $hierarchy)
    {
        $hierarchies = Hierarchy::where('id', $id)->first();
        return View ('admins.organisations.hierarchies.show',compact('hierarchies'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hierarchy  $hierarchy
     * @return \Illuminate\Http\Response
     */
    public function edit(Hierarchy $hierarchy)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hierarchy  $hierarchy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hierarchy $hierarchy)
    {
        $hierarchy->update($request->all());
        return response()->json($hierarchy, 201);//
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hierarchy  $hierarchy
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hierarchy $hierarchy)
    {
        $hierarchy->delete();
        return redirect()->back();
    }
}
