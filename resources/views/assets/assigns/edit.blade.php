<div id="modal_assign_edit" class="modal fade in" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Edit Assign Details</h1>
                    {!! Form::open(['url' => 'assigns', 'id' => 'assign_edit']) !!}
                    {{ Form::hidden('id', null , ['id' => 'id']) }}
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group ">
                            <select class="form-control" name="employee_id" id="assign_name_edit" data-parsley-required="true">

                                    @foreach ($employees as $e)
                                    <option value="{{ $e->id }}">{{ $e->username}}</option>
                                    @endforeach

                                </select>
                    </div>
                    <div class="form-group ">
                        {{Form::date('started_at', null , ['id' => 'assign_started_edit', 'required','class' => 'form-control','placeholder' => 'edit asset type'])}}
                    </div>
                    <div class="form-group ">
                        {{Form::date('ended_at', null , ['id' => 'assign_ended_edit', 'required','class' => 'form-control','placeholder' => 'edit asset type'])}}
                    </div>
                    <div class="form-group ">
                            <select class="form-control" name="assign_type" id="assign_type_edit" data-parsley-required="true">

                                    @foreach ($types as $type)
                                    <option value="{{ $type->id }}">{{ $type->type_name}}</option>
                                    @endforeach

                                </select>
                    </div>
                    <div class="form-group ">
                            <select class="form-control" name="assign_brand" id="assign_brand_edit" data-parsley-required="true">

                                    @foreach ($brands as $brand)
                                    <option value="{{ $brand->id }}">{{ $brand->brand_name}}</option>
                                    @endforeach

                                </select>
                    </div>
                    <div class="form-group ">
                        {{Form::select('assign_status', ['Active' => 'Active', 'Inactive' => 'Inactive'], null, ['class' => 'form-control', 'placeholder' => 'Select Status'])}}
                    </div>

                    {{Form::submit('Update Details',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                    <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

                    {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('js')
    @parent
    <script>

    $( "#assign_edit" ).on( "submit", function( event ) {
        event.preventDefault();
        swal({
            title: "Update User Assign",
            text: "Confirm to update asset assignment?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                var form = document.getElementById('assign_edit');
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: '/assigns/'+form.id.value,
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(msg) {
                        return msg;
                    }
                });
            }
        }).then((result) => {
            $('#modal_assign_edit').modal('toggle');
            if(result.value){
                $('#assign_table').DataTable().ajax.reload();
                swal('Asset Updated!', '--', 'success');
            }
        });
    });
    </script>
@endsection
