@extends('layouts.default.master')

@section('content')

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Payslip</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                <ol class="breadcrumb">
                    <li><a href="dashboard-adminview.html">Dashboard</a></li>
                    <li><a href="admin-payslip.html">Payroll</a></li>
                    <li class="active">Generate Salary</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <!-- ============================================================== -->
        <!-- Different data widgets -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-sm-3">
                <div class="white-box">
                    <h3 class="box-title">Choose Month</h3>
                    <select class="selectpicker" data-style="form-control">
                        <option>Select Month</option>
                        <option>January</option>
                        <option>February</option>
                        <option>March</option>
                        <option>April</option>
                        <option>May</option>
                        <option>June</option>
                        <option>July</option>
                        <option>August</option>
                        <option>September</option>
                        <option>October</option>
                        <option>November</option>
                        <option>December</option>
                    </select>
                    <button style="margin-top:10px" class="btn btn-info">Generate</button>
                </div>
            </div>
            <div class="col-sm-9">
                <div style="overflow:auto;" class="white-box">
                    <h3 class="box-title">May 2018 Salary</h3>
                    <div class="row">
                        <div class="col-sm-4">
                            <blockquote>
                                <table width="100%">
                                    <tr>
                                        <th>Total Employee</th>
                                        <td style="text-align: right">49</td>
                                    </tr>
                                </table>
                            </blockquote>
                        </div>
                        <div class="col-sm-4">
                            <blockquote>
                                <table width="100%">
                                    <tr>
                                        <th>Total Generated Pay</th>
                                        <td style="text-align: right">10</td>
                                    </tr>
                                </table>
                            </blockquote>
                        </div>
                        <div class="col-sm-4">
                            <blockquote>
                                <table width="100%">
                                    <tr>
                                        <th>Total Ungenerated Pay</th>
                                        <td style="text-align: right">39</td>
                                    </tr>
                                </table>
                            </blockquote>
                        </div>
                    </div>
                     <div class="generate-pay-tab">

                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#generated-pay">Generated Payslip</a></li>
                            <li><a  data-toggle="tab" href="#ungenerated-pay">Ungenerated Payslip</a></li>
                        </ul>

                        <div class="tab-content">
                            <div id="generated-pay" class="tab-pane fade in active">
                                <div class="table-responsive">
                                    <table  id="myTable" class="table display">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Employee</th>
                                                <th width="190px" class="text-center">Payment Type</th>
                                                <th width="190px" class="text-center">Amount</th>
                                                <th width="120px" class="text-right">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Husna Bahrudin</td>
                                                <td class="text-center">Monthly</td>
                                                <td class="text-center">RM5,000.00</td>
                                               <td class="text-right"><a  class="m-r-10 label label-info"><i data-toggle="tooltip" data-title="View Summary" class="fa fa-eye"></i></a>

                                                <a class="m-r-10 label label-purple"><i data-toggle="tooltip" data-title="Print Payslip" class="fa fa-print"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Muhaimin Juhari</td>
                                                <td class="text-center">Web Developer</td>
                                                <td class="text-center">RM0.00</td>
                                               <td class="text-right"><a  class="m-r-10 label label-info"><i data-toggle="tooltip" data-title="View Summary" class="fa fa-eye"></i></a>

                                                <a class="m-r-10 label label-purple"><i data-toggle="tooltip" data-title="Print Payslip" class="fa fa-print"></i></a></td>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>Nor Emil Badli Munawir</td>
                                                <td class="text-center">Web Developer</td>
                                                <td class="text-center">RM0.00</td>
                                              <td class="text-right"><a  class="m-r-10 label label-info"><i data-toggle="tooltip" data-title="View Summary" class="fa fa-eye"></i></a>

                                                <a class="m-r-10 label label-purple"><i data-toggle="tooltip" data-title="Print Payslip" class="fa fa-print"></i></a></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div id="ungenerated-pay" class="tab-pane fade">
                                <div class="table-responsive">
                                    <table  id="myTable" class="table display">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Employee</th>
                                                <th width="190px" class="text-center">Payment Type</th>
                                                <th width="190px" class="text-center">Amount</th>
                                                <th width="120px" class="text-right">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>Husna Bahrudin</td>
                                                <td class="text-center">Monthly</td>
                                                <td class="text-center">RM5,000.00</td>
                                                {{-- <td class="text-right"><a href="{{route('generates.generate')}}" class="m-r-10 label label-info"><i data-toggle="tooltip" class="fa fa-print"></i> Generate</a></td> --}}
                                            </tr>
                                            <tr>
                                                <td>2</td>
                                                <td>Muhaimin Juhari</td>
                                                <td class="text-center">Web Developer</td>
                                                <td class="text-center">RM0.00</td>
                                                <td class="text-right"><a  class="m-r-10 label label-info"><i data-toggle="tooltip" class="fa fa-print"></i> Generate</a></td>
                                            </tr>
                                            <tr>
                                                <td>1</td>
                                                <td>Nor Emil Badli Munawir</td>
                                                <td class="text-center">Web Developer</td>
                                                <td class="text-center">RM0.00</td>
                                               <td class="text-right"><a  class="m-r-10 label label-info"><i data-toggle="tooltip" class="fa fa-print"></i> Generate</a></td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>





        <div class="modal fade" id="set-increment">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h1>Set Increment</h1>
                       <div class="form-group">
                         <input type="text" class="form-control" disabled="disabled" value="Employee : Husna Bahrudin">
                       </div>
                       <div class="form-group row">
                        <div class="col-md-6">
                         <input type="text" class="form-control" placeholder="Set increment. E.g 130.00">
                        </div>
                        <div class="col-md-6">
                         <select class="selectpicker" data-style="form-control">
                            <option>Select Type of Increment</option>
                            <option>Monthly</option>
                            <option>Hourly</option>
                         </select>
                        </div>
                       </div>


                       <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Save Salary</button>
                                    <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


    <!-- /.container-fluid -->
</div>
@endsection
@section('js')

@endsection
