<div class="family-info  m-b-30">
        <div class="section-title">
            <h3 class="box-title">Professional Certificate</h3>
        </div>

        <a href="#add-cert" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Certificate</a>
        <br />
        <br />

        <div class="table-responsive">
            <table id="cert_table" class="table">
                <thead>
                    <tr style="border-bottom:3px solid #eee">
                        <th>#</th>
                        <th class="text-left">Certificate Name</th>
                        <th class="text-center">Expiry</th>
                        <th class="text-center">Issued By</th>
                        <th class="text-center">Ref No.</th>
                        <th class="text-right">Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>


    @include('admins.employees.pages.personal.professional.create')
    @include('admins.employees.pages.personal.professional.edit')

    @section('js')
    @parent

    <script>
        $(function () {
            $('#cert_table').DataTable({
                serverSide: true,
                processing: true,
                ajax: '{{ route("employees.certificates.index", $employee->id) }}',
                columns: [
                    {data: 'DT_Row_Index', orderable: false, searchable: false},
                    {data: 'title'},
                    {data: 'expired_at'},
                    {data: 'issued_by'},
                    {data: 'ref_number'},
                    {data: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@endsection
