<?php

namespace App\Http\Controllers;

use App\Generate;
use Illuminate\Http\Request;

class GenerateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Generate  $generate
     * @return \Illuminate\Http\Response
     */
    public function show(Generate $generate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Generate  $generate
     * @return \Illuminate\Http\Response
     */
    public function edit(Generate $generate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Generate  $generate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Generate $generate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Generate  $generate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Generate $generate)
    {
        //
    }
}
