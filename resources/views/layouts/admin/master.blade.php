<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ URL::asset('img/favicon.png') }}">
        <title>IX - HRMS</title>

        <link href=" {{ URL::asset('bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href=" {{ URL::asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet">
        <link href=" {{ URL::asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') }}" rel="stylesheet">
        <link href=" {{ URL::asset('plugins/bower_components/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
        <link href=" {{ URL::asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
        <link href=" {{ URL::asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
        <link href=" {{ URL::asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
        <link href=" {{ URL::asset('plugins/bower_components/calendar/dist/fullcalendar.css') }}" rel="stylesheet">
        <link href=" {{ URL::asset('css/animate.css') }}" rel="stylesheet">
        <link href=" {{ URL::asset('css/style.css') }}" rel="stylesheet">
        <link href=" {{ URL::asset('css/colors/blue-dark.css') }}" rel="stylesheet">

    </head>

    <body>
        @include('layouts.admin.navbar')
        @include('layouts.admin.sidebar')

        @yield('content')

        <script src="{{ URL::asset('plugins/bower_components/jquery/dist/jquery.min.js')}}"></script>
        <script src="{{ URL::asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>
        <script src="{{ URL::asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js')}}"></script>
        <script src="{{ URL::asset('js/jquery.slimscroll.js')}}"></script>
        <script src="{{ URL::asset('js/waves.js')}}"></script>
        <script src="{{ URL::asset('bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <script src="{{ URL::asset('plugins/bower_components/waypoints/lib/jquery.waypoints.js')}}"></script>
        <script src="{{ URL::asset('plugins/bower_components/counterup/jquery.counterup.min.js')}}"></script>
        <script src="{{ URL::asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{ URL::asset('plugins/bower_components/moment/moment.js')}}"></script>
        <script src="{{ URL::asset('plugins/bower_components/calendar/dist/fullcalendar.min.js')}}"></script>
        <script src="{{ URL::asset('plugins/bower_components/calendar/dist/cal-init.js')}}"></script>
        <script src="{{ URL::asset('js/custom.min.js')}}"></script>
        <script src="{{ URL::asset('js/cbpFWTabs.js')}}"></script>
        <script src="{{ URL::asset('plugins/bower_components/toast-master/js/jquery.toast.js')}}"></script>

        @yield('js')

        <script>
            (function() {
            [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
                new CBPFWTabs(el);
            });
        })();
        $('.user-pro').removeClass('active');
        // Date Picker
        var defaults = $.fn.datepicker.defaults = {

        orientation: "auto",

        };
        jQuery('.mydatepicker, #datepicker').datepicker();
        jQuery('.datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        </script>
    </body>
</html>
