<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Employee extends Authenticatable implements HasMedia
{
    use Notifiable;
    use HasRoles;
    use HasMediaTrait;

    protected $guarded = [];
    protected $table = 'employees';
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $casts = [
        'working_days' => 'array',
    ];

    public function organisation(){
        return $this->belongsTo('App\Organisation', 'company_id');
    }

    public function department(){
        return $this->belongsTo('App\Department');
    }

    public function hierarchies(){
        return $this->belongsTo('App\Hierarchy');
    }

    public function designation(){
        return $this->belongsTo('App\Designation');
    }

    public function claims(){
        return $this->hasMany(Claims::class,'emp_id','id');
    }

    public function annual() {
        return $this->hasMany('App\Annual');
    }

    public function contacts(){
        return $this->hasMany(EmployeeContact::class);
    }

    public function histories(){
        return $this->hasMany(EmployeeHistory::class);
    }

    public function educations(){
        return $this->hasMany(EmployeeEducation::class);
    }

    public function certificates(){
        return $this->hasMany(EmployeeCertificate::class);
    }

    public function medicals(){
        return $this->hasMany(EmployeeMedical::class);
    }

    public function approvals(){
        return $this->hasMany(Approval::class);
    }

    public function assigns(){
        return $this->hasMany(Assign::class,'employee_id');
    }

    public function documents(){
        return $this->hasMany(EmployeeDocument::class,'employee_id');
    }

    public function salary(){
        return $this->hasOne(Salary::class);
    }

    public function leave_allocs(){
        return $this->hasMany(EmployeeLeaveAllocation::class);
    }

    public function leaves(){
        return $this->hasMany(EmployeeLeave::class);
    }
    public function notice()
    {
        return $this->hasMany(Notice::class);
    }
    public function gen()
    {
        return $this->hasMany(Generate::class,'employee_id');
    }

    public function asset_alloc(){
        return $this->hasMany(Assign::class);
    }

    public function activate(){
        return $this->hasOne(ActivateToken::class);
    }

    public function reset(){
        return $this->hasMany(Reset::class);
    }
    // public function assign(){
    //     return $this->hasMany(Assign::class);
    // }

    public function getFullNameAttribute($value){
        return ucfirst($this->fname) . ' ' . ucfirst($this->lname);
     }
}

