<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close">
                <i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span>
                <span class="hide-menu">Navigation</span>
            </h3>
        </div>
        <ul class="nav" id="side-menu">
            <li class="user-pro">
                <a href="{{ route('profile.my') }}">
                    @if(Auth::user()->getMedia('profile')->first())
                    <img src="{{ Auth::user()->getMedia('profile')->first()->getUrl() }}" alt="user-img" class="img-circle">
                    @else
                    <img src="{{ asset('images/Avatar.png') }}" alt="user-img" class="img-circle">
                    @endif
                    <span class="hide-menu"> {{ Auth::user()->fname }} {{ Auth::user()->lname }}</span>
                </a>
            </li>

            @can('view_employees')
            <li><a href="{{ route('employees.index') }}" class="waves-effect"><i class="fa fa-users fa-fw"></i> <span class="hide-menu">Employees</span></a></li>
            @endcan

            @can('view_organisations')
            <li><a href="{{ url('/organisations') }}" class="waves-effect"><i class="fa fa-university fa-fw"></i> <span class="hide-menu">Organisations</span></a></li>
            @endcan

            @can('view_leaves')
            <li><a href="{{ url('/leaves') }}" class="waves-effect"><i class="mdi mdi-calendar fa-fw"></i> <span class="hide-menu">Leave</span></a></li>
            @endcan

            @can('view_claims')
            <li><a href="{{ url('/claims') }}" class="waves-effect"><i class="mdi mdi-coin fa-fw"></i> <span class="hide-menu">Claims</span></a></li>
            @endcan

            @can('view_notices')
            <li><a href="{{ url('/notices') }}" class="waves-effect"><i class="mdi mdi-newspaper fa-fw"></i> <span class="hide-menu">Notices</span></a></li>
            @endcan

            @can('view_generates')
            <li><a href="{{ url('/payslips') }}" class="waves-effect"><i class="mdi mdi-format-list-bulleted fa-fw" data-icon="v"></i> <span class="hide-menu">Salary List</span></a></li>
            @endcan

            @can('view_payslips')
            <li><a href="{{ url('/generates') }}" class="waves-effect"><i class="mdi mdi-file-outline fa-fw" data-icon="v"></i> <span class="hide-menu">Generate Salary</span></a></li>
            @endcan

            @can('view_events')
            <li><a href="{{ url('/events') }}" class="waves-effect"><i class="mdi mdi-calendar-text fa-fw" data-icon="v"></i> <span class="hide-menu">Event & Calendar</span></a></li>
            @endcan

            @can('view_employee_events')
            <li><a href="{{ url('/empevents') }}" class="waves-effect"><i class="mdi mdi-calendar-text fa-fw" data-icon="v"></i> <span class="hide-menu">Calendar</span></a></li>
            @endcan

            @can('view_empgenerates')
            <li><a href="{{ url('/empgenerates') }}" class="waves-effect"><i class="mdi mdi-file-outline fa-fw" data-icon="v"></i> <span class="hide-menu">My Payslip</span></a></li>
            @endcan


            @can('view_admin_leaves')
            <li><a href="{{ url('/adminleaves') }}" class="waves-effect"><i class="mdi mdi-calendar fa-fw"></i> <span class="hide-menu">Leave Approval</span></a></li>
            @endcan
            @can('view_adminclaims')
            <li><a href="{{ url('/adminclaims') }}" class="waves-effect"><i class="mdi mdi-calendar fa-fw"></i> <span class="hide-menu">Claim Approval</span></a></li>
            @endcan

            @can('view_assets')
            <li><a href="{{ url('/assets') }}" class="waves-effect"><i class="mdi mdi-treasure-chest fa-fw"></i> <span class="hide-menu">Assets</span></a></li>
            @endcan

            @can('view_roles')
            <li><a href="{{ url('/roles') }}" class="waves-effect"><i class="mdi mdi-human-male fa-fw"></i> <span class="hide-menu">Roles</span></a></li>
            @endcan

            @can('view_system_setting')
            <li><a href="{{ route('settings.index') }}" class="waves-effect"><i class="mdi mdi-settings fa-fw"></i> <span class="hide-menu">Settings</span></a></li>
            @endcan
            {{-- @role('User')
            <li><a href="{{ url('/leaves') }}" class="waves-effect"><i class="mdi mdi-calendar fa-fw"></i> <span class="hide-menu">Leaves</span></a></li>
            <li><a href="{{ url('/claims') }}" class="waves-effect"><i class="mdi mdi-coin fa-fw"></i> <span class="hide-menu">Claim</span></a></li>
            <li><a href="{{ url('/notices') }}" class="waves-effect"><i class="mdi mdi-message-bulleted fa-fw"></i> <span class="hide-menu">Notice</span></a></li>
            <li><a href="{{ url('/generates') }}" class="waves-effect"><i class="mdi mdi-av-timer fa-fw" data-icon="v"></i> <span class="hide-menu"> Payslip</span></a></li>
            @endrole --}}
            {{-- @role('Admin')
            <li>
                <a href="" class="waves-effect"><i class="mdi mdi-av-timer fa-fw" data-icon="v"></i> <span class="hide-menu"> Administration <span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="{{ url('/organisations') }}" class="waves-effect"><i class="fa fa-university fa-fw"></i> <span class="hide-menu">Organisation</span></a></li>
                    <li><a href="{{ route('employees.index') }}" class="waves-effect"><i class="fa fa-users fa-fw"></i> <span class="hide-menu">Employee</span></a></li>
                    <li><a href="{{ url('/adminleaves') }}" class="waves-effect"><i class="mdi mdi-calendar fa-fw"></i> <span class="hide-menu">Leaves</span></a></li>
                    <li><a href="{{ url('/adminclaims') }}" class="waves-effect"><i class="mdi mdi-coin fa-fw"></i> <span class="hide-menu">Claims</span></a></li>
                    <li><a href="{{ url('/events') }}" class="waves-effect"><i class="mdi mdi-calendar fa-fw"></i> <span class="hide-menu">Holiday</span></a></li>
                    <li><a href="{{ url('/notices') }}" class="waves-effect"><i class="mdi mdi-message-bulleted fa-fw"></i> <span class="hide-menu">Notice</span></a></li>
                    <li><a href="{{ url('/payslips') }}" class="waves-effect"><i class="mdi mdi-av-timer fa-fw" data-icon="v"></i> <span class="hide-menu"> Payslip <span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('/payslips') }}"><i class=" fa-fw"></i><span class="hide-menu">Payroll</span></a> </li>
                            <li> <a href="{{ url('/generates')}}"><i class=" fa-fw"></i><span class="hide-menu">Generate</span></a> </li>
                        </ul>
                    </li>
                    <li><a href="{{ url('/assets') }}" class="waves-effect"><i class="mdi mdi-settings fa-fw"></i> <span class="hide-menu">Assets</span></a></li>
                    <li><a href="{{ url('/roles') }}" class="waves-effect"><i class="mdi mdi-settings fa-fw"></i> <span class="hide-menu">Roles</span></a></li>
                    <li><a href="{{ route('settings.index') }}" class="waves-effect"><i class="mdi mdi-settings fa-fw"></i> <span class="hide-menu">Settings</span></a></li>
                </ul>
            </li>
            @endrole --}}

        </ul>
    </div>
</div>
