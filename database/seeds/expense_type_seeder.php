<?php

use Illuminate\Database\Seeder;
use App\ExpenseType;

class expense_type_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('expense'=>'Hotel & Travelling'),
            array('expense'=>'Meal Allowance'),
            array('expense'=>'Refreshment & Entertainment'),
            array('expense'=>'Mileage'),
            array('expense'=>'Toll & Parking'),
            array('expense'=>'Claimable Allowances'),
            array('expense'=>'Office Supplies'),
            array('expense'=>'Miscellaneous'),
        );

        ExpenseType::insert($data);
    }
}
