<?php

namespace App\Http\Controllers;

use App\AssetType;
use App\AssetBrand;
use DataTables;
use Illuminate\Http\Request;

class AssetTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function list(){
        return Datatables::eloquent(AssetType::query())
        ->addColumn('action', function($model){
            $url = 'types';
            $modal_view_id = 'modal_type_show';
            $modal_edit_id = 'modal_type_edit';
            $delete_datatable_id_name_refresh = 'type_table';
            return view('shared._table_action', compact('model', 'url', 'modal_view_id', 'modal_edit_id', 'delete_datatable_id_name_refresh'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('assets.types.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $types = AssetType::create($request->only('type_name','status'));

        $types->save();
        return response()->json($types, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssetType  $assetType
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $types = AssetType::find($id);
        return view('assets.types.show',compact('types'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssetType  $assetType
     * @return \Illuminate\Http\Response
     */
    public function edit(AssetType $assetType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssetType  $assetType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $asset = AssetType::find($id);
        $asset->update($request->all());

        return response()->json($asset, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssetType  $assetType
     * @return \Illuminate\Http\Response
     */

    public function destroy(AssetType $type)
    {
        $type->delete();
        return redirect()->back();
    }
}
