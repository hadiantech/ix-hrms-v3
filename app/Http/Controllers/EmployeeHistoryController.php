<?php

namespace App\Http\Controllers;

use App\EmployeeHistory;
use App\Employee;
use Illuminate\Http\Request;
use DataTables;

class EmployeeHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Employee $employee)
    {
        return Datatables::eloquent(EmployeeHistory::where('employee_id', $employee->id))
        ->addColumn('period', function($model){
            return $model->started_at." - ".$model->ended_at;
        })
        ->addColumn('action', function($model) use ($employee){
            $model_2 = $employee;
            $modal_view_id = 'modal_history_show';
            $modal_edit_id = 'modal_history_edit';
            $url = 'employees.histories';
            $show_hide = true;
            return view('shared._table_action', compact('model', 'url', 'modal_view_id', 'modal_edit_id', 'model_2', 'show_hide'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $history = EmployeeHistory::create($request->all());
        $employee->histories()->save($history);
        return response()->json($history, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeHistory  $employeeHistory
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeHistory $employeeHistory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeHistory  $employeeHistory
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeHistory $employeeHistory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeHistory  $employeeHistory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee, EmployeeHistory $history)
    {
        $history->update($request->all());
        return response()->json($history, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeHistory  $employeeHistory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee, EmployeeHistory $history)
    {
        $history->delete();
        return redirect()->back();
    }
}
