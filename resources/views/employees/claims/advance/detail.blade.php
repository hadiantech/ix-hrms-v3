@extends('layouts.default.master')

@section('content')

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add Advanced Claim</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li><a href="{{ route('claims.index') }}">Claim Dashboard</a></li>
                    <li class="active">Add Advanced Claim</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title text-uppercase m-b-30">New Advanced Claim : Project {{ $claim->project_id }}
                        @if($claim->claim_status == 'draft')
                        <a href="#add-expense" data-toggle="modal" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Add Expenses</a>
                        @endif
                    </h3>
                    {{-- <div class="alert alert-warning alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Your advanced claim amount is lower match the advance requested. Kindly return the balance to HR.
                    </div> --}}
                    <div class="table-responsive">
                        <table id="expenses_table" class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Type</th>
                                    <th>Item Name & Desc</th>
                                    <th class="text-center">Project Code</th>
                                    <th class="text-center">Amount (RM)</th>
                                    <th class="text-center">Item Date</th>
                                    <th class="text-center">Receipt No.</th>
                                    <th class="text-center">Ref No.</th>
                                    <th class="text-right">Action</th>
                            </thead>
                        </table>
                    </div>
                    <br/><br/><br/>
                    <div class="table-responsive">
                        <table id="" class="table">
                            <tfoot class="claim-amount">
                                <tr>
                                    <th colspan="6" rowspan="4"></th>
                                    <th class="text-right">TOTAL AMOUNT</th>
                                    <th colspan="2" class="text-right"><span class="amount" id="total_expenses">{{ $claim->claim_total }}</span></th>
                                </tr>
                                <tr>
                                    <th class="text-right">APPROVED ADVANCE REQUEST</th>
                                    <th colspan="2" class="text-right"><span class="amount" id="total_claim">{{ $claim->claim_total_advance }}</span></th>
                                </tr>
                                <tr>
                                    <th class="text-right">BALANCE</th>
                                    <th colspan="2" class="text-right"><span class="amount" id="total_balance"></span></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                    {!! Form::model($claim,['url' => '', 'id' => 'claim_update']) !!}
                    <input type="hidden" name="_method" value="PUT">
                    <input id="claim_id" name="claim_id" type="hidden" value="{{ $claim->id }}" />
                    
                    @if($approver == 1 or $approver == 2)
                        <fieldset>
                    @else
                        @if($claim->claim_status == 'submit')<fieldset disabled>
                        @else<fieldset>
                        @endif
                    @endif
                    
                    <div class="payment-form row m-b-20">
                        <div class="col-md-6">
                            <h3 class="box-title">BALANCE PAYMENT UPDATE</h3>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <select name="method_payment" class="form-control" data-style="">
                                        <option selected disabled>Method of Payment</option>
                                        <option @if($claim->method_payment == 'Online Transfer') selected @endif value="Online Transfer">Online Transfer</option>
                                        <option @if($claim->method_payment == 'Cash By Hand') selected @endif value="Cash By Hand">Cash By Hand</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <select name="payment_status" class="form-control">
                                        <option @if($claim->payment_status == 'Unpaid') value="Unpaid" selected @endif>Unpaid</option>
                                        <option @if($claim->payment_status == 'Paid') value="Paid" selected @endif>Paid</option>
                                    </select>
                                </div>
                            </div>
                            <p>If you have balance to pay back to the company, kindly transfer the balance to the
                                following bank details and upload the payment receipt for verification.
                            </p>
                            <p style="background:#eee;padding:20px;margin:0">
                                <strong>BONEYBONE VENTURES SDN BHD</strong><br>
                                CIMB BANK BERHAD<br>
                                712345678
                            </p>
                        </div>
                        <div class="col-md-6">
                            <h3 class="box-title">&nbsp;</h3>
                            <div class="form-group">
                                {{ Form::text('bank_name',null,['class' => 'form-control', 'placeholder' => 'Bank Name'])}}
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    {{ Form::text('reference_no',null,['class' => 'form-control', 'placeholder' => 'Reference Number'])}}
                                </div>
                                <br/><br/><br/>
                                <div class="col-md-6">
                                    <label>Date</label>
                                    {{ Form::date('ref_date',null,['class' => 'form-control', 'placeholder' => 'Date'])}}
                                </div>
                                <div class="col-md-6">
                                    <label>Time</label>
                                    {{ Form::time('ref_time', null,['class' => 'form-control', 'placeholder' => 'Time'])}}
                                </div>
                            </div>
                            {{-- <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" placeholder="ds" data-trigger="fileinput">
                                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                    <span class="fileinput-filename"></span>
                                </div>
                                <span class="input-group-addon btn btn-default btn-file">
                                    <span class="fileinput-new">Select file</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="file" name="...">
                                </span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div> --}}
                        </div>
                    </div>
                </div>
                </fieldset>

                @if(Auth::user()->id == $claim->emp_id)
                    @if($claim->claim_status == 'draft')
                    <button type="submit" onclick="claim_status('draft')" style="width:200px" class="m-r-10 btn-lg btn-rounded btn-info text-uppercase">Save Claim</button>
                    <button type="submit" onclick="claim_status('submit')" style="width:200px" class=" btn-lg btn-rounded btn-success text-uppercase">Confirm & Submit</button>
                    @endif
                @endif
                @if($approver == 1)
                <button type="submit" onclick="proc_status('return')" style="width:200px" class="m-r-10 btn-lg btn-rounded btn-info text-uppercase">Return</button>
                <button type="submit" onclick="proc_status('proc_advance')" style="width:200px" class=" btn-lg btn-rounded btn-success text-uppercase">Reviewed</button>
                @endif

                @if($approver == 2)
                <button type="submit" onclick="proc_status('return')" style="width:200px" class="m-r-10 btn-lg btn-rounded btn-info text-uppercase">Return</button>
                <button type="submit" onclick="proc_status('approved_advance')" style="width:200px" class=" btn-lg btn-rounded btn-success text-uppercase">Approve</button>
                @endif
                 {!! Form::close() !!}
            </div>
        </div>
        
        @include('employees.claims.monthly.create')
        @include('employees.claims.monthly.edit')

    </div>
</div>
@endsection
@section('js')
@parent
<script>

$(function () {
    
    bal = $('#total_claim').text() - $('#total_expenses').text();
    $('#total_balance').text(bal.toFixed(2));
});

</script>
<script>
    $(function () {
        $('#expenses_table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/expenses/list/{{ $claim->id }}',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false, class: 'text-left'},
                {data: 'expense_type.expense', name: 'item_type'},
                {data: 'item_name_desc', name: 'item_name'},
                {data: 'project_code'},
                {data: 'amount'},
                {data: 'purchase_date'},
                {data: 'receipt_no'},
                {data: 'reference_no'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
<script>
    $( "#expenses_create" ).on( "submit", function( event ) {
        event.preventDefault();
        swal({
            title: "Confirm submission",
            text: "Are you sure you want to submit?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                return new Promise(function(resolve, reject){
                    var form = document.getElementById('expenses_create');
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url: '{{ route('expenses.store') }}',
                        data: formData,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            resolve(data);
                        },
                        error: function(){
                            swal.showValidationMessage('server error');
                            swal.hideLoading();
                        }
                    });
                });
            }
        }).then((result) => {
            if(result.value){
                $('#add-expense').modal('toggle');
                location.reload(true);
                //$('#expenses_table').DataTable().ajax.reload();
                //swal('Add Success!', '--', 'success');
                //$('#amount_view').text(result.value.claim_total); --}}
            }
        });
    });
</script>
<script>
    $(document).off('click', '.open-modal').on("click", ".open-modal", function () {
        //console.log('run');

        var data = $(this).data('modal_data');

        $('#edit_expense').on('shown.bs.modal', function(e){
            $(".modal-body #claim_id").val(data.claim_id);
            $(".modal-body #item_type").val(data.item_type).change();
            $(".modal-body #project_code").val(data.project_code);
            $(".modal-body #item_name").val(data.item_name);
            $(".modal-body #description").val(data.description);
            $(".modal-body #purchase_date").val(data.purchase_date);
            $(".modal-body #amount").val(data.amount);
            $(".modal-body #amount_old").val(data.amount);
            $(".modal-body #receipt_no").val(data.receipt_no);
            $(".modal-body #reference_no").val(data.reference_no);
            $(".modal-body #expenses_id").val(data.id);
            $(this).off('shown.bs.modal');
        });

        $('#edit_expense').on('hide.bs.modal', function(e){
            $('#expenses_create').trigger("reset");
            $(this).off('hide.bs.modal');
        });
    });
</script>
<script>

    var status;
    var proc_status;

    function claim_status(e){
        
        if(e == 'submit'){
            status = e;
            proc_status = 'unproc_advance';
        }
        else{
            status = e;
            proc_status = 'unproc_advance';
        }
    }

     function proc_status(e){
        if(e == 'return'){
            proc_status = 'unproc_advance';
            status = 'draft';
        }
        else if(e == 'proc_advance'){
            proc_status  = e;
            status = 'submit';
        }
        else if(e == 'approved_advance'){
            proc_status = e;
            status = 'submit';
        }
        else{
            proc_status = e;
        }
    }

    $("#claim_update").on("submit", function (event) {
        event.preventDefault();
        swal({
            title: "Update",
            text: "Are you sure you want to update?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                return new Promise(function(resolve, reject){
                    var form = document.getElementById('claim_update');
                    var formData = new FormData(form);
                    formData.append('claim_status', status);
                    formData.append('claim_proc_status', proc_status);
                    $.ajax({
                        type: "POST",
                        url: "/claims/submit/"  + form.claim_id.value,
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            resolve(data);
                        },
                        error: function(){
                            swal.showValidationMessage('server error');
                            swal.hideLoading();
                        }
                    });
                });
            }
        }).then((result) => {
            if(result.value){
                //  swal('Updated!', '--', 'success');
                location.reload(true);
            }
        });
    });
</script>

@endsection
