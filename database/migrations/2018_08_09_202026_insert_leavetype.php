<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertLeavetype extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leave_type', function (Blueprint $table) {
            //
            DB::table('leave_type')->insert([

                'leave' => 'Annual Leave',
                'leave_quota' => '16',
                'editable' => '0',
                'applicable_days' => '5',
                'isDefault' => '1',
                'isProbation' => '0',
                'GenderType' => 'Both',
                ]);
                DB::table('leave_type')->insert([
                'leave' => 'Medical Leave',
                'leave_quota' => '30',
                'editable' => '0',
                'applicable_days' => '0',
                'isDefault' => '1',
                'isProbation' => '0',
                'GenderType' => 'Both',

]);
DB::table('leave_type')->insert([
                'leave' => 'Emergency Leave',
                'leave_quota' => '0',
                'editable' => '0',
                'applicable_days' => '0',
                'isDefault' => '1',
                'isProbation' => '1',
                'GenderType' => 'Both',
                ]);
                DB::table('leave_type')->insert([
                'leave' => 'Compassionate Leave',
                'leave_quota' => '0',
                'editable' => '0',
                'applicable_days' => '0',
                'isDefault' => '1',
                'isProbation' =>'0',
                'GenderType' => 'Both',
                ]);
                DB::table('leave_type')->insert([
                'leave' => 'Paternity Leave',
                'leave_quota' => '30',
                'editable' => '0',
                'applicable_days' => '0',
                'isDefault' => '1',
                'isProbation' => '0',
                'GenderType' => 'Both',
                ]);
                DB::table('leave_type')->insert([
                'leave' => 'Maternity Leave',
                'leave_quota' => '90',
                'editable' => '0',
                'applicable_days' => '0',
                'isDefault' => '1',
                'isProbation' => '0',
                'GenderType' => 'Female',
                ]);
                DB::table('leave_type')->insert([
                'leave' => 'Unpaid Leave',
                'leave_quota' => '0',
                'editable' => '3',
                'applicable_days' => '3',
                'isDefault' => '0',
                'isProbation' => '1',
                'GenderType' => 'Both'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leave_type', function (Blueprint $table) {
            //
        });
    }
}
