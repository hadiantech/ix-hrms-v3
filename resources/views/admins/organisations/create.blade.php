
<div id="add-company" class="modal fade in" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Add New Company</h1>

                {!! Form::open(['url' => 'organisations', 'id' => 'organisation_create', 'files' => true]) !!}
                    <div class="  form-group ">

                        {{ Form::text('company_name', null , ['required','class' => 'form-control','placeholder' => 'Company Name']) }}
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                        {!! Form::label('reg_num','Company Registration Number') !!}
                        {{  Form::text('reg_num', null , ['required','class' => 'form-control','placeholder' => 'Register Number']) }}
                        </div>

                         <div class="col-md-6">
                        {!! Form::label('company_logo','Company Logo') !!}
                             <div class="m-b-0 fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="file"> <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                    <span class="fileinput-filename"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file">
                                    <span class="fileinput-new">Select file</span>
                                    <span class="fileinput-exists">Change</span>
                                    {{ Form::file('file', null, ['required', 'placeholder' => 'Upload Logo']) }}
                                    </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                        {!! Form::label('company_address1','Company Address') !!}
                        {{ Form::text('address1', null , ['required','class' => 'form-control','placeholder' => 'Address Line 1']) }}
                        </div>
                        <div class="col-md-6">
                        {!! Form::label('company_address1','Company Address Line 2') !!}
                        {{ Form::text('address2', null , ['required','class' => 'form-control','placeholder' => 'Address Line 2']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-4">
                        {{ Form::text('city', null , ['required','class' => 'form-control','placeholder' => 'City']) }}
                        </div>
                        <div class="col-md-4">
                        {{ Form::text('state', null , ['required','class' => 'form-control','placeholder' => 'State']) }}
                        </div>
                        <div class="col-md-4">
                        {{ Form::text('country', null , ['required','class' => 'form-control','placeholder' => 'Country']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                        {!! Form::label('website','Company Website Details') !!}
                            {{ Form::text('website', null , ['required','class' => 'form-control','placeholder' => 'Website link']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-4">
                            {!! Form::label('social','Company Facebook') !!}
                            {{ Form::text('fb', null , ['required','class' => 'form-control','placeholder' => 'Facebook']) }}
                        </div>
                        <div class="col-md-4">
                        {!! Form::label('social','Company Linkedin') !!}
                            {{ Form::text('linkedin', null , ['required','class' => 'form-control','placeholder' => 'LinkedIn']) }}
                        </div>
                        <div class="col-md-4">
                        {!! Form::label('social','Company Twitter') !!}
                            {{ Form::text('other', null , ['required','class' => 'form-control','placeholder' => 'Twitter']) }}
                        </div>
                    </div>
                    <div class="row form-group m-b-30">
                        <div class="col-md-6">
                        {{ Form::text('number', null , ['required','class' => 'form-control','placeholder' => 'Phone Number']) }}
                        </div>
                        <div class="col-md-6">
                        {{ Form::text('fax', null , ['required','class' => 'form-control','placeholder' => 'Fax Number']) }}</div>
                    </div>
                    <div class="row form-group m-b-30">
                            <div class="col-md-6">
                            {!! Form::label('natureofbusiness','Company Nature of Business') !!}
                            {{ Form::text('business', null , ['required','class' => 'form-control','placeholder' => 'insert nature of business']) }}
                            </div>
                            <div class="col-md-6">
                            {!! Form::label('natureoforg','Company Nature of Organisation') !!}
                                    <select name="status" class="form-control" required>
                                            <option value="">Please  Select Nature</option>
                                            <option value="sdnbhd">Sdn Bhd</option>
                                            <option value="bhd">Berhad</option>
                                            <option value="ent">Enterprise</option>
                                            <option value="prtnr">Partnership</option>
                                            <option value="sole">Sole Organisation</option>
                                    </select>
                            </div>
                        </div>
                    <div class="row form-group m-b-30">
                        <div class="col-md-6">
                        {!! Form::label('tax','Company SST Registration Number') !!}
                        {{ Form::text('tax', null , ['required','class' => 'form-control','placeholder' => 'insert sst number']) }}
                        </div>
                        <div class="col-md-6">
                                {!! Form::label('Company Date of Incorporation') !!}
                                {{ Form::date('start_date',null,['required','class'=>'form-control','placeholder'=>'date of incorporation']) }}
                            </div>
                        </div>
                        {{ Form::submit('Add Company',['class' =>'btn btn-rounded btn-info text-uppercase']) }}
                    </div>
                     
                        

                   

                {!! Form::close() !!}
            
        </div>
    </div>
</div>

@section('js')
    @parent
    <script>
        $('#add-company').on('hidden.bs.modal', function(e) {
            $(this).find('#organisation_create')[0].reset();
        });

        $( "#organisation_create" ).on( "submit", function( event ) {
            event.preventDefault();
            swal({
                title: "Submit Company",
                text: "Are you sure you want to submit this company?",
                type: "info",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                preConfirm: function() {
                    var form = document.getElementById('organisation_create');
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url: '{{ route('organisations.store') }}',
                        data: formData,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: function(msg) {
                            return msg;
                        }
                    });
                }
            }).then((result) => {
                $('#add-company').modal('toggle');
                if(result.value){
                    $('#org_table').DataTable().ajax.reload();
                    swal('New Company Added!', '--', 'success');
                }
            });
        });
    </script>
@endsection
