<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GeneratePay extends Model
{

    public function paycategory(){
        return $this->hasMany('App\PayrollCategory', 'brand_asset');
    }
}
