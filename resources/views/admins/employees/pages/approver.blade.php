<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h3 class="box-title">Approval Layer</h3>

        {!! Form::open(['url' => route('approve.save', $employee->id), "id" => "employee_approval"]) !!}
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr style="border-bottom:3px solid #eee">
                        <th>Leave Application</th>
                        <th width="40%" class="text-right">Approver</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Approver Level 1</td>
                        <td>
                            {{ Form::select('leave_approver_lvl_1', $top_employees, $employee->approvals()->type('leave')->level(1)->first() ? $employee->approvals()->type('leave')->level(1)->first()->approver_id : '' , ['class' => 'form-control', 'placeholder' => 'Select one']) }}
                        </td>
                    </tr>
                    <tr>
                        <td>Approver Level 2</td>
                        <td>
                            {{ Form::select('leave_approver_lvl_2', $top_employees, $employee->approvals()->type('leave')->level(2)->first() ? $employee->approvals()->type('leave')->level(2)->first()->approver_id : '' , ['class' => 'form-control', 'placeholder' => 'Select one']) }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr style="border-bottom:3px solid #eee">
                        <th>Claim Application</th>
                        <th width="40%" class="text-right">Approver</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Approver Level 1</td>
                        <td>
                            {{ Form::select('claim_approver_lvl_1', $top_employees, $employee->approvals()->type('claim')->level(1)->first() ? $employee->approvals()->type('claim')->level(1)->first()->approver_id : '' , ['class' => 'form-control', 'placeholder' => 'Select one']) }}
                        </td>
                    </tr>
                    <tr>
                        <td>Approver Level 2</td>
                        <td>
                            {{ Form::select('claim_approver_lvl_2', $top_employees, $employee->approvals()->type('claim')->level(2)->first() ? $employee->approvals()->type('claim')->level(2)->first()->approver_id : '' , ['class' => 'form-control', 'placeholder' => 'Select one']) }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr style="border-bottom:3px solid #eee">
                        <th>Leave In Lieu Application</th>
                        <th width="40%" class="text-right">Approver</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Approver Level 1</td>
                        <td>
                            {{ Form::select('lieu_approver_lvl_1', $top_employees, $employee->approvals()->type('lieu')->level(1)->first() ? $employee->approvals()->type('lieu')->level(1)->first()->approver_id : '' , ['class' => 'form-control', 'placeholder' => 'Select one']) }}
                        </td>
                    </tr>
                    <tr>
                        <td>Approver Level 2</td>
                        <td>
                            {{ Form::select('lieu_approver_lvl_2', $top_employees, $employee->approvals()->type('lieu')->level(2)->first() ? $employee->approvals()->type('lieu')->level(2)->first()->approver_id : '' , ['class' => 'form-control', 'placeholder' => 'Select one']) }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr style="border-bottom:3px solid #eee">
                        <th>Advanced Request</th>
                        <th width="40%" class="text-right">Approver</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Approver Level 1</td>
                        <td>
                            {{ Form::select('advanced_approver_lvl_1', $top_employees, $employee->approvals()->type('advanced')->level(1)->first() ? $employee->approvals()->type('advanced')->level(1)->first()->approver_id : '' , ['class' => 'form-control', 'placeholder' => 'Select one']) }}
                        </td>
                    </tr>
                    <tr>
                        <td>Approver Level 2</td>
                        <td>
                            {{ Form::select('advanced_approver_lvl_2', $top_employees, $employee->approvals()->type('advanced')->level(2)->first() ? $employee->approvals()->type('advanced')->level(2)->first()->approver_id : '' , ['class' => 'form-control', 'placeholder' => 'Select one']) }}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        {{Form::submit('Save',['class' =>'m-b-20 btn-lg btn-rounded btn-info text-uppercase'])}}
        {!! Form::close() !!}

    </div>

</div>


@section('js')
    @parent

    <script>

    $( "#employee_approval" ).on( "submit", function( event ) {
        event.preventDefault();

        swal({
        title: 'Saving',
        onOpen: () => {
            swal.showLoading()
            return new Promise(function(resolve, reject) {
                var form = document.getElementById('employee_approval');
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: '{{ route('approve.save', $employee->id) }}',
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        swal.close();
                    },
                    error: function(data) {
                        errormsg = '';
                        if(data.status == 422 ){
                            $.each(data.responseJSON.errors, function (key, value) {
                                errormsg = errormsg + value[0] + "<br />";
                            });
                        }else{
                            errormsg = "Server error. Please try again";
                        }
                        swal.showValidationMessage(errormsg);
                        swal.hideLoading();
                    },
                });
            });
        }})
    });
    </script>

@endsection
