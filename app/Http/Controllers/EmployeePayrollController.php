<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payroll;
// use Auth;
use App\Salary;
use App\Employee;
use App\Generate;
use App\PayrollCategory;
use DataTables;
use App\Operand;
use App\Organisation;
use DB;
use Auth;

class EmployeePayrollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all();
        $categories = PayrollCategory::all();
        $generates = Generate::where('employee_id',Auth::id());
        $operands = Operand::all();
        $salaries = Salary::where('basic_salary')->count();
        // return $operands;
        return view('employees.payslips.index', compact('generates','salaries','operands','employees','categories'));
    }
    public function index2($years, $month)
    {

        // return $years;
        $employees = Employee::all();
        $categories = PayrollCategory::all();
        $operands = Operand::all();
        // return $operands;
        return view('employees.payslips.list', compact('month','years','operands','employees','categories'));
    }

    public function list($years, $month)
    {
// $gen = Generate::where('employee_id', Auth::id());
        // return Datatables::eloquent(Generate::where('employee_id', Auth::id()))
        return Datatables::eloquent(
        Generate::where('employee_id', Auth::id())->where('year',$years)->where('month',$month)->where('status','generated'))
        ->addColumn('action', function($model) use ($years, $month){
            $url = 'empgenerates';
            $show_hide = true;
            $delete_hide = true;
            $edit_hide = true;
            $modal_view_id = 'modal_payroll_show';
            $modal_edit_id = 'modal_payroll_edit';

            //hide custom button if generate exist

            $custom_btn = [
                '<a class="btn btn-xs btn-default open-modal" data-modal_data=\''.$model.'\' href="'.route('generates.print',['years'=> $years, 'month'=> $month, 'id' => $model->employee_id]).'" data-toggle="modal">Print</a>',
            ];

            return view('shared._table_action', compact('model', 'url', 'edit_hide','show_hide','delete_hide','modal_view_id', 'modal_edit_id','custom_btn','years','month' ))->render();
        })
        ->addIndexColumn()
        ->make(true);
        }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function print($years, $month, $id)
    {
        $employees = Employee::find($id);
        $organisations = Organisation::all();
        $salaries = Salary::where('employee_id', $id)->first();
        try{
            $a = decrypt($salaries->basic_salary);
         }
         catch(\Exception $e){
             'N/A';
         }
        $generates = Generate::where('year',$years)->where('month',$month)->where('employee_id',$id)->get();//nak payroll category untuk refer ke table payroll_categories
        $categories = PayrollCategory::all();
        $operands = Operand::all();
        // $totals = Generate::where('employee_id',$id)->sum('value');
        $totals = Generate::where('employee_id',$id)->get();
        // $totaladd = Generate::where('operand_id','1')->sum('value');
        // $totalded = Generate::where('operand_id','2')->sum('value');

        $totaladd = Generate::where('month',$month)->where('employee_id',$id)->where('operand_id','1')->sum('value');
        $totalded = Generate::where('month',$month)->where('employee_id',$id)->where('operand_id','2')->sum('value');

        $earns = $a + $totaladd - $totalded;

        $tax = 0.11 * $a;
        $epf = 0.11 * $a;

        return View('employees.payslips.print',compact('a','epf','tax','earns','totalded','totaladd','totals','operands','employees','categories','salaries','generates','adds','deductions'));
    }
}
