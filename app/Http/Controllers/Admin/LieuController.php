<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Approval;
use App\EmployeeLieu;
use DataTables;
use Auth;
use Carbon\Carbon;

class LieuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emp_ids = Approval::where('approver_id', Auth::id())->type('lieu')->pluck('employee_id');

        return Datatables::eloquent(EmployeeLieu::with('employee')->whereIn('employee_id', $emp_ids)->select('employee_lieus.*'))
        ->addColumn('employee', function($model){
            return $model->employee->full_name;
        })
        ->addColumn('action', function($model){
            $url = 'lieus';
            $custom_permission = 'admin_lieus';
            $modal_view_id = 'lieus_modal_view';
            $edit_hide = true;
            $delete_hide = true;
            return view('shared._table_action', compact('model', 'url', 'modal_view_id', 'custom_permission', 'edit_hide', 'delete_hide'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    public function list()
    {
        return Datatables::eloquent(EmployeeLeave::with('employee'))
        ->addColumn('employee', function($model){
            return $model->username;
        })
        ->addColumn('action', function($model){
            $url = 'adminlieus';
            $custom_permission = 'admin_lieus';
            $modal_view_id = 'leaves_modal_view';
            $modal_edit_id = 'leaves_modal_edit';
            $delete_hide = true;
            $delete_datatable_id_name_refresh = 'lieu_table';
            $modal_data_customs = array('employee' => $model->employee);

            if($model->status == 'Pending'){
                $custom_btn = [
                    '<a class="btn btn-xs btn-danger" data-method="delete" data-confirm="confirm" href="'. route($url.'.destroy', $model->id) .'">Cancel</a>'
                ];
            }
            return view('shared._table_action', compact('model','url','delete_hide','custom_btn','custom_permission','modal_view_id','modal_edit_id' ))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    public function liew_update_status($id, $status){
        $liew = EmployeeLieu::find($id);
        $liew->approved_by = Auth::id();
        $liew->approved_at = Carbon::now();
        $liew->status = $status;
        $liew->save();
        return response()->json($liew, 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
