@can('view_'.$url)


<a class="btn btn-xs btn-success" href="claims/month/{{ $model->id }}">Show</a>


@endcan

@can('edit_'.$url)

<a class="btn btn-xs btn-warning" href="claims/month/{{ $model->id }}">Edit</a>

@endcan

@can('delete_'.$url)
<a class="btn btn-xs btn-danger" data-method="delete" data-confirm="confirm" href="{{ route($url.'.destroy', $model->id) }}">Delete</a>
@endcan
