{{-- Modal --}}
<div class="modal fade" id="edit_expense">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h1>Edit Expense</h1>

                {!! Form::open(['url' => '', 'id' => 'expenses_edit']) !!}
                <input type="hidden" name="_method" value="PUT">
                <input id="claim_id" name="claim_id" type="hidden" value="{{ $claim->id }}" />
                <input id="expenses_id" name="expenses_id" type="hidden" value="" />
                <div class="form-group row">
                    <div class="col-md-6">
                        <select id="item_type" name="item_type" class="selectpicker" data-style="form-control">
                            <option selected disabled>Expense Type</option>
                            @foreach ( $expenses_type as $t )
                            <option value="{{ $t->id }}">{{ $t->expense }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6">
                        <input id="project_code" name="project_code" type="text" class="form-control" placeholder="Project Code">
                    </div>
                </div>
                <div class="form-group">
                    <input id="item_name" name="item_name" type="text" class="form-control" placeholder="Item Name">
                </div>
                <div class="form-group">
                    <textarea id="description" name="description" rows="4" class="form-control" placeholder="Item Description..."></textarea>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        {{ Form::date('purchase_date',null,['class' => 'form-control', 'placeholder' => 'Item Date','id'=>'purchase_date'])}}
                    </div>
                    <div class="col-md-6">
                        <input id="amount" name="amount" type="float" class="form-control" placeholder="Amount">
                        <input type="hidden" id="amount_old" name="amount_old" type="float" class="form-control" value="" placeholder="Amount">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        <input id="receipt_no" name="receipt_no" type="text" class="form-control" placeholder="Receipt No.">
                    </div>
                    <div class="col-md-6">
                        <input id="reference_no" name="reference_no" type="number" class="form-control" placeholder="Reference No.">
                    </div>
                </div>

                <button type="submit" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Save
                    Expense
                </button>
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close
                </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('js')
@parent
<script>
    $("#expenses_edit").on("submit", function (event) {
        console.log('mew');
        event.preventDefault();
        swal({
            title: "Update",
            text: "Are you sure you want to update?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function(resolve, reject){
                    var form = document.getElementById('expenses_edit');
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url: '/expenses/' + form.expenses_id.value,
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            resolve(data);
                        },
                        error: function(){
                            swal.showValidationMessage('server error');
                            swal.hideLoading();
                        }
                    });
                });
            }
        }).then((result) => {

            if (result.value) {
                $('#edit_expense').modal('toggle');
                location.reload(true);
                //$('#expenses_table').DataTable().ajax.reload();
                //swal('Updated!', '--', 'success');
                //$('#amount_view').text(result.value.claim_total);
            }

        });
    });
</script>
@endsection

