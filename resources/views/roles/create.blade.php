@extends('layouts.default.master')

@section('content')
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Role</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                <ol class="breadcrumb">
                    <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li><a href="{{ route('roles.index') }}">Role</a></li>
                    <li class="active">Create Role</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>


        {{-- Role List --}}
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title">Create Role</h3>

                    <div class="row">
                        <div class="col-sm-12">
                        {!! Form::open(['url' => route('roles.store')]) !!}

                        <div class="form-group">
                            <label>Name</label>
                            {{ Form::text('name', null, ['class' => 'form-control']) }}
                            @if ($errors->has('name'))
                                <div class="text-danger">{{ $errors->first('name') }}</div>
                            @endif
                        </div>

                        <div class="form-group form-check">
                            <label>Permission</label>
                            <br />
                            @php
                                $i = 0;
                            @endphp
                            @foreach ($permissions as $permission)
                                {{ Form::checkbox('permissions[]',  $permission->id ) }}
                                {{ Form::label($permission->name, ucwords(str_replace('_', ' ', $permission->name))) }}
                                <br />
                                @php
                                    $i++;
                                @endphp
                                @if ($i > 3)
                                    <br />
                                    @php
                                        $i = 0;
                                    @endphp
                                @endif
                            @endforeach
                        </div>
                        {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}

                        {!! Form::close() !!}
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
</div>

@endsection





