<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> <span class="hide-menu">Navigation</span></h3>
        </div>
        <ul class="nav" id="side-menu">
            <li class="user-pro">
                <a href="#" class="waves-effect"><img src="{{ URL::asset('plugins/images/users/varun.jpg') }}" alt="user-img" class="img-circle"> <span class="hide-menu"> Steve Gection<span class="fa arrow"></span></span>
                </a>
                <ul class="nav nav-second-level collapse" aria-expanded="false" style="height: 0px;">
                    <li><a href="javascript:void(0)"><i class="ti-user"></i> <span class="hide-menu">My Profile</span></a></li>
                    <li><a href="javascript:void(0)"><a href="/logout"><i class="fa fa-power-off"></i> <span class="hide-menu" >Logout</span></a></li>
                </ul>
            </li>
            <li><a href="{{ url('/organisations') }}" class="waves-effect"><i class="fa fa-university fa-fw"></i> <span class="hide-menu">Organisation</span></a></li>
            <li><a href="{{ url('/roles') }}" class="waves-effect"><i class="fa fa-university fa-fw"></i> <span class="hide-menu">Roles</span></a></li>
            <li><a href="{{ url('/adminemployees') }}" class="waves-effect"><i class="fa fa-users fa-fw"></i> <span class="hide-menu">Employee</span></a></li>
            <li><a href="{{ url('/adminleaves') }}" class="waves-effect"><i class="mdi mdi-calendar fa-fw"></i> <span class="hide-menu">Leaves</span></a></li>
            <li><a href="{{ url('/adminclaims') }}" class="waves-effect"><i class="mdi mdi-coin fa-fw"></i> <span class="hide-menu">Claims</span></a></li>
            <li><a href="{{ url('/holiday') }}" class="waves-effect"><i class="mdi mdi-calendar fa-fw"></i> <span class="hide-menu">Holiday</span></a></li>
            <li><a href="{{ url('/adminnotices') }}" class="waves-effect"><i class="mdi mdi-calendar fa-fw"></i> <span class="hide-menu">Holiday</span></a></li>
            <li><a href="{{ url('/adminsettings') }}" class="waves-effect"><i class="mdi mdi-settings fa-fw"></i> <span class="hide-menu">Settings</span></a></li>
        </ul>
    </div>
</div>
