<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Generate extends Model
{
    // protected $primaryKey = 'id';
    public $table = "generates";

    protected $fillable = ['employee_id','category_id','operand_id','value','year','month','status'];

    public function paycategory()
    {
        return $this->belongsTo(PayrollCategory::class,'category_id','id');
    }

    public function operand()
    {
        return $this->hasMany(Operand::class,'operand_id');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function salary()
    {
        return $this->belongsTo(Salary::class, 'employee_id');
    }

    // public function addition($query){
    //     $query = Operand::where('operand','1');
    // }
}
