<section id="approver">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3 class="box-title">Approval Layer</h3>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr style="border-bottom:3px solid #eee">
                            <th>Leave Application</th>
                            <th width="40%" class="text-right">Approver</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Approver Level 1</td>
                            <td>
                                <select class="selectpicker" data-style="form-control">
                                    <option selected disabled>Employee Name</option>
                                    <option>Employee 1</option>
                                    <option>Employee 2</option>
                                    <option>Employee 3</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Approver Level 2</td>
                            <td>
                                <select class="selectpicker" data-style="form-control">
                                    <option selected disabled>Employee Name</option>
                                    <option>Employee 1</option>
                                    <option>Employee 2</option>
                                    <option>Employee 3</option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr style="border-bottom:3px solid #eee">
                            <th>Claim Application</th>
                            <th width="40%" class="text-right">Approver</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Approver Level 1</td>
                            <td>
                                <select class="selectpicker" data-style="form-control">
                                    <option selected disabled>Employee Name</option>
                                    <option>Employee 1</option>
                                    <option>Employee 2</option>
                                    <option>Employee 3</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Approver Level 2</td>
                            <td>
                                <select class="selectpicker" data-style="form-control">
                                    <option selected disabled>Employee Name</option>
                                    <option>Employee 1</option>
                                    <option>Employee 2</option>
                                    <option>Employee 3</option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr style="border-bottom:3px solid #eee">
                            <th>Leave In Lieu Application</th>
                            <th width="40%" class="text-right">Approver</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Approver Level 1</td>
                            <td>
                                <select class="selectpicker" data-style="form-control">
                                    <option selected disabled>Employee Name</option>
                                    <option>Employee 1</option>
                                    <option>Employee 2</option>
                                    <option>Employee 3</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Approver Level 2</td>
                            <td>
                                <select class="selectpicker" data-style="form-control">
                                    <option selected disabled>Employee Name</option>
                                    <option>Employee 1</option>
                                    <option>Employee 2</option>
                                    <option>Employee 3</option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr style="border-bottom:3px solid #eee">
                            <th>Advanced Request</th>
                            <th width="40%" class="text-right">Approver</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Approver Level 1</td>
                            <td>
                                <select class="selectpicker" data-style="form-control">
                                    <option selected disabled>Employee Name</option>
                                    <option>Employee 1</option>
                                    <option>Employee 2</option>
                                    <option>Employee 3</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Approver Level 2</td>
                            <td>
                                <select class="selectpicker" data-style="form-control">
                                    <option selected disabled>Employee Name</option>
                                    <option>Employee 1</option>
                                    <option>Employee 2</option>
                                    <option>Employee 3</option>
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <button style="width:200px" class="m-b-20 btn-lg btn-rounded btn-info text-uppercase">Save</button>
        </div>

    </div>
</section>
