<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\LeaveType;
use App\Annual;
use App\EmployeeLeave;
use Auth;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;

class CompassionateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $employee = Employee::lists(['id']);
        $leavetype = LeaveType::lists(['id']);
        return view('employees.leaves.compassionates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        $compassionate_leave_quota = Auth::user()->leave_allocs()->type('Compassionate Leave')->first()->quota;
        $compassionate_leave_taken = Auth::user()->leaves()->type('Compassionate Leave')->sum('total_days');
        $balance = $compassionate_leave_quota - $compassionate_leave_taken;


        $a = Carbon::parse($request->started_at);
        $b = Carbon::parse($request->ended_at);
        $days = $b->diffInDays($a);
        $days = $days + 1;

        $unpaid = $balance - $days;

        $comp = new EmployeeLeave;
        $comp->employee_id = Auth::user()->id;
        $comp->leave_type = 'Compassionate Leave';
        $comp->started_at = $request->started_at;
        $comp->ended_at = $request->ended_at;
        $comp->reason = $request->reason;
        $comp->total_days = $days;
        $comp->as_unpaid = $unpaid;
        $comp->leave_proc_status = 'unproc';
        // $emergency->as_lieu = $request->as_lieu;
        // $emergency->remark = $request->remark;
        $comp->status = 'Pending';
        $comp->leave_proc_status = 'unproc';

        if ($request->hasFile('file')) {
            $comp->addMediaFromRequest('file')->toMediaCollection('leaves');
        }

        $comp->save();
        $leave = $comp->id;
        $sendResult = $client->sendEmailWithTemplate(

            "hassanul@boneybone.com",
            $approval_lv1->approvedBy->email,
            10030123,
                [
                    "system_name" => "[IX HRMS] Leave Application Approval",
                    "fname" => $employees->fname,
                    "lname" => $employees->lname,
                    "uid" => $employees->employee_uid,
                    "approve" =>  $approval_lv1->approvedBy->fname,
                    "approver" =>  $approval_lv1->approvedBy->lname,
                    "leave_type" => 'Compassionate Leave',
                    "started_at" => $request->started_at,
                    "ended_at" => $request->ended_at,
                    "reason" => $request->reason,
                    "accept" => route('adminleaves.update_email',$leave),
                    "reject" => route('adminleaves.reject_email',$leave),
                    "link" => url('http://hrms.ixtelecom.net')
                ]);

            $sendResult = $client->sendEmailWithTemplate(

                    "hassanul@boneybone.com",
                    $approval_lv2->approvedBy->email,
                    10030123,
                        [
                            "system_name" => "[IX HRMS] Leave Application Approval",
                            "fname" => $employees->fname,
                            "lname" => $employees->lname,
                            "uid" => $employees->employee_uid,
                            "approve" =>  $approval_lv2->approvedBy->fname,
                            "approver" =>  $approval_lv2->approvedBy->lname,
                            "leave_type" => 'Compassionate Leave',
                            "started_at" => $request->started_at,
                            "ended_at" => $request->ended_at,
                            "reason" => $request->reason,
                            "accept" => route('adminleaves.approve_email',$leave),
                            "reject" => route('adminleaves.reject_email',$leave),
                            "link" => url('http://hrms.ixtelecom.net')
                        ]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeLeave $employeeLeave, $id)
    {
        $employeeLeave = EmployeeLeave::find($id);
        $employeeLeave->delete();
        return redirect()->back();
    }
}
