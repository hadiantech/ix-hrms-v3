<?php

namespace App\Http\Controllers;

use App\Organisation;
use App\Department;
use App\Designation;
use App\Hierarchy;
use Illuminate\Http\Request;
use DataTables;

class OrganisationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $designations = Designation::all();
        $hierarchies = Hierarchy::all();
        $departments = Department::all();

        return view('admins.organisations.index',compact('designations','hierarchies', 'departments'));
    }

    public function list(){
        return Datatables::eloquent(Organisation::query())
        ->addColumn('action', function($model){
            $url = 'organisations';
            $show_hide = true;
            $modal_view_id = 'modal_organisations_show';
            $modal_edit_id = 'modal_organisations_edit';
            if($model->getMedia('logo')->first()){
                $modal_data_customs = array('logo' => $model->getMedia('logo')->first()->getUrl());
            }
            return view('shared._table_action', compact('model', 'url', 'show_hide', 'modal_view_id', 'modal_edit_id', 'modal_data_customs'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.organisations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'company_name' => 'bail|required',
            'reg_num' => 'bail|required',
            'address1' => 'bail|required',
            'address2' => 'bail|required',
            'state' => 'bail|required',
            'country' => 'bail|required',
            'website' => 'bail|required',
            'fb' => 'bail|required',
            'linkedin' => 'bail|required',
            'other' => 'bail|required',
            'number' => 'bail|required',
            'fax' => 'bail|required',
            'business' => 'bail|required',
            'tax' => 'bail|required',

        ]);

        // $organisation = Organisation::create($request->only('company_name','reg_num'));

        $organisation = new Organisation;
        $organisation->company_name = $request->company_name;
        $organisation->reg_num = $request->reg_num;
        $organisation->address1 = $request->address1;
        $organisation->address2 = $request->address2;
        $organisation->city = $request->city;
        $organisation->state = $request->state;
        $organisation->country = $request->country;
        $organisation->number = $request->number;
        $organisation->fax = $request->fax;
        $organisation->website = $request->website;
        $organisation->fb = $request->fb;
        $organisation->linkedin = $request->linkedin;
        $organisation->other = $request->other;
        $organisation->business = $request->business;
        $organisation->natureoforg = $request->status;
        $organisation->tax = $request->tax;

        $organisation->save();

        if ($request->hasFile('file')) {
            $organisation->addMediaFromRequest('file')->toMediaCollection('logo');
        }

        return response()->json($organisation, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $designations = Designation::find($id);
        // $departments = Department::find($id);
        // $organisations = Organisation::find($id);
        // return view('admins.organisations.show',compact('organisations','designations','departments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $organisation = Organisation::find($id);
        $organisation->update($request->all());

        if ($request->hasFile('file')) {
            $organisation->clearMediaCollection('logo');
            $organisation->addMediaFromRequest('file')->toMediaCollection('logo');
        }

        return response()->json($organisation, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $organisation = Organisation::find($id);
        $organisation->delete();
        return redirect()->back();
    }
}
