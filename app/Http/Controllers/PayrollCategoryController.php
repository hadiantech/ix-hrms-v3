<?php

namespace App\Http\Controllers;
use App\Payroll;
use App\Salary;
use App\Employee;
use App\PayrollCategory;
use App\Operand;
use DataTables;
use Illuminate\Http\Request;
use Auth;

class PayrollCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all();
        $categories = PayrollCategory::all();
        $salaries = Salary::all();
        $operands = Operand::all();
        return view('payslips.index', compact('employees','categories','salaries','operands'));
    }

    public function list(){
        return Datatables::eloquent(PayrollCategory::with('operand'))
        ->addColumn('operand', function($model){
            return $model->operand->operand_name;
        })
        ->addColumn('action', function($model){
            $url = 'payslips';

            $modal_view_id = 'modal_payroll_show';
            $modal_edit_id = 'modal_payroll_edit';
            $delete_datatable_id_name_refresh = 'payroll_table';
            return view('shared._table_action', compact('model', 'url', 'modal_view_id', 'modal_edit_id'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    public function list_based_employee($id){
        return Salary::where('employee_id', $id)->get();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $operands = Operand::lists(['operand_name']);
        return view('payslips.create', compact('operands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $add = new PayrollCategory;
        $add->name = $request->name;
        $add->operand_id = $request->operand_id;

        $add->save();
        return response()->json($add, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PayrollCategory  $payrollCategory
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $categories = PayrollCategory::where('id', $id)->first();
        return view('payslips.show',compact('payslips'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PayrollCategory  $payrollCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(PayrollCategory $payrollCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PayrollCategory  $payrollCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = PayrollCategory::find($id);
        $category->update($request->all());

        return response()->json($category, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PayrollCategory  $payrollCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = PayrollCategory::find($id);
        $category->delete();
        return redirect()->back();
    }

    public function print($id)
    {
        return view('payslips.print');
    }
}
