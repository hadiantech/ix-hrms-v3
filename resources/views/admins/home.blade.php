@extends('layouts.default.master')
@section('content')

<div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Admin Dashboard</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                <ol class="breadcrumb hidden">
                    <li><a href="dashboard.html">Dashboard</a></li>
                    <li class="active">Dashboard 1</li>
                </ol>
            </div>
                    <!-- /.col-lg-12 -->
        </div>
<!-- /.row -->
<!-- ============================================================== -->
<!-- Different data widgets -->
<!-- ============================================================== -->

<div class="row">
    {{-- claim --}}
    <div class="col-md-4 col-sm-12">
            <div class="white-box" style="background: #707cd2">
                <ul class="col-in">
                    <li>
                        <span class="circle circle-md bg-white"><i style="color:#ff7676" class="ti-money"></i></span>
                    </li>
                    <li class="col-last">
                        <h3 class="counter text-right m-t-15 white">{{ $claims }}</h3>
                    </li>
                    <li class="col-middle" style="width:40%">
                        <h4 class="white">Monthly Claims</h4>
                    </li>
                </ul>
            </div>
        </div>
    {{-- leave --}}
    <div class="col-md-4 col-sm-12">
            <div class="white-box" style="background: #707cd2">
                <ul class="col-in">
                    <li>
                        <span class="circle circle-md bg-white"><i style="color:#ff7676" class="ti-calendar"></i></span>
                    </li>
                    <li class="col-last">
                        <h3 class="counter text-right m-t-15 white">{{ $leaves }}</h3>
                    </li>
                    <li class="col-middle" style="width:40%">
                        <h4 class="white">Leave Application</h4>
                    </li>
                </ul>
            </div>
        </div>
        {{-- user notification --}}
    <div class="col-md-4 col-sm-12">
        <div class="white-box" style="background: #707cd2">
            <ul class="col-in">
                <li>
                    <span class="circle circle-md bg-white"><i style="color:#ff7676" class="ti-user"></i></span>
                </li>
                <li class="col-last">
                    <h3 class="counter text-right m-t-15 white">{{ $employees }}</h3>
                </li>
                <li class="col-middle" style="width:40%">
                    <h4 class="white">Need Review</h4>
                </li>
            </ul>
        </div>
    </div>


{{-- leave summary --}}
<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">Leave Summary</h3>
            <!-- Nav tabs -->

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="unprocessed">
                    <div class="table-responsive">
                        <table id="leave_unproc" class="table display">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Employee</th>
                                    <th>Reason</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Status</th>
                                    <th class="text-center">Created at</th>

                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
{{-- leave summary --}}


{{-- claim summary --}}

<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">Employee Claim Summary</h3>
            <!-- Nav tabs -->

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="unprocessed">
                    <div class="table-responsive">
                        <table id="claim_unproc" class="table display">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Employee</th>
                                    <th class="text-center">Claim Desc</th>
                                    <th class="text-center">Applied Date</th>
                                    <th class="text-center">Amount</th>
                                    <th class="text-center">Status</th>

                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

{{-- claim summary --}}

{{-- probation summary --}}

<div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">Employee Probation Summary</h3>
            <!-- Nav tabs -->

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade active in" id="unprocessed">
                    <div class="table-responsive">
                        <table id="probation_table" class="table display">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Employee</th>
                                    <th class="text-center">Employee UID</th>
                                    <th class="text-center">Probation Status</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Status</th>

                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

{{-- probation summary --}}





@endsection
@section('js')
    @parent
    <script>
            $(function () {
                $('#leave_unproc').DataTable({
                    serverSide: true,
                    processing: true,
                    order: [[6,'desc']],
                    ajax: '/dashboard/adminleaves/list/unproc',
                    columns: [{
                            data: 'DT_Row_Index',
                            orderable: false,
                            searchable: false,
                            class: 'text-left'
                        },
                        {data: 'employee'},
                        {data: 'reason'},
                        {data: 'started_at'},
                        {data: 'ended_at'},
                        {
                            data: 'status',
                            name: 'status',
                            class: 'text-center'
                        },
                        {data: 'created_at'},

                    ]
                });
            });
        </script>
         <script>
                    $(function () {
                        $('#claim_unproc').DataTable({
                            serverSide: true,
                            processing: true,
                            ajax: '/dashboard/adminclaims/list/unproc',
                            columns: [{
                                    data: 'DT_Row_Index',
                                    orderable: false,
                                    searchable: false,
                                    class: 'text-left'
                                },
                                {
                                    data: 'employee'
                                },
                                {
                                    data: 'desc',
                                    class: 'text-center'
                                },
                                {
                                    data: 'claim_applied_date',
                                    class: 'text-center'
                                },
                                {
                                    data: 'claim_total',
                                    class: 'text-center'
                                },
                                {
                                    data: 'status',
                                    name: 'claim_status',
                                    class: 'text-center'
                                },

                            ]
                        });
                    });
                </script>
                <script>
                    $(function () {
                        $('#probation_table').DataTable({
                            serverSide: true,
                            processing: true,
                            ajax: '/dashboard/employees/list',
                            columns: [{
                                data: 'DT_Row_Index',
                                orderable: false,
                                searchable: false,
                                class: 'text-left'
                            },
                            {
                                data: 'fname'
                            },
                            {
                                data: 'employee_uid',
                                class: 'text-center'
                            },
                            {
                                data: 'months',
                                class: 'text-center'
                            },
                            {
                                data: 'status',
                                class: 'text-center'
                            },
                            {
                                data: 'status',
                                class: 'text-center'
                            },

                            ]
                        });
                    });
                </script>
                @endsection
