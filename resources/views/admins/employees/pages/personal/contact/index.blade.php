<div class="family-info  m-b-30">
    <div class="section-title">
        <h3 class="box-title">Contacts: Spouse, Children & Others</h3>
    </div>

    <a href="#add-contact" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Contact</a>
    <br />
    <br />

    <div class="table-responsive">
        <table id="contact_table" class="table">
            <thead>
                <tr style="border-bottom:3px solid #eee">
                    <th>#</th>
                    <th>Name</th>
                    <th class="text-center">MyKAD/Passport No.</th>
                    <th class="text-center">Relation</th>
                    <th class="text-center">DOB</th>
                    <th class="text-right">Action</th>
                </tr>
            </thead>
        </table>
    </div>

</div>
@include('admins.employees.pages.personal.contact.create')
@include('admins.employees.pages.personal.contact.edit')


@section('js')
    @parent

    <script>
        $(function () {
            $('#contact_table').DataTable({
                serverSide: true,
                processing: true,
                ajax: '{{ route("employees.contacts.index", $employee->id) }}',
                columns: [
                    {data: 'DT_Row_Index', orderable: false, searchable: false},
                    {data: 'name'},
                    {data: 'ic'},
                    {data: 'relation'},
                    {data: 'dob'},
                    {data: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@endsection
