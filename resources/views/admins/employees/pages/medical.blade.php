<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <h3 class="box-title">Medical Info</h3>

        <a href="#add-medical" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Medical Info</a>
        <br />
        <br />
        <div class="table-responsive">
            <table id="medical_table" class="table">
                <thead>
                    <tr style="border-bottom:3px solid #eee">
                        <th>#</th>
                        <th>Insurance Name</th>
                        <th class="text-center">Policy No.</th>
                        <th class="text-center">Start Date</th>
                        <th class="text-center">Expiry Date</th>
                        <th class="text-center">Value ({{ settings('currency_code') }})</th>
                        <th class="text-right">Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>


{{-- modal --}}
<div class="modal fade" id="add-medical" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Add Medical Info</h1>

                    {!! Form::open(['url' => '', 'id' => 'medical_create']) !!}
                    <div class="form-group row">
                        <div class="col-md-6">
                            {!! Form::label('Insurance Name') !!}
                            {!! Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Insurance Name', 'required')) !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('Policy No.') !!}
                            {!! Form::text('policy_no', null, array('class' => 'form-control', 'placeholder' => 'Policy No.', 'required')) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            {!! Form::label('Start Date') !!}
                            {!! Form::date('started_at', null, array('class' => 'form-control', 'placeholder' => 'Started Date', 'required')) !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('Expiry Date') !!}
                            {!! Form::date('expired_at', null, array('class' => 'form-control', 'placeholder' => 'Expiry Date', 'required')) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            {!! Form::label('Value ('.settings('currency_code').')') !!}
                            {!! Form::text('value', null, array('class' => 'form-control', 'placeholder' => 'Value ('.settings('currency_code').')', 'required')) !!}
                        </div>
                    </div>

                    {{Form::submit('Add Medical',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                    <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                    {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_medical_edit" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    <h1>Edit Medical Info</h1>

                        {!! Form::open(['url' => '', 'id' => 'medical_edit']) !!}
                        {{ Form::hidden('employee_id', null) }}
                        {{ Form::hidden('id', null) }}
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group row">
                            <div class="col-md-6">
                                {!! Form::label('Insurance Name') !!}
                                {!! Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Insurance Name', 'required')) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('Policy No.') !!}
                                {!! Form::text('policy_no', null, array('class' => 'form-control', 'placeholder' => 'Policy No.', 'required')) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                {!! Form::label('Start Date') !!}
                                {!! Form::date('started_at', null, array('class' => 'form-control', 'placeholder' => 'Started Date', 'required')) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('Expiry Date') !!}
                                {!! Form::date('expired_at', null, array('class' => 'form-control', 'placeholder' => 'Expiry Date', 'required')) !!}
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                {!! Form::label('Value ('.settings('currency_code').')') !!}
                                {!! Form::text('value', null, array('class' => 'form-control', 'placeholder' => 'Value ('.settings('currency_code').')', 'required')) !!}
                            </div>
                        </div>

                        {{Form::submit('Add Medical',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                        <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@section('js')
    @parent

    <script>
        $(function () {
            $('#medical_table').DataTable({
                serverSide: true,
                processing: true,
                ajax: '{{ route("employees.medicals.index", $employee->id) }}',
                columns: [
                    {data: 'DT_Row_Index', orderable: false, searchable: false},
                    {data: 'name'},
                    {data: 'policy_no'},
                    {data: 'started_at'},
                    {data: 'expired_at'},
                    {data: 'value'},
                    {data: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>

    <script>
        $('#add-medical').on('hidden.bs.modal', function(e) {
            $(this).find('#medical_create')[0].reset();
        });

        $( "#medical_create" ).on( "submit", function( event ) {
            event.preventDefault();
            swal({
                title: "Submit Medical",
                text: "Are you sure you want to submit this medical info?",
                type: "info",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                allowOutsideClick: () => !swal.isLoading(),
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        var form = document.getElementById('medical_create');
                        var formData = new FormData(form);
                        $.ajax({
                            type: "POST",
                            url: '{{ route('employees.medicals.store', $employee->id) }}',
                            data: formData,
                            cache:false,
                            contentType: false,
                            processData: false,
                            success: function(data) {
                                resolve(data);
                            },
                            error: function(data) {
                                errormsg = '';
                                if(data.status == 422 ){
                                    $.each(data.responseJSON.errors, function (key, value) {
                                        errormsg = errormsg + value[0] + "<br />";
                                    });
                                }else{
                                    console.log(data);
                                    errormsg = "Server error. Please try again";
                                }
                                swal.showValidationMessage(errormsg);
                                swal.hideLoading();
                            },
                        });
                    });

                }
            }).then((result) => {
                if(result.value){
                    $('#add-medical').modal('toggle');
                    swal('Medical Info Saved!', '--', 'success');
                    $('#medical_table').DataTable().ajax.reload();
                }
            });
        });


    </script>


    <script>
        $( "#medical_edit" ).on( "submit", function( event ) {
            event.preventDefault();
            swal({
                title: "Edit Medical Info",
                text: "Are you sure you want to edit this medical info?",
                type: "info",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                allowOutsideClick: () => !swal.isLoading(),
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        var form = document.getElementById('medical_edit');
                        var formData = new FormData(form);
                        $.ajax({
                            type: "POST",
                            url: '/employees/'+form.employee_id.value+'/medicals/'+form.id.value,
                            data: formData,
                            cache:false,
                            contentType: false,
                            processData: false,
                            success: function(data) {
                                resolve(data);
                            },
                            error: function(data) {
                                errormsg = '';
                                if(data.status == 422 ){
                                    $.each(data.responseJSON.errors, function (key, value) {
                                        errormsg = errormsg + value[0] + "<br />";
                                    });
                                }else{
                                    console.log(data);
                                    errormsg = "Server error. Please try again";
                                }
                                swal.showValidationMessage(errormsg);
                                swal.hideLoading();
                            },
                        });
                    });

                }
            }).then((result) => {
                if(result.value){
                    $('#modal_medical_edit').modal('toggle');
                    swal('Medical Info Updated!', '--', 'success');
                    $('#medical_table').DataTable().ajax.reload();
                }
            });
        });

    </script>

@endsection
