<?php

namespace App\Http\Controllers;

use App\AssetBrand;
use App\AssetType;
use DataTables;
use Illuminate\Http\Request;

class AssetBrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function list(){
        return Datatables::eloquent(AssetBrand::with('type'))
        ->addColumn('type', function($model){
            return $model->type->type_name;
        })
        ->addColumn('action', function($model){
            $url = 'brands';
            $modal_view_id = 'modal_brand_show';
            $modal_edit_id = 'modal_brand_edit';
            $modal_data_customs = array('type' => $model->type);
            $delete_datatable_id_name_refresh = 'brand_table';
            return view('shared._table_action', compact('model', 'url', 'modal_view_id', 'modal_edit_id', 'delete_datatable_id_name_refresh', 'modal_data_customs'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    public function list_based_type($id){
        return AssetBrand::where('brand_asset', $id)->get();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = AssetType::lists(['brand_asset']);
        return view('assets.brands.create', compact('types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $brands = AssetBrand::create($request->only('brand_name','brand_asset','status'));

        $brands->save();
        return response()->json($brands, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AssetBrand  $assetBrand
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $brands = AssetBrand::where('id', $id)->first();
        $types = AssetType::find($id);
        return view('assets.brands.show',compact('brands','types'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AssetBrand  $assetBrand
     * @return \Illuminate\Http\Response
     */
    public function edit(AssetBrand $assetBrand)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AssetBrand  $assetBrand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $brand = AssetBrand::find($id);
        $brand->update($request->all());

        return response()->json($brand, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AssetBrand  $assetBrand
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssetBrand $brand)
    {
        $brand->delete();
        return redirect()->back();
    }
}
