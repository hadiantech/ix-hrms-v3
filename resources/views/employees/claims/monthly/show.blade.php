@extends('layouts.default.master')

@section('content')

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Claim</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li><a href="{{ route('claims.index') }}">Claim Dashboard</a></li>
                    <li class="active">Claim</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title text-uppercase">Claim Details

                        @if($claim->claim_proc_status == 'return')
                        <p style="text-align: right;margin-top: -30px;margin-bottom: 30px;"><span class="label label-danger">Returned</span></p>
                        @else
                            @if($claim->claim_status == 'draft')
                            <p  style="text-align: right;margin-top: -30px;margin-bottom: 30px;"><span class="label label-warning">{{ $claim->claim_status }}</span></p>
                            @elseif($claim->claim_status == 'submit')
                            <p><span class="label label-success">Submitted</span></p>
                            @endif
                        @endif

                        @if($claim->claim_status == 'draft' or $claim->claim_status == 'rejected' or $claim->claim_proc_status == 'return')
                        <a href="#add-expense" data-toggle="modal" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Add
                            Expenses
                        </a>
                        @endif
                    </h3>
                   

                    {!! Form::open(['url' => '', 'id' => 'claim_update']) !!}
                    <input type="hidden" name="_method" value="PUT">
                    <input id="claim_id" name="claim_id" type="hidden" value="" />

                    <div class="form-group row m-t-20">
                        @if($claim->claim_status == 'draft' or $claim->claim_status == 'rejected')
                        <div class="col-md-3">
                            <select name="claim_month" class="selectpicker" data-style="form-control">
                                @foreach ($months as $index => $m)
                                <option value="{{ $m }}">{{ \Carbon\Carbon::createFromDate(null, $m, null)->format('M')}}</option>
                                @endforeach
                            </select>
                        </div>
                        @endif
                    </div>
                    <!-- <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        Sorry, claim submission for May 2018 has ended. You are unable to proceed with the claim.
                    </div> -->
                    @if($claim->claim_remark)
                    <div class="alert alert-info">
                        REMARK: {{ $claim->claim_remark }} <br/>
                    </div>
                    @endif

                    <div class="table-responsive">
                        <table id="expenses_table" class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Type</th>
                                    <th>Item Name & Desc</th>
                                    <th class="text-center">Project Code</th>
                                    <th class="text-center">Amount (RM)</th>
                                    <th class="text-center">Item Date</th>
                                    <th class="text-center">Receipt No.</th>
                                    <th class="text-center">Ref No.</th>
                                    <th class="text-right">Action</th>
                                </tr>
                            </thead>
                            <tfoot class="claim-amount">
                                <tr>
                                    <th colspan="6"></th>
                                    <th class="text-right">TOTAL AMOUNT</th>
                                    <th colspan="2" class="text-right">
                                        <input type="hidden" id="amount" name="amount" value="" />
                                        <span class="amount">RM</span><span id="amount_view" class="amount">{{ $claim->claim_total }}</span>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    @if($claim->claim_status == 'draft' or $claim->claim_proc_status == 'return')
                    <button id="save_draft" onclick="claimstatus('draft')" value="save_draft" type="submit" style="width:200px" class="m-r-10 btn-lg btn-rounded btn-info text-uppercase">Save Draft</button>
                    <button id="save_submit" onclick="claimstatus('submit')" value="save_submit" type="submit" style="width:200px" class=" btn-lg btn-rounded btn-success text-uppercase">Confirm & Submit</button>
                    @endif
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@include('employees.claims.monthly.create');
@include('employees.claims.monthly.edit');

@endsection
@section('js')
@parent

<script>
    $(function () {
        $('#expenses_table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/expenses/list/{{ $claim->id }}',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false, class: 'text-center'},
                {data: 'expense_type.expense', name: 'item_type', class: 'text-left'},
                {data: 'item_name_desc', name: 'item_name', class: 'text-left'},
                {data: 'project_code', class: 'text-center'},
                {data: 'amount', class: 'text-center'},
                {data: 'purchase_date', class: 'text-center'},
                {data: 'receipt_no', class: 'text-center'},
                {data: 'reference_no', class: 'text-center'},
                {data: 'action', orderable: false, searchable: false, class: 'text-center'}
            ]
        });
    });
</script>
<script>
    $( "#expenses_create" ).on( "submit", function( event ) {
        event.preventDefault();
        swal({
            title: "Confirm submission",
            text: "Are you sure you want to submit?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                return new Promise(function(resolve, reject){
                    var form = document.getElementById('expenses_create');
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url: "{{ route('expenses.store') }}",
                        data: formData,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            resolve(data);
                        },
                        error: function(){
                            swal.showValidationMessage('server error');
                            swal.hideLoading();
                        }
                    });
                });
            }
        }).then((result) => {
            if(result.value){
                $('#add-expense').modal('toggle');
                $('#expenses_table').DataTable().ajax.reload();
                swal('Add Success!', '--', 'success');

                //refresh total value kat bawah
                $('#amount_view').text(result.value.claim_total);
            }
        });
    });
</script>
<script>
    $(document).off('click', '.open-modal').on("click", ".open-modal", function () {
        //console.log('run');

        var data = $(this).data('modal_data');

        $('#edit_expense').on('shown.bs.modal', function(e){
            $(".modal-body #claim_id").val(data.claim_id);
            $(".modal-body #item_type").val(data.item_type).change();
            $(".modal-body #project_code").val(data.project_code);
            $(".modal-body #item_name").val(data.item_name);
            $(".modal-body #description").val(data.description);
            $(".modal-body #purchase_date").val(data.purchase_date);
            $(".modal-body #amount").val(data.amount);
            $(".modal-body #amount_old").val(data.amount);
            $(".modal-body #receipt_no").val(data.receipt_no);
            $(".modal-body #reference_no").val(data.reference_no);
            $(".modal-body #expenses_id").val(data.id);
            $(this).off('shown.bs.modal');
        });

        $('#edit_expense').on('hide.bs.modal', function(e){
            $('#expenses_create').trigger("reset");
            $(this).off('hide.bs.modal');
        });
    });
</script>
<script>

    var status;

    function claimstatus(e){
        status = e;

    }

    $("#claim_update").on("submit", function (event) {

        event.preventDefault();
        swal({
            title: "Update",
            text: "Are you sure you want to update?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                return new Promise(function(resolve, reject){
                    var form = document.getElementById('claim_update');
                    var formData = new FormData(form);
                    formData.append('claim_status', status);
                    formData.append('claim_proc_status', 'unproc')
                    $.ajax({
                        type: "POST",
                        url: "{{ route('claims.update',$claim->id) }}",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            resolve(data);
                        },
                        error: function(){
                            swal.showValidationMessage('server error');
                            swal.hideLoading();
                        }
                    });
                });
            }
        }).then((result) => {
            if(result.value){
                //  swal('Updated!', '--', 'success');
                location.reload(true);
            }
        });
    });
</script>

@endsection

