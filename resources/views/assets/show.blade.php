<div id="modal_asset_show" class="modal fade in" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <p class="text-muted">ASSET DETAILS</p>
                    <h1><span id="modal_asset_name"></span></h1>
                    <hr>
                    <p class="text-muted">ASSET SERIAL NUMBER</p>
                    <H1><span id="modal_asset_serialnum"> </span></H1>
                    <hr>
                    <p class="text-muted">ASSET WARRANTY</p>
                    <H1><span id="modal_asset_warranty"> </span></H1>

            </div>
        </div>
    </div>
 </div>
