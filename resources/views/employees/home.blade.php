@extends('layouts.default.master')
@section('content')

<div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
</div>

<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
  <div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Dashboard</h4>
        </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                <ol class="breadcrumb hidden">
                    <li><a href="dashboard.html">Dashboard</a></li>
                    <li class="active">Dashboard 1</li>
                </ol>
            </div>
                    <!-- /.col-lg-12 -->
    </div>
<!-- /.row -->
<!-- ============================================================== -->
<!-- Different data widgets -->
<!-- ============================================================== -->
<div class="row">
     {{-- leaves --}}
     <div class="col-md-4 col-sm-12">
            <div class="white-box" style="background: #2cabe3">
                <ul class="col-in">
                    <li>
                        <span class="circle circle-md bg-white"><i style="color:#2cabe3" class="ti-calendar"></i></span>
                    </li>
                    <li class="col-last">
                        <h3 class="counter text-right m-t-15 white">{{ $leaves }}</h3>
                    </li>
                    <li class="col-middle" style="width:50%">
                        <h4 class="white">Your Leave Application</h4>
                    </li>
                </ul>
            </div>
        </div>
    {{-- claims --}}
    <div class="col-md-4 col-sm-12">
            <div class="white-box" style="background: #2cabe3">
                <ul class="col-in">
                    <li>
                        <span class="circle circle-md bg-white"><i style="color:#2cabe3" class="ti-money"></i></span>
                    </li>
                    <li class="col-last">
                        <h3 class="counter text-right m-t-15 white">{{ $claims }}</h3>
                    </li>
                    <li class="col-middle" style="width:50%">
                        <h4 class="white">Your Monthly Claims</h4>
                    </li>
                </ul>
            </div>
        </div>

{{-- notice --}}
<div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title">Notices</h3>

                <div class="table-responsive">
                    <table  id="notice_table" class="">
                        <thead>
                            <tr>
                                <th style="width: 5%">#</th>
                                <th>Title</th>
                                <th>Published Date</th>
                                <th>Created by</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

        </div>
    </div>

            </div>
            <!-- /.row -->

        </div>
        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2018 &copy; IX HRMS. All Rights Reserved. </footer>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

    {{--  <script>
           var el_t = document.getElementById('userupdate');
            var length = el_t.getAttribute("maxlength");
            var el_c = document.getElementById('count');
            el_c.innerHTML = length + (' left');
            el_t.onkeyup = function () {
              document.getElementById('count').innerHTML = (length - this.value.length) + (' left');
            };
    </script>  --}}

@endsection
@section('js')
@parent
    <script>
        $(function () {
            $('#notice_table').DataTable({
                serverSide: true,
                processing: true,
                ajax: '/dashboard/notices/list',
                columns: [
                    {data: 'DT_Row_Index', orderable: false, searchable: false},
                    {data: 'title'},
                    {data: 'created_at'},
                    {data: 'createdby'},
                    {data: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
    <script>

            $(document).off('click', '.open-modal').on("click", ".open-modal", function () {

                var data = $(this).data('modal_data');
                if($(this).data('logo') ){
                var logo = $(this).data('logo');
                }

            });
    </script>
@endsection
