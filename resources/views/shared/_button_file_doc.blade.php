@if($model->getMedia('document')->first())
<a href="{{ $model->getMedia('document')->first()->getUrl() }}" target="_blank" class="btn btn-sm btn-success"><i class="fa fa-file"></i></a>
@endif

@if($model->url)
<a href="{{ $model->url }}" target="_blank" class="btn btn-sm btn-success"><i class="fa fa-link"></i></a>
@endif
