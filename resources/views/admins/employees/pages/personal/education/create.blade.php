<div class="modal fade" id="add-educational" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Add Education</h1>

                    {!! Form::open(['url' => '', 'id' => 'education_create']) !!}
                    <div class="form-group row">
                        <div class="col-md-6">
                            {!! Form::label('Qualification') !!}
                            {!! Form::text('qualification', null, array('class' => 'form-control', 'placeholder' => 'Qualification', 'required')) !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('Field of Study') !!}
                            {!! Form::text('field', null, array('class' => 'form-control', 'placeholder' => 'Field of Study', 'required')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('University/Institution') !!}
                        {!! Form::text('university', null, array('class' => 'form-control', 'placeholder' => 'University/Institution', 'required')) !!}

                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            {!! Form::label('Started At') !!}
                            {!! Form::date('started_at', null, array('class' => 'form-control', 'placeholder' => 'Started At', 'required')) !!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('Graduated At') !!}
                            {!! Form::date('graduated_at', null, array('class' => 'form-control', 'placeholder' => 'Graduated At', 'required')) !!}
                        </div>
                    </div>

                    {{ Form::submit('Add Education',['class' =>'btn btn-rounded btn-info text-uppercase']) }}
                    <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                    {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>

@section('js')
    @parent

    <script>

        $('#add-educational').on('hidden.bs.modal', function(e) {
            $(this).find('#education_create')[0].reset();
        });

        $( "#education_create" ).on( "submit", function( event ) {
            event.preventDefault();
            swal({
                title: "Submit Education Background",
                text: "Are you sure you want to submit this education backgroud?",
                type: "info",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                allowOutsideClick: () => !swal.isLoading(),
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        var form = document.getElementById('education_create');
                        var formData = new FormData(form);
                        $.ajax({
                            type: "POST",
                            url: '{{ route('employees.educations.store', $employee->id) }}',
                            data: formData,
                            cache:false,
                            contentType: false,
                            processData: false,
                            success: function(data) {
                                resolve(data);
                            },
                            error: function(data) {
                                errormsg = '';
                                if(data.status == 422 ){
                                    $.each(data.responseJSON.errors, function (key, value) {
                                        errormsg = errormsg + value[0] + "<br />";
                                    });
                                }else{
                                    console.log(data);
                                    errormsg = "Server error. Please try again";
                                }
                                swal.showValidationMessage(errormsg);
                                swal.hideLoading();
                            },
                        });
                    });

                }
            }).then((result) => {
                if(result.value){
                    $('#add-educational').modal('toggle');
                    swal('Education Saved!', '--', 'success');
                    $('#education_table').DataTable().ajax.reload();
                }
            });
        });

    </script>

@endsection
