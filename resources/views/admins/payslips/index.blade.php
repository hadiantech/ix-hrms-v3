@extends('layouts.default.master')

@section('content')

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>


<div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Payslip</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    <ol class="breadcrumb">
                        <li><a href="dashboard-adminview.html">Dashboard</a></li>
                        <li class="active">Payslip</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!-- ============================================================== -->
            <!-- Different data widgets -->
            <!-- ============================================================== -->
            <!--<div class="row">
                <div class="col-sm-6">
                    <div class="white-box">
                        <h3 class="box-title">Generate Payslip</h3>
                        <select class="selectpicker" data-style="form-control">
                            <option>Select Month</option>
                            <option>January</option>
                            <option>February</option>
                            <option>March</option>
                            <option>April</option>
                            <option>May</option>
                            <option>June</option>
                            <option>July</option>
                            <option>August</option>
                            <option>September</option>
                            <option>October</option>
                            <option>November</option>
                            <option>December</option>
                        </select>
                        <button style="margin-top:10px" class="btn btn-info">Generate</button>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div style="overflow:auto;" class="white-box">
                        <h3 class="box-title">Provident Fund</h3>
                        <p>Review EPF and other fund information</p>
                        <button class="btn btn-info">Review</button>
                    </div>
                </div>
            </div>-->

            <div class="row">
                <div class="col-sm-12">


                    <div class="white-box">
                        <h3 class="box-title">Payroll Categories
                           <a href="#add-payroll-cat" data-toggle="modal" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Add Payroll Category</a>
                             </h3>
                        <div class="table-responsive">

                            <table  id="myTable" class="table display">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Category Name</th>
                                        <th width="190px" class="text-center">Operand</th>
                                        <th width="190px" class="text-center">Amount</th>
                                        <th width="190px" class="text-center">Default</th>
                                        <th width="120px" class="text-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>PTPTN - Husna</td>
                                        <td class="text-center">Deduction</td>
                                        <td class="text-center">RM300</td>
                                        <td class="text-center">No</td>
                                        <td class="text-center">  link invalid</td>
                                       {{-- <td class="text-right"><a href="{{route('adminpayslips.show')}}" class="m-r-10 label label-info"><i class="fa fa-eye"></i></a><a class="label label-danger"><i class="fa fa-trash"></i></a></td> --}}
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Meal Allowance</td>
                                        <td class="text-center">Addition</td>
                                        <td class="text-center">RM90</td>
                                        <td class="text-center">Yes</td>
                                        <td class="text-right"><a class="m-r-10 label label-info"><i class="fa fa-eye"></i></a><a class="label label-danger"><i class="fa fa-trash"></i></a></td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Monthly Parking Fee</td>
                                        <td class="text-center">Deduction</td>
                                        <td class="text-center">RM80</td>
                                        <td class="text-center">No</td>
                                       <td class="text-right"><a class="m-r-10 label label-info"><i class="fa fa-eye"></i></a><a class="label label-danger"><i class="fa fa-trash"></i></a></td>
                                    </tr>



                                </tbody>
                            </table>

                        </div>
                        <ul class="pagination pagination-split m-t-10">
                            <li> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li class="disabled"> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>
                    </div>




                </div>
            </div>
            <!--row -->

            <div class="row">
                <div class="col-sm-12">


                    <div class="white-box">
                        <h3 class="box-title">Salary List
                           <!--<a href="#add-payroll-cat" data-toggle="modal" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Add Payroll Category</a>-->
                             </h3>
                        <div class="table-responsive">

                            <table  id="myTable" class="table display">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Employee</th>
                                        <th width="190px" class="text-center">Designation</th>
                                        <th width="190px" class="text-center">Salary</th>
                                        <th width="120px" class="text-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Husna Bahrudin</td>
                                        <td class="text-center">Web Developer</td>
                                        <td class="text-center">RM5,000.00</td>
                                       <td class="text-right"><a href="#set-salary" data-toggle="modal" class="m-r-10 label label-info"><i data-toggle="tooltip" data-title="Set Salary" class="fa fa-pencil"></i></a>

                                        <a href="#set-increment" data-toggle="modal" class="m-r-10 label label-purple"><i data-toggle="tooltip" data-title="Set Increment" class="fa fa-arrow-up"></i></a></td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Muhaimin Juhari</td>
                                        <td class="text-center">Web Developer</td>
                                        <td class="text-center">RM0.00</td>
                                       <td class="text-right"><a href="#set-salary" data-toggle="modal" class="m-r-10 label label-info"><i data-toggle="tooltip" data-title="Set Salary" class="fa fa-pencil"></i></a>

                                        <a href="#set-increment" data-toggle="modal" class="m-r-10 label label-purple"><i data-toggle="tooltip" data-title="Set Increment" class="fa fa-arrow-up"></i></a></td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Nor Emil Badli Munawir</td>
                                        <td class="text-center">Web Developer</td>
                                        <td class="text-center">RM0.00</td>
                                       <td class="text-right"><a href="#set-salary" data-toggle="modal" class="m-r-10 label label-info"><i data-toggle="tooltip" data-title="Set Salary" class="fa fa-pencil"></i></a>

                                        <a href="#set-increment" data-toggle="modal" class="m-r-10 label label-purple"><i data-toggle="tooltip" data-title="Set Increment" class="fa fa-arrow-up"></i></a></td>
                                    </tr>



                                </tbody>
                            </table>

                        </div>
                        <ul class="pagination pagination-split m-t-10">
                            <li> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                            <li class="disabled"> <a href="#">1</a> </li>
                            <li class="active"> <a href="#">2</a> </li>
                            <li> <a href="#">3</a> </li>
                            <li> <a href="#">4</a> </li>
                            <li> <a href="#">5</a> </li>
                            <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                        </ul>
                    </div>




                </div>
            </div>
            <!--row -->




            <!--modal add category-->
            @include('admins.payslips.create')

             <!--modal set salary-->
            @include('admins.payslips.salary')

              <!--modal set increment-->
            @include('admins.payslips.increment')



        </div>
        <!-- /.container-fluid -->
    </div>
@endsection

@section('js')
@endsection
