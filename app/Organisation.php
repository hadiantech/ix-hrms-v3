<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Organisation extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = ['company_name','reg_num','address1','address2','city','state','country','number','fax','website','fb','linkedin','other','business','natureoforg','tax'];
}
