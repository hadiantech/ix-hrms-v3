<?php

namespace App\Http\Controllers;

use App\Visa;
use DataTables;
use Carbon\Carbon;
use Illuminate\Http\Request;


class VisaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function list($id){
        return Datatables::eloquent(Visa::where(['advance_claims_id' => $id]))
            ->addColumn('action', function($model){

                if($model->claim->claim_status == "submit"){
                    $delete_hide = true;
                    $edit_hide = true;
                }
                else{
                    $delete_hide = false;
                    $edit_hide = false;
                }

                $show_hide = true;
                $modal_edit_id = 'modal_visa_edit';
                $url = 'visas';
                return view('shared._table_action', compact('model', 'url','show_hide','modal_edit_id','delete_hide','edit_hide'))->render();
            })
            ->escapeColumns([])
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $visa = Visa::create($request->all());
        return response()->json($visa, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Visa $visa)
    {
        $visa->update($request->all());
        return response()->json($visa, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Visa $visa)
    {
        $visa->delete();
        return redirect()->back();
    }
}
