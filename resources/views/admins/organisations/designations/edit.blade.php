<div id="modal_designations_edit" class="modal fade in" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Edit Designation</h1>
                {!! Form::open(['url' => 'designations', 'id' => 'designation_edit']) !!}
                {{ Form::hidden('designation_id', null , ['id' => 'designation_id']) }}
                <input type="hidden" name="_method" value="PUT">

                <div class="row form-group">
                    <div class="col-md-8">
                        {{Form::text('designation_name', null , ['id' => 'designation_name', 'required','class' => 'form-control','placeholder' => 'Designation Name'])}}
                    </div>
                    <div class="col-md-4">
                        <select class="form-control" name="department_id" id="department_id" data-parsley-required="true">
                        @foreach ($departments as $department)
                            <option value="{{ $department->id }}">{{ $department->department_name }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                {{Form::submit('Save Designation',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

                {!! Form::close() !!}
            </div>

        </div>
    </div>
</div>

@section('js')
    @parent
    <script>

    $( "#designation_edit" ).on( "submit", function( event ) {
        event.preventDefault();
        swal({
            title: "Update Designation",
            text: "Are you sure you want to update this designation?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                var form = document.getElementById('designation_edit');
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: '/designations/'+form.designation_id.value,
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(msg) {
                        return msg;
                    }
                });
            }
        }).then((result) => {
            $('#modal_designations_edit').modal('toggle');
            if(result.value){
                $('#designation_table').DataTable().ajax.reload();
                swal('Designation Updated!', '--', 'success');
            }
        });
    });
    </script>
@endsection
