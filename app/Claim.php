<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Claim extends Model
{
    //cara untuk declare table name lain
    //protected $table = 'claims';
    //protected $primaryKey = 'id';

    //terima apa2 je data
    protected $guarded = [];

    public function employee(){
        return $this->belongsTo(Employee::class,'emp_id', 'id');
    }

    public function type(){
        return $this->hasOne(ClaimType::class,'claim_type');
    }

    public function expenses(){
        return $this->hasMany(Expense::class,'claim_id');
    }

    public function travellers(){
        return $this->hasMany(Traveller::class,'advance_claim_id');
    }

    public function visas(){
        return $this->hasMany(Visa::class,'advance_claim_id');
    }

    public function flights(){
        return $this->hasMany(Flight::class,'advance_claim_id');
    }

    public function hotels(){
        return $this->hasMany(Hotel::class,'advance_claim_id');
    }




}
