@extends('layouts.default.master')

@section('content')
<!-- Include stylesheet -->
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
</div>

    <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Notice</h4>
                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                        <ol class="breadcrumb">
                            <li><a href="dashboard-adminview.html">Dashboard</a></li>
                            <li class="active">Notice</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                {{-- Notice Show --}}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title">Notice / Memo</h3>

                            <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            {{-- {{ Form::hidden('id', null , ['id' => 'id']) }} --}}
                                            <div class="form-group ">
                                                </div>
                                            <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">NOTICE TITLE</h5>
                                            <h3>{{ $notices->title }}</h3>
                                            <hr>

                                            <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">NOTICE DESCRIPTION</h5>
                                                <form>
                                                        {!! $notices->description !!}
                                                </form>
                                                <hr>
                                            {{-- <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">NOTICE CREATOR</h5> --}}

                                                <div class="hr-details row">

                                                        <div class="col-md-7">
                                                            <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">CREATED BY</h5>
                                                            <ul class="side-icon-text">
                                                                <li> @if(Auth::user()->getMedia('profile')->first())
                                                                        <img src="{{ Auth::user()->getMedia('profile')->first()->getUrl() }}" alt="user-img" width="36" class="img-circle">
                                                                        @else
                                                                        <img src="{{ asset('images/Avatar.png') }}" alt="user-img" width="36" class="img-circle">
                                                                        @endif<span class="di vm"><h5 class="m-b-0">{{ $notices->employee->fname }} {{ $notices->employee->lname }}</h5>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                </div><hr>
                                                <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">PUBLISHED DATE: {{ $notices->created_at }}</h5>
                                            </div>

                                        </div>
                                    </div>
                        </div>

                    </div>
                </div>
                <!--row -->

        </div>
            <!-- /.container-fluid -->
        </div>

        @endsection

        //end
