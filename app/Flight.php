<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flight extends Model
{
    protected $table = 'flight_informations';
    protected $guarded =  [];

    public function claim(){
        return $this->belongsTo(Claim::class,'advance_claims_id');
    }
}
