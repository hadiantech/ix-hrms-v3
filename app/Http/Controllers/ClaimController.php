<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Claim;
use App\ClaimAdvance;
use App\ExpenseType;
use App\Expense;
use App\Employee;
use App\Approval;
use App\Organisation;
use Auth;
use Carbon\Carbon;
use DataTables;
use Postmark\PostmarkClient;

class ClaimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('employees.claims.index');
    }

    // Datatable for listing normal claim for employee
    public function list(){
        return Datatables::eloquent(Claim::where(['emp_id' => Auth::user()->id,'claim_type'=>'1']))
            ->addColumn('desc', function($model){

                $status = $model->claim_status;
                if($status == 'draft'){
                    $label = '<span class="label label-warning">Draft</span>';
                }
                else if($status == 'submit'){
                    $label = '<span class="label label-info">Submitted</span>';
                }
                return 'Normal Claim - '.Carbon::create($model->claim_month)->format('F').' '.$label;
            })

            ->addColumn('status', function($model){

                $proc_status = $model->claim_proc_status;
                if($proc_status == 'unproc'){
                    $label = '<span class="label label-warning">Pending</span>';
                }
                else if($proc_status == 'proc'){
                    $label = '<span class="label label-info">Reviewed</span>';
                }
                else if($proc_status == 'return'){
                    $label = '<span class="label label-danger">Returned</span>';
                }
                else if($proc_status == 'approved'){
                    $label = '<span class="label label-success">Approved</span>';
                }

                return $label;
            })
            ->addColumn('action', function($model){

                $status = $model->claim_status;
                $proc_status = $model->claim_proc_status;

                if($status == 'submit'){
                    $delete_hide = true;
                }
                else if($status == 'draft'){
                    $delete_hide = false;
                }
                $edit_hide = true;

                $url = 'claims';
                return view('shared._table_action', compact('model', 'url','edit_hide','delete_hide'))->render();
            })
            ->escapeColumns([])
            ->addIndexColumn()
            ->make(true);
    }
    public function list_request(){
        return Datatables::eloquent(Claim::where(['emp_id' => Auth::user()->id,'claim_type'=>'2']))
            ->addColumn('desc', function($model){


                $status = $model->claim_status;
                if($status == 'draft'){
                    $label = '<span class="label label-warning">Draft</span>';
                }
                else if($status == 'submit'){
                    $label = '<span class="label label-info">Submitted</span>';
                }

                return 'Advance Claim - '.Carbon::create($model->claim_month)->format('F').' '.$label;
            })
            ->addColumn('status', function($model){

                $proc_status = $model->claim_proc_status;

                if($proc_status == 'unproc'){
                    $label = '<span class="label label-warning">Pending</span>';
                }
                else if($proc_status == 'proc'){
                    $label = '<span class="label label-info">Reviewed</span>';
                }
                else if($proc_status == 'return'){
                    $label = '<span class="label label-danger">Returned</span>';
                }
                else if($proc_status == 'approved'){
                    $label = '<span class="label label-success">Approved</span>';
                }
                else if($proc_status == 'proc_advance'){
                    $label = '<span class="label label-info">Reviewed</span>';
                }
                else if($proc_status == 'approved_advance'){
                    $label = '<span class="label label-success">Approved</span>';
                }
                else{
                    $label = '<span class="label label-warning">Pending</span>';
                }

                return $label;
            })
            ->addColumn('action', function($model){

                $status = $model->claim_status;
                $proc_status = $model->claim_proc_status;

                if($status == 'submit'){
                    $delete_hide = true;
                }
                else{
                    $delete_hide = false;
                }
                $edit_hide = true;
                $url = 'claimadvances';

                if($model->claim_proc_status == 'approved_advance' or $model->claim_proc_status == 'unproc_advance' or $model->claim_proc_status == 'proc_advance'){
                    $custom_btn = [
                        '<a class="btn btn-xs btn-primary" href="'.route('claimadvances_detail.show',$model->id).'"><i class="fa fa-pencil"></i></a>',
                    ];
                }
                else{
                    $custom_btn = [];
                }

                return view('shared._table_action', compact('model', 'url','custom_btn','edit_hide','delete_hide'))->render();
            })
            ->escapeColumns([])
            ->addIndexColumn()
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('employees.claims.create');
    }

    public function createAdvance()
    {
        return view('employees.advance.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $day = Carbon::now()->format('d');

        if($day < 10){
            //get month
            //get last month
            $thismonth =  Carbon::now()->format('m');
            $lastmonth =  Carbon::now()->subMonth()->format('m');
            $months = [$lastmonth, $thismonth];
        }
        else{

            //get this month
            $thismonth =  Carbon::now()->format('m');
            $months = [$thismonth];
        }

        $expenses_type = ExpenseType::all();

        $claim = Claim::find($id);
        return view('employees.claims.monthly.show', compact('claim', 'months','expenses_type'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Claim $claim)
    {
         // email
         $client = new PostmarkClient("73a69c7d-8c56-4f59-877c-74d05cd5e247");
         $approval_lv1 = Approval::where('employee_id', Auth::user()->id)->where('level',1)->where('type','claim')->first();
         $approval_lv2 = Approval::where('employee_id', Auth::user()->id)->where('level',2)->where('type','claim')->first();
         $aid = Auth::user()->id;
         $employees = Employee::find($aid);
         //

        $claim->claim_month = $request->claim_month;
        $claim->claim_status = $request->claim_status;
        $claim->claim_proc_status = $request->claim_proc_status;
        $claim->claim_applied_date = Carbon::now();

        if($request->claim_remark != ''){
            $claim->claim_remark = $request->claim_remark;
        }
        else{
            $claim->claim_remark = '';
        }


        $claim->save();
        $cl = $claim->id;

        $sendResult = $client->sendEmailWithTemplate(

            "hassanul@boneybone.com",
            $approval_lv1->approvedBy->email,
            10374712,
                [
                    "system_name" => "[IX HRMS] Claim Application",
                    "fname" => $employees->fname,
                    "lname" => $employees->lname,
                    "uid" => $employees->employee_uid,
                    "approve" =>  $approval_lv1->approvedBy->fname,
                    "approver" =>  $approval_lv1->approvedBy->lname,
                    "link" => url('http://hrms.ixtelecom.net')
                ]);
                $sendResult = $client->sendEmailWithTemplate(

                    "hassanul@boneybone.com",
                    $approval_lv2->approvedBy->email,
                    10374712,
                        [
                            "system_name" => "[IX HRMS] Claim Application",
                            "fname" => $employees->fname,
                            "lname" => $employees->lname,
                            "uid" => $employees->employee_uid,
                            "approve" =>  $approval_lv2->approvedBy->fname,
                            "approver" =>  $approval_lv2->approvedBy->lname,
                            "link" => url('http://hrms.ixtelecom.net')
                        ]);
        return response()->json($claim, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $claim = Claim::find($id);
        $claim->delete();

        return redirect()->back();
    }

    public function destroy_advance($id){
        $claim = Claim::find($id);
        $claim->delete();

        return redirect()->back();
    }

    public function create_draft(){


        $claim = new Claim;

        $claim->emp_id = Auth::user()->id;
        $claim->claim_status = 'draft';
        $claim->claim_total = 0;
        $claim->claim_proc_status = 'unproc';
        $claim->claim_type = '1';
        $claim->save();


        return redirect()->route('claims.monthly.show', $claim->id);
    }

    public function create_advance_draft(){
        $advance = new Claim;

        $advance->emp_id = Auth::user()->id;

        //return $advance->emp_id;
        $advance->claim_status = 'draft';
        $advance->claim_proc_status = 'unproc';
        $advance->claim_type = '2';
        $advance->save();

        return redirect()->route('claimadvances.show', $advance->id);
    }



    public function show_advance($id)
    {
        $day = Carbon::now()->format('d');

        if($day < 10){
            //get this month
            //get last month
            $thismonth =  Carbon::now()->format('m');
            $nextmonth =  Carbon::now()->addMonth()->format('m');
            $months = [$thismonth, $nextmonth];
        }
        else{
            //get this month
            $thismonth =  Carbon::now()->format('m');
            $months = [$thismonth];
        }

        $claim = Claim::find($id);
        $organisation = Organisation::all();
        $expenses_type = ExpenseType::all();

        $country = $this->country_list();



        return view('employees.claims.advance.show', compact('claim', 'months','expenses_type','organisation','country'));
    }

    public function update_advance(Request $request, $id)
    {
// email
$client = new PostmarkClient("73a69c7d-8c56-4f59-877c-74d05cd5e247");
$approval_lv1 = Approval::where('employee_id', Auth::user()->id)->where('type','advanced')->where('level',1)->first();
$approval_lv2 = Approval::where('employee_id', Auth::user()->id)->where('type','advanced')->where('level',2)->first();
$aid = Auth::user()->id;
$employees = Employee::find($aid);
//

        $claim = Claim::find($id);

        $claim->claim_status = $request->claim_status;
        $claim->project_id = $request->project_id;
        $claim->claim_total_advance =$request->claim_total_advance;
        $claim->organisation_id = $request->organisation_id;
        $claim->claim_location = $request->claim_location;
        $claim->claim_purpose = $request->claim_purpose;
        $claim->claim_type_advance = $request->claim_type_advance;
        $claim->travel_date_from = $request->travel_date_from;
        $claim->travel_date_to = $request->travel_date_to;

        if($request->claim_remark != ''){
            $claim->claim_remark = $request->claim_remark;
        }
        else{
            $claim->claim_remark = '';
        }

        $claim->claim_proc_status = $request->claim_proc_status;

        //add applied date when confirm & submit
        $claim->claim_applied_date = Carbon::now();

        $claim->save();
        $req = $claim->id;

        $sendResult = $client->sendEmailWithTemplate(

            "hassanul@boneybone.com",
            $approval_lv1->approvedBy->email,
            10374712,
                [
                    "system_name" => "[IX HRMS] Claim Application",
                    "fname" => $employees->fname,
                    "lname" => $employees->lname,
                    "uid" => $employees->employee_uid,
                    "approve" =>  $approval_lv1->approvedBy->fname,
                    "approver" =>  $approval_lv1->approvedBy->lname,
                    "link" => url('http://hrms.ixtelecom.net')
                ]);
        $sendResult = $client->sendEmailWithTemplate(

            "hassanul@boneybone.com",
            $approval_lv2->approvedBy->email,
            10374712,
                [
                    "system_name" => "[IX HRMS] Claim Application",
                    "fname" => $employees->fname,
                    "lname" => $employees->lname,
                    "uid" => $employees->employee_uid,
                    "approve" =>  $approval_lv2->approvedBy->fname,
                    "approver" =>  $approval_lv2->approvedBy->lname,
                    "link" => url('http://hrms.ixtelecom.net')
                ]);
        return response()->json($claim, 201);
    }

    public function submit_advance(Request $request, $id)
    {
         // email
         $client = new PostmarkClient("73a69c7d-8c56-4f59-877c-74d05cd5e247");
         $approval_lv1 = Approval::where('employee_id', Auth::user()->id)->where('level',1)->first();

         $aid = Auth::user()->id;
         $employees = Employee::find($aid);
         //

        $claim = Claim::find($id);

        $approval_lv1 = Approval::where('employee_id', Auth::user()->id)->where('level',1)->where('type','advanced')->first();
        $approval_lv2 = Approval::where('employee_id', Auth::user()->id)->where('level',2)->where('type','advanced')->first();

        $employees = Employee::find($id);

        $claim->reference_no = $request->reference_no;
        $claim->bank_name = $request->bank_name;
        $claim->ref_date = $request->ref_date;
        $claim->ref_time = $request->ref_time;
        $claim->method_payment = $request->method_payment;
        $claim->payment_status = $request->payment_status;
        $claim->claim_proc_status = $request->claim_proc_status;
        $claim->claim_status = $request->claim_status;

        $claim->save();
        $req = $claim->id;

        $sendResult = $client->sendEmailWithTemplate(

            "hassanul@boneybone.com",
            $approval_lv1->approvedBy->email,
            10374712,
                [
                    "system_name" => "[IX HRMS] Claim Application",
                    "fname" => $employees->fname,
                    "lname" => $employees->lname,
                    "uid" => $employees->employee_uid,
                    "approve" =>  $approval_lv1->approvedBy->fname,
                    "approver" =>  $approval_lv1->approvedBy->lname,
                    "link" => url('http://hrms.ixtelecom.net')
                ]);

        return response()->json($claim, 201);
    }

    public function show_advance_detail($id)
    {
        $day = Carbon::now()->format('d');

        if($day < 10){
            //get this month
            //get last month
            $thismonth =  Carbon::now()->format('m');
            $nextmonth =  Carbon::now()->addMonth()->format('m');
            $months = [$thismonth, $nextmonth];
        }
        else{
            //get this month
            $thismonth =  Carbon::now()->format('m');
            $months = [$thismonth];
        }

        $claim = Claim::find($id);
        $organisation = Organisation::all();
        $expenses_type = ExpenseType::all();

        //get total expenses
        $total_expenses = Expense::where('claim_id',$claim->id)->sum('amount');

        $country = $this->country_list();

        //get approver
        $emp_id = $claim->emp_id;

        $approver_id_1 = Approval::where('employee_id',$emp_id)->type('claim')->level(1)->pluck('approver_id')->first();
        $approver_id_2 = Approval::where('employee_id',$emp_id)->type('claim')->level(2)->pluck('approver_id')->first();

        if($approver_id_1 == Auth::id()){
            $approver = 1;
        }
        else if($approver_id_2 == Auth::id()){
            $approver = 2;
        }
        else{
            $approver = '0';
        }

        return view('employees.claims.advance.detail', compact('claim', 'months','expenses_type','organisation','country','total_expenses','approver'));
    }

    public function country_list(){

        $country_array = array("Malaysia","Afghanistan", "Aland Islands", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Barbuda", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Trty.", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Caicos Islands", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern Territories", "Futuna Islands", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard", "Herzegovina", "Holy See", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Jan Mayen Islands", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea", "Korea (Democratic)", "Kuwait", "Kyrgyzstan", "Lao", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macao", "Macedonia", "Madagascar", "Malawi", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "McDonald Islands", "Mexico", "Micronesia", "Miquelon", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "Nevis", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestinian Territory, Occupied", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Principe", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Barthelemy", "Saint Helena", "Saint Kitts", "Saint Lucia", "Saint Martin (French part)", "Saint Pierre", "Saint Vincent", "Samoa", "San Marino", "Sao Tome", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia", "South Sandwich Islands", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "The Grenadines", "Timor-Leste", "Tobago", "Togo", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Turks Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "US Minor Outlying Islands", "Uzbekistan", "Vanuatu", "Vatican City State", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (US)", "Wallis", "Western Sahara", "Yemen", "Zambia", "Zimbabwe");

        return $country_array;
    }
}
