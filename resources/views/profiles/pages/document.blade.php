<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h3 class="box-title">Supporting Documents</h3>
        <br />
        <br />
        <div class="table-responsive m-b-30">
            <table id="document_table" class="table">
                <thead>
                    <tr style="border-bottom:3px solid #eee">
                        <th>#</th>
                        <th>Document Name</th>
                        <th>URL</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>

@section('js')
    @parent
    <script>
        $(function () {
            $('#document_table').DataTable({
                serverSide: true,
                processing: true,
                ajax: '{{ route("profile.documents") }}',
                columns: [
                    {data: 'DT_Row_Index', orderable: false, searchable: false},
                    {data: 'name'},
                    {data: 'url_view', orderable: false, searchable: false},
                ]
            });
        });
    </script>

@endsection
