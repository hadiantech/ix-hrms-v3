<?php

namespace App\Http\Controllers;

use App\MarriageLeave;
use Illuminate\Http\Request;

class MarriageLeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MarriageLeave  $marriageLeave
     * @return \Illuminate\Http\Response
     */
    public function show(MarriageLeave $marriageLeave)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MarriageLeave  $marriageLeave
     * @return \Illuminate\Http\Response
     */
    public function edit(MarriageLeave $marriageLeave)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MarriageLeave  $marriageLeave
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MarriageLeave $marriageLeave)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MarriageLeave  $marriageLeave
     * @return \Illuminate\Http\Response
     */
    public function destroy(MarriageLeave $marriageLeave)
    {
        //
    }
}
