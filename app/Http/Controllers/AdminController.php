<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use App\Admin;
use App\Employee;
use App\Role;
use App\Department;
use App\Designation;
use App\Organisation;
use App\Hierarchy;
use Illuminate\Http\Request;
use Auth;
use View;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $employees = Employee::all();
        $roles = Role::all();
        $departments = Department::all();
        $organisations = Organisation::all();
        $designations = Designation::get();
        $hierarchies = Hierarchy::all();
        return view('admins.employees.index',compact('employees','roles','organisations','departments','designations','hierarchies'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $organisations = Organisation::lists(['organisation_name']);
        $departments = Department::lists(['department_name']);
        $designations = Designation::lists(['designation_name']);
        $roles = Role::lists(['role_name']);
        $hierarchies = Hierarchy::lists(['hierarchy_name']);
        return view('admins.employees.create',compact('employees','roles','organisations','departments','designations','hierarchies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $employees = Employee::create($request->only('username','email','password','lname','fname','ic','designation_id','department_id','company_id','role_id'));

        // $employee = new Employee();
        // $employees->fname = $request->fname;
        // $employees->fname       = Input::get('fname');
        // $employees->lname = $request->lname;
        // $employees->username = $request->username;
        // $employees->email = $request->email;
        // $employees->password = bcrypt();
        // $employees->ic = $request->ic;
        // $employees->designation_id = $request->designation_id;
        // $employees->department_id = $request->department_id;
        // $employees->company_id = $request->company_id;
        // $employees->role_id = $request->role_id;

        $employees->save();
        return redirect()-> route('employees.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $organisations = Organisation::find($id);
        $departments = Department::find($id);
        $designations = Designation::find($id);
        $employees = Employee::find($id);
        $hierarchies = Hierarchy::find($id);
        return view('admins.employees.show',compact('employees','organisations','departments','designations','hierarchies'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $organisations = Organisation::find($id);
        $departments = Department::find($id);
        $designations = Designation::find($id);
        $roles = Role::find($id);
        $employees = Employee::find($id);
        return view('admins.employees.edit',compact('employees','organisations','departments','designations'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $organisations = Organisation::find($id);
        $departments = Department::find($id);
        $designations = Designation::find($id);
        $roles = Role::find($id);
        $employees = Employee::find($id);

        $employees->fname = $request->fname;
        $employees->lname = $request->lname;
        // $employees->username = $request->username;
        // $employees->email = $request->email;
        // $employees->password = $request->password;
        $employees->ic = $request->ic;
        // $employees->designation_id = $request->designation_id;
        // $employees->department_id = $request->department_id;
        // $employees->company_id = $request->company_id;
        // $employees->role_id = $request->role_id;
        $employees->gender = $request->gender;
        $employees->marriage_status = $request->marriage_status;
        $employees->race = $request->race;
        $employees->religion = $request->religion;
        // $employees->phone_number = $request->phone_number;
        $employees->pre_address_line1 = $request->pre_address_line1;
        $employees->pre_address_line2 = $request->pre_address_line2;
        $employees->pre_city = $request->pre_city;
        $employees->pre_state = $request->pre_state;
        $employees->pre_postcode = $request->pre_postcode;
        $employees->pre_country = $request->pre_country;
        $employees->pre_phone_number1 = $request->pre_phone_number1;
        $employees->pre_phone_number2 = $request->pre_phone_number2;
        $employees->bank_name = $request->bank_name;
        $employees->bank_account_no = $request->bank_account_no;


        $employees->save();

        return redirect()-> route("employees.index", $employees->id);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
