<div id="lieu-leave-form" class="modal fade in" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <p class="text-uppercase">Apply Lieu Days</p>
                <h1>Leave In Lieu</h1>
                    <div class="alert alert-warning">Leave in lieu will expires 3 months after the approval date.</div>
                    {!! Form::open(['url' => '', 'id' => 'lieu_create','method' => 'post']) !!}
                    <div class="form-group">
                        {!! Form::label('Event Name') !!}
                        {!!Form::text('event_name','',array('id'=>'lieu_event_name','class'=>'form-control','placeholder'=>'Event Name', 'required'))!!}
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            {!! Form::label('Started At') !!}
                            {!!Form::date('started_at','',array('id'=>'lieu_started_at','class'=>'form-control','placeholder'=>'Started At', 'required'))!!}
                        </div>
                        <div class="col-md-6">
                            {!! Form::label('Ended At') !!}
                            {!!Form::date('ended_at','',array('id'=>'lieu_ended_at','class'=>'form-control','placeholder'=>'End Date', 'required'))!!}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            {!! Form::label('Remark') !!}
                            {!!Form::textarea('remark','',array('id'=>'lieu_remark','class'=>'form-control','placeholder'=>'Remark'))!!}
                        </div>
                    </div>
                    <div class="row form-group ">
                        <div class="col-md-6">
                            <label>
                                Total Day(s) : <span id="lieu_days">0</span>
                            </label>
                        </div>
                        <div class="col-md-6">
                            <label>
                                Expired At : <span id="lieu_expired_at">0</span>
                            </label>
                        </div>
                    </div>
                    {{Form::submit('Submit Application' , ['class' => 'm-r-5 text-uppercase btn btn-info'])}}
                    {!! Form::close() !!}

            </div>

        </div>
    </div>
</div>


@section('js')
    @parent
    <script>
        $(document).ready(function(){
        });

        $('#lieu-leave-form').on('hidden.bs.modal', function(e) {
            $(this).find('#lieu_create')[0].reset();
            $("#lieu_days").text(0);
            $("#lieu_expired_at").text(0);
        });

        $('#lieu_started_at').change(function() {
            var date = $("#lieu_started_at").val();
            $("#lieu_ended_at")[0].setAttribute('min', date);
            $("#lieu_ended_at").val('');
            $("#lieu_days").text(0);
            $("#lieu_expired_at").text(0);
        });

        $('#lieu_ended_at').change(function() {
            expired_at = moment($("#lieu_ended_at").val()).add(3, 'months').format('MM/DD/YYYY');
            a = moment($("#lieu_ended_at").val());
            b = moment($("#lieu_started_at").val());
            days = a.diff(b, 'days');
            days = days + 1;
            $("#lieu_days").text(days);
            $("#lieu_expired_at").text(expired_at);
        });

        $( "#lieu_create" ).on( "submit", function( event ) {
            event.preventDefault();
            swal({
            title: 'Saving',
            onOpen: () => {
                swal.showLoading()
                return new Promise(function(resolve, reject) {
                    var form = document.getElementById('lieu_create');
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url: "{{ route('lieus.store') }}",
                        data: formData,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            swal.close();
                            $('#lieu-leave-form').modal('toggle');
                            swal('Success', 'Please wait for approval', 'success');
                            $('#lieu_table').DataTable().ajax.reload();
                        },
                        error: function(data) {
                            errormsg = '';
                            if(data.status == 422 ){
                                $.each(data.responseJSON.errors, function (key, value) {
                                    errormsg = errormsg + value[0] + "<br />";
                                });
                            }else{
                                errormsg = "Server error. Please try again";
                            }
                            swal.showValidationMessage(errormsg);
                            swal.hideLoading();
                        },
                    });
                });
            }})
        });

    </script>
@endsection
