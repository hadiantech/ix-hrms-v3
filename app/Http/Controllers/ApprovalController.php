<?php

namespace App\Http\Controllers;

use App\Approval;
use App\Employee;
use Illuminate\Http\Request;

class ApprovalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Approval  $approval
     * @return \Illuminate\Http\Response
     */
    public function show(Approval $approval)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Approval  $approval
     * @return \Illuminate\Http\Response
     */
    public function edit(Approval $approval)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Approval  $approval
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Approval $approval)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Approval  $approval
     * @return \Illuminate\Http\Response
     */
    public function destroy(Approval $approval)
    {
        //
    }

    public function approve_update(Request $request, Employee $employee){

        $this->approval_save_func($request->leave_approver_lvl_1, $employee, 'leave', 1);
        $this->approval_save_func($request->leave_approver_lvl_2, $employee, 'leave', 2);

        $this->approval_save_func($request->claim_approver_lvl_1, $employee, 'claim', 1);
        $this->approval_save_func($request->claim_approver_lvl_2, $employee, 'claim', 2);

        $this->approval_save_func($request->lieu_approver_lvl_1, $employee, 'lieu', 1);
        $this->approval_save_func($request->lieu_approver_lvl_2, $employee, 'lieu', 2);

        $this->approval_save_func($request->advanced_approver_lvl_1, $employee, 'advanced', 1);
        $this->approval_save_func($request->advanced_approver_lvl_2, $employee, 'advanced', 2);


        return response()->json('success', 201);

    }

    public function approval_save_func($data, $employee, $type, $level){
        if($employee->approvals()->type($type)->level($level)->first()){
            $e1 = $employee->approvals()->type($type)->level($level)->first();
            $e1->approver_id = $data;
            $e1->save();
         }else{
             $employee->approvals()->create([
                 'approver_id' => $data,
                 'type' => $type,
                 'level' => $level,
             ]);
         }
    }
}
