<?php

namespace App\Http\Controllers;

use App\EmployeeMedical;
use App\Employee;
use Illuminate\Http\Request;
use DataTables;

class EmployeeMedicalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Employee $employee)
    {
        return Datatables::eloquent(EmployeeMedical::where('employee_id', $employee->id))
        ->addColumn('action', function($model) use ($employee){
            $model_2 = $employee;
            $show_hide = true;
            $modal_edit_id = 'modal_medical_edit';
            $url = 'employees.medicals';
            $show_hide = true;
            return view('shared._table_action', compact('model', 'url', 'modal_edit_id', 'model_2', 'show_hide'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $medical = EmployeeMedical::create($request->all());
        $employee->medicals()->save($medical);
        return response()->json($medical, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeMedical  $employeeMedical
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeMedical $employeeMedical)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeMedical  $employeeMedical
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeMedical $employeeMedical)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeMedical  $employeeMedical
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee, EmployeeMedical $medical)
    {
        $medical->update($request->all());
        return response()->json($medical, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeMedical  $employeeMedical
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee, EmployeeMedical $medical)
    {
        $medical->delete();
        return redirect()->back();
    }
}
