<div id="modal_salary_increment" class="modal fade in" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">

                <h1>Set Salary Increment</h1>
                {!! Form::open(['url' => '', 'id' => 'increment_create']) !!}
                <input type="hidden" name="_method" value="PUT">
                <input type="text" name="id" id="employee_id">
                <div class="form-group">
                    {!! Form::label('employee name') !!}
                    <p><span id="modal_salary_edit"> </span></p>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        {!! Form::label('set increment') !!}
                        {{Form::text('basic_salary_increment', null , ['required','class' =>'form-control','placeholder' => 'insert increment'])}}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('increment type') !!}
                        {{Form::select('increment_type', ['Monthly' => 'Monthly', 'Hourly' => 'Hourly'], null, ['class'=> 'form-control', 'placeholder' => 'Select Status'])}}
                    </div>

                </div>
                {{Form::submit('Set Salary Increment',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@section('js')
@parent
<script>
    $('#modal_salary_increment').on('hidden.bs.modal', function (e) {
        $(this).find('#increment_create')[0].reset();
    });

    $("#increment_create").on("submit", function (event) {
        event.preventDefault();
        swal({
            title: "Update Assets",
            text: "Are you sure you want to update this asset?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function () {
                var form = document.getElementById('increment_create');
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: '/salaries/' + form.employee_id.value,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (msg) {
                        return msg;
                    }
                });
            }
        }).then((result) => {
            $('#modal_salary_increment').modal('toggle');
            if (result.value) {
                $('#salary_table').DataTable().ajax.reload(function (json) {
                    var len = json.data.length;
                    $("#salaries").empty();
                    $("#salaries").append("<option value=''>Please Select</option>");
                    for (var i = 0; i < len; i++) {
                        var id = json.data[i]['id'];
                        var name = json.data[i]['title'];
                        $("#salaries").append("<option value='" + id + "'>" + name +
                            "</option>");
                    }
                });

                //reload select designation


                swal('Information Added!', '--', 'success');
            }
        });
    });
</script>
@endsection
