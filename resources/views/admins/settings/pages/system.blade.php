<section id="global-setting">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            {!! Form::open(['url' => '', "id" => "system_saved"]) !!}
            <h3 class="box-title">Global Setting</h3>

            <div class="form-group">
                <div class="table-responsive">
                    <table class="table">

                    <tbody>
                            <tr>
                                <td>Date format</td>
                                <td width="20%">
                                    {{ Form::select('date_format', $date_format, settings('date_format') ? settings('date_format') : null, ['class'=>"form-control", 'placeholder'=>"Select One"]) }}
                                </td>
                            </tr>
                            <tr>
                                <td>Currency Code</td>
                                <td>
                                    {{ Form::text('currency_code', settings('currency_code') ? settings('currency_code') : null, ['required', 'min'=>'0', 'class'=>'form-control']) }}
                                </td>
                            </tr>

                    </tbody>
                </table>

                </div>
            </div>
            {{Form::submit('Save',['class' =>'m-b-20 btn-lg btn-rounded btn-info text-uppercase', 'style'=>"width:200px"])}}

            {!! Form::close() !!}

        </div>
    </div>

</section>

@section('js')
    @parent

    <script>
    $( "#system_saved" ).on( "submit", function( event ) {
        event.preventDefault();

        swal({
        title: 'Saving',
        onOpen: () => {
            swal.showLoading()
            return new Promise(function(resolve, reject) {
                var form = document.getElementById('system_saved');
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: "{{ route('settings.store_system') }}",
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        swal.close();
                    },
                    error: function(data) {
                        errormsg = '';
                        if(data.status == 422 ){
                            $.each(data.responseJSON.errors, function (key, value) {
                                errormsg = errormsg + value[0] + "<br />";
                            });
                        }else{
                            errormsg = "Server error. Please try again";
                        }
                        swal.showValidationMessage(errormsg);
                        swal.hideLoading();
                    },
                });
            });
        },
        onClose: () => {

        }
        }).then((result) => {
            console.log(result);
        })
    });
    </script>

@endsection

