<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
Use Redirect;
use App\EmployeeLeave;
use App\Approval;
use App\Employee;
use Carbon\Carbon;
use Input;

use Spatie\MediaLibrary\Models\Media;
use DataTables;
use Auth;
use Postmark\PostmarkClient;

class AdminLeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $leaves = EmployeeLeave::all();
        $employees = Employee::all();

        return view('admins.leaves.index',compact('leave','employees'));
    }

    public function list($type)
    {
        $emp_ids = Approval::where('approver_id', Auth::id())->pluck('employee_id');

        if($type == 'unproc'){
            $query = EmployeeLeave::with(['employee'])->where(['leave_proc_status' => 'unproc'])->whereIn('employee_id',$emp_ids);
        }
        else if($type == 'proc'){
            $query = EmployeeLeave::with(['employee'])->where(['leave_proc_status' => 'proc'])->whereIn('employee_id',$emp_ids);
        }
        else if($type == 'approve'){
            $query = EmployeeLeave::with(['employee'])->where(['leave_proc_status' => 'approve'])->whereIn('employee_id',$emp_ids);
        }
        else{
            return 'No leave applied';
        }

        return Datatables::eloquent($query)
        ->addColumn('employee', function($model){
            return $model->employee->fname." ".$model->employee->lname;
        })
        ->addColumn('status', function($model){
            $status = $model->leave_proc_status;

            if($status == 'unproc'){
                $status = 'Pending';
                $class = 'default';
            }
            elseif($status == 'proc'){
                $status = 'Reviewed';
                $class = 'warning';
            }
            elseif($status == 'approve'){
                $status = 'Approved';
                $class = 'success';
            }
            elseif($status == 'returned'){
                $status = 'Returned';
                $class = 'danger';
            }
            else{
                $class = 'primary';
            }
            return view('shared._button_color', compact('status', 'class'))->render();
        })
        ->addColumn('action', function($model){
            $url = 'adminleaves';
            $custom_permission = 'admin_leaves';
            $delete_hide = true;
            $edit_hide = true;
            // $modal_view_id = 'leaves_modal_view';
            $modal_edit_id = 'leaves_modal_edit';
            if($model->getMedia('leaves')->first()){
                $modal_data_customs = array('leaves' => $model->getMedia('leaves')->first()->getUrl());
            }
            // $edit_hide = true;
            return view('shared._table_action', compact('model','edit_hide','modal_data_customs','url','modal_view_id','modal_edit_id','custom_permission','delete_hide'))->render();
        })
        ->escapeColumns([])
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Media $mediaItem,$id)
    {
        $leaves = EmployeeLeave::find($id);
        $employees = Employee::find($id);


        // $approver_level = Approval::where('employee_id',$leaves->employee_id)->where('approver_id',Auth::id())->first();

        //filter view by approver level
        $approver_level = Approval::where('employee_id',$leaves->employee_id)->where('approver_id',Auth::id())->first();

        // return response()->download($mediaItem->getPath(), $mediaItem->name);

        // return response()->download(public_path('file_path/from_public_dir.pdf'));
        return view('admins.leaves.show',compact('leaves','employees','approver_level'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $leaves = EmployeeLeave::all();
        $employees = Employee::all();


        return view('admins.leaves.edit',compact('leaves','employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = new PostmarkClient("73a69c7d-8c56-4f59-877c-74d05cd5e247");

        $leave = EmployeeLeave::find($id);
        $leave->leave_proc_status = $request->leave_proc_status;
        $leave->status = $request->status;
        $leave->save();

    }
    public function update_email(Request $request, $id)
    {
        $client = new PostmarkClient("73a69c7d-8c56-4f59-877c-74d05cd5e247");
        $leave = EmployeeLeave::find($id);
        $employees = $leave->employee;
        // $leaves = EmployeeLeave::where('employee_id', Auth::user()->id)->get();
        // $approval = Approval::where('employee_id', Auth::user()->id)->first();
        $approval_lv1 = Approval::where('employee_id', $employees->id)->where('level',1)->first();
        $approval_lv2 = Approval::where('employee_id', $employees->id)->where('level',2)->first();

        $url = "http://hrms.ixtelecom.net";

        $leave->leave_proc_status = "proc";
        $leave->status = "processed";
        $leave->save();

        $sendResult = $client->sendEmailWithTemplate(

            "hassanul@boneybone.com",
            $approval_lv2->approvedBy->email,
            10169332,
                [
                    "system_name" => "[IX HRMS] Leave Application Approval",
                    "fname" => $employees->fname,
                    "lname" => $employees->lname,
                    "uid" => $employees->employee_uid,
                    "approve" =>  $approval_lv2->approvedBy->fname,
                    "approver" =>  $approval_lv2->approvedBy->lname,
                    "leave_type" => $leave->leave_type,
                    "started_at" => $leave->started_at,
                    "ended_at" => $leave->ended_at,
                    "reason" => $leave->reason,
                    "proc_status" => "proc",
                    "status" => "processed",
                    "accept" => route('adminleaves.approve_email',$leave),
                    "reject" => route('adminleaves.reject_email',$leave),

                ]);
                return view('thanks');
    }
    public function approve_email(Request $request, $id)
    {
        $client = new PostmarkClient("73a69c7d-8c56-4f59-877c-74d05cd5e247");
        $leave = EmployeeLeave::find($id);
        $employees = $leave->employee;
        // $leaves = EmployeeLeave::where('employee_id', Auth::user()->id)->get();
        // $approval = Approval::where('employee_id', Auth::user()->id)->first();
        $approval_lv1 = Approval::where('employee_id', $employees->id)->where('level',1)->first();
        $approval_lv2 = Approval::where('employee_id', $employees->id)->where('level',2)->first();

        $url = "http://hrms.ixtelecom.net";
        $leave->leave_proc_status = "approved";
        $leave->status = "approved";
        $leave->save();

        $sendResult = $client->sendEmailWithTemplate(

            "hassanul@boneybone.com",
            $employees->email,
            10017544,
                [
                    "system_name" => "[IX HRMS] Leave Application Approved",
                    "fname" => $employees->fname,
                    "lname" => $employees->lname,
                    "uid" => $employees->employee_uid,
                    "leave_type" => $leave->leave_type,
                    "started_at" => $leave->started_at,
                    "ended_at" => $leave->ended_at,
                    "reason" => $leave->reason,
                    "proc_status" => "approved",
                    "status" => "approved"
                ]);
                return view('thanks');
    }

    public function reject_email(Request $request, $id)
    {
        $client = new PostmarkClient("73a69c7d-8c56-4f59-877c-74d05cd5e247");
        $leave = EmployeeLeave::find($id);
        $employees = $leave->employee;
        $approval_lv1 = Approval::where('employee_id', $employees->id)->where('level',1)->first();
        $approval_lv2 = Approval::where('employee_id', $employees->id)->where('level',2)->first();



        $leave->leave_proc_status = "rejected";
        $leave->status = "rejected";
        $leave->save();

        $url = "http://hrms.ixtelecom.net";
        $sendResult = $client->sendEmailWithTemplate(

            "hassanul@boneybone.com",
            $employees->email,
            10045832,
                [
                    "system_name" => "[IX HRMS] Leave Application Rejected",
                    "fname" => $employees->fname,
                    "lname" => $employees->lname,
                    "uid" => $employees->employee_uid,
                    "leave_type" => $leave->leave_type,
                    "started_at" => $leave->started_at,
                    "ended_at" => $leave->ended_at,
                    "reason" => $leave->reason,
                    "proc_status" => "rejected",
                    "status" => "rejected"

                ]);
            $leave->delete();
            return view('rejected');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function download_slip(Request $request, EmployeeLeave $leaves)
    {


    }
}
