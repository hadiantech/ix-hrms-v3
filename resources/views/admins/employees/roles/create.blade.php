{{-- Modal for Roles Create --}}

<div id="add-role" class="modal fade in" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

        <h1>Add New Role</h1>
        {!! Form::open(['url' => 'adminroles']) !!}
            <div class="row form-group">
                <div class="col-md-8">
                    {{Form::text('role_name', null , ['required','class' => 'form-control','placeholder' => 'Role Name'])}}

                </div>
                <div class="col-md-4">
                    {{Form::select('status', ['Active' => 'Active', 'Inactive' => 'Inactive'], null, ['class' => 'form-control', 'placeholder' => 'Select Status'])}}
                </div>
            </div>

            <div class="form-group">
            <div class="panel panel-inverse">
            <div class="panel-heading">Employee Settings
                <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>  </div>
            </div>
            <div class="panel-wrapper collapse in" aria-expanded="true" style="">
                <div class="panel-body">
                {{-- </div> --}}
                </div>
            </div>

            <div class="panel panel-inverse">
                <div class="panel-heading">Leave Settings
                    <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-plus"></i></a> </div>
                </div>
                <div class="panel-wrapper collapse" aria-expanded="false" style="height: 0px;">
                <div class="panel-body">
                    {{Form::checkbox('dashboardleave','1', false ) }} Leave Dashboard<br><br>
                    {{Form::checkbox('addleave','2', false) }} Add New Leave<br><br>
                    {{Form::checkbox('lieuleave', '3', false) }} Apply Lieu Leave<br><br>
                    {{Form::checkbox('employeeleave', '4' , false) }} Manage Employee Leaves<br><br>
                    {{Form::checkbox('ownleave', '5' , false) }} Manage Own Leaves<br><br>
                </div>
                </div>
            </div>

        <div class="panel panel-inverse">
        <div class="panel-heading">Claim Settings
        <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-plus"></i></a>  </div>
        </div>
        <div class="panel-wrapper collapse" aria-expanded="false" style="height: 0px;">
        <div class="panel-body">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
        </div>
        </div>
        </div>

        <div class="panel panel-inverse">
            <div class="panel-heading">Payroll Settings
                <div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-plus"></i></a>  </div>
            </div>
            <div class="panel-wrapper collapse" aria-expanded="false" style="height: 0px;">
                <div class="panel-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
                </div>
            </div>
        </div>
<div class="panel panel-inverse">
<div class="panel-heading">Notice Settings
<div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-plus"></i></a>  </div>
</div>
<div class="panel-wrapper collapse" aria-expanded="false" style="height: 0px;">
<div class="panel-body">
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
</div>
</div>
</div>
<div class="panel panel-inverse">
<div class="panel-heading">Feedback Settings
<div class="pull-right"><a href="#" data-perform="panel-collapse"><i class="ti-plus"></i></a> </div>
</div>
<div class="panel-wrapper collapse" aria-expanded="false" style="height: 0px;">
<div class="panel-body">
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt est vitae ultrices accumsan. Aliquam ornare lacus adipiscing, posuere lectus et, fringilla augue.</p>
</div>
</div>
</div>

</div>
{{Form::submit('Add Role' , ['class' => 'm-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light'])}}
{!! Form::close() !!}
</div>

</div>
</div>
</div>
