<div id="add-department" class="modal fade in" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Add New Department</h1>
                {!! Form::open(['url' => 'departments', 'id' => 'department_create']) !!}
                    <div class="form-group ">
                        {{Form::text('department_name', null , ['required','class' => 'form-control','placeholder' => 'Department Name'])}}
                    </div>
                    {{Form::submit('Add Department',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                    <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('js')
    @parent
    <script>

    $('#add-department').on('hidden.bs.modal', function(e) {
        $(this).find('#department_create')[0].reset();
    });

    $( "#department_create" ).on( "submit", function( event ) {
        event.preventDefault();
        swal({
            title: "Submit Department",
            text: "Are you sure you want to submit this department?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                var form = document.getElementById('department_create');
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: '{{ route('departments.store') }}',
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(msg) {
                        return msg;
                    }
                });
            }
        }).then((result) => {
            $('#add-department').modal('toggle');
            if(result.value){
                $('#dept_table').DataTable().ajax.reload(function(json){
                    var len = json.data.length;
                    $("#departments").empty();
                    $("#designation_edit #department_id").empty();
                    $("#departments").append("<option value=''>Please Select</option>");
                    $("#designation_edit #department_id").append("<option value=''>Please Select</option>");
                    for( var i = 0; i<len; i++){
                        var id = json.data[i]['id'];
                        var name = json.data[i]['department_name'];
                        $("#departments").append("<option value='"+id+"'>"+name+"</option>");
                        $("#designation_edit #department_id").append("<option value='"+id+"'>"+name+"</option>");

                    }
                });

                //reload select designation


                swal('New Department Added!', '--', 'success');
            }
        });
    });
    </script>
@endsection
