<?php

namespace App\Http\Controllers;

use App\EmployeeEducation;
use App\Employee;
use Illuminate\Http\Request;
use DataTables;

class EmployeeEducationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Employee $employee)
    {
        return Datatables::eloquent(EmployeeEducation::where('employee_id', $employee->id))
        ->addColumn('period', function($model){
            return $model->started_at." - ".$model->graduated_at;
        })
        ->addColumn('action', function($model) use ($employee){
            $model_2 = $employee;
            $modal_view_id = 'modal_education_show';
            $modal_edit_id = 'modal_education_edit';
            $url = 'employees.educations';
            $show_hide = true;
            return view('shared._table_action', compact('model', 'url', 'modal_view_id', 'modal_edit_id', 'model_2', 'show_hide'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $education = EmployeeEducation::create($request->all());
        $employee->educations()->save($education);
        return response()->json($education, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeEducation  $employeeEducation
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeEducation $employeeEducation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeEducation  $employeeEducation
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeEducation $employeeEducation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeEducation  $employeeEducation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee, EmployeeEducation $education)
    {
        $education->update($request->all());
        return response()->json($education, 201);//
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeEducation  $employeeEducation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee, EmployeeEducation $education)
    {
        $education->delete();
        return redirect()->back();
    }
}
