{{-- Modal --}}
<div class="modal fade" id="modal_visa_edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h1>Edit Visa</h1>

                {!! Form::open(['url' => '', 'id' => 'visa_edit']) !!}
                <input type="hidden" name="_method" value="PUT">
                <input id="visa_id" name="id" type="hidden">
                <div class="form-group row">
                    <div class="col-md-6">
                        {{ Form::text('visa_owner',null,['class' => 'form-control', 'placeholder' => 'Visa Owner'])}}
                    </div>
                    <div class="col-md-6">
                        <select name="country" class="selectpicker" data-style="form-control">
                            <option selected disabled>Country</option>
                            @foreach ($country as $ct )
                            <option value="{{ $ct }}">{{ $ct }}</option>
                            @endforeach
                        </select>
                        {{-- {{ Form::text('country',null,['class' => 'form-control', 'placeholder' => 'Country Origin'])}} --}}
                    </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            {{ Form::text('type',null,['class' => 'form-control', 'placeholder' => 'Visa Type'])}}
                        </div>
                        <div class="col-md-6">
                            {{ Form::date('entry_date',null,['class' => 'form-control', 'placeholder' => 'Entry Date'])}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            {{ Form::text('nationality',null,['class' => 'form-control', 'placeholder' => 'Nationality'])}}
                        </div>
                    </div>
                    <button type="submit" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">
                        Save Visa
                    </button>
                    <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close
                    </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('js')
@parent
<script>
    $("#visa_edit").on("submit", function (event) {
        //console.log('mew');
        event.preventDefault();
        swal({
            title: "Update",
            text: "Are you sure you want to update?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    var form = document.getElementById('visa_edit');
                    var formData = new FormData(form);
                    console.log(form);
                    $.ajax({
                        type: "POST",
                        url: "/visas/" + form.visa_id.value,
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            resolve(data);
                        },
                        error: function () {
                            swal.showValidationMessage('server error');
                            swal.hideLoading();
                        }
                    });
                });
            }
        }).then((result) => {
            if (result.value) {
                $('#modal_visa_edit').modal('toggle');
                $('#visa_table').DataTable().ajax.reload();
                swal('Updated!', '--', 'success');

            }

        });
    });
</script>
@endsection
