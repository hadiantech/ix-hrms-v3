<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hierarchy extends Model
{
    protected $table = 'hierarchies';
    //
    protected $fillable = ['hierarchy_name','hierarchy_ranking'];

    public function employee(){

        return $this->hasMany('App\Employee');

        }
}
