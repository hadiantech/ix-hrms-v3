<div class="modal fade" id="set-salary">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    {!! Form::open(['url' => 'adminpayslips', "id" => "set_salary",]) !!}
                <h1>Set Salary</h1>
               <div class="form-group">
                    {{Form::text('employeename', null , ['class' => 'form-control','placeholder' => 'Employee'])}}
               </div>
               <div class="form-group row">
                <div class="col-md-6">
                    {!! Form::label('Set Salary') !!}
                    {{Form::text('newsalary', null , ['required','class' => 'form-control','placeholder' => 'Set salary. E.g 1,200.00'])}}
                </div>
                <div class="col-md-6">
                    {!! Form::label('Type of Salary') !!}
                    <select class="selectpicker" name="payment" data-style="form-control">
                    <option>Monthly</option>
                    <option>Hourly</option>
                 </select>
                </div>
               </div>

               <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Save Salary</button>
                            <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
