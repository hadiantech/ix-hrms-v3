@extends('layouts.employee.master')

@section('content')

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Notice</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                <ol class="breadcrumb">
                    <li><a href="dashboard-adminview.html">Dashboard</a></li>
                    <li class="active">Notice</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <!-- ============================================================== -->
        <!-- Different data widgets -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-sm-12">


                <div class="white-box">
                    <h3 class="box-title">Notice Listings
                         </h3>
                    <div class="table-responsive">

                        <table  id="myTable" class="table display">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Notice Title</th>

                                    <th width="190px" class="text-center">Published Date</th>
                                    <th width="120px" class="text-right">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Eid 2018 : IX One Holiday</td>

                                    <td class="text-center">14 June 2018</td>

                                    <td class="text-right"><a data-toggle="modal" href="#view-notice" class="m-r-10 label label-info"><i class="fa fa-eye"></i></a> </td>
                                </tr>
                                {{-- <tr>
                                    <td>2</td>
                                    <td>IX Pantry : Reminder on Cleanliness</td>

                                    <td class="text-center">14 January 2018</td>

                                    <td class="text-right"><a class="m-r-10 label label-info"><i class="fa fa-eye"></i></a> </td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>Fire Drill : CoPlace 2</td>

                                    <td class="text-center">03 December 2017</td>

                                    <td class="text-right"><a class="m-r-10 label label-info"><i class="fa fa-eye"></i></a> </td>
                                </tr> --}}


                            </tbody>
                        </table>

                    </div>
                    <ul class="pagination pagination-split m-t-10">
                        <li> <a href="#"><i class="fa fa-angle-left"></i></a> </li>
                        <li class="disabled"> <a href="#">1</a> </li>
                        <li class="active"> <a href="#">2</a> </li>
                        <li> <a href="#">3</a> </li>
                        <li> <a href="#">4</a> </li>
                        <li> <a href="#">5</a> </li>
                        <li> <a href="#"><i class="fa fa-angle-right"></i></a> </li>
                    </ul>
                </div>
                {{-- modal show notice --}}
                @include('employees.notices.show')

            </div>
        </div>
        <!--row -->




    </div>
    <!-- /.container-fluid -->
</div>
@endsection

@section('js')
@endsection
