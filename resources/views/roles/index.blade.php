@extends('layouts.default.master')

@section('content')
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Role</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                <ol class="breadcrumb">
                    <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li class="active">Role</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        {{-- Role List --}}
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title">Role Listings
                        <a href="{{ route('roles.create') }}" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Add Role</a>
                    </h3>
                    <div class="table-responsive">
                        <table  id="roles_table" class="">
                            <thead>
                                <tr>
                                    <th style="width: 5%">#</th>
                                    <th>Role Name</th>
                                    <th>Created At</th>
                                    <th style="width: 20%">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- /.container-fluid -->
</div>

@endsection

@section('js')
@parent

<script>
$(function () {

    $('#roles_table').DataTable({
        serverSide: true,
        processing: true,
        ajax: '/roles/list',
        columns: [
            {data: 'DT_Row_Index', orderable: false, searchable: false},
            {data: 'name'},
            {data: 'created_at'},
            {data: 'action', orderable: false, searchable: false}
        ]
    });

});

</script>

@endsection





