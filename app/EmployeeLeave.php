<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class EmployeeLeave extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $guarded = [];
    protected $table = 'employee_leaves';

    public function scopeType($query, $type){
        return $query->where('leave_type', $type);
    }

    public function employee(){
        return $this->belongsTo(Employee::class,'employee_id', 'id');
    }

}
