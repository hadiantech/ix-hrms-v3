<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claims', function (Blueprint $table) {
            $table->increments('id');
            $table->longtext('claim_number')->nullable();
            $table->string('claim_month')->nullable();
            $table->longtext('claim_type')->nullable();
            $table->date('claim_applied_date')->nullable();
            $table->longtext('claim_status')->nullable();
            $table->float('claim_total')->nullable();
            $table->integer('project_id')->nullable();
            $table->longtext('claim_remark')->nullable();
            $table->string('paid_status')->nullable();
            $table->integer('req_layer')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claims');
    }
}
