@php
    if(!isset($c_placeholder)){
        $placeholder = 'Please Select';
    }

    if(!isset($c_class)){
        $class = 'form-control';
    }

    if(!isset($default_value)){
        $default_value = null;
    }

    if(!isset($c_name)){
        $c_name = 'country';
    }

    if(!isset($data)){
        $data = [];
    }
@endphp

@if($default_value != null)
@javascript('default_value', $default_value)
@else
@javascript('default_value', null)
@endif

{{ Form::select($c_name, $data, $default_value, ['id' => 'country_list', 'placeholder' => $placeholder, 'class' => $class]) }}

@section('js')
    @parent
    <script>
    $(document).ready(function(){
        $.ajax({
            url: '/api/countries',
            type: 'GET',
            dataType: 'json',
            success:function(response){
                var len = response.length;
                $("#country_list").empty();
                $("#country_list").append("<option value=''>Please Select</option>");
                for( var i = 0; i<len; i++){
                    var name = response[i];
                    $("#country_list").append("<option value='"+name+"'>"+name+"</option>");
                }

                $("#country_list").val(default_value).change();
            }
        });
    });

    </script>
@endsection
