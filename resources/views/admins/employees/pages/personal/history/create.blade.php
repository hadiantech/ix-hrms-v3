<div class="modal fade" id="add-history" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Add Employment</h1>

                {!! Form::open(['url' => '', 'id' => 'history_create']) !!}
                <div class="form-group  ">
                    {!! Form::label('Company Name') !!}
                    {!! Form::text('company_name', null, array('class' => 'form-control', 'placeholder' => 'Company Name', 'required')) !!}
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        {!! Form::label('Position') !!}
                        {!! Form::text('company_position', null, array('class' => 'form-control', 'placeholder' => 'Position', 'required')) !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('Company Contact Number') !!}
                        {!! Form::text('company_contact', null, array('class' => 'form-control', 'placeholder' => 'Company Contact Number', 'required')) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        {!! Form::label('Started At') !!}
                        {!! Form::date('started_at', null, array('class' => 'form-control', 'placeholder' => 'Started At', 'required')) !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('Ended At') !!}
                        {!! Form::date('ended_at', null, array('class' => 'form-control', 'placeholder' => 'Ended At', 'required')) !!}
                    </div>
                </div>

                {{ Form::submit('Add Employment',['class' =>'btn btn-rounded btn-info text-uppercase']) }}
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>


@section('js')
    @parent

    <script>

        $('#add-history').on('hidden.bs.modal', function(e) {
            $(this).find('#history_create')[0].reset();
        });

        $( "#history_create" ).on( "submit", function( event ) {
            event.preventDefault();
            swal({
                title: "Submit Employment History",
                text: "Are you sure you want to submit this history?",
                type: "info",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                allowOutsideClick: () => !swal.isLoading(),
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        var form = document.getElementById('history_create');
                        var formData = new FormData(form);
                        $.ajax({
                            type: "POST",
                            url: '{{ route('employees.histories.store', $employee->id) }}',
                            data: formData,
                            cache:false,
                            contentType: false,
                            processData: false,
                            success: function(data) {
                                resolve(data);
                            },
                            error: function(data) {
                                errormsg = '';
                                if(data.status == 422 ){
                                    $.each(data.responseJSON.errors, function (key, value) {
                                        errormsg = errormsg + value[0] + "<br />";
                                    });
                                }else{
                                    console.log(data);
                                    errormsg = "Server error. Please try again";
                                }
                                swal.showValidationMessage(errormsg);
                                swal.hideLoading();
                            },
                        });
                    });

                }
            }).then((result) => {
                if(result.value){
                    $('#add-history').modal('toggle');
                    swal('History Saved!', '--', 'success');
                    $('#history_table').DataTable().ajax.reload();
                }
            });
        });

    </script>

@endsection
