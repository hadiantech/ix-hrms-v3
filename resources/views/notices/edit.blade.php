@extends('layouts.default.master')

@section('content')
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Role</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                <ol class="breadcrumb">
                    <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li><a href="{{ route('notices.index') }}">Role</a></li>
                    <li class="active">Edit Role: {{ $notices->name }}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
                {{-- Notice Show --}}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title">[Notice / Memo] Update

                            <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                                {!! Form::model($notices, ['url' => route('notices.update', ['id' => $notices->id]),'method' => 'PUT']) !!}
                                            <div class="row">
                                                <div class="col-sm-12">

                                            {{-- {{ Form::hidden('id', null , ['id' => 'id']) }} --}}
                                            <div class="form-group ">
                                                    <div class="col-md-6">
                                                            <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">NOTICE STATUS</h5>
                                                            {!! Form::select('status', ['Draft'=>'Draft', 'Publish'=>'Publish'], null,['class'=>'form-control']) !!}
                                                    </div>
                                            </div>

                                            <div class="form-group ">
                                                    <div class="col-md-12">

                                                            <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">NOTICE TITLE</h5>
                                                            {{Form::text('title', null , ['id' => 'title', 'required','class' => 'form-control','placeholder' => 'Notice Title'])}}
                                                        </div>
                                            </div>
                                            <div class="form-group ">
                                                    <div class="col-md-12">
                                                        <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">NOTICE DESCRIPTION</h5>
                                                        <form>
                                                                <textarea class="ckeditor" name="editor" id="editor" rows="10" cols="80">
{{ $notices->description }}}}
                                                                    </textarea>
                                                        </form>
                                                        <hr>
                                                    </div>
                                            </div>
                                            <div class="form-group ">
                                                        <div class="col-md-7">
                                                            <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">CREATED BY</h5>
                                                            <ul class="side-icon-text">
                                                                <li> @if(Auth::user()->getMedia('profile')->first())
                                                                        <img src="{{ Auth::user()->getMedia('profile')->first()->getUrl() }}" alt="user-img" width="36" class="img-circle">
                                                                        @else
                                                                        <img src="{{ asset('images/Avatar.png') }}" alt="user-img" width="36" class="img-circle">
                                                                        @endif<span class="di vm"><h5 class="m-b-0">{{ $notices->employee->fname }} {{ $notices->employee->lname }}</h5>
                                                                </li>
                                                            </ul>
                                                        </div>
                                            </div>
                                            <div class="form-group ">
                                                    <div class="col-md-7">
                                                        <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">PUBLISHED DATE: {{ $notices->created_at }}</h5>
                                                    </div>
                                            </div>

                                        </div>
                                        <div class="form-group ">
                                                <div class="col-md-7">
                                        {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}

                                        {!! Form::close() !!}
                                                </div>
                                            </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--row -->

        </div>
            <!-- /.container-fluid -->
        </div>

        @endsection

        //end
    @section('js')
    @parent
    <script src="{{ URL::asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}" type="text/javascript"></script>
    <script>
        var data = CKEDITOR.instances.editor.getData();
    </script>
        <script>

        $( "#notice_edit" ).on( "submit", function( event ) {
            event.preventDefault();
            swal({
                title: "Update Notice",
                text: "Are you sure you want to update this notice?",
                type: "info",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                preConfirm: function() {
                    var form = document.getElementById('notice_edit');
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url: '/notices/'+form.id.value,
                        data: formData,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: function(msg) {
                            return msg;
                        }
                    });
                }
            }).then((result) => {
                $('#notice_edit').modal('toggle');
                if(result.value){
                    $('#notice_table').DataTable().ajax.reload();
                    swal('Notice Updated!', '--', 'success');
                }
            });
        });
        </script>
    @endsection
