<?php

namespace App\Http\Controllers;

use App\Emergency;
use Illuminate\Http\Request;
use App\Employee;
use App\LeaveType;
use App\Annual;
use App\EmployeeLeave;
use Auth;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Postmark\PostmarkClient;
use App\Approval;

class EmergencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $employee = Employee::lists(['id']);
        $leavetype = LeaveType::lists(['id']);
        return view('employees.leaves.emergencies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = new PostmarkClient("73a69c7d-8c56-4f59-877c-74d05cd5e247");


        $leaves = EmployeeLeave::where('employee_id', Auth::user()->id)->get();
        $approval_lv1 = Approval::where('employee_id', Auth::user()->id)->where('level',1)->first();
        $approval_lv2 = Approval::where('employee_id', Auth::user()->id)->where('level',2)->first();
        //return $approval->approvedBy;

        $aid = Auth::user()->id;
        $employees = Employee::find($aid);

        $annual_leave_quota = Auth::user()->leave_allocs()->type('Annual Leave')->first()->quota;
        $annual_leave_taken = Auth::user()->leaves()->type('Annual Leave')->sum('total_days');
        $balance = $annual_leave_quota - $annual_leave_taken;

        // $a = Carbon::parse($request->started_at);
        // $b = Carbon::parse($request->ended_at);
        // $days = $b->diffInDays($a);
        // $days = $days + 1;

        // $unpaid = $balance - $days;

        $emergency = new EmployeeLeave;
        $emergency->employee_id = Auth::user()->id;
        $emergency->leave_type = 'Annual Leave';
        $emergency->remark = 'Emergency';
        $emergency->started_at = $request->started_at;
        $emergency->ended_at = $request->ended_at;
        $emergency->reason = $request->reason;
        $emergency->total_days = $request->erow;
        // $emergency->as_unpaid = $unpaid;

        $emergency->status = 'Pending';
        $emergency->leave_proc_status = 'unproc';

        $emergency->save();
        $leave = $emergency->id;

        $sendResult = $client->sendEmailWithTemplate(

            "hassanul@boneybone.com",
            $approval_lv1->approvedBy->email,
            10030123,
                [
                    "system_name" => "[IX HRMS] Leave Application Approval",
                    "fname" => $employees->fname,
                    "lname" => $employees->lname,
                    "uid" => $employees->employee_uid,
                    "approve" =>  $approval_lv1->approvedBy->fname,
                    "approver" =>  $approval_lv1->approvedBy->lname,
                    "leave_type" => 'Emergency Leave',
                    "started_at" => $request->started_at,
                    "ended_at" => $request->ended_at,
                    "reason" => $request->reason,
                    "accept" => route('adminleaves.update_email',$leave),
                    "reject" => route('adminleaves.reject_email',$leave),
                    "link" => url('http://hrms.ixtelecom.net')
                ]);

            $sendResult = $client->sendEmailWithTemplate(

                    "hassanul@boneybone.com",
                    $approval_lv2->approvedBy->email,
                    10030123,
                        [
                            "system_name" => "[IX HRMS] Leave Application Approval",
                            "fname" => $employees->fname,
                            "lname" => $employees->lname,
                            "uid" => $employees->employee_uid,
                            "approve" =>  $approval_lv2->approvedBy->fname,
                            "approver" =>  $approval_lv2->approvedBy->lname,
                            "leave_type" => 'Emergency Leave',
                            "started_at" => $request->started_at,
                            "ended_at" => $request->ended_at,
                            "reason" => $request->reason,
                            "accept" => route('adminleaves.approve_email',$leave),
                            "reject" => route('adminleaves.reject_email',$leave),
                            "link" => url('http://hrms.ixtelecom.net')
                        ]);
        return response()->json($emergency, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Emergency  $emergency
     * @return \Illuminate\Http\Response
     */
    public function show(Emergency $emergency)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Emergency  $emergency
     * @return \Illuminate\Http\Response
     */
    public function edit(Emergency $emergency)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Emergency  $emergency
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Emergency $emergency)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Emergency  $emergency
     * @return \Illuminate\Http\Response
     */
    public function destroy(Emergency $emergency)
    {
        //
    }
}
