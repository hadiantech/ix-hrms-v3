<section id="personalinfo">
    <div class="row">
            {!! Form::model($employees, ['route' => ['employees.update', $employees->id], 'method' => 'PUT']) !!}
        <div class="col-md-4 text-center m-b-40">
            <img src="https://cdn.iconscout.com/public/images/icon/free/png-512/avatar-user-teacher-312a499a08079a12-512x512.png" class="profile-photo">
            <br>
            <button class="m-t-30 btn btn-rounded btn-info text-uppercase">Upload Photo</button>
        </div>
        <div class="col-md-8">
                <div class="form-group row">
                <div class="col-md-6">
                    {!! Form::text('fname', $employees->fname, array('class' => 'form-control', 'placeholder' => 'First Name')) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::text('lname', $employees->lname, array('class' => 'form-control','placeholder' => 'Last Name')) !!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-4">
                    <div class="input-group">
                        <input type="text" class="form-control datepicker-autoclose" value="Date of Birth">
                        <span class="input-group-addon">
                            <i class="icon-calender"></i>
                        </span>
                    </div>
                </div>
                <div class="col-md-4">
                    {{Form::select('gender', ['Male' => 'Male', 'Female' => 'Female'], null, ['class' => 'form-control', 'placeholder' => 'Gender'])}}
                </div>
                <div class="col-md-4">
                    {{Form::select('marriage_status', ['Single' => 'Single', 'Married' => 'Married' , 'Widowed' => 'Widowed'], null, ['class' => 'form-control', 'placeholder' => 'Marital Status'])}}
                </div>
            </div>
            <div class="form-group row">

                <div class="col-md-6">
                    {{Form::select('race', ['Melayu' => 'Melayu', 'Chinese' => 'Chinese' , 'India' => 'India'], null, ['class' => 'form-control', 'placeholder' => 'Race'])}}
                </div>
                <div class="col-md-6">
                    {{Form::select('religion', ['Islam' => 'Islam', 'Others' => 'Others' ], null, ['class' => 'form-control', 'placeholder' => 'Religion'])}}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    {!! Form::text('ic', $employees->ic, array('class' => 'form-control','placeholder' => 'IC Number')) !!}
                </div>
                {{-- <div class="col-md-6"> --}}
                    {{-- <input type="text" class="form-control" placeholder="Phone Number"> --}}
                    {{-- {!! Form::text('phone_number', null, ['class' => 'form-control', 'placeholder'=>'Phone Number']) !!} --}}
                {{-- </div> --}}
            </div>
            <div class="employee-addresses m-b-30">
                <div class="form-group row">
                    <div class="col-md-12">
                        <h3 class="box-title">Permanent Address</h3>
                    </div>
                    <div class="col-md-6">
                            {!! Form::text('pre_address_line1', null, ['class' => 'form-control', 'placeholder'=>'Address Line 1', 'id' =>'pre_add1']) !!}
                    </div>
                    <div class="col-md-6">
                            {!! Form::text('pre_address_line2', null, ['class' => 'form-control', 'placeholder'=>'Address Line 2','id' =>'pre_add2']) !!}
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                            {!! Form::text('pre_city', null, ['class' => 'form-control', 'placeholder'=>'City','id' =>'pre_city']) !!}
                    </div>
                    <div class="col-md-6">
                            {!! Form::text('pre_state', null, ['class' => 'form-control', 'placeholder'=>'State','id' =>'pre_state']) !!}
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                            {!! Form::text('pre_postcode', null, ['class' => 'form-control', 'placeholder'=>'Postcode','id' =>'pre_postcode']) !!}
                    </div>
                    <div class="col-md-6">
                            {!! Form::text('pre_country', null, ['class' => 'form-control', 'placeholder'=>'Country','id' =>'pre_country']) !!}
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                        {!! Form::text('pre_phone1', null, ['class' => 'form-control', 'placeholder'=>'Phone No.','id' =>'pre_phone1']) !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::text('pre_phone2', null, ['class' => 'form-control', 'placeholder'=>'Mobile No.','id' =>'pre_phone2']) !!}
                    </div>
                </div>


                <div class="form-group row">
                    <div class="col-md-12">
                        <h3 class="box-title">CORRESPONDANCE ADDRESS</h3>
                        <div class="m-b-15 checkbox checkbox-info">
                            <input id="corcheck" type="checkbox" onclick="copy()">
                            <label for="corcheck">Same as the above?</label>
                        </div>
                    </div>
                    <div class="col-md-6">
                            {!! Form::text('cor_address_line1', null, ['class' => 'form-control', 'placeholder'=>'Address Line 1', 'id' =>'cor_add1']) !!}
                    </div>
                    <div class="col-md-6">
                            {!! Form::text('cor_address_line2', null, ['class' => 'form-control', 'placeholder'=>'Address Line 2','id' =>'cor_add2']) !!}
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                            {!! Form::text('cor_city', null, ['class' => 'form-control', 'placeholder'=>'City','id' =>'cor_city']) !!}
                    </div>
                    <div class="col-md-6">
                            {!! Form::text('cor_state', null, ['class' => 'form-control', 'placeholder'=>'State','id' =>'cor_state']) !!}
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                            {!! Form::text('cor_postcode', null, ['class' => 'form-control', 'placeholder'=>'Postcode','id' =>'cor_postcode']) !!}
                    </div>
                    <div class="col-md-6">
                            {!! Form::text('cor_country', null, ['class' => 'form-control', 'placeholder'=>'Country','id' =>'cor_country']) !!}
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                        {!! Form::text('cor_phone1', null, ['class' => 'form-control', 'placeholder'=>'Phone No.','id' =>'cor_phone1']) !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::text('cor_phone2', null, ['class' => 'form-control', 'placeholder'=>'Mobile No.','id' =>'cor_phone2']) !!}
                    </div>
                </div>
            </div>
            <div class="bank-info   m-b-30">
                <div class="section-title">
                    <h3 class="box-title">Banking Info</h3>

                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                        {!! Form::text('bank_name', null, ['class' => 'form-control', 'placeholder'=>'Bank Name']) !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::text('bank_account_no', null, ['class' => 'form-control', 'placeholder'=>'Account No']) !!}
                    </div>
                </div>
            </div>
            <div class="family-info  m-b-30">
                <div class="section-title">
                    <h3 class="box-title">Spouse, Children & Others</h3>

                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr style="border-bottom:3px solid #eee">
                                <th>#</th>
                                <th>Name</th>
                                <th class="text-center">MyKAD/Passport No.</th>
                                <th class="text-center">Relation</th>
                                <th class="text-center">DOB</th>
                                <th class="text-right">Action</th>
                            </tr>
                        </thead>
                        <tbody>


                            <tr style="border-bottom:3px solid #eee">
                                <td colspan="6">
                                    <a href="#add-family" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Family</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="employment-info m-b-30">
                <div class="section-title">
                    <h3 class="box-title">Employment Background</h3>

                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr style="border-bottom:3px solid #eee">
                                <th>#</th>
                                <th>Company</th>
                                <th class="text-center">Position</th>
                                <th class="text-center">Contact</th>
                                <th class="text-center">Period</th>
                                <th class="text-right">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr style="border-bottom:3px solid #eee">
                                <td colspan="6">
                                    <a href="#add-employment" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Employment</a>
                                </td>

                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="education-info m-b-30">
                <div class="section-title">
                    <h3 class="box-title">Education Background</h3>

                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr style="border-bottom:3px solid #eee">
                                <th>#</th>
                                <th>Field of Study</th>
                                <th class="text-center">University</th>
                                <th class="text-center">Period</th>
                                <th class="text-right">Action</th>
                            </tr>
                        </thead>
                        <tbody>


                            <tr style="border-bottom:3px solid #eee">
                                <td colspan="6">
                                    <a href="#add-educational" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Education</a>
                                </td>

                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="family-info  m-b-30">
                <div class="section-title">
                    <h3 class="box-title">Professional Certificate</h3>

                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr style="border-bottom:3px solid #eee">
                                <th>#</th>
                                <th class="text-left">Certificate Field & Name</th>
                                <th class="text-center">Expiry</th>
                                <th class="text-center">Issued By</th>
                                <th class="text-center">Ref No.</th>
                                <th class="text-right">Action</th>
                            </tr>
                        </thead>
                        <tbody>


                            <tr style="border-bottom:3px solid #eee">
                                <td colspan="6">
                                    <a href="#add-cert" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Certificate</a>
                                </td>

                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            {{Form::submit('Save',['class'=>'btn-lg btn-rounded btn-info text-uppercase','style'=>'width:200px'])}}

        </div>
        {!! Form::close() !!}
    </div>
</section>

{{-- Modal Here --}}

<div class="modal fade" id="add-family" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Add Family</h1>

                    <div class="form-group  ">

                        <input type="text" class="form-control" placeholder="Full Name">

                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="Relationship">
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="MyKAD No./Passport No.">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="Phone No.">
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" class="form-control datepicker-autoclose" placeholder="Date of Birth">
                                <span class="input-group-addon">
                                    <i class="icon-calender"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Add Family</button>
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add-cert" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Add Professional Certificate</h1>

                    <div class="form-group row ">
                        <div class="col-md-6">
                            <select class="selectpicker" data-style="form-control">
                                <option selected disabled>Field</option>
                                <option>Engineering</option>
                                <option>Marketing</option>
                                <option>Social</option>
                                <option>Accounting</option>
                                <option>Finance</option>
                                <option>Management</option>
                                <option>Others</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="If others, please specify field...">
                        </div>

                    </div>
                    <div class="form-group ">

                        <input type="text" class="form-control" placeholder="Certificate Name">

                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="Issued By">
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" class="form-control datepicker-autoclose" placeholder="Expiry Date">
                                <span class="input-group-addon">
                                    <i class="icon-calender"></i>
                                </span>
                            </div>
                        </div>

                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="Reference No.">
                        </div>
                        <div class="col-md-6">


                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput">
                                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                    <span class="fileinput-filename"></span>
                                </div>
                                <span class="input-group-addon btn btn-default btn-file">
                                    <span class="fileinput-new">Select file</span>
                                    <span class="fileinput-exists">Change</span>
                                    <input type="file" name="..."> </span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>
                    </div>
                <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Add Certificate</button>
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add-employment" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Add Employment</h1>

                    <div class="form-group  ">

                        <input type="text" class="form-control" placeholder="Company Name">

                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="Position">
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="Contact No.">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" class="form-control datepicker-autoclose" placeholder="Date from">
                                <span class="input-group-addon">
                                    <i class="icon-calender"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" class="form-control datepicker-autoclose" placeholder="Date to">
                                <span class="input-group-addon">
                                    <i class="icon-calender"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Add Employment</button>
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add-educational" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Add Education</h1>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="Qualification">
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="Field of Study">
                        </div>
                    </div>
                    <div class="form-group">

                        <input type="text" class="form-control" placeholder="University/Institution">

                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" class="form-control datepicker-autoclose" placeholder="Date from">
                                <span class="input-group-addon">
                                    <i class="icon-calender"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" class="form-control datepicker-autoclose" placeholder="Date to">
                                <span class="input-group-addon">
                                    <i class="icon-calender"></i>
                                </span>
                            </div>
                        </div>
                    </div>

                <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Add Education</button>
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

@section('js')
@parent
<script>

    function copy(){
        // Get the checkbox
        var checkBox = document.getElementById("corcheck");

        // If the checkbox is checked, display the output text
        if (checkBox.checked == true){
            $('#cor_add1').val( $('#pre_add1').val() );
            $('#cor_add1').prop('disabled', true);
            $('#cor_add2').val( $('#pre_add2').val() );
            $('#cor_add2').prop('disabled', true);
            $('#cor_city').val( $('#pre_city').val() );
            $('#cor_city').prop('disabled', true);
            $('#cor_state').val( $('#pre_state').val() );
            $('#cor_state').prop('disabled', true);
            $('#cor_postcode').val( $('#pre_postcode').val() );
            $('#cor_postcode').prop('disabled', true);
            $('#cor_phone1').val( $('#pre_phone1').val() );
            $('#cor_phone1').prop('disabled', true);
            $('#cor_phone2').val( $('#pre_phone2').val() );
            $('#cor_phone2').prop('disabled', true);
            $('#cor_country').val( $('#pre_country').val() );
            $('#cor_country').prop('disabled', true);


        } else {
            $('#cor_add1').val('');
            $('#cor_add1').prop('disabled', false)
            $('#cor_add2').val('');
            $('#cor_add2').prop('disabled', false);
            $('#cor_city').val('');
            $('#cor_city').prop('disabled', false);
            $('#cor_state').val('');
            $('#cor_state').prop('disabled', false);
            $('#cor_postcode').val('');
            $('#cor_postcode').prop('disabled', false);
            $('#cor_phone1').val('');
            $('#cor_phone1').prop('disabled', false);
            $('#cor_phone2').val('');
            $('#cor_phone2').prop('disabled', false);
            $('#cor_country').val('');
            $('#cor_country').prop('disabled', false);

        }
    }


</script>
@endsection
