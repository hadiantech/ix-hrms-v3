<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class EmployeeDocument extends Model implements HasMedia
{
    use HasMediaTrait;
    protected $guarded = [];

    public function scopeShared($query, $lvl){
        return $query->where('shared', '1');
    }
}
