{{-- Modal --}}
<div class="modal fade" id="modal_traveller_create">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h1>Add Traveller</h1>

                {!! Form::open(['url' => '', 'id' => 'traveller_create']) !!}

                <input name="advance_claims_id" type="hidden" value="{{ $claim->id }}" />

                <div class="form-group row">
                    <div class="col-md-6">
                        {{ Form::text('name',null,['class' => 'form-control', 'placeholder' => 'Traveller Name'])}}
                    </div>
                    <div class="col-md-6">
                        {{ Form::text('phone',null,['class' => 'form-control', 'placeholder' => 'Phone'])}}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        {{ Form::text('email',null,['class' => 'form-control', 'placeholder' => 'Email'])}}
                    </div>
                    <div class="col-md-6">
                        {{ Form::text('mykad',null,['class' => 'form-control', 'placeholder' => 'Mykad'])}}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        {{ Form::text('passport',null,['class' => 'form-control', 'placeholder' => 'Passport'])}}
                    </div>
                    <div class="col-md-6">
                        {{ Form::date('passport_expired',null,['class' => 'form-control', 'placeholder' => 'Passport Expiration Date'])}}
                    </div>
                </div>
                <button type="submit" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">
                    Add Traveller
                </button>
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close
                </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('js')
@parent
<script>
    $("#traveller_create").on("submit", function (event) {
        console.log('mew');
        event.preventDefault();
        swal({
            title: "Create New Traveller",
            text: "Are you sure you want to create?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function(resolve, reject){
                    var form = document.getElementById('traveller_create');
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url:"{{ route('travellers.store') }}",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            resolve(data);
                        },
                        error: function(){
                            swal.showValidationMessage('server error');
                            swal.hideLoading();
                        }
                    });
                });
            }
        }).then((result) => {
            if (result.value) {
                $('#modal_traveller_create').modal('toggle');
                $('#traveller_create').trigger("reset");
                $('#traveller_table').DataTable().ajax.reload();
                swal('New Traveller Added!', '--', 'success');

            }

        });
    });
</script>


@endsection


