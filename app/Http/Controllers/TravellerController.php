<?php

namespace App\Http\Controllers;

use App\Traveller;
use DataTables;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TravellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function list($id){
        return Datatables::eloquent(Traveller::where(['advance_claims_id' => $id]))
            ->addColumn('details', function($model){
                return $model->name.'<br/>'.$model->email.'<br/>'.$model->phone;
            })
            ->addColumn('action', function($model){

                if($model->claim->claim_status == "submit"){
                    $delete_hide = true;
                    $edit_hide = true;
                }
                else{
                    $delete_hide = false;
                    $edit_hide = false;
                }

                $show_hide = true;
                $modal_edit_id = 'modal_traveller_edit';
                $url = 'travellers';
                return view('shared._table_action', compact('model', 'url','show_hide','modal_edit_id','delete_hide','edit_hide'))->render();
            })
            ->escapeColumns([])
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $traveller = Traveller::create($request->all());

        return response()->json($traveller, 201);


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Traveller  $traveller
     * @return \Illuminate\Http\Response
     */
    public function show(Traveller $traveller)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Traveller  $traveller
     * @return \Illuminate\Http\Response
     */
    public function edit(Traveller $traveller)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Traveller  $traveller
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Traveller $traveller)
    {
        $traveller->update($request->all());

        return response()->json($traveller, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Traveller  $traveller
     * @return \Illuminate\Http\Response
     */
    public function destroy(Traveller $traveller)
    {
        $traveller->delete();
        return redirect()->back();
    }
}
