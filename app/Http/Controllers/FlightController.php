<?php

namespace App\Http\Controllers;

use App\Flight;
use DataTables;
use Carbon\Carbon;
use Illuminate\Http\Request;

class FlightController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function list($id){
        return Datatables::eloquent(Flight::where(['advance_claims_id' => $id]))
            ->addColumn('details', function($model){
                return $model->name.'<br/>'.$model->email.'<br/>'.$model->phone;
            })
            ->addColumn('action', function($model){

                if($model->claim->claim_status == "submit"){
                    $delete_hide = true;
                    $edit_hide = true;
                }
                else{
                    $delete_hide = false;
                    $edit_hide = false;
                }

                $show_hide = true;
                $modal_edit_id = 'modal_flight_edit';
                $url = 'flights';
                return view('shared._table_action', compact('model', 'url','show_hide','modal_edit_id','delete_hide','edit_hide'))->render();
            })
            ->escapeColumns([])
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $flight = Flight::create($request->all());
        return response()->json($flight, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Flights  $flights
     * @return \Illuminate\Http\Response
     */
    public function show(Flight $flight)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Flights  $flights
     * @return \Illuminate\Http\Response
     */
    public function edit(Flight $flight)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Flights  $flights
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Flight $flight)
    {
        $flight->update($request->all());

        return response()->json($flight, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Flights  $flights
     * @return \Illuminate\Http\Response
     */
    public function destroy(Flight $flight)
    {
        //
    }
}
