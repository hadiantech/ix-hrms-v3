<div id="modal_organisations_show" class="modal fade in" tabindex="-1" role="dialog" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    <h1>Company Details</h1>
                    <img width="50px" height="50px" id="company_image" src="{{ asset('images/Image.png') }}"/>

                    <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">COMPANY NAME</h5>
                    <h5><span style="font-weight:500" id="company_name"></span></span></h5>
                    <hr>
                    <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">COMPANY REGISTRY NUMBER</h5>
                    <h5><span style="font-weight:500" id="reg_num"></span></span></h5>
                    <hr>
                    <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">COMPANY ADDRESS</h5>
                    <h5><span style="font-weight:500"  id="address1"></span></h5>
                    <h5><span style="font-weight:500"  id="address2"></span></h5>
                    <h5><span style="font-weight:500"  id="state"></span></h5>
                    <h5><span style="font-weight:500"  id="city"></span></h5>
                    <h5><span style="font-weight:500"  id="country"></span></h5>
                    <hr>
                    <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">COMPANY CONTACTS</h5>
                    <h5>PHONE NUMBER : <span style="font-weight:500" id="number"></span></h5>
                    <h5>FAX NUMBER   : <span style="font-weight:500" id="fax"></span></h5>
                    <hr>
            </div>
        </div>
    </div>
</div>

