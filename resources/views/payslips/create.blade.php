<div class="modal fade" id="add-payroll-cat">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    {!! Form::open(['url' => 'payslips', "id" => "payroll_create",]) !!}
                    <h1>Add Payroll Category</h1>

                   <div class="form-group">
                        {!! Form::label('Payroll Category Name') !!}
                        {{Form::text('name', null , ['required','class' => 'form-control','placeholder' => 'Create Name'])}}
                    </div>

                    <div class="form-group row">
                        <div class="col-md-6">
                                {!! Form::label('Operand Select') !!}
                                <select class="form-control" name="operand_id" id="operand_id" data-parsley-required="true">
                                        @foreach ($operands as $operand)
                                            <option value="{{ $operand->id }}">{{ $operand->operand_name }}</option>
                                        @endforeach
                                </select>
                                {{-- {{Form::select('operand', ['Addition' => 'Addition', 'Deduction' => 'Deduction'], null, ['class' => 'form-control', 'placeholder' => 'Select Status'])}} --}}
                            </div>
                    </div>
               {{Form::submit('Create Category',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
               <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
               {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    @section('js')
    @parent
    <script>

    $('#add-category').on('hidden.bs.modal', function(e) {
        $(this).find('#payroll_create')[0].reset();
    });

    $( "#payroll_create" ).on( "submit", function( event ) {
        event.preventDefault();
        swal({
            title: "Payroll Category",
            text: "Confirm to payroll category?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                var form = document.getElementById('payroll_create');
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: '{{ route('payslips.store') }}',
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(msg) {
                        return msg;
                    }
                });
            }
        }).then((result) => {
            $('#add-category').modal('toggle');
            if(result.value){
                $('#payroll_table').DataTable().ajax.reload(function(json){
                    var len = json.data.length;
                    $("#payslips").empty();
                    $("#payslips").append("<option value=''>Please Select</option>");
                    for( var i = 0; i<len; i++){
                        var id = json.data[i]['id'];
                        var name = json.data[i]['title'];
                        $("#payslips").append("<option value='"+id+"'>"+name+"</option>");

                    }
                });

                //reload select designation


                swal('New Category Added!', '--', 'success');
            }
        });
    });
    </script>
    @endsection
