<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Claim;
use App\ClaimAdvance;
use App\ExpenseType;
use App\Expense;
use App\Employee;
use App\Organisation;
use App\Approval;
use Auth;
use Carbon\Carbon;
use DataTables;
use Postmark\PostmarkClient;

class AdminClaimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //get approval emp ids
        $emp_ids = Approval::where('approver_id', Auth::id())->type('claim')->pluck('employee_id');

        //get total unproc claim
        $claim_month_count = Claim::where(['claim_proc_status' => 'unproc','claim_status' => 'submit', 'claim_type' => 1])->whereIn('emp_id',$emp_ids)->count();

        //get total unproc advance claim
        $claim_advance_count = Claim::where(['claim_proc_status' => 'unproc','claim_status' => 'submit', 'claim_type' => 2])->whereIn('emp_id',$emp_ids)->count();

        //get total unproc request claim
        $claim_request_count = Claim::where(['claim_proc_status' => 'approved','claim_status' => 'submit', 'claim_type' => 2])->whereIn('emp_id',$emp_ids)->count();

        return view('admins.claims.index',compact('claim_month_count','claim_advance_count','claim_request_count'));
    }

    // Datatable for listing normal claim for Admin
    public function list($type){

        //get approval emp ids
        $emp_ids = Approval::where('approver_id', Auth::id())->type('claim')->pluck('employee_id');

        if($type == 'unproc'){
            $query = Claim::with(['employee'])->where(['claim_proc_status' => $type, 'claim_status' => 'submit', 'claim_type' => '1'])->whereIn('emp_id',$emp_ids);
        }
        else if($type == 'proc'){
            $query = Claim::with(['employee'])->where(['claim_proc_status' => $type, 'claim_status' => 'submit', 'claim_type' => '1'])->whereIn('emp_id',$emp_ids);
        }
        else if($type == 'approved'){
            $query = Claim::with(['employee'])->where(['claim_proc_status' => $type, 'claim_status' => 'submit', 'claim_type' => '1'])->whereIn('emp_id',$emp_ids);
        }
        else{
            return 'No claim type defined';
        }

        return Datatables::eloquent($query)
            ->addColumn('employee', function($model){
                return $model->employee->fname." ".$model->employee->lname;
            })
            ->addColumn('desc', function($model){
                return 'Normal Claim - '.Carbon::create($model->claim_month)->format('F');
            })
            ->addColumn('status', function($model){
                $status = $model->claim_proc_status;

                if($status == 'unproc'){
                    $status = 'Pending';
                    $class = 'default';
                }
                elseif($status == 'proc'){
                    $status = 'Reviewed';
                    $class = 'warning';
                }
                elseif($status == 'approved'){
                    $status = 'Approved';
                    $class = 'success';
                }
                elseif($status == 'returned'){
                    $status = 'Returned';
                    $class = 'danger';
                }
                else{
                    $class = 'primary';
                }
                return view('shared._button_color', compact('status', 'class'))->render();
            })
            ->addColumn('action', function($model){
                $url = 'adminclaims';
                $delete_hide = true;
                $edit_hide = true;
                return view('shared._table_action', compact('model', 'url','delete_hide','edit_hide'))->render();
            })
            ->escapeColumns([])
            ->addIndexColumn()
            ->make(true);

    }

    public function list_request($type){

        //get approval emp ids
        $emp_ids = Approval::where('approver_id', Auth::id())->type('advanced')->pluck('employee_id');

        if($type == 'approved_advance'){
            $query = Claim::with('Employee')->where(['claim_proc_status' => $type, 'claim_status' => 'draft', 'claim_type' => '2'])->whereIn('emp_id',$emp_ids);
        }
        else{
            $query = Claim::with('Employee')->where(['claim_proc_status' => $type, 'claim_status' => 'submit', 'claim_type' => '2'])->whereIn('emp_id',$emp_ids);
        }

        return Datatables::eloquent($query)
            ->addColumn('employee', function($model){
                $employee = Employee::find($model->emp_id);
                return $employee->fname." ".$employee->lname;
            })
            ->addColumn('desc', function($model){
                return 'Advance Request Claim - '.Carbon::create($model->claim_month)->format('F');
            })
            ->addColumn('status', function($model){
                $status = $model->claim_proc_status;

                if($status == 'unproc'){
                    $status = 'Pending';
                    $class = 'default';
                }
                elseif($status == 'proc'){
                    $status = 'Reviewed';
                    $class = 'warning';
                }
                elseif($status == 'approved'){
                    $status = 'Approved';
                    $class = 'success';
                }
                elseif($status == 'return'){
                    $status = 'Returned';
                    $class = 'danger';
                }
                elseif($status == 'approved_advance'){
                    $status = 'Approved';
                    $class = 'success';
                }
                else{
                    $class = 'primary';
                }
                return view('shared._button_color', compact('status', 'class'))->render();
            })
            ->addColumn('action', function($model){
                $url = 'adminclaims_request';
                $delete_hide = true;
                $edit_hide = true;
                return view('shared._table_action', compact('model', 'url','delete_hide','edit_hide'))->render();
            })
            ->escapeColumns([])
            ->addIndexColumn()
            ->make(true);
    }

    public function list_advance($type){

        //get approval emp ids
        $emp_ids = Approval::where('approver_id', Auth::id())->type('advanced')->pluck('employee_id');
        $query = Claim::with('Employee')->where(['claim_proc_status' => $type, 'claim_status' => 'submit', 'claim_type' => '2'])->whereIn('emp_id',$emp_ids);

        return Datatables::eloquent($query)
            ->addColumn('employee', function($model){
                $employee = Employee::find($model->emp_id);
                return $employee->fname." ".$employee->lname;
            })
            ->addColumn('desc', function($model){
                return 'Advance Claim - '.$model->claim_type_advance;
            })
            ->addColumn('status', function($model){
                $status = $model->claim_proc_status;

                if($status == 'unproc'){
                    $status = 'Pending';
                    $class = 'default';
                }
                elseif($status == 'proc'){
                    $status = 'Reviewed';
                    $class = 'warning';
                }
                elseif($status == 'approved'){
                    $status = 'Approved';
                    $class = 'success';
                }
                elseif($status == 'returned'){
                    $status = 'Returned';
                    $class = 'danger';
                }
                elseif($status == 'unproc_advance'){
                    $status = 'Pending';
                    $class = 'danger';
                }
                elseif($status == 'proc_advance'){
                    $status = 'Reviewed';
                    $class = 'warning';
                }
                elseif($status == 'approved_advance'){
                    $status = 'Approved';
                    $class = 'success';
                }
                else{
                    $class = 'primary';
                }
                return view('shared._button_color', compact('status', 'class'))->render();
            })
            ->addColumn('action', function($model){
                $url = 'adminclaims_request';
                $delete_hide = true;
                $edit_hide = true;
                $show_hide = true;
                $custom_btn = [
                    '<a class="btn btn-xs btn-primary" href="'.route('claimadvances_detail.show',$model->id).'"><i class="fa fa-pencil"></i></a>',
                ];
                return view('shared._table_action', compact('model', 'url','show_hide','delete_hide','edit_hide','custom_btn'))->render();
            })
            ->escapeColumns([])
            ->addIndexColumn()
            ->make(true);
    }
    public function update_admin(Request $request, $id)
    {
         // email
         $client = new PostmarkClient("73a69c7d-8c56-4f59-877c-74d05cd5e247");
         $approval_lv1 = Approval::where('employee_id', Auth::user()->id)->where('level',1)->where('type','claim')->first();
         $approval_lv2 = Approval::where('employee_id', Auth::user()->id)->where('level',2)->where('type','claim')->first();

        $claim = Claim::find($id);
        $employees = $claim->employee;
        $claim->claim_remark = $request->claim_remark;

        if($request->submit_val == 'return'){
            $claim->claim_proc_status = 'return';
        }
        else if($request->submit_val == 'approved'){
            $claim->claim_proc_status = 'approved';
            // return $request->submit_val;
        }
        else if($request->submit_val == 'submit'){
            $claim->claim_proc_status = $request->claim_proc_status;
        }

        $claim->save();

        $req = $claim->id;
        //
        if ($claim->claim_proc_status == 'return')
        // email send ke user return
        $sendResult = $client->sendEmailWithTemplate(

            "hassanul@boneybone.com",
            $employees->email,
            10374377,
                [
                    "system_name" => "[IX HRMS] Claim Application",
                    "fname" => $employees->fname,
                    "lname" => $employees->lname,
                    "uid" => $employees->employee_uid,
                    "link" => url('http://hrms.ixtelecom.net')
                ]);

        //

        elseif ($claim->claim_proc_status =='approved')
                    $sendResult = $client->sendEmailWithTemplate(

                        "hassanul@boneybone.com",
                        $employees->email,
                        10534102,
                            [
                                "system_name" => "[IX HRMS] Claim Application",
                                "fname" => $employees->fname,
                                "lname" => $employees->lname,
                                "uid" => $employees->employee_uid,
                                "link" => url('http://hrms.ixtelecom.net')
                            ]);



                return redirect('adminclaims');
    }

    public function update_request(Request $request, $id)
    {
         // email
         $client = new PostmarkClient("73a69c7d-8c56-4f59-877c-74d05cd5e247");
         $approval_lv1 = Approval::where('employee_id', Auth::user()->id)->where('level',1)->where('type','advanced')->first();
         $approval_lv2 = Approval::where('employee_id', Auth::user()->id)->where('level',2)->where('type','advanced')->first();

        $claim = Claim::find($id);
        $employees = $claim->employee;
        $claim->claim_remark = $request->claim_remark;

        if($request->submit_val == 'return'){
            $claim->claim_proc_status = 'return';
        }
        else if($request->submit_val == 'approved'){
            $claim->claim_proc_status = 'approved';
            // return $request->submit_val;
        }
        else if($request->submit_val == 'submit'){
            $claim->claim_proc_status = $request->claim_proc_status;
        }

        $claim->save();

        $req = $claim->id;
        //
        if ($claim->claim_proc_status == 'return')
        // email send ke user return
        $sendResult = $client->sendEmailWithTemplate(

            "hassanul@boneybone.com",
            $employees->email,
            10374377,
                [
                    "system_name" => "[IX HRMS] Claim Application",
                    "fname" => $employees->fname,
                    "lname" => $employees->lname,
                    "uid" => $employees->employee_uid,
                    "link" => url('http://hrms.ixtelecom.net')
                ]);

        //
        elseif ($claim->claim_proc_status =='approved')
                    $sendResult = $client->sendEmailWithTemplate(

                        "hassanul@boneybone.com",
                        $employees->email,
                        10534102,
                            [
                                "system_name" => "[IX HRMS] Claim Application",
                                "fname" => $employees->fname,
                                "lname" => $employees->lname,
                                "uid" => $employees->employee_uid,
                                "link" => url('http://hrms.ixtelecom.net')
                            ]);

                            elseif ($claim->claim_status == 'draft' and $claim->claim_proc_status =='approved_advance')
                            $sendResult = $client->sendEmailWithTemplate(

                                "hassanul@boneybone.com",
                                $employees->email,
                                10534102,
                                    [
                                        "system_name" => "[IX HRMS] Claim Application",
                                        "fname" => $employees->fname,
                                        "lname" => $employees->lname,
                                        "uid" => $employees->employee_uid,
                                        "link" => url('http://hrms.ixtelecom.net')
                                    ]);

                return redirect('adminclaims');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $expense = Claim::find($id)->expenses;
        $claim = Claim::find($id);

        //filter view by approver level
        $approver_level = Approval::where('employee_id',$claim->emp_id)->where('approver_id',Auth::id())->first();

        //return $approver_level->level;

        return view('admins.claims.show',compact('expense','claim','approver_level'));
    }

    public function show_request($id){

        $day = Carbon::now()->format('d');

        if($day < 10){
            //get this month
            //get last month
            $thismonth =  Carbon::now()->format('m');
            $nextmonth =  Carbon::now()->addMonth()->format('m');
            $months = [$thismonth, $nextmonth];
        }
        else{
            //get this month
            $thismonth =  Carbon::now()->format('m');
            $months = [$thismonth];
        }

        $claim = Claim::find($id);
        $organisation = Organisation::all();
        $expenses_type = ExpenseType::all();

        $emp_ids = Approval::where('approver_id', Auth::id())->pluck('employee_id');
        $approver = 'no';

        //check if can view approver remarks
        if($emp_ids){
            foreach($emp_ids as $e){
                if($claim->emp_id == $e ){
                    $approver = 'yes';
                }
            }
        }else{
            $approver = 'no';
        }

        //get approver level
        $approver_level = Approval::where('approver_id', Auth::id())->where('employee_id', $claim->emp_id)->first();

        $country = $this->country_list();

        return view('employees.claims.advance.show',compact('claim', 'months','expenses_type','organisation','country','approver','approver_level'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function country_list(){

        $country_array = array("Malaysia","Afghanistan", "Aland Islands", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Barbuda", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Trty.", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Caicos Islands", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia", "French Southern Territories", "Futuna Islands", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard", "Herzegovina", "Holy See", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Jan Mayen Islands", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea", "Korea (Democratic)", "Kuwait", "Kyrgyzstan", "Lao", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macao", "Macedonia", "Madagascar", "Malawi", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "McDonald Islands", "Mexico", "Micronesia", "Miquelon", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "Nevis", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Palestinian Territory, Occupied", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Principe", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Barthelemy", "Saint Helena", "Saint Kitts", "Saint Lucia", "Saint Martin (French part)", "Saint Pierre", "Saint Vincent", "Samoa", "San Marino", "Sao Tome", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia", "South Sandwich Islands", "Spain", "Sri Lanka", "Sudan", "Suriname", "Svalbard", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "The Grenadines", "Timor-Leste", "Tobago", "Togo", "Tokelau", "Tonga", "Trinidad", "Tunisia", "Turkey", "Turkmenistan", "Turks Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "US Minor Outlying Islands", "Uzbekistan", "Vanuatu", "Vatican City State", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (US)", "Wallis", "Western Sahara", "Yemen", "Zambia", "Zimbabwe");

        return $country_array;
    }
}
