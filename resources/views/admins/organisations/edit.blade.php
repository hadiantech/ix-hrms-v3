<div id="modal_organisations_edit" class="modal fade in" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
 
                <h1>Edit Company</h1>
                <div class="row">
                <div class="col-sm-4"> 
               
                <img width="300px" id="company_image_edit" src="{{ asset('images/Image.png') }}"/>
                </div>
                </div>
                
                <br />

                {!! Form::open(['url' => 'organisations', 'id' => 'organisation_edit', 'files' => true]) !!}
                    {{ Form::hidden('organisation_id', null , ['id' => 'organisation_id']) }}
                    <input type="hidden" name="_method" value="PUT">
                    <div class="  form-group ">
                        <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">COMPANY NAME</h5>
                        {{ Form::text('company_name', null , ['id' => 'company_name', 'required','class' => 'form-control','placeholder' => 'Company Name']) }}
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                        <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">COMPANY REGISTER NUMBER</h5>
                        {{  Form::text('reg_num', null , ['id' => 'reg_num', 'required','class' => 'form-control','placeholder' => 'Register Number']) }}
                        </div>

                        <div class="col-md-6">
                            <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">COMPANY LOGO</h5>
                            <div class="m-b-0 fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="file"> <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                    <span class="fileinput-filename"></span></div>
                                    <span class="input-group-addon btn btn-default btn-file">
                                    <span class="fileinput-new">Select file</span>
                                    <span class="fileinput-exists">Change</span>
                                    {{ Form::file('file', null, ['required', 'placeholder' => "Upload Logo"]) }}
                                    </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                            </div>
                            <small>Leave blank if do not want to update</small>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                        <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">COMPANY ADDRESS</h5>
                        {{ Form::text('address1', null , ['id' => 'address1', 'required','class' => 'form-control','placeholder' => 'Company Address']) }}
                        </div>
                        <div class="col-md-6">
                        <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">COMPANY ADDRESS 2</h5>
                        {{ Form::text('address2', null , ['id' => 'address2', 'required','class' => 'form-control','placeholder' => 'Company Address 2']) }}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-4">
                        <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">CITY</h5>
                        {{ Form::text('city', null , ['id' => 'city', 'required','class' => 'form-control','placeholder' => 'City']) }}
                        </div>
                        <div class="col-md-4">
                        <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">STATE</h5>
                        {{ Form::text('state', null , ['id' => 'state', 'required','class' => 'form-control','placeholder' => 'State']) }}
                        </div>
                        <div class="col-md-4">
                        <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">COUNTRY</h5>
                        {{ Form::text('country', null , ['id' => 'country', 'required','class' => 'form-control','placeholder' => 'Country']) }}
                        </div>
                    </div>
                    <div class="row form-group m-b-30">
                        <div class="col-md-6">
                        <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">COMPANY CONTACT NUMBER</h5>
                        {{ Form::text('number', null , ['id' => 'number', 'required','class' => 'form-control','placeholder' => 'Phone Number']) }}
                        </div>
                        <div class="col-md-6">
                        <h5 class="text-muted" style="font-weight:500; margin-bottom:15px;">COMPANY FAX NUMBER</h5>
                        {{ Form::text('fax', null , ['id' => 'fax', 'required','class' => 'form-control','placeholder' => 'Fax Number']) }}</div>
                    </div>
                    {{ Form::submit('Save Company',['class' =>'btn btn-rounded btn-info text-uppercase']) }}
                    <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('js')
    @parent
    <script>

    $( "#organisation_edit" ).on( "submit", function( event ) {
        event.preventDefault();
        swal({
            title: "Update Organisation",
            text: "Are you sure you want to update this organisation?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                var form = document.getElementById('organisation_edit');
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: '/organisations/'+form.organisation_id.value,
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(msg) {
                        return msg;
                    }
                });
            }
        }).then((result) => {
            $('#modal_organisations_edit').modal('toggle');
            if(result.value){
                $('#org_table').DataTable().ajax.reload();
                swal('Department Updated!', '--', 'success');
            }
        });
    });
    </script>
@endsection
