@extends('layouts.default.master')

@section('content')

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Employee Lieu Process</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="dashboard-adminview.html">Dashboard</a></li>
                    <li><a href="admin-claim-dashboard.html">Leave Dashboard</a></li>
                    <li class="active">Employee Lieus</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="m-b-20 box-title text-uppercase">Employee Lieu Application Process</h3>
                <div class="employee-lieu-details">
                    <div class="row">
                        <div class="col-md-4">

                        <h1>Lieu Applications</h1>
                        <div class="employee-lieu-details">
                        <div class="row">
                        <div class="col-md-12">
                                <h4>Lieu Details</h4>
                                <label>Name</label> <span class="m-l-5">{{ $lieus->employee->fname }} {{ $lieus->employee->lname }}</span><br>
                                <label>ID</label> <span class="m-l-5">{{ $lieus->employee->employee_uid }}</span><br>
                                <label>Lieu Event Name</label> <span class="m-l-5">{{ $lieus->event_name }}</span><br>
                                <label>Lieu Reason</label> <span class="m-l-5">{{ $lieus->remark }}</span><br>
                                <label>Lieu Start Date</label> <span class="m-l-5">{{ $lieus->started_at }}</span><br>
                                <label>Lieu End Date</label> <span class="m-l-5">{{ $lieus->ended_at }}</span><br>

                                            <br>
                                            {{-- <label>Department</label> <span class="m-l-5">{{ $leaves->employee->department->department_name }}, {{ $leaves->employee->organisation->company_name }}</span><br> --}}
                                            {{-- <label>Applied Date</label> <span class="m-l-5">{{ Carbon\Carbon::parse($leaves->claim_applied_date)->format('d F Y') }}</span><br> --}}


                                </div>
                                </div>
                                <br>
                                {!! Form::open(['url' => '', 'id' => 'adminlieu_update']) !!}
                                    <input type="hidden" name="_method" value="PUT">
                                    <input id="id" name="id" type="hidden" value="{{ $lieus->id }}" />

                                    <button type="submit" onclick="proc_status('reject')" style="width:200px" class="m-r-10 btn-lg btn-rounded btn-danger text-uppercase">Reject</button>
                                    <button type="submit" onclick="proc_status('proc')" style="width:200px" class="m-r-10 btn-lg btn-rounded btn-success text-uppercase">Reviewed</button>

                                    @if($approver_level->level == '2')
                                    <button type="submit" onclick="proc_status('approve')" style="width:200px" class="m-r-10 btn-lg btn-rounded btn-success text-uppercase">Approve</button>
                                    @endif
                                {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                                </div>
                        </div>



@endsection
@section('js')
@parent
<script>
        var status;
        var proc_status;

        function proc_status(e){
            if(e == 'reject'){
                proc_status = 'reject';
                status = 'rejected';
            }
            else if(e == 'proc'){
                proc_status  = e;
                status = 'processed';
            }
            else if(e == 'approve'){
                proc_status = e;
                status = 'approve';
            }
            else{
                proc_status = e;
            }
        }
        $( "#adminlieu_update" ).on( "submit", function( event ) {
            event.preventDefault();
            swal({
                title: "Submit Changes",
                text: "Update Employee Leave Application?",
                type: "info",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                allowOutsideClick: () => !swal.isLoading(),
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        var form = document.getElementById('adminlieu_update');
                        var formData = new FormData(form);
                        formData.append('status', status);
                        formData.append('lieu_proc_status', proc_status);
                        $.ajax({
                            type: "POST",
                            url: '/adminlieus/'+form.id.value,
                            data: formData,
                            cache:false,
                            contentType: false,
                            processData: false,
                            success: function(data) {
                                resolve(data);
                            },
                            error: function(){
                                swal.showValidationMessage('server error');
                                swal.hideLoading();
                            }
                        });
                    });

                }
            }).then((result) => {
                if(result.value){
                    swal('Application Updated!', '--', 'success').then((result) => {
                        window.location.href = "{{  route('adminleaves.index') }}";
                    });
                }
            });
        });
        </script>
        @endsection


        {{-- $( "#" ).on( "submit", function( event ) {
            event.preventDefault();
            swal({
                title: "Update Status",
                text: "Change this user leave status?",
                type: "info",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                preConfirm: function() {
                    var form = document.getElementById('');
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url: '/adminlieus/'+form.id.value,
                        data: formData,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: function(msg) {
                            return msg;
                        }
                    });
                }
            }).then((result) => {
                $('#lieus_modal_view').modal('toggle');
                if(result.value){
                    $('#leave_table').DataTable().ajax.reload();
                    swal('Status Updated!', '--', 'success');
                }
            });
        });
        </script>
    @endsection --}}



{{--
<div class="modal fade" id="lieus_modal_view" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Leave in Lieu</h1>
                <hr />
                <p>
                    Request by: <b><span id="employee_name">Nazar</span></b>
                    <br />
                    Event Name: <b><span id="event_name">HRMS</span></b>
                    <br />
                    Total Days <b><span id="total_days">HRMS</span></b>
                    <br />
                    Date : <b><span id="started_at">2019/01/01</span> - <span id="ended_at">2019/01/01</span></b>
                    <br />
                    Expired At: <b><span id="expired_at">2019/01/01</span></b>
                    <br />
                    Remark:
                    <br />
                    <span id="remark">lorem ipsum</span>
                    <br />
                    <hr />
                    <a class="btn btn-success" onclick="change_status('Accept')">Accept</a> <a class="btn btn-danger" onclick="change_status('Reject')">Reject</a>
                </p>

            </div>
        </div>
    </div>
</div> --}}
