<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Asset;
use App\AssetType;
use App\AssetBrand;
use App\Employee;
use DataTables;
use App\Assign;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assets = Asset::all();
        $types = AssetType::all();
        $brands = AssetBrand::all();
        $employees = Employee::all();
        $assigns = Assign::all();
        return view('assets.index', compact('assets','types','brands','employees','assigns'));
    }

    public function list(){
        return Datatables::eloquent(Asset::query())
        ->addColumn('action', function($model){
            $url = 'assets';
            $modal_view_id = 'modal_asset_show';
            $modal_edit_id = 'modal_asset_edit';
            $delete_datatable_id_name_refresh = 'asset_table';
            return view('shared._table_action', compact('model', 'url', 'modal_view_id', 'modal_edit_id', 'delete_datatable_id_name_refresh'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    public function list_based_asset($id){
        return Asset::where('asset_type', $id)->get();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('assets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $assets = Asset::create($request->only('name','serialnum','warranty','manufact_name','asset_type','status','price'));

        $assets->save();
        return response()->json($assets, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $assets = Asset::find($id);
        return view('assets.show',compact('assets'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $asset = Asset::find($id);
        $asset->update($request->all());


        return response()->json($asset, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Asset $asset)
    {
        $asset->delete();
        return redirect()->back();
    }
}
