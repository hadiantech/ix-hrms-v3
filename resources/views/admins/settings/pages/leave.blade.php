<section id="leave-setting">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">

            {!! Form::open(['url' => '', "id" => "leave_saved"]) !!}
            <h3 class="box-title">Default Allocation</h3>

            <div class="form-group">

                <div class="table-responsive">
                    <table class="table">
                    <thead>
                            <tr style="border-bottom:3px solid #eee">
                                <th>Leave Type</th>
                                <th width="20%" class="text-right">Allocation</th>
                            </tr>
                    </thead>
                    <tbody>
                            <tr>
                                <td>Annual Leave</td>
                                <td>
                                    {{ Form::number('annual_leave', settings('Annual Leave') ? settings('Annual Leave') : 0, ['required', 'min'=>'0', 'class'=>'form-control']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>Emergency Leave</td>
                                <td>
                                    {{ Form::number('emergency_leave', settings('Emergency Leave') ? settings('Emergency Leave') : 0, ['required', 'min'=>'0', 'class'=>'form-control']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>Medical Leave</td>
                                <td>
                                    {{ Form::number('medical_leave', settings('Medical Leave') ? settings('Medical Leave') : 0, ['required', 'min'=>'0', 'class'=>'form-control']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>Compassionate Leave</td>
                                <td>
                                    {{ Form::number('compassionate_leave', settings('Compassionate Leave') ? settings('Compassionate Leave') : 0, ['required', 'min'=>'0', 'class'=>'form-control']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>Maternity Leave</td>
                                <td>
                                    {{ Form::number('maternity_leave', settings('Maternity Leave') ? settings('Maternity Leave') : 0, ['required', 'min'=>'0', 'class'=>'form-control']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>Paternity Leave</td>
                                <td>
                                    {{ Form::number('paternity_leave', settings('Paternity Leave') ? settings('Paternity Leave') : 0, ['required', 'min'=>'0', 'class'=>'form-control']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>Marriage Leave</td>
                                <td>
                                    {{ Form::number('marriage_leave', settings('Marriage Leave') ? settings('Marriage Leave') : 0, ['required', 'min'=>'0', 'class'=>'form-control']) }}
                                </td>
                            </tr>


                    </tbody>
                </table>

                </div>
            </div>

            <h3 class="box-title">Other Settings</h3>

            <div class="form-group">
                <div class="table-responsive">
                    <table class="table">

                    <tbody>
                            <tr>
                                <td>Working days in advanced to apply annual leave</td>
                                <td width="20%">
                                    {{ Form::number('working_days_to_apply_annual_leave', settings('working_days_to_apply_annual_leave') ? settings('working_days_to_apply_annual_leave') : 0, ['required', 'min'=>'0', 'class'=>'form-control']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>Maximum calendar days to apply annual leave</td>
                                <td>
                                    {{ Form::number('max_calendar_to_apply_annual_leave', settings('max_calendar_to_apply_annual_leave') ? settings('max_calendar_to_apply_annual_leave') : 0, ['required', 'min'=>'0', 'class'=>'form-control']) }}
                                </td>
                            </tr>
                            <tr>
                                <td>Working days to apply lieu days</td>
                                <td>
                                    {{ Form::number('working_days_to_apply_lieu_days', settings('working_days_to_apply_lieu_days') ? settings('working_days_to_apply_lieu_days') : 0, ['required', 'min'=>'0', 'class'=>'form-control']) }}
                                </td>
                            </tr>

                    </tbody>
                </table>

                </div>
            </div>

            {{Form::submit('Save',['class' =>'m-b-20 btn-lg btn-rounded btn-info text-uppercase', 'style'=>"width:200px"])}}

            {!! Form::close() !!}
        </div>
    </div>
</section>


@section('js')
    @parent

    <script>
    $( "#leave_saved" ).on( "submit", function( event ) {
        event.preventDefault();

        swal({
        title: 'Saving',
        onOpen: () => {
            swal.showLoading()
            return new Promise(function(resolve, reject) {
                var form = document.getElementById('leave_saved');
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: "{{ route('settings.store_leave') }}",
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        swal.close();
                    },
                    error: function(data) {
                        errormsg = '';
                        if(data.status == 422 ){
                            $.each(data.responseJSON.errors, function (key, value) {
                                errormsg = errormsg + value[0] + "<br />";
                            });
                        }else{
                            errormsg = "Server error. Please try again";
                        }
                        swal.showValidationMessage(errormsg);
                        swal.hideLoading();
                    },
                });
            });
        },
        allowOutsideClick: () => !swal.isLoading()
        });
    });
    </script>

@endsection
