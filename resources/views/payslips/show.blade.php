<div id="modal_payroll_show" class="modal fade in" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <p class="text-muted">PAYROLL CATEGORY</p>
                <h3><span id="modal_payroll_name"></span></h3>
                <hr>
                <p class="text-muted">PAYROLL OPERAND</p>
                <H3><span id="modal_payroll_operand"> </span></H3>
                <hr>

            </div>
        </div>
    </div>
</div>
