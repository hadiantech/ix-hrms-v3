<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvanceClaim extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advance_claim', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('emp_id')->nullable();
            $table->integer('claim_id')->nullable();
            $table->integer('expense_id')->nullable();
            $table->text('type')->nullable();
            $table->text('purpose')->nullable();
            $table->longtext('venue')->nullable();
            $table->double('total_amount',10,2)->nullable();
            $table->double('total_used',10,2)->nullable();
            $table->integer('charger_company')->nullable();
            $table->text('status')->nullable();
            $table->integer('req_layer')->nullable();
            $table->text('paid_status')->nullable();
            $table->text('remark')->nullable();
            $table->text('visa_req')->nullable();
            $table->text('country')->nullable();
            $table->text('entry')->nullable();
            $table->text('nationality')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advance_claim');
    }
}
