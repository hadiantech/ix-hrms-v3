<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvanceClaimTraveller extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advance_claim_traveller', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('advance_id')->nullable();
            $table->string('mobile')->nullable();
            $table->text('passport_num')->nullable();
            $table->date('passport_expired')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advance_claim_traveller');
    }
}
