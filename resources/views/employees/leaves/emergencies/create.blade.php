<div id="emergency-leave-form" class="modal fade in" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <p class="text-uppercase">Apply Leave</p>
                <h1>Emergency Leave</h1>
                @javascript('key',$dates)
                    {!! Form::open(['url'=>'emergencies', 'id'=>'emergency_leave_create']) !!}
                    <div class="row form-group">
                        <div class="col-md-4">

                            {!! Form::label('Start Date') !!}
                            <div class="input-group">
                                    <input type="text" name="started_at" id="leave_from_emergency" class="form-control datepicker"/>
                            </div>
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('End Date') !!}
                            <div class="input-group">
                                    <input type="text" name="ended_at" id="leave_to_emergency" class="form-control datepicker"/>
                                </div>
                        </div>
                        <div class="col-md-4">
                            {!! Form::label('Half Day?') !!}
                            <select name="half_day" class="form-control">
                                <option value="">No</option>
                                <option value="0.5">Halfday AM on Start Date</option>
                                <option value="0.5">Halfday PM on Start Date</option>
                                <option value="0.5">Halfday AM on End Date</option>
                                <option value="0.5">Halfday PM on End Date</option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            {!! Form::label('Leave Reason') !!}
                            {{ Form::textarea('reason', null , ['required','class' => 'form-control','placeholder' => 'Leave Reason']) }}
                        </div>
                    </div>

                    <div class="row form-group ">
                        <div class="col-md-6">
                            {{ Form::hidden('erow','eleave_days',['id'=>'erow']) }}
                            <p class="text-muted m-b-0 text-uppercase">Total Leave : <span id="eleave_days">0.0</span> days</p>
                        </div>
                        <div class="col-md-6">
                            <p class="text-muted m-b-0 text-uppercase">Balance: <span id="ebalance_days">{{ $annual_leave_quota - $annual_leave_taken }}</span> days</p>
                        </div>

                        <div class="col-md-6">
                            <p class="text-muted m-b-0 text-uppercase">Total Unpaid Leave : <span id="eunpaid_days">0.0</span> days</p>
                        </div>
                        {{-- <div class="col-md-12">
                            <p class="m-b-0 text-uppercase">This application will proceed as <em>Unpaid Leave</em>.</p>
                        </div> --}}
                    </div>
                    {{ Form::submit('Submit Application' , ['class' => 'm-r-5 text-uppercase btn btn-info']) }}
                    {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>

@section('js')
@parent

<script>
        var date = JSON.parse('<?= $js_array; ?>');

                $(function () {
                    $("#leave_from_emergency").datepicker({
                        format: 'yyyy-mm-dd',
                        daysOfWeekDisabled:non,
                        datesDisabled:key,
                        autoclose: true
                    }).on('changeDate',function(e){
                        $('#leave_to_emergency').datepicker('setStartDate',e.non)
                    });
                    $("#leave_to_emergency").datepicker({
                        format: 'yyyy-mm-dd',
                        daysOfWeekDisabled:non,
                        datesDisabled:key,
                        autoclose: true
                    });
                });
</script>
<script type='text/javascript'>

    var non = JSON.parse('<?= $js_array; ?>');
$('#leave_from_emergency').change(function() {
var date = $("#leave_from_emergency").val();
    $("#leave_to_emergency")[0].setAttribute('min', date);
    $("#leave_to_emergency").val('');
    $("#eleave_days").text(0.0);
});

$('#leave_to_emergency').change(function() {
    var date_from = $('#leave_from_emergency').datepicker().val();
    var date_to = $('#leave_to_emergency').datepicker().val();

    var date1 = new Date(date_from);
    var date2 = new Date(date_to);

    var miliseconds = getDaysInSeconds(date2.getTime(),date1.getTime());

    var days = miliseconds/(1000*60*60*24);

    var dates = [];

    for(var h=0;h<=days;h++){
        var date = new Date(date_from);
        date.setDate(date.getDate() + h);
        var tarikh = date.getDate();
        var bulan = date.getMonth()+1;
        var tahun = date.getFullYear();
        var hari = date.getDay();

        if(bulan<10){
            bulan = '0'+bulan;
        }
        if(tarikh<10){
            tarikh = '0'+tarikh;
        }
        var date_h = tahun+'-'+bulan+'-'+tarikh;
        dates.push(date_h);
        var day_h = hari;
    }

    var counter_days = dates.length;

   // console.log('key:',key);
   // console.log('non:',non);

    for(j=0;j<dates.length;j++){
        var found = '0';
        for(h=0;h<key.length;h++){
            if(key[h]==dates[j]){
                counter_days--;
                //console.log('lepas tolak key:',counter_days);
                var found = '1';
                break;
            }

            }
        if ( found = '0'){
        var date_y = new Date(dates[j]);
        var date_z = date_y.getDay();

        for(h1=0;h1<non.length;h1++){
            if(non[h1]==date_z){
                counter_days--;
                var found = '1';
                //console.log('lepas tolak non:',counter_days);
                break;
            }

            }
        }
    }
    //console.log('after key:',counter_days);
    $("#erow").val(counter_days);
    $("#eleave_days").text(counter_days);

});

function getDaysInSeconds(date_to,date_from){
    return date_to-date_from;
}

        $( "#emergency_leave_create" ).on( "submit", function( event ) {
            event.preventDefault();
            if(unpaid < 0){
                swal({
                    title: 'Unpaid Leave',
                    text: "This application will proceed as <em>Unpaid Leave</em>",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Proceed'
                }).then((result) => {
                    if (result.value) {
                        emergency_submit_now();
                    }
                });
            }else{
                emergency_submit_now();
            }
        });

        function emergency_submit_now(){
            swal({
            title: 'Saving',
            onOpen: () => {
                swal.showLoading()
                return new Promise(function(resolve, reject) {
                    var form = document.getElementById('emergency_leave_create');
                    var formData = new FormData(form);

                    $.ajax({
                        type: "POST",
                        url: "{{ route('leaves.store.emergency') }}",
                        data:formData,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            swal.close();
                            $('#emergency-leave-form').modal('toggle');
                            swal('Success', 'Please wait for approval', 'success');
                            $('#leave_table').DataTable().ajax.reload();
                        },
                        error: function(data) {
                            errormsg = '';
                            if(data.status == 422 ){
                                $.each(data.responseJSON.errors, function (key, value) {
                                    errormsg = errormsg + value[0] + "<br />";
                                });
                            }else{
                                errormsg = "Server error. Please try again";
                            }
                            swal.showValidationMessage(errormsg);
                            swal.hideLoading();
                        },
                    });
                });
            }})
        }

    </script>
@endsection
