<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClaimAdvance extends Model
{
    protected $table = 'claims`';

    public function employee(){
        return $this->belongsTo(Employee::class,'emp_id');

    }

    public function organisation(){
        return $this->belongsTo(Organisation::class,'company_id');

    }
}
