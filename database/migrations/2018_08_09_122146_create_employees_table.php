<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->string('password');

            $table->string('fname')->nullable();
            $table->string('lname')->nullable();
            $table->string('ic')->nullable();
            $table->integer('company_id')->nullable();
            $table->integer('department_id')->nullable();
            $table->integer('designation_id')->nullable();
            $table->string('hierarchy_id')->nullable();

            $table->string('gender')->nullable();
            $table->string('marriage_status')->nullable();
            $table->string('race')->nullable();
            $table->string('religion')->nullable();
            $table->string('pre_address_line1')->nullable();
            $table->string('pre_address_line2')->nullable();
            $table->string('pre_city')->nullable();
            $table->string('pre_state')->nullable();
            $table->string('pre_postcode')->nullable();
            $table->string('pre_country')->nullable();
            $table->string('pre_phone_number1')->nullable();
            $table->string('pre_phone_number2')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bank_account_no')->nullable();

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
