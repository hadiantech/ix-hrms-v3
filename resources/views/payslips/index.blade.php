@extends('layouts.default.master')

@section('content')

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>

<div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Payslip</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="dashboard-adminview.html">Dashboard</a></li>
                        <li class="active">Payslip</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title">Payroll Categories
                           <a href="#add-payroll-cat" data-toggle="modal" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Add Payroll Category</a></h3>
                        <div class="table-responsive">
                            <table  id="payroll_table" class="">
                                <thead>
                                    <tr>
                                        <th style="width: 5%">#</th>
                                        <th>Category Name</th>
                                        <th>Operand</th>
                                        <th style="width: 20%">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!--row -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title">Salary List
                           {{-- <a href="#add-salary" data-toggle="modal" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Add Payroll Category</a> --}}
                             </h3>
                        <div class="table-responsive">
                            <table  id="salary_table" class="">
                                <thead>
                                    <tr>
                                        <th style="width: 5%">#</th>
                                        <th>Employee</th>
                                        <th>Salary</th>
                                        <th style="width: 20%">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>


                    </div>
                </div>
            </div>
            <!--row -->

            <!--modal category-->
            @include('payslips.show')
            @include('payslips.create')
            @include('payslips.edit')

             <!--modal set salary-->
            @include('payslips.salaries.set')

              {{-- <!--modal set increment-->
            @include('payslips.salaries.increment') --}}

        </div>
        <!-- /.container-fluid -->
    </div>
@endsection

@section('js')
@parent

<script>
$(function () {

    $('#payroll_table').DataTable({
        serverSide: true,
        processing: true,
        ajax: '/payslips/list',
        columns: [
            {data: 'DT_Row_Index', orderable: false, searchable: false},
            {data: 'name'},
            {data: 'operand', name: 'operand.operand_name'},
            {data: 'action', orderable: false, searchable: false}
        ]
    });

    $('#salary_table').DataTable({
        serverSide: true,
        processing: true,
        ajax: '/salaries/list',
        columns: [
            {data: 'DT_Row_Index', orderable: false, searchable: false},
            {data: 'username'},
            {data: 'salary' },
            {data: 'action', orderable: false, searchable: false}
        ]
    });

});

    $(document).off('click', '.open-modal').on("click", ".open-modal", function () {


        var data = $(this).data('modal_data');

        //if($(this).data('salary') ){ //data-employee
        //    var salary = $(this).data('salary');
        //}

        // Payroll

        $('#modal_payroll_show').on('shown.bs.modal', function(e){
            $(".modal-body #modal_payroll_name").text(data.name);
            $(".modal-body #modal_payroll_operand").val(data.operand_id)
            $(this).off('shown.bs.modal');
            console.log(name);
        });

        $('#modal_payroll_show').on('hide.bs.modal', function(e){
            $(".modal-body #modal_payroll_name").text('');
            $(".modal-body #modal_payroll_operand").text('');
            $(this).off('hide.bs.modal');
        });

        $('#modal_payroll_edit').on('shown.bs.modal', function(e){
            $(".modal-body #payroll_name_edit").val(data.name);
            $(".modal-body #modal_payroll_operand").val(data.operand_id)
            $(".modal-body #id").val(data.id);
            $(this).off('shown.bs.modal');
        });

        $('#modal_payroll_edit').on('hide.bs.modal', function(e){
            $(this).find('#payroll_edit')[0].reset();
            $(this).off('hide.bs.modal');
        });

        // Salary

        $('#modal_salary_set').on('shown.bs.modal', function(e){

            $(".modal-body #modal_salary_name").text(data.lname);

            $(".modal-body #modal_salary_username").text(data.fname);
            $(".modal-body #employee_id").val(data.id);
            $(".modal-body #id").val(data.id);
            $(this).off('shown.bs.modal');
        });


        $('#modal_salary_increment').on('shown.bs.modal', function(e){
            $(".modal-body #modal_salary_edit").text(data.username);
            $(".modal-body #employee_id").val(data.id);
            $(".modal-body #id").val(data.id);

            $(this).off('shown.bs.modal');
        });

    });
</script>
@endsection
