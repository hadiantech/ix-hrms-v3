@extends('layouts.default.master')
@include('layouts.default.picker')

@section('content')

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Leave Dashboard</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="dashboard.html">Dashboard</a></li>
                    <li class="active">Leave Dashboard</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <!--leave summary-->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box p-b-0">
                    <h3 class="box-title text-uppercase">Leave Summary</h3>
                    <div class="table-responsive manage-table">
                        <table class="table" cellspacing="14">
                            <thead>
                                <tr>
                                    <th>LEAVE TYPE</th>
                                    <th class="text-center">LEAVE GIVEN</th>
                                    <th class="text-center">LEAVE TAKEN</th>
                                    <th class="text-center">LEAVE BALANCE</th>
                                    <th class="text-center">ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="advance-table-row">
                                    <td>Annual Leave</td>
                                        <td class="text-center">{{ $annual_leave_quota }}</td>
                                        <td class="text-center">{{ $annual_leave_taken }}</td>
                                        <td class="text-center">{{ $annual_leave_quota - $annual_leave_taken }}</td>
                                    <td class="text-center">
                                        <a class="btn-rounded btn-sm btn-info text-uppercase" href="#annual-leave-form" data-toggle="modal" role="dialog">Apply</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="sm-pd"></td>
                                </tr>

                                <tr class="advance-table-row">
                                    <td>Emergency Leave</td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center"></td>
                                    <td class="text-center">
                                        <a class="btn-rounded btn-sm btn-info text-uppercase" href="#emergency-leave-form" data-toggle="modal" role="dialog">Apply</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="sm-pd"></td>
                                </tr>

                                <tr class="advance-table-row">
                                    <td>Medical Leave</td>
                                    <td class="text-center">{{ $medical_leave_quota }}</td>
                                    <td class="text-center">{{ $medical_leave_taken }}</td>
                                    <td class="text-center">{{ $medical_leave_quota - $medical_leave_taken }}</td>
                                    <td class="text-center">
                                        <a class="btn-rounded btn-sm btn-info text-uppercase" href="#medical-leave-form" data-toggle="modal" role="dialog">Apply</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="sm-pd"></td>
                                </tr>
                                @if(Auth::user()->marriage_status == 'Single')
                                <tr class="advance-table-row">
                                    <td>Marriage Leave</td>
                                    <td class="text-center">{{ $marriage_leave_quota }}</td>
                                    <td class="text-center">{{ $marriage_leave_taken }}</td>
                                    <td class="text-center">{{ $marriage_leave_quota - $marriage_leave_taken }}</td>
                                    <td class="text-center">
                                        <a class="btn-rounded btn-sm btn-info text-uppercase" href="#marriage-leave-form" data-toggle="modal" role="dialog">Apply</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="5" class="sm-pd"></td>
                                </tr>
                                @endif
                                <tr class="advance-table-row">
                                    <td>Compassionate Leave</td>
                                    <td class="text-center">{{ $compassionate_leave_quota }}</td>
                                    <td class="text-center">{{ $compassionate_leave_taken }}</td>
                                    <td class="text-center">{{ $compassionate_leave_quota - $compassionate_leave_taken }}</td>
                                    <td class="text-center">
                                        <a class="btn-rounded btn-sm btn-info text-uppercase" href="#compassionate-leave-form" data-toggle="modal" role="dialog">Apply</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="sm-pd"></td>
                                </tr>

                                @if(Auth::user()->gender == 'Female')
                                <tr class="advance-table-row">
                                    <td>Maternity Leave</td>
                                    <td class="text-center">{{ $maternity_leave_quota }}</td>
                                    <td class="text-center">{{ $maternity_leave_taken }}</td>
                                    <td class="text-center">{{ $maternity_leave_quota -  $maternity_leave_taken  }}</td>
                                    <td class="text-center">
                                        <a class="btn-rounded btn-sm btn-info text-uppercase" href="#maternity-leave-form" data-toggle="modal" role="dialog">Apply</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="sm-pd"></td>
                                </tr>
                                @endif
                                @if(Auth::user()->gender == 'Male')
                                <tr class="advance-table-row">
                                        <td>Paternity Leave</td>
                                        <td class="text-center">{{ $paternity_leave_quota }}</td>
                                        <td class="text-center">{{ $paternity_leave_taken }}</td>
                                        <td class="text-center">{{ $paternity_leave_quota - $paternity_leave_taken }}</td>
                                        <td class="text-center">
                                            <a class="btn-rounded btn-sm btn-info text-uppercase" href="#paternity-leave-form" data-toggle="modal" role="dialog">Apply</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" class="sm-pd"></td>
                                    </tr>
                                    @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!--leave lieu-->
        @include('employees.leaves.lieus.index')
        <!--leave status-->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title text-uppercase">Leave Status</h3>
                    <div class="table-responsive">
                        <table id="leave_table" class="table display">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Leave Type</th>
                                    <th>Reason</th>
                                    <th>Leave From</th>
                                    <th>Leave To</th>
                                    <th>Total Days</th>
                                    <th>Status</th>
                                    <th>Created</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!--modal annual leave-->
        @include('employees.leaves.annuals.create')

        <!--modal emergency leave-->
        @include('employees.leaves.emergencies.create')

        <!--modal medical leave-->
        @include('employees.leaves.medicals.create')

        <!--modal compasionate leave-->
        @include('employees.leaves.compassionates.create')

        <!--modal paternity leave-->
        @include('employees.leaves.paternity.create')

        <!--modal maternity leave-->
        @include('employees.leaves.maternity.create')

        <!--modal marriage leave-->
        @include('employees.leaves.marriages.create')

        <!--modal lieu leave-->
        @include('employees.leaves.lieus.create')

        <!--modal view leave status-->
        @include('employees.leaves.lieus.show')
    </div>
</div>

@endsection

@section('js')
    @parent

    <script>
        $(function () {
            $('#leave_table').DataTable({
                serverSide: true,
                processing: true,
                order: [[7,'desc']],
                ajax: '/leaves/list',
                columns: [
                    {data: 'DT_Row_Index', orderable: false, searchable: false},
                    {data: 'leave_type'},
                    {data: 'reason'},
                    {data: 'started_at'},
                    {data: 'ended_at'},
                    {data: 'total_days'},
                    {data: 'status'},
                    {data: 'created_at'},
                    {data: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@endsection


