<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Approval extends Model
{
    protected $guarded = [];

    public function approvedBy(){
        return $this->belongsTo(Employee::class, 'approver_id');
    }

    public function scopeType($query, $type){
        return $query->where('type', $type);
    }

    public function scopeLevel($query, $lvl){
        return $query->where('level', $lvl);
    }
}
