<?php

namespace App\Http\Controllers;

use App\Department;
use App\Designation;
use Illuminate\Http\Request;
use DataTables;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function list(){ 
        return Datatables::eloquent(Department::query())
        ->addColumn('action', function($model){
            $url = 'departments';
            $show_hide = true;
            $modal_view_id = 'modal_departments_show';
            $modal_edit_id = 'modal_departments_edit';
            $delete_datatable_id_name_refresh = 'dept_table';
            return view('shared._table_action', compact('model', 'url', 'show_hide', 'modal_view_id', 'modal_edit_id', 'delete_datatable_id_name_refresh'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.organisations.departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $departments = Department::create($request->only('department_name'));
        $departments->save();
        return response()->json($departments, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $departments = Department::find($id);
        return view('admins.organisations.departments.show',compact('departments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        $department->department_name = $request->department_name;
        $department->save();
        return response()->json($department, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        $department->delete();
        return redirect()->back();
    }
}
