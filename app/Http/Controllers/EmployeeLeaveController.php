<?php

namespace App\Http\Controllers;
use App\Employee;
use App\EmployeeLieu;
use App\Http\Controllers\Controller;
use App\EmployeeLeave;
use Illuminate\Support\Facades\Input;
use App\LeaveType;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Event;
use DataTables;
use JavaScript;
use App\Approval;
use Illuminate\Notifications\Notification;
use App\Notifications\LeaveRequest;
use Mail;
use Auth;
use Exception;
use Postmark\PostmarkClient;

use DateTime;
// dah ubah
class EmployeeLeaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aid = Auth::user()->id;
        $employees = Employee::find($aid);

        // panggil approver id
        $leaves = EmployeeLeave::where('employee_id', Auth::user()->id)->get();

        $approval = Approval::where('employee_id', Auth::user()->id)->first();
        //return $approval->approvedBy;
        //return Event::pluck('start_date');
        $dates = [];

        $Events = Event::get(['start_date','end_date']);
        foreach($Events as $event){
            $firstDate = new DateTime($event->start_date);
            $lastDate = new DateTime($event->end_date);

            for($i = $firstDate; $i <= $lastDate; $i->modify('+1 day'))
		    {
                $v = $i->format("Y-m-d");
                array_push($dates,$v);
            }
        }

        if(Auth::user()->leave_allocs()->type('Annual Leave')->first()){
            $annual_leave_quota = Auth::user()->leave_allocs()->type('Annual Leave')->first()->quota;
        }else{
            $annual_leave_quota = settings('Annual Leave');
        }
        $annual_leave_taken = Auth::user()->leaves()->type('Annual Leave')->sum('total_days');

        if(Auth::user()->leave_allocs()->type('Annual Leave')->first()){
            $annual_leave_quota = Auth::user()->leave_allocs()->type('Annual Leave')->first()->quota;
        }else{
            $annual_leave_quota = settings('Annual Leave');
        }
        $$annual_leave_taken = Auth::user()->leaves()->type('Annual Leave')->sum('total_days');

        if(Auth::user()->leave_allocs()->type('Medical Leave')->first()){
            $medical_leave_quota = Auth::user()->leave_allocs()->type('Medical Leave')->first()->quota;
        }else{
            $medical_leave_quota = settings('Medical Leave');
        }
        $medical_leave_taken = Auth::user()->leaves()->type('Medical Leave')->sum('total_days');

        if(Auth::user()->leave_allocs()->type('Maternity Leave')->first()){
            $maternity_leave_quota = Auth::user()->leave_allocs()->type('Maternity Leave')->first()->quota;
        }else{
            $maternity_leave_quota = settings('Maternity Leave');
        }
        $maternity_leave_taken = Auth::user()->leaves()->type('Maternity Leave')->sum('total_days');

        if(Auth::user()->leave_allocs()->type('Compassionate Leave')->first()){
            $compassionate_leave_quota = Auth::user()->leave_allocs()->type('Compassionate Leave')->first()->quota;
        }else{
            $compassionate_leave_quota = settings('Compassionate Leave');
        }
        $compassionate_leave_taken = Auth::user()->leaves()->type('Compassionate Leave')->sum('total_days');

        if(Auth::user()->leave_allocs()->type('Paternity Leave')->first()){
            $paternity_leave_quota = Auth::user()->leave_allocs()->type('Paternity Leave')->first()->quota;
        }else{
            $paternity_leave_quota = settings('Paternity Leave');
        }
        $paternity_leave_taken = Auth::user()->leaves()->type('Paternity Leave')->sum('total_days');

        if(Auth::user()->leave_allocs()->type('Marriage Leave')->first()){
            $marriage_leave_quota = Auth::user()->leave_allocs()->type('Marriage Leave')->first()->quota;
        }else{
            $marriage_leave_quota = settings('Marriage Leave');
        }
        $marriage_leave_taken = Auth::user()->leaves()->type('Marriage Leave')->sum('total_days');

        $working_days = Employee::where('id',$aid)
        ->get();
        // return ($working_days[0]->working_days);

        $php_array = $working_days[0]->working_days;

        $js_array = json_encode($php_array);
        $js_array = str_replace('"', '', $js_array);

        return view('employees.leaves.index', compact('date_array','js_array','working_days','approver','aid','employees','mula','dates','marriage_leave_taken','marriage_leave_quota','annual_leave_taken','annual_leave_quota','emergency_leave_quota', 'emergency_leave_taken','medical_leave_quota', 'medical_leave_taken', 'compassionate_leave_quota','compassionate_leave_taken','maternity_leave_taken','maternity_leave_quota','paternity_leave_taken','paternity_leave_quota'));
    }

    public function list()
    {
        return Datatables::eloquent(EmployeeLeave::where('employee_id', Auth::id()))
        ->addColumn('action', function($model){
            $url = 'leaves';
            // $show_hide = true;
            // $edit_hide = true;
            // $delete_hide = true;
            if($model->status == 'Pending'){
                $custom_btn = [
                    '<a class="btn btn-xs btn-danger" data-method="delete" data-confirm="confirm" href="'. route($url.'.destroy', $model->id) .'">Cancel</a>'
                ];
            }
            return view('shared._table_action', compact('model', 'url', 'custom_btn'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //  return view('employees.leaves.annuals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $a = Carbon::parse($request->started_at);
        $b = Carbon::parse($request->ended_at);
        $days = $b->diffInDays($a);
        $days = $days + 1;
        $expired_at = Carbon::parse($request->ended_at)->addMonths(3);

        $lieu = new EmployeeLieu;
        $lieu->employee_id = Auth::id();
        $lieu->total_days = $days;
        $lieu->total_used_days = '0';
        $lieu->event_name = $request->event_name;
        $lieu->remark = $request->remark;
        $lieu->status = 'Pending';
        $lieu->started_at = $request->started_at;
        $lieu->ended_at = $request->ended_at;
        $lieu->expired_at = $expired_at;

        $lieu->save();

        return response()->json($lieu, 201);
    }

    public function store_emergency(Request $request)
    {
        // $emergency_leave_quota = Auth::user()->leave_allocs()->type('emergency_leave')->first()->quota;
        // $emergency_leave_taken = Auth::user()->leaves()->type('emergency_leave')->sum('total_days');
        // $balance = $emergency_leave_quota - $emergency_leave_taken;

        // $a = Carbon::parse($request->started_at);
        // $b = Carbon::parse($request->ended_at);
        // $days = $b->diffInDays($a);
        // $days = $days + 1;

        // $unpaid = $balance - $days;

        // $emergency = new EmployeeLeave;
        // $emergency->employee_id = Auth::user()->id;
        // $emergency->leave_type = 'Emergency Leave';
        // $emergency->started_at = $request->started_at;
        // $emergency->ended_at = $request->ended_at;
        // $emergency->reason = $request->reason;
        // $emergency->total_days = $days;
        // $emergency->as_unpaid = $unpaid;

        // $emergency->status = 'Pending';
        // $emergency->leave_proc_status = 'unproc';

        // $emergency->save();

        // if ($request->hasFile('file')) {
        //     $emergency->addMediaFromRequest('file')->toMediaCollection('leaves');
        // }
        // return response()->json($emergency, 201);
    }

    public function store_annual(Request $request)
    {
        $client = new PostmarkClient("73a69c7d-8c56-4f59-877c-74d05cd5e247");
        $approval_lv1 = Approval::where('employee_id', Auth::user()->id)->where('level',1)->first();
        $approval_lv2 = Approval::where('employee_id', Auth::user()->id)->where('level',2)->first();
        $leaves = EmployeeLeave::where('employee_id', Auth::user()->id)->get();

        // $approval = Approval::where('approver_id', Auth::id())->first();

        $aid = Auth::user()->id;
        $employees = Employee::find($aid);
       // $emp_ids = Approval::where('approver_id', Auth::id())->pluck('employee_id')();
        //return $emp_ids;
        //$approver = Approval::where('employee_id',$leaves->employee_id)->where('approver_id',Auth::id())->first();
        $annual_leave_quota = Auth::user()->leave_allocs()->type('Annual Leave')->first()->quota;
        $annual_leave_taken = Auth::user()->leaves()->type('Annual Leave')->sum('total_days');
        $balance = $annual_leave_quota - $annual_leave_taken;

        // $a = Carbon::parse($request->started_at);
        // $b = Carbon::parse($request->ended_at);

        // $days = $b->diffInDays($a);
        // $days = $days + 1;

        // $unpaid = $balance - $days;
        $annual = new EmployeeLeave;
        $annual->employee_id = Auth::user()->id;
        $annual->leave_type = 'Annual Leave';
        $annual->started_at = $request->started_at;
        $annual->ended_at = $request->ended_at;
        $annual->reason = $request->reason;
        $annual->total_days = $request->row;
        // $annual->as_unpaid = $unpaid;
        // $emergency->as_lieu = $request->as_lieu;
        // $emergency->remark = $request->remark;
        $annual->status = 'Pending';
        $annual->leave_proc_status = 'unproc';

        $annual->save();
        $leave = $annual->id;
        //
            $sendResult = $client->sendEmailWithTemplate(

            "hassanul@boneybone.com",
            $approval_lv1->approvedBy->email,
            10030123,
                [
                    "system_name" => "[IX HRMS] Leave Application Approval",
                    "fname" => $employees->fname,
                    "lname" => $employees->lname,
                    "uid" => $employees->employee_uid,
                    "approve" =>  $approval_lv1->approvedBy->fname,
                    "approver" =>  $approval_lv1->approvedBy->lname,
                    "leave_type" => 'Annual Leave',
                    "started_at" => $request->started_at,
                    "ended_at" => $request->ended_at,
                    "reason" => $request->reason,
                    "accept" => route('adminleaves.update_email',$leave),
                    "reject" => route('adminleaves.reject_email',$leave),
                    "link" => url('http://hrms.ixtelecom.net')
                ]);

            $sendResult = $client->sendEmailWithTemplate(

                    "hassanul@boneybone.com",
                    $approval_lv2->approvedBy->email,
                    10030123,
                        [
                            "system_name" => "[IX HRMS] Leave Application Approval",
                            "fname" => $employees->fname,
                            "lname" => $employees->lname,
                            "uid" => $employees->employee_uid,
                            "approve" =>  $approval_lv2->approvedBy->fname,
                            "approver" =>  $approval_lv2->approvedBy->lname,
                            "leave_type" => 'Annual Leave',
                            "started_at" => $request->started_at,
                            "ended_at" => $request->ended_at,
                            "reason" => $request->reason,
                            "accept" => route('adminleaves.approve_email',$leave),
                            "reject" => route('adminleaves.reject_email',$leave),
                            "link" => url('http://hrms.ixtelecom.net')
                        ]);

        return response()->json($annual, 201);
    }

    public function store_medical(Request $request)
    {

    }

    public function store_compassionate(Request $request)
    {

    }

    public function store_maternity(Request $request)
    {

    }

    public function store_paternity(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeLeave  $employeeLeave
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeLeave $employeeLeave)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeLeave  $employeeLeave
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeLeave $employeeLeave)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeLeave  $employeeLeave
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeLeave $employeeLeave)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeLeave  $employeeLeave
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeLeave $employeeLeave, $id)
    {
        $employeeLeave = EmployeeLeave::find($id);
        $employeeLeave->delete();
        return redirect()->back();
    }
}
