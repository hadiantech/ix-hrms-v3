<div id="lieus_modal_edit" class="modal fade in" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">

                    <h1 style="padding-bottom:20px;">Approve/Reject Leave Application</h1>
                        {!! Form::open(['url' => '', 'id' => 'lieus_edit']) !!}
                        {{ Form::hidden('id', null , ['id' => 'id']) }}
                        <input type="hidden" name="_method" value="PUT">
                        <div class="row form-group ">
                            <div class="col-md-6">
                                Request by: <b><span id="employee_name"></span></b>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <select class="form-control" name="status" id="status" data-parsley-required="true">
                                        <option value="Reject">Reject</option>
                                        <option value="Approve">Approve</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    <div class="form-group">
                        Event Name: <b><span id="event_name"></span></b>
                    </div>
                   <div class="row form-group">
                    <div class="col-md-6">
                            <div class="input-group">
                                Date : <b><span id="started_at"></span></b>
                            </div>
                    </div>
                    <div class="col-md-6">
                            <div class="input-group">
                                Expired At: <b><span id="expired_at"></span></b>
                            </div>
                    </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            Remark: <br>
                            <b><span id="remark"></span></b>
                        </div>
                    </div>
                    <div class="row form-group ">
                        <div class="col-md-12">
                            Total Days  : <b><span id="total_days"></span></b>
                        </div>
                    </div>
                    {{Form::submit('SAVE',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                    <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

                    {!! Form::close() !!}

            </div>

        </div>
    </div>
</div>

                        {{-- <input type="hidden" name="_method" value="PUT">
                        <div class="row form-group ">
                            <div class="col-md-6">
                                Request by: <b><span id="employee_name"></span></b>
                            </div>
                            <div class="form-group row"> <div class="col-md-6">
                                    <select class="form-control" name="status" id="status" data-parsley-required="true">
                                        <option value="Reject">Reject</option>
                                        <option value="Approve">Approve</option>
                                    </select>
                                    </div></div>
                        </div>
                    <div class="form-group">
                        Event Name: <b><span id="event_name"></span></b>
                    </div>
                   <div class="row form-group">
                    <div class="col-md-6">
                            <div class="input-group">
                                Date : <b><span id="started_at"></span></b>
                            </div>
                    </div>
                    <div class="col-md-6">
                            <div class="input-group">
                                Expired At: <b><span id="expired_at"></span></b>
                            </div>
                    </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            Remark: <br>
                            <b><span id="remark"></span></b>
                        </div>
                    </div>
                    <div class="row form-group ">
                        <div class="col-md-12">
                            Total Days  : <b><span id="total_days"></span></b>
                        </div>
                    </div>
                    {{Form::submit('SAVE',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                    <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

                    {!! Form::close() !!}

            </div>

        </div>
    </div>
</div> --}}

@section('js')
    @parent
    <script>
        $( "#lieus_edit" ).on( "submit", function( event ) {
            event.preventDefault();
            swal({
                title: "Update Status",
                text: "Change this user lieu status?",
                type: "info",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                preConfirm: function() {
                    var form = document.getElementById('lieus_edit');
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url: '/adminlieus/'+form.id.value,
                        data: formData,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: function(msg) {
                            return msg;
                        }
                    });
                }
            }).then((result) => {
                $('#lieus_modal_edit').modal('toggle');
                if(result.value){
                    $('#adminlieu_table').DataTable().ajax.reload();
                    swal('Status Updated!', '--', 'success');
                }
            });
        });
        </script>
    @endsection


