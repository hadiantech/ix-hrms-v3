<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visa extends Model
{
    protected $table = 'visa_informations';
    protected $guarded = [];

    public function claim(){
        return $this->belongsTo(Claim::class,'advance_claims_id');
    }
}
