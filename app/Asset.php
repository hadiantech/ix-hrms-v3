<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset extends Model
{
    public $table = "assets";

    protected $fillable = ['name','serialnum','warranty','manufact_name','asset_type','status','price'];

    public function assign()
    {
        return $this->hasMany(Assign::class);
    }

    public function type(){
        return $this->belongsTo(AssetType::class,'asset_type');
    }
}
