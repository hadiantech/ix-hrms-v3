<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveType extends Model
{
    public $table = "leave_type";
    //
    // protected $fillable = ['id','leave','leave_quota','editable','applicable_days','isDefault','isProbation','GenderType'];

    public function annuals(){

        return $this->hasMany('App\Annual');
    }


}
