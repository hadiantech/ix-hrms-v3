<?php

namespace App\Http\Controllers;

use App\EmployeeAsset;
use App\Employee;
use App\Asset;
use App\Assign;
use Auth;
use Illuminate\Http\Request;
use DataTables;

class EmployeeAssetController extends Controller
{
    //

    public function index(Employee $employee)
    {

        return Datatables::eloquent(EmployeeAsset::where('employee_id', $employee->id)->with('assign'))
        ->addColumn('assign', function($model){
            return $model->assign->assign_type;
        })
        // ->addColumn('asset', function($model){
        //     return $model->asset->name;
        // })
        ->addColumn('action', function($model) use ($employee){
            $url = 'employee.assets';
            $modal_view_id = 'modal_assign_show';
            $modal_edit_id = 'modal_assign_edit';
            $modal_data_customs = array('assign' => $model->assign, 'employee' => $model->employee, 'brands' => $model->brands);

            return view('shared._table_action', compact('model', 'url', 'modal_view_id', 'modal_edit_id', 'modal_data_customs'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

}
