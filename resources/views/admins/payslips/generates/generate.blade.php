@extends('layouts.default.master')

@section('content')

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Generate Pay</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    <ol class="breadcrumb">
                        <li><a href="dashboard-adminview.html">Dashboard</a></li>
                        <li><a href="admin-payslip.html">Payroll</a></li>
                        <li class="active">Generate Salary</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!-- ============================================================== -->
            <!-- Different data widgets -->
            <!-- ============================================================== -->
            <button data-target="#add-item-pay" data-toggle="modal" class="pull-right btn btn-rounded btn-info"><i class="fa fa-plus"></i> Add Item</button><br>
            <br><br>
            <div class="row">
                <div class="col-sm-4">
                    <div class="white-box">
                        <h3 class="box-title">Earning</h3><br>
                        <table class="table" width="100%">
                            <tr>
                                <th>Earning Name</th>
                                <th class="text-right">Total (MYR)</th>
                            </tr>
                            <tr>
                                <td>Basic Earning</td>
                                <td class="text-right">5,300.00</td>
                            </tr>
                            <tr>
                                <td>Overtime<br><a style="font-size:12px;" class="text-danger">Delete</a></td>
                                <td class="text-right">100.00</td>
                            </tr>
                            <tr>
                                <th>Total Earning</th>
                                <th class="text-right">5,400.00</th>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="white-box">
                        <h3 class="box-title">Deductions</h3> <br>
                        <table class="table" width="100%">
                            <tr>
                                <th>Deduction Name</th>
                                <th class="text-right">Total (MYR)</th>
                            </tr>
                            <tr>
                                <td>Unpaid Leave</td>
                                <td class="text-right">430.00</td>
                            </tr>
                            <tr>
                                <td>Zakat Pendapatan<br><a style="font-size:12px;" class="text-danger">Delete</a></td>
                                <td class="text-right"><input class="text-right form-control" type="number" placeholder="0.00"></td>
                            </tr>
                            <tr>
                                <td>Unicef<br>
                                <a style="font-size:12px;" class="text-danger">Delete</a></td>
                                <td class="text-right"><input class="text-right form-control" type="number" placeholder="0.00"></td>
                            </tr>
                            <tr>
                                <th>Total Deduction</th>
                                <th class="text-right">1,000.00</th>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="white-box">
                        <h3 class="box-title">Additional Item</h3> <br>
                        <table class="table" width="100%">
                            <tr>
                                <td>Monthy Tax</td>
                                <td class="text-right">
                                    <input class="text-right form-control" type="number" placeholder="0.00">
                                </td>
                            </tr>
                            <tr>
                                <td>EPF</td>
                                <td class="text-right">
                                    <input class="text-right form-control" type="number" placeholder="0.00">
                                </td>
                            </tr>
                            <tr>
                                <td>SOCSO</td>
                                <td class="text-right">
                                    <input class="text-right form-control" type="number" placeholder="0.00">
                                </td>
                            </tr>
                            <tr>
                                <td>EIS</td>
                                <td class="text-right">
                                    <input class="text-right form-control" type="number" placeholder="0.00">
                                </td>
                            </tr>
                            <tr>
                                <td>Payment Type</td>
                                <td class="text-right">
                                    <select class="selectpicker" data-style="form-control">
                                        <option>Select Payment Type</option>
                                        <option>Bank Transfer</option>
                                        <option>Cheque</option>
                                        <option>Cash Payment</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <table width="100%">
                        <tr>
                            <th>TOTAL PAYMENT</th>
                            <th class="text-right">RM 5,300.00</th>
                        </tr>

                    </table><br><br>
<button data-toggle="modal" data-target="#confirm-pay" style="width:100%" class="btn btn-lg btn-rounded btn-info">GENERATE PAY</button>

                    <br>
                    <br>
                    <br>

                </div>
            </div>





            <div class="modal fade" id="add-item-pay">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h1>Add Items to Payslip</h1>

                           <div class="form-group  ">


                             <select class="selectpicker" data-style="form-control">
                                <option>Select Payroll Category</option>
                                <option>Category 1</option>
                                <option>Category 2</option>
                                <option>Category 3</option>
                                <option>Category 4</option>
                             </select>

                           </div>


                           <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Add to Payslip</button>
                                        <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="confirm-pay">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h1>Confirm Generate Pay</h1>

                           <div class="form-group  ">
                             <p>Are you sure?</p>



                           </div>


                           <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Yes</button>
                                        <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>


        <!-- /.container-fluid -->

    </div>

    @endsection

    @section('js')
    @endsection
