<div class="modal fade" id="deductitem">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                {!! Form::open(['url' => 'generates','method'=>'post', "id" => "deduct"]) !!}
                <div class="  form-group ">

                        <div style="width:90px !important">
                        {{Form::hidden('basic_salary',$salaries->basic_salary,array('class' => 'form-control')) }}
                        </div>
                </div>
                <div class="  form-group ">

                    <div style="width:90px !important">
                    {{Form::hidden('username',$employees->username,array('class' => 'form-control')) }}
                    </div>
                </div>
                <div class="  form-group ">

                    <div style="width:90px !important">
                    {{Form::hidden('employee_id',$salaries->employee_id,array('class' => 'form-control')) }}
                    </div>
                </div>

                <div class="form-group  ">
                    {!! Form::label('Payroll Category') !!}
                    <select class="form-control" name="category_id" id="category_id" data-parsley-required="true">
                        @foreach ($categories as $categorie)
                                @if ($categorie->operand_id == null)
                                {{ "" }}
                                @elseif ($categorie->operand_id == 2)
                                <option value="{{ $categorie->id }}">{{ $categorie->name }}</option>
                            @elseif ($categorie->operand_id == 1)
                                {{ "" }}
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class=" form-group ">
                        <div style="width:90px !important">
                        {{Form::text('value', null , ['required','class' => 'form-control','placeholder' => 'insert value'])}}
                        </div>
                    </div>
                {{Form::submit('Add Item',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
               <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
               {!! Form::close() !!}



            </div>
        </div>
    </div>
</div>


@section('js')
    @parent
    <script>

        $( "#deduct" ).on( "submit", function( event ) {
            event.preventDefault();
            swal({
                title: "Submit Payslip",
                text: "Confirm to submit employee payslip?",
                type: "info",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                preConfirm: function() {
                    var form = document.getElementById('deduct');
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url: "{{ route('generates.store_deduct',['years'=> $years, 'month'=> $month]) }}",
                        data: formData,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: function(msg) {
                            return msg;
                        }
                    });
                }
            }).then((result) => {
                $('#deduct').modal('toggle');
                if(result.value){
                    $('#payroll_table').DataTable().ajax.reload(function(json){
                        var len = json.data.length;
                        $("#generates").empty();
                        $("#generates").append("<option value=''>Please Select</option>");
                        for( var i = 0; i<len; i++){
                            var id = json.data[i]['id'];
                            var name = json.data[i]['title'];
                            $("#generates").append("<option value='"+id+"'>"+name+"</option>");

                        }
                    });

                    //reload select designation


                    swal('Payslip Added!', '--', 'success');
                }
            });
        });
        </script>
        @endsection


