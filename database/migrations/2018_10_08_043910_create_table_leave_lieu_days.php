<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLeaveLieuDays extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_lieu_days', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('emp_id');
        $table->date('date_start');
        $table->date('date_end');
        $table->double('totalday');
        $table->double('totalused');
        $table->longtext('project_name');
        $table->longtext('remark');
        $table->longtext('status');
        $table->longtext('approve_by');
        $table->longtext('approve_date');
        $table->date('date_expired');
        $table->unsignedInteger('order_column')->nullable();
        $table->timestamp('created_at')->nullable();
        $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_lieu_days');
    }
}
