<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Payroll;
// use Auth;
use App\Salary;
use App\Employee;
use App\Generate;
use App\PayrollCategory;
use DataTables;
use App\Operand;
use App\Organisation;
use DB;

class PayrollController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $employees = Employee::all();
        $categories = PayrollCategory::all();
        $operands = Operand::all();
        $salaries = Salary::where('basic_salary')->count();
        // return $operands;
        return view('payslips.salaries.generates.index', compact('salaries','operands','employees','categories'));
    }

    public function index2($years, $month)
    {

        // return $years;
        $employees = Employee::all();
        $categories = PayrollCategory::all();
        $operands = Operand::all();
        // return $operands;
        return view('payslips.salaries.generates.list', compact('month','years','operands','employees','categories'));
    }

    public function generate($years, $month, $id)
    {
        $employees = Employee::find($id);
        $salaries = Salary::where('employee_id', $id)->first();
        try{
           $a = decrypt($salaries->basic_salary);
        }
        catch(\Exception $e){
            'N/A';
        }
        // filter page generate by year n month
        $generates = Generate::where('year',$years)->where('month',$month)->where('employee_id',$id)->get();//nak payroll category untuk refer ke table payroll_categories
        $categories = PayrollCategory::all();
        $operands = Operand::all();

        $totals = Generate::where('employee_id',$id)->get();
       // panggil dari table Category, display by year n month
        $totaladd = Generate::where('month',$month)->where('employee_id',$id)->where('operand_id','1')->sum('value');
        $totalded = Generate::where('month',$month)->where('employee_id',$id)->where('operand_id','2')->sum('value');

        $earns = $a + $totaladd - $totalded;

        $tax = 0.11 * $a;
        $epf = 0.11 * $a;
        return view('payslips.salaries.generates.generate',compact('a','month','years','epf','tax','earns','totalded','totaladd','totals','operands','employees','categories','salaries','generates','adds','deductions'));
    }

    public function list($years, $month){
        $a = Generate::where('year',$years)->where('month',$month)->get();

// nak view list ikot tahun n bulan
// nak masokkan var $a tu ke dalam datatable
        // ->addColumn('status', function($model){
        //     $generat = $model->status;
        // if($status == 'generated'){
        //     $label = '<span class="label label-warning">Generated</span>';
        //     }
        // else if($status == ''){
        //     $label = '<span class="label label-info">Ungenerated</span>';
        //     }
        // return $label;
// filter ikot tahun n bulan

        $tahun = $years;
        $bulan = $month;

        return Datatables::eloquent(Employee::with('gen'))

        //->addColumn('generate', function($model){
         //   return $model->gen->status;
            //return $model->generate::;
            // return $model;
       // })
        // ->addColumn('generate', function($model){
        // $stat = $model->generate;
        // if($stat == 'generated'){
        //     $label = '<span class="label label-warning">Generated</span>';
        //     }
        // else if($stat == ''){
        //     $label = '<span class="label label-info">Ungenerated</span>';
        //     }
        // return $stat;
        // })

        ->addColumn('action', function($model) use ($tahun, $bulan){
            $url = 'generates';
            // $show_hide = true;
            $delete_hide = true;
            $edit_hide = true;
            $modal_view_id = 'modal_payroll_show';
            $modal_edit_id = 'modal_payroll_edit';
            $custom_btn = [
             '<a class="btn btn-xs btn-default" data-modal_data=\''.$model.'\' href="'.route('generates.generate',['years'=> $tahun, 'month'=> $bulan, 'id' => $model->id]).'" data-toggle="modal">Generate</a>',
                // '<a data=\''.$model.'\''.$tahun.'\''.$bulan.'\' href="'.route('generates.generate',['years'=> $tahun, 'month'=> $bulan, 'id' => $model->id]).'" type="button" class="btn btn-default">Left</a>',
                // '<a href="/generates/{$model->id}" type="button">Generate</a>'
            ];

            return view('shared._table_action', compact('model', 'url','edit_hide','delete_hide','modal_view_id', 'modal_edit_id','custom_btn','years','month','modal_data_customs' ))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    public function genlist($years, $month){
        //find either this year and month already generate
        return Datatables::eloquent(Employee::query())

        ->addColumn('action', function($model) use ($years, $month){
            $url = 'generates';
            $show_hide = true;
            $delete_hide = true;
            $edit_hide = true;
            $modal_view_id = 'modal_payroll_show';
            $modal_edit_id = 'modal_payroll_edit';

            //hide custom button if generate exist

            $custom_btn = [
                '<a class="btn btn-xs btn-default open-modal" data-modal_data=\''.$model.'\' href="'.route('generates.print',['years'=> $years, 'month'=> $month, 'id' => $model->id]).'" data-toggle="modal">Print</a>',
            ];
            return view('shared._table_action', compact('model', 'url', 'edit_hide','show_hide','delete_hide','modal_view_id', 'modal_edit_id','custom_btn','years','month' ))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }


    public function list_based_employee($id){
        return Payroll::where('employee_id', $id)->get();
    }

    public function create()
    {
        // $categories = PayrollCategory::all();
        // $operands = Operand::all();
        // return View('payslips.salaries.generates.additem', compact('operands','categories','employees'));
    }

    public function addition($years, $month)
    {
        $categories = PayrollCategory::where('operand_id','1')->get();
        $operands = Operand::all();

         return View('payslips.salaries.generates.additem', compact('years','month','operands','categories','employees'));
    }

    public function deduction()
    {
        $categories = PayrollCategory::where('operand_id','2')->get();
        $operands = Operand::all();
        return View('payslips.salaries.generates.deduction', compact('operands','categories','employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store_add(Request $request, $years, $month)
    {
        $categories = PayrollCategory::all();
        $employees = Employee::all();
        $set = Salary::all();
        $operands = Operand::all();

        $add = new Generate;
        $add->employee_id = $request->employee_id;
        $add->category_id = $request->category_id;
        $add->operand_id = '1';
        $add->value = $request->value;
        $add->year = $years;
        $add->month = $month;
        $add->status = '';
        $add->save();
        return back();
    }

    public function store_deduct(Request $request, $years, $month)
    {
        $categories = PayrollCategory::all();
        $employees = Employee::all();
        $set = Salary::all();
        $operands = Operand::all();

        $add = new Generate;
        $add->employee_id = $request->employee_id;
        $add->category_id = $request->category_id;
        $add->operand_id = '2';
        $add->value = $request->value;
        $add->year = $years;
        $add->month = $month;
        $add->status = '';
        $add->save();
        return back();
    }

    public function store_gen(Request $request, $years, $month)
    {

        $categories = PayrollCategory::all();
        $employees = Employee::all();
        $set = Salary::all();
        $operands = Operand::all();

        $add = new Generate;

        $add->employee_id = $request->employee_id;
        $add->value = 0;
        $add->year = $years;
        $add->month = $month;
        $add->status = 'generated';
        $add->save();
        return response()->json($add, 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Generate $gen)
    {
        $del = Generate::find($gen->category_id);
        // $del->delete();
        // $generate->delete();
        // return redirect()->back();
        // return $del;
        // $claim = Claim::find($expense->claim_id);
        // $claim->claim_total = $claim->claim_total - $expense->amount;
        // $claim->save();
    }

    public function print($years, $month, $id)
    {
        $employees = Employee::find($id);
        $organisations = Organisation::all();
        $salaries = Salary::where('employee_id', $id)->first();
        try{
            $a = decrypt($salaries->basic_salary);
         }
         catch(\Exception $e){
             'N/A';
         }
        $generates = Generate::where('year',$years)->where('month',$month)->where('employee_id',$id)->get();//nak payroll category untuk refer ke table payroll_categories
        $categories = PayrollCategory::all();
        $operands = Operand::all();
        // $totals = Generate::where('employee_id',$id)->sum('value');
        $totals = Generate::where('employee_id',$id)->get();
        // $totaladd = Generate::where('operand_id','1')->sum('value');
        // $totalded = Generate::where('operand_id','2')->sum('value');

        $totaladd = Generate::where('month',$month)->where('employee_id',$id)->where('operand_id','1')->sum('value');
        $totalded = Generate::where('month',$month)->where('employee_id',$id)->where('operand_id','2')->sum('value');

        $earns = $a + $totaladd - $totalded;

        $tax = 0.11 * $a;
        $epf = 0.11 * $a;

        return View('payslips.salaries.generates.print',compact('a','epf','tax','earns','totalded','totaladd','totals','operands','employees','categories','salaries','generates','adds','deductions'));
    }
}
