@extends('layouts.default.master')

@section('content')

<div class="preloader">
   <svg class="circular" viewBox="25 25 50 50">
       <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
   </svg>
</div>

<div id="page-wrapper">
   <div class="container-fluid">
       <div class="row bg-title">
           <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
               <h4 class="page-title">Payslip</h4>
           </div>
           <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

               <ol class="breadcrumb">
                   <li><a href="dashboard-adminview.html">Dashboard</a></li>
                   <li><a href="admin-payslip.html">Payroll</a></li>
                   <li class="active">Generate Salary</li>
               </ol>
           </div>
           <!-- /.col-lg-12 -->
       </div>


    @endsection

    @section('js')
    @parent


    <script type="text/javascript">
            var start = 2019;
            var end = new Date().getFullYear();
            var options = "";
            for(var year = start ; year <=end; year++){
              options += "<option>"+ year +"</option>";
            }
            document.getElementById("year").innerHTML = options;
    </script>

    <script type="text/javascript">
        var d = new Date();
        var monthArray = new Array();
        monthArray[0] = "January";
        monthArray[1] = "February";
        monthArray[2] = "March";
        monthArray[3] = "April";
        monthArray[4] = "May";
        monthArray[5] = "June";
        monthArray[6] = "July";
        monthArray[7] = "August";
        monthArray[8] = "September";
        monthArray[9] = "October";
        monthArray[10] = "November";
        monthArray[11] = "December";
        for(m = 0; m <= 11; m++) {
            var optn = document.createElement("OPTION");
            optn.text = monthArray[m];
            // server side month start from one
            optn.value = (m+1);
            document.getElementById('month').options.add(optn);
        }
        </script>
        <script language="JavaScript">
                $('#btnmonth').on('click', function() {
                    var g = $('#month').val();
                  //  alert ($('#year').val());
                  //  alert ($('#month').val());
                    window.location.href = "/{month}/"+ g;

                  })

                </script>

    @endsection
