<html>
<head>
	<title>Leave Status Update</title>

<style>
body {
	background: #eee;
	font-size:14px;
	font-family:sans-serif;
	margin:0;

}
.wrap {
	margin:90px auto 50px auto;
	width:600px;
	max-width:100%;
	text-align:center;
}
.header {
	background-color: #d95867;
	color:#fff;
	font-weight:400;
	font-size:17px;
	padding:30px;
	border-top-right-radius: 11px;
	border-top-left-radius: 11px
}
.white-box {
	padding:50px ;
	color:#333;
	font-size:14px;
	font-weight:300;
	line-height: 1.6em;
	background:#fff;
	border-bottom-right-radius: 11px;
	border-bottom-left-radius: 11px
}
.footer {
	margin-top:15px;
	font-size:12px;
	font-weight:300;
}
@media screen and (max-width:760px) {
.wrap {
	margin:20px auto;
	max-width:96%;

}

</style>
</head>


<body>
	<div class="wrap">
		<img src="http://hrms.ixtelecom.net/plugins/images/ixlogo.png" align="center"><br><br><br><Br>
		<div class="header">LEAVE APPLICATION REJECTED</div>
		<div class="white-box">Thank you for your update. We will update this application approval in the system and notify the employee.</div>
		<div class="footer">Leave Application Approval via IX HRMS</div>
</body>
</html>
