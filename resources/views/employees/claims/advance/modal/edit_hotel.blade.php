{{-- Modal --}}
<div class="modal fade" id="modal_hotel_edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h1>Edit Hotel</h1>

                {!! Form::open(['url' => '', 'id' => 'hotel_edit']) !!}
                <input type="hidden" name="_method" value="PUT">
                <input id="hotel_id" name="id" type="hidden">
                <div class="form-group row">
                    <div class="col-md-12">
                        {{ Form::text('name',null,['class' => 'form-control', 'placeholder' => 'Hotel Name'])}}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        {{ Form::text('location',null,['class' => 'form-control', 'placeholder' => 'Location'])}}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        {{ Form::number('room',null,['class' => 'form-control', 'placeholder' => 'No. Of Room'])}}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        {{ Form::label('', 'Check In') }}
                        {{ Form::date('check_in',null,['class' => 'form-control', 'placeholder' => 'Check In'])}}
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        {{ Form::label('', 'Check Out') }}
                        {{ Form::date('check_out',null,['class' => 'form-control', 'placeholder' => 'Check Out'])}}
                    </div>
                </div>
                <button type="submit" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">
                    Save Hotel
                </button>
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">
                    Close
                </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('js')
@parent
<script>
    $("#hotel_edit").on("submit", function (event) {
        //console.log('mew');
        event.preventDefault();
        swal({
            title: "Update",
            text: "Are you sure you want to update?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    var form = document.getElementById('hotel_edit');
                    var formData = new FormData(form);
                    //console.log(form.traveller_id.value);
                    $.ajax({
                        type: "POST",
                        url: "/hotels/" + form.hotel_id.value,
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            resolve(data);
                        },
                        error: function () {
                            swal.showValidationMessage('server error');
                            swal.hideLoading();
                        }
                    });
                });
            }
        }).then((result) => {
            if (result.value) {
                $('#modal_hotel_edit').modal('toggle');
                $('#hotel_table').DataTable().ajax.reload();
                swal('Updated!', '--', 'success');

            }

        });
    });
</script>
@endsection
