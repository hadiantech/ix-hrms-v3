<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payroll extends Model
{
    protected $table = 'employee_payrolls';

    protected $fillable = ['salary_id','ee_epf','ee_scs','ee_eis','tax','er_epf','er_scs','er_eis','net_salary'];

    public function salary(){
        return $this->belongsTo(Salary::class,'salary_id');
    }

    public function payrollcategory(){
        return $this->hasMany(PayrollCategory::class);
    }
    public function employee()
    {
        return $this->belongsTo(Employee::class,'employee_id');
    }
    public function generate()
    {
        return $this->hasMany(Generate::class);
    }

}
