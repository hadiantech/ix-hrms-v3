@extends('layouts.default.master')

@section('content')

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Employee Leave Process</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="dashboard-adminview.html">Dashboard</a></li>
                    <li><a href="admin-claim-dashboard.html">Leave Dashboard</a></li>
                    <li class="active">Employee Leave</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="m-b-20 box-title text-uppercase">Employee Leave Process</h3>
                <div class="employee-leave-details">
                    <div class="row">
                        <div class="col-md-4">

                <h1>Leave Applications</h1>
                <div class="employee-leave-details">
                <div class="row">
                <div class="col-md-12">
                            <h4>Leave Details</h4>
                            <label>Name</label> <span class="m-l-5">{{ $leaves->employee->fname }} {{ $leaves->employee->lname }}</span><br>
                            <label>ID</label> <span class="m-l-5">{{ $leaves->employee->employee_uid }}</span><br>
                            <label>Leave Type</label> <span class="m-l-5">{{ $leaves->leave_type }}</span><br>
                            @if ($leaves->remark == 'Emergency')
                            <label>Remark</label> <span class="m-l-5">{{ $leaves->remark }}</span><br>
                            @else
                            @endif
                            <label>Leave Reason</label> <span class="m-l-5">{{ $leaves->reason }}</span><br>
                            <label>Leave Start Date</label> <span class="m-l-5">{{ $leaves->started_at }}</span><br>
                            <label>Leave End Date</label> <span class="m-l-5">{{ $leaves->ended_at }}</span><br>
                            <label>Leave Attachment (if any)</label>
                            <span class="m-l-5"><br>
                                        @if($leaves->getMedia('leaves')->first())
                                            <img src="{{ $leaves->getMedia('leaves')->first()->getUrl() }}" width="200" height="100" class="leaves">
                                            <br><a href="{{ $leaves->getMedia('leaves')->first()->getUrl() }}" width="200" height="100" class="leaves">View Slip</a><br>
                                        @else
                                            No Slip was attached. Please check with applicant or check Leave Type.
                                            Annual Leave does not need any atachments.
                                        @endif
<br>
                            </span>
                            <br>
                            {{-- <label>Department</label> <span class="m-l-5">{{ $leaves->employee->department->department_name }}, {{ $leaves->employee->organisation->company_name }}</span><br> --}}
                            {{-- <label>Applied Date</label> <span class="m-l-5">{{ Carbon\Carbon::parse($leaves->claim_applied_date)->format('d F Y') }}</span><br> --}}

                            <div class="col-md-12">
                                    <select disabled="disabled" class="selectpicker" data-style="form-control">
                                        <option value="" disabled>Half Day?</option>
                                        <option selected>Halfday AM on Start Date</option>
                                        <option>Halfday PM on Start Date</option>
                                        <option>Halfday AM on End Date</option>
                                        <option>Halfday PM on End Date</option>
                                    </select>
                            </div>
                </div>
                </div>
                <br>
                {!! Form::open(['url' => '', 'id' => 'adminleave_update']) !!}
                            <input type="hidden" name="_method" value="PUT">
                            <input id="id" name="id" type="hidden" value="{{ $leaves->id }}" />

                        <button type="submit" onclick="proc_status('reject')" style="width:200px" class="m-r-10 btn-lg btn-rounded btn-warning text-uppercase">Reject</button>
                        <button type="submit" onclick="proc_status('proc')" style="width:200px" class="m-r-10 btn-lg btn-rounded btn-success text-uppercase">Reviewed</button>

                        @if($approver_level->level == '2')
                        <button type="submit" onclick="proc_status('approve')" style="width:200px" class="m-r-10 btn-lg btn-rounded btn-success text-uppercase">Approve</button>
                        @endif
                {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
                </div>
        </div>

@endsection
@section('js')
@parent
 <script>
    var status;
    var proc_status;

    function proc_status(e){
        if(e == 'reject'){
            proc_status = 'reject';
            status = 'pending';
        }
        else if(e == 'proc'){
            proc_status  = e;
            status = 'processed';
        }
        else if(e == 'approve'){
            proc_status = e;
            status = 'approve';
        }
        else{
            proc_status = e;
        }
    }

    $( "#adminleave_update" ).on( "submit", function( event ) {
            event.preventDefault();
            swal({
                title: "Submit Changes",
                text: "Update Employee Leave Application?",
                type: "info",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                allowOutsideClick: () => !swal.isLoading(),
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        var form = document.getElementById('adminleave_update');
                        var formData = new FormData(form);
                        formData.append('status', status);
                        formData.append('leave_proc_status', proc_status);
                        $.ajax({
                            type: "POST",
                            url: '/adminleaves/'+form.id.value,
                            data: formData,
                            cache:false,
                            contentType: false,
                            processData: false,
                            success: function(data) {
                                resolve(data);
                            },
                            error: function(){
                                swal.showValidationMessage('server error');
                                swal.hideLoading();
                            }
                        });
                    });

                }
            }).then((result) => {
                if(result.value){
                    swal('Application Updated!', '--', 'success').then((result) => {
                        window.location.href = "{{  route('adminleaves.index') }}";
                    });

                }
            });
        });
        </script>
        @endsection
