<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();;
            $table->string('serialnum')->nullable();;
            $table->string('warranty')->nullable();;
            $table->double('price')->nullable();;
            $table->string('asset_type')->nullable();;
            $table->string('asset_brand')->nullable();;
            $table->string('description')->nullable();;
            $table->string('status')->nullable();;
            $table->string('created_by')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
