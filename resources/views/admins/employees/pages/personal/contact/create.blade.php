<div class="modal fade" id="add-contact" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Add Contact</h1>

                {!! Form::open(['url' => '', 'id' => 'contact_create']) !!}
                <div class="form-group">
                    {!! Form::label('Full Name') !!}
                    {!! Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'First Name', 'required')) !!}
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                        {!! Form::label('Relationship') !!}
                        {!! Form::text('relation', null, array('class' => 'form-control', 'placeholder' => 'Relationship', 'required')) !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('MyKAD No./Passport No.') !!}
                        {!! Form::text('ic', null, array('class' => 'form-control', 'placeholder' => 'MyKAD No./Passport No.', 'required')) !!}
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                        {!! Form::label('Phone No.') !!}
                        {!! Form::text('phone', null, array('class' => 'form-control', 'placeholder' => 'Phone No.', 'required')) !!}
                    </div>

                    <div class="col-md-6">
                        {!! Form::label('Date of Birth') !!}
                        {!! Form::date('dob', null, array('class' => 'form-control', 'placeholder' => 'Date of Birth', 'required')) !!}
                    </div>
                </div>

                {{Form::submit('Add Contact',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('js')
    @parent
    <script>
        $('#add-contact').on('hidden.bs.modal', function(e) {
            $(this).find('#contact_create')[0].reset();
        });

        $( "#contact_create" ).on( "submit", function( event ) {
            event.preventDefault();
            swal({
                title: "Submit Contact",
                text: "Are you sure you want to submit this contact?",
                type: "info",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                allowOutsideClick: () => !swal.isLoading(),
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        var form = document.getElementById('contact_create');
                        var formData = new FormData(form);
                        $.ajax({
                            type: "POST",
                            url: '{{ route('employees.contacts.store', $employee->id) }}',
                            data: formData,
                            cache:false,
                            contentType: false,
                            processData: false,
                            success: function(data) {
                                resolve(data);
                            },
                            error: function(data) {
                                errormsg = '';
                                if(data.status == 422 ){
                                    $.each(data.responseJSON.errors, function (key, value) {
                                        errormsg = errormsg + value[0] + "<br />";
                                    });
                                }else{
                                    console.log(data);
                                    errormsg = "Server error. Please try again";
                                }
                                swal.showValidationMessage(errormsg);
                                swal.hideLoading();
                            },
                        });
                    });

                }
            }).then((result) => {
                if(result.value){
                    $('#add-contact').modal('toggle');
                    swal('Contact Saved!', '--', 'success');
                    $('#contact_table').DataTable().ajax.reload();
                }
            });
        });


    </script>
@endsection
