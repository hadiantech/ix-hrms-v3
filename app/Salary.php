<?php

namespace App;

use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    use HasRoles;
    protected $guarded = [];
    protected $table = 'employee_salaries';

    protected $fillable = ['employee_id','basic_salary','salary_type','basic_salary_increment','increment_type'];

    public function employee(){
        return $this->belongsTo(Employee::class,'employee_id');
    }

    public function payroll(){
        return $this->hasMany(Payroll::class);
    }
}
