<?php

namespace App\Http\Controllers;

use App\EmployeeLieu;
use Illuminate\Http\Request;

class EmployeeLieuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeLieu  $employeeLieu
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeLieu $employeeLieu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeLieu  $employeeLieu
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeLieu $employeeLieu)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeLieu  $employeeLieu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeLieu $employeeLieu)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeLieu  $employeeLieu
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeLieu $employeeLieu)
    {
        //
    }
}
