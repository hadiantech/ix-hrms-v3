<div class="row">
    <div class="col-md-8 col-md-offset-2">
        {!! Form::model($employee, ['route' => ['employees.update', $employee->id], 'id' => 'employee_update_leave', 'method' => 'PUT']) !!}
        <h3 class="box-title">Leave Allocation</h3>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr style="border-bottom:3px solid #eee">
                        <th>Leave Type</th>
                        <th width="20%" class="text-right">Allocation</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Annual Leave</td>
                        <td>
                            {{ Form::number('annual_leave', $employee->leave_allocs()->type('Annual Leave')->first() ? $employee->leave_allocs()->type('Annual Leave')->first()->quota : settings('Annual Leave'), ['required', 'min'=>'0', 'class'=>'form-control']) }}
                        </td>
                    </tr>
                    <tr>
                    <td>Emergency Leave</td>
                        <td>
                            {{ Form::number('emergency_leave', $employee->leave_allocs()->type('Emergency Leave')->first() ? $employee->leave_allocs()->type('Emergency Leave')->first()->quota : settings('Emergency Leave'), ['required', 'min'=>'0', 'class'=>'form-control']) }}
                        </td>
                    </tr>
                    <tr>
                        <td>Medical Leave</td>
                        <td>
                            {{ Form::number('medical_leave', $employee->leave_allocs()->type('Medical Leave')->first() ? $employee->leave_allocs()->type('Medical Leave')->first()->quota : settings('Medical Leave'), ['required', 'min'=>'0', 'class'=>'form-control']) }}
                        </td>
                    </tr>
                    <tr>
                        <td>Marriage Leave</td>
                        <td>
                            {{ Form::number('marriage_leave', $employee->leave_allocs()->type('Marriage Leave')->first() ? $employee->leave_allocs()->type('Marriage Leave')->first()->quota : settings('Marriage Leave'), ['required', 'min'=>'0', 'class'=>'form-control']) }}
                        </td>
                    </tr>
                    <tr>
                        <td>Compassionate Leave</td>
                        <td>
                            {{ Form::number('compassionate_leave', $employee->leave_allocs()->type('Compassionate Leave')->first() ? $employee->leave_allocs()->type('Compassionate Leave')->first()->quota : settings('Compassionate Leave'), ['required', 'min'=>'0', 'class'=>'form-control']) }}
                        </td>
                    </tr>
                    <tr>
                        <td>Maternity Leave</td>
                        <td>
                            {{ Form::number('maternity_leave', $employee->leave_allocs()->type('Maternity Leave')->first() ? $employee->leave_allocs()->type('Maternity Leave')->first()->quota : settings('Maternity Leave'), ['required', 'min'=>'0', 'class'=>'form-control']) }}
                        </td>
                    </tr>
                    <tr>
                        <td>Paternity Leave</td>
                        <td>
                            {{ Form::number('paternity_leave', $employee->leave_allocs()->type('Paternity Leave')->first() ? $employee->leave_allocs()->type('Paternity Leave')->first()->quota : settings('Paternity Leave'), ['required', 'min'=>'0', 'class'=>'form-control']) }}
                        </td>
                    </tr>


                </tbody>
            </table>
        </div>
        {{Form::submit('Save',['class' =>'m-b-20 btn-lg btn-rounded btn-info text-uppercase', 'style'=>'width:200px'])}}
        {!! Form::close() !!}
    </div>
</div>

 @section('js')
    @parent
    <script>
    $( "#employee_update_leave" ).on( "submit", function( event ) {
        event.preventDefault();

        swal({
        title: 'Saving',
        onOpen: () => {
            swal.showLoading()
            return new Promise(function(resolve, reject) {
                var form = document.getElementById('employee_update_leave');
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: "{{ route('employees.leave.update', $employee->id) }}",
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        swal.close();
                    },
                    error: function(data) {
                        errormsg = '';
                        if(data.status == 422 ){
                            $.each(data.responseJSON.errors, function (key, value) {
                                errormsg = errormsg + value[0] + "<br />";
                            });
                        }else{
                            errormsg = "Server error. Please try again";
                        }
                        swal.showValidationMessage(errormsg);
                        swal.hideLoading();
                    },
                });
            });
        },
        allowOutsideClick: () => !swal.isLoading()
        })
    });
    </script>
 @endsection
