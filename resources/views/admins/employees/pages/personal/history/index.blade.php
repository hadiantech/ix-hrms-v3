<div class="employment-info m-b-30">
    <div class="section-title">
        <h3 class="box-title">Employment Background</h3>
    </div>

    <a href="#add-history" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Employment</a>
    <br />
    <br />

    <div class="table-responsive">
        <table id="history_table" class="table">
            <thead>
                <tr style="border-bottom:3px solid #eee">
                    <th>#</th>
                    <th>Company</th>
                    <th class="text-center">Position</th>
                    <th class="text-center">Contact</th>
                    <th class="text-center">Period</th>
                    <th class="text-right">Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>


@include('admins.employees.pages.personal.history.create')
@include('admins.employees.pages.personal.history.edit')


@section('js')
    @parent

    <script>
        $(function () {
            $('#history_table').DataTable({
                serverSide: true,
                processing: true,
                ajax: '{{ route("employees.histories.index", $employee->id) }}',
                columns: [
                    {data: 'DT_Row_Index', orderable: false, searchable: false},
                    {data: 'company_name'},
                    {data: 'company_position'},
                    {data: 'company_contact'},
                    {data: 'period', orderable: false, searchable: false},
                    {data: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@endsection
