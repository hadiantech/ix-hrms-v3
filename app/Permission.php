<?php

namespace App;

class Permission  extends \Spatie\Permission\Models\Permission
{
    public static function defaultPermissions()
    {
        return [

            'view_employees',
            'add_employees',
            'edit_employees',
            'delete_employees',

            'view_employees_contacts',
            'add_employees_contacts',
            'edit_employees_contacts',
            'delete_employees_contacts',

            'view_employees_histories',
            'add_employees_histories',
            'edit_employees_histories',
            'delete_employees_histories',

            'view_employees_educations',
            'add_employees_educations',
            'edit_employees_educations',
            'delete_employees_educations',

            'view_employees_certificates',
            'add_employees_certificates',
            'edit_employees_certificates',
            'delete_employees_certificates',

            'view_employees_medicals',
            'add_employees_medicals',
            'edit_employees_medicals',
            'delete_employees_medicals',

            'view_employees_documents',
            'add_employees_documents',
            'edit_employees_documents',
            'delete_employees_documents',

            'view_organisations',
            'add_organisations',
            'edit_organisations',
            'delete_organisations',

            'view_departments',
            'add_departments',
            'edit_departments',
            'delete_departments',

            'view_designations',
            'add_designations',
            'edit_designations',
            'delete_designations',

            'view_hierarchies',
            'add_hierarchies',
            'edit_hierarchies',
            'delete_hierarchies',

            'view_claims',
            'add_claims',
            'edit_claims',
            'delete_claims',

            'view_expenses',
            'add_expenses',
            'edit_expenses',
            'delete_expenses',

            'view_adminclaims',
            'add_adminclaims',
            'edit_adminclaims',
            'delete_adminclaims',

            'view_claimadvances',
            'add_claimadvances',
            'edit_claimadvances',
            'delete_claimadvances',

            'view_notices',
            'add_notices',
            'edit_notices',
            'delete_notices',

            'view_employee_events',
            'add_employee_events',
            'edit_employee_events',
            'delete_employee_events',

            'view_events',
            'add_events',
            'edit_events',
            'delete_events',

            'view_travellers',
            'add_travellers',
            'edit_travellers',
            'delete_travellers',

            'view_assets',
            'add_assets',
            'edit_assets',
            'delete_assets',

            'view_types',
            'add_types',
            'edit_types',
            'delete_types',

            'view_brands',
            'add_brands',
            'edit_brands',
            'delete_brands',

            'view_assigns',
            'add_assigns',
            'edit_assigns',
            'delete_assigns',

            'view_visas',
            'add_visas',
            'edit_visas',
            'delete_visas',

            'view_flights',
            'add_flights',
            'edit_flights',
            'delete_flights',

            'view_hotels',
            'add_hotels',
            'edit_hotels',
            'delete_hotels',

            'view_adminclaims_request',
            'add_adminclaims_request',
            'edit_adminclaims_request',
            'delete_adminclaims_request',

            'view_payslips',
            'add_payslips',
            'edit_payslips',
            'delete_payslips',

            'view_salaries',
            'add_salaries',
            'edit_salaries',
            'delete_salaries',
            'salary_salaries',

            'view_generates',
            'add_generates',
            'edit_generates',
            'delete_generates',

            'view_empgenerates',
            'add_empgenerates',
            'edit_empgenerates',
            'delete_empgenerates',

            'view_adminlieus',
            'add_adminlieus',
            'edit_adminlieus',
            'delete_adminlieus',

            'view_admin_lieus',
            'add_admin_lieus',
            'edit_admin_lieus',
            'delete_admin_lieus',

            'view_default_documents',
            'add_default_documents',
            'edit_default_documents',
            'delete_default_documents',

            'view_admin_leaves',
            'add_admin_leaves',
            'edit_admin_leaves',
            'delete_admin_leaves',

            'view_leaves',
            'add_leaves',
            'edit_leaves',
            'delete_leaves',

            'view_roles',
            'add_roles',
            'edit_roles',
            'delete_roles',

            'view_dashboard',
            'view_system_setting'

        ];
    }
}
