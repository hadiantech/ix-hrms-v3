<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operand extends Model
{
    public $table = "payroll_operands";

    protected $fillable = ['operand_name'];

    public function payrollcategory()
    {
        return $this->hasMany(PayrollCategory::class);
    }

    public function generate()
    {
        return $this->hasMany(Generate::class);
    }
    // public function scopeAddition($query){
    //     return $query->where('id',1);
    // }
}
