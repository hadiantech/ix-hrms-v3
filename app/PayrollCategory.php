<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollCategory extends Model
{
    // protected $primaryKey = 'id';
    public $table = "payroll_categories";

    protected $fillable = ['name','code','operand_id','amount','default'];

    public function generate(){
        return $this->hasMany(Generate::class);
    }

    public function operand(){
        return $this->belongsTo(Operand::class,'operand_id');
    }
    // public function scopeAddition($query){
    //     return $query->where('operand_id',1);
    // }

}
