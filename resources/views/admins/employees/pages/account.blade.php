<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h3 class="box-title">Account Details</h3>
        {!! Form::model($employee, ['url' => '', 'id' => 'account_update', 'method' => 'PUT']) !!}
        <div class="form-group row">
            <div class="col-md-6">
                {!! Form::label('Username') !!}
                {!! Form::text('username', null, array('class' => 'form-control', 'placeholder' => 'Username')) !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('Email') !!}
                {!! Form::email('email', null, array('class' => 'form-control', 'placeholder' => 'Email')) !!}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                {!! Form::label('Password') !!}
                {!! Form::password('password', array('id' => 'password', 'class' => 'form-control', 'placeholder' => 'Password')) !!}
                <small>Leave blank if do not want to change</small>
            </div>
        </div>
        <button style="width:200px" class=" btn-lg btn-rounded btn-info text-uppercase">Save</button>
        {!! Form::close() !!}
    </div>
</div>


@section('js')
@parent
    <script>
    $( "#account_update" ).on( "submit", function( event ) {
        event.preventDefault();

        swal({
        title: 'Saving',
        onOpen: () => {
            swal.showLoading()
            return new Promise(function(resolve, reject) {
                var form = document.getElementById('account_update');
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: "{{ route('employees.account.update', $employee->id) }}",
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        $('#password').val('');
                        swal.close();
                    },
                    error: function(data) {
                        errormsg = '';
                        if(data.status == 422 ){
                            $.each(data.responseJSON.errors, function (key, value) {
                                errormsg = errormsg + value[0] + "<br />";
                            });
                        }else{
                            errormsg = "Server error. Please try again";
                        }
                        swal.showValidationMessage(errormsg);
                        swal.hideLoading();
                    },
                });
            });
        }})
    });
    </script>
@endsection
