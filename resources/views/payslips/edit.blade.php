<div id="modal_payroll_edit" class="modal fade in" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Edit Payroll Category</h1>
                    {!! Form::open(['url' => 'payslips', 'id' => 'payroll_edit']) !!}
                    {{ Form::hidden('id', null , ['id' => 'id']) }}
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group ">
                        <p class="text-muted">PAYROLL CATEGORY NAME</p>
                        {{Form::text('name', null , ['id' => 'payroll_name_edit', 'required','class' => 'form-control','placeholder' => 'edit asset name'])}}
                    </div>
                    <div class="form-group ">
                        <p class="text-muted">PAYROLL OPERAND</p>
                        <H4><span id="modal_payroll_operand"> </span></H4>
                    </div>
                    {{Form::submit('Update Details',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                    <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                    {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('js')
    @parent
    <script>

    $( "#payroll_edit" ).on( "submit", function( event ) {
        event.preventDefault();
        swal({
            title: "Update Category Details",
            text: "Change the details of this category?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                var form = document.getElementById('payroll_edit');
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: '/payslips/'+form.id.value,
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(msg) {
                        return msg;
                    }
                });
            }
        }).then((result) => {
            $('#modal_payroll_edit').modal('toggle');
            if(result.value){
                $('#payroll_table').DataTable().ajax.reload();
                swal('Category Details Updated!', '--', 'success');
            }
        });
    });
    </script>
@endsection
