<?php

namespace App\Http\Controllers;

use App\EmployeeCertificate;
use App\Employee;
use Illuminate\Http\Request;
use DataTables;

class EmployeeCertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Employee $employee)
    {
        return Datatables::eloquent(EmployeeCertificate::where('employee_id', $employee->id))
        ->addColumn('action', function($model) use ($employee){
            $model_2 = $employee;
            $modal_view_id = 'modal_cert_show';
            $modal_edit_id = 'modal_cert_edit';
            $url = 'employees.certificates';
            $show_hide = true;

            if($model->getMedia('certificate')->first()){
                $custom_btn = [
                    '<a class="btn btn-xs btn-success" href="'.$model->getMedia('certificate')->first()->getUrl().'" target="_blank">View Certificate</a>',
                ];
            }

            return view('shared._table_action', compact('model', 'url', 'modal_view_id', 'modal_edit_id', 'model_2', 'show_hide', 'custom_btn'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee)
    {
        $request->validate([
            'cert' => 'required|max:2048',
        ]);

        $certificate = EmployeeCertificate::create($request->except(['cert']));
        $employee->educations()->save($certificate);
        $certificate->addMediaFromRequest('cert')->toMediaCollection('certificate');
        return response()->json($certificate, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmployeeCertificate  $employeeCertificate
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeCertificate $employeeCertificate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmployeeCertificate  $employeeCertificate
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeCertificate $employeeCertificate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmployeeCertificate  $employeeCertificate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee, EmployeeCertificate $certificate)
    {
        $request->validate([
            'cert' => 'max:2048',
        ]);

        $certificate->update($request->except(['cert']));

        if ($request->hasFile('cert')) {
            $certificate->clearMediaCollection('certificate');
            $certificate->addMediaFromRequest('cert')->toMediaCollection('certificate');
        }

        return response()->json($certificate, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmployeeCertificate  $employeeCertificate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee, EmployeeCertificate $certificate)
    {
        $certificate->delete();
        return redirect()->back();
    }
}
