<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PragmaRX\Countries\Package\Countries;
use Auth;

class HomeController extends Controller
{

    public function welcome(){
        if (Auth::check()) {
            return redirect()->route('dashboard');
        }
        return redirect()->route('login');
    }

    public function countries(){

        $countries = Countries::all()->pluck('name.common');

        return $countries;

        $formatted_tags = [];
        $i = 1;

        foreach ($countries as $country) {
            $formatted_tags[] = ['id' => $country, 'text' => $country];
            $i++;
        }

        return $countries;

    }

}
