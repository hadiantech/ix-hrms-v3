<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <h3 class="box-title">Employee Info</h3>
        {!! Form::model($employee, ['url' => '', 'id' => 'employee_info_update', 'method' => 'PUT']) !!}
        <div class="form-group row">

            <div class="col-md-4">
                {!! Form::label('Company') !!}
                {{ Form::select('company_id', $organisations, null, array('id'=>"organisations", 'class'=>'form-control', 'placeholder'=>'Please Select ...')) }}
            </div>
            <div class="col-md-4">
                {!! Form::label('Employee ID') !!}
                {{Form::text('employee_uid', null , ['class' => 'form-control','placeholder' => 'Employee ID'])}}
            </div>
            <div class="col-md-4">
                {!! Form::label('Date Joined') !!}
                {{ Form::date('joined_at',null,['class'=>'form-control','placeholder'=>'Date Joined']) }}
            </div>
            <div class="col-md-4">
                {!! Form::label('Employment Status') !!}
                {{Form::select('status', ['Permanent' => 'Permanent', 'Contract' => 'Contract'], null, ['required', 'class' => 'form-control', 'placeholder' => 'Please  Select'])}}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-4">
                {!! Form::label('Probation Status') !!}
                {{Form::select('probation_status', ['Yes' => 'Yes', 'No' => 'No'], null, ['class' => 'form-control', 'placeholder' => 'Probation Status'])}}
            </div>
            <div class="col-md-4">
                {!! Form::label('Date of Confirmation') !!}
                {{ Form::date('confirmed_at',null,['class'=>'form-control','placeholder'=>'Date of Confirmation']) }}
            </div>
            <div class="col-md-4">
                {!! Form::label('Contract End Date') !!}
                {{ Form::date('contract_ended_at',null,['class'=>'form-control','placeholder'=>'Contract End Date"']) }}
            </div>
        </div>

        <hr>
        <h3 class="box-title">Position Details</h3>

        <div class="form-group row m-b-40">
            <div class="col-md-4">
                    {!! Form::label('Department') !!}
                    {{ Form::select('department_id', $departments, null, array('id'=>"department_id", 'class'=>'form-control', 'placeholder'=>'Please Select ...')) }}
            </div>
            <div class="col-md-4">
                    {!! Form::label('Designation') !!}
                    {{ Form::select('designation_id', $designations, null, array('id'=>"designation_id", 'class'=>'form-control', 'placeholder'=>'Please Select ...')) }}
            </div>
            <div class="col-md-4">
                {!! Form::label('Hierarchy') !!}
                {{ Form::select('hierarchy_id', $hierarchies, null, array('id'=>"hierarchies", 'class'=>'form-control', 'placeholder'=>'Please Select ...')) }}
            </div>
            {{-- <div class="col-md-4">
                    {!! Form::label('Designation') !!}
                    {{ Form::select('designation_id', $designations, null, array('id'=>"designations", 'class'=>'form-control', 'placeholder'=>'Please Select ...')) }}
                </div> --}}
        </div>
        <div class="form-group row m-b-20">
            <div class="col-md-4">
                    {!! Form::label('Employment Role') !!}
                    {{ Form::select('roles',$roles) }}
                    {{--  @foreach($roles as $role)<br>  --}}

                    {{--  {{ Form::checkbox('role[]',  $role->role_id ) }}  --}}
                    {{--  <input type="checkbox" id="role_id" name="role[]" value="{{$role->id}}">  --}}
                    {{--  {{ Form::hidden('role[]', false) }}  --}}
                    {{--  {{ Form::checkbox('role[]', true) }}  --}}
                    {{--  {{ Form::checkbox('role[]', '$role->id', in_array($role->id, $role->permissions)) }}  --}}
                    {{--  {{ Form::checkbox('role[]', $role->id, $role->permissions) }}  --}}
                    {{--  {{ $role->name  }}  --}}

                    {{--  @endforeach  --}}
            </div>
        </div>



        <h3 class="box-title">Non Working Days</h3>
        @php
        if($employee->working_days ==null){
            $days = [];
        }else{
            $days = $employee->working_days;
        }
        @endphp
        <div class="form-inline row m-b-40">
            <div class="checkbox checkbox-info">
                {{ Form::checkbox('days[]','1', in_array('1', $days)) }}
                {!! Form::label('Monday') !!}
            </div>
            <div class="checkbox checkbox-info">
                {{ Form::checkbox('days[]','2', in_array('2', $days)) }}
                {!! Form::label('Tuesday') !!}
            </div>
            <div class="checkbox checkbox-info">
                {{ Form::checkbox('days[]','3', in_array('3', $days)) }}
                {!! Form::label('Wednesday') !!}
            </div>
            <div class="checkbox checkbox-info">
                {{ Form::checkbox('days[]','4', in_array('4', $days)) }}
                {!! Form::label('Thursday') !!}
            </div>
            <div class="checkbox checkbox-info">
                {{ Form::checkbox('days[]','5', in_array('5', $days)) }}
                {!! Form::label('Friday') !!}
            </div>
            <div class="checkbox checkbox-info">
                {{ Form::checkbox('days[]','6', in_array('6', $days)) }}
                {!! Form::label('Saturday') !!}
            </div>
            <div class="checkbox checkbox-info">
                {{ Form::checkbox('days[]','0', in_array('0', $days)) }}
                {!! Form::label('Sunday') !!}
            </div>

        </div>
        <button style="width:200px" class=" btn-lg btn-rounded btn-info text-uppercase">Save</button>
        {!! Form::close() !!}
    </div>

</div>


@section('js')
@parent


<script>
    $(document).ready(function(){

        $("#department_id").change(function(){
            var deptid = $(this).val();

            $.ajax({
                url: '/designations/department/'+deptid,
                type: 'GET',
                dataType: 'json',
                success:function(response){
                    var len = response.length;
                    $("#designation_id").empty();
                    $("#designation_id").append("<option value=''>Please Select</option>");
                    for( var i = 0; i<len; i++){
                        var id = response[i]['id'];
                        var name = response[i]['designation_name'];
                        $("#designation_id").append("<option value='"+id+"'>"+name+"</option>");

                    }
                }
            });
        });

    });
    </script>



    <script>
    $( "#employee_info_update" ).on( "submit", function( event ) {
        event.preventDefault();

        swal({
        title: 'Saving',
        onOpen: () => {
            swal.showLoading()
            return new Promise(function(resolve, reject) {
                var form = document.getElementById('employee_info_update');
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: "{{ route('employees.info.update', $employee->id) }}",
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(data) {
                        swal.close();
                    },
                    error: function(data) {
                        errormsg = '';
                        if(data.status == 422 ){
                            $.each(data.responseJSON.errors, function (key, value) {
                                errormsg = errormsg + value[0] + "<br />";
                            });
                        }else{
                            errormsg = "Server error. Please try again";
                        }
                        swal.showValidationMessage(errormsg);
                        swal.hideLoading();
                    },
                });
            });
        }})
    });
    </script>
@endsection
