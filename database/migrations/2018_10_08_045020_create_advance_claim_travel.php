<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvanceClaimTravel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advance_claim_travel', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('advance_id')->nullable();
            $table->text('flight_type')->nullable();
            $table->longtext('flight_detail')->nullable();
            $table->date('date_travel')->nullable();
            $table->text('flight_from')->nullable();
            $table->text('flight_to')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advance_claim_travel');
    }
}
