<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssetType extends Model
{
    public $table = "asset_types";

    protected $fillable = ['type_name','status'];

    public function assign()
    {
        return $this->hasMany('App\Assign','type_name');
    }
    public function asset()
    {
        return $this->belongsTo(Asset::class);
    }
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

}
