<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project', function (Blueprint $table) {
            $table->increments('id');
            $table->longtext('project_name')->nullable();
            $table->longtext('project_code')->nullable();
            $table->double('project_budget',10,2)->nullable();
            $table->double('project_balance',10,2)->nullable();
            $table->longtext('project_desc')->nullable();
            $table->date('project_start')->nullable();
            $table->date('project_end')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
}
