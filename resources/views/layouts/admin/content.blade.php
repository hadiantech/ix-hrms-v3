<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add New Employee</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                <ol class="breadcrumb">
                    <li>
                        <a href="dashboard-adminview.html">Dashboard</a>
                    </li>
                    <li>
                        <a href="admin-employee-dashboard.html">Employee Dashboard</a>
                    </li>
                    <li class="active">Edit Employee</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->



        <!--employee -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box p-0">
                    <div class="sttabs tabs-style-iconbox">
                        <form>
                            <nav>
                                <ul>
                                    <li>
                                        <a href="#personalinfo" class="sticon ti-user">
                                            <span>Personal Details</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#employementinfo" class="sticon ti-list">
                                            <span>Employment Details</span>
                                        </a>
                                    </li>

                                    <li>
                                        <a href="#medicalinfo" class="sticon ti-info-alt">
                                            <span>Medical Info</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#leaveallocation" class="sticon ti-calendar">
                                            <span>Leave Allocation</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#approver" class="sticon ti-layers-alt">
                                            <span>Approval Layer</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#supportingdoc" class="sticon ti-files">
                                            <span>Supporting Documents</span>
                                        </a>
                                    </li>
                                    </li>
                                </ul>
                            </nav>
                            <div class="content-wrap p-t-20">
                                <section id="personalinfo">
                                    <div class="row">
                                        <div class="col-md-4 text-center m-b-40">
                                            <img src="https://cdn.iconscout.com/public/images/icon/free/png-512/avatar-user-teacher-312a499a08079a12-512x512.png" class="profile-photo">
                                            <br>
                                            <button class="m-t-30 btn btn-rounded btn-info text-uppercase">Upload Photo</button>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group row">
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" placeholder="First Name">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" placeholder="Last Name">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control datepicker-autoclose" placeholder="Date of Birth">
                                                        <span class="input-group-addon">
                                                            <i class="icon-calender"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <select class="selectpicker" data-style="form-control">
                                                        <option selected disabled>Gender</option>
                                                        <option>Male</option>
                                                        <option>Female</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <select class="selectpicker" data-style="form-control">
                                                        <option selected disabled>Marital Status</option>
                                                        <option>Single</option>
                                                        <option>Married</option>
                                                        <option>Widowed</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">

                                                <div class="col-md-6">
                                                    <select class="selectpicker" data-style="form-control">
                                                        <option selected disabled>Race</option>
                                                        <option>Race 1</option>
                                                        <option>Race 2</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-6">
                                                    <select class="selectpicker" data-style="form-control">
                                                        <option selected disabled>Religion</option>
                                                        <option>Religion 1</option>
                                                        <option>Religion 1</option>
                                                        <option>Religion 1</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" placeholder="MyKAD No./Passport No.">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" placeholder="Phone Number">
                                                </div>
                                            </div>
                                            <div class="employee-addresses m-b-30">
                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        <h3 class="box-title">Permanent Address</h3>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" placeholder="Address Line 1">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" placeholder="Address Line 2">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" placeholder="City">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" placeholder="State">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" placeholder="Postcode">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" placeholder="Country">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" placeholder="Phone No.">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" placeholder="Mobile No.">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        <h3 class="box-title">Correspondance Address</h3>
                                                        <div class="m-b-15 checkbox checkbox-info">
                                                            <input id="checkbox4" type="checkbox">
                                                            <label for="checkbox4">Same as the above?</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" placeholder="Address Line 1">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" placeholder="Address Line 2">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" placeholder="City">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" placeholder="State">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" placeholder="Postcode">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" placeholder="Country">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" placeholder="Phone No.">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" placeholder="Mobile No.">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="bank-info   m-b-30">
                                                <div class="section-title">
                                                    <h3 class="box-title">Banking Info</h3>

                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" placeholder="Bank Name">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" placeholder="Account No">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="family-info  m-b-30">
                                                <div class="section-title">
                                                    <h3 class="box-title">Spouse, Children & Others</h3>

                                                </div>
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                            <tr style="border-bottom:3px solid #eee">
                                                                <th>#</th>
                                                                <th>Name</th>
                                                                <th class="text-center">MyKAD/Passport No.</th>
                                                                <th class="text-center">Relation</th>
                                                                <th class="text-center">DOB</th>
                                                                <th class="text-right">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>Husnatul Heehaw</td>
                                                                <td class="text-center">140605160124</td>
                                                                <td class="text-center">Daughter</td>
                                                                <td class="text-center">08 Aug 2018</td>
                                                                <td class="text-right">
                                                                    <a class="m-r-5 label label-info text-uppercase">
                                                                        <i class="fa fa-pencil"></i>
                                                                    </a>
                                                                    <a class="label label-danger text-uppercase">
                                                                        <i class="fa fa-trash"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>2</td>
                                                                <td>John Cena Husni</td>
                                                                <td class="text-center">140605160124</td>
                                                                <td class="text-center">Son</td>
                                                                <td class="text-center">08 Aug 2018</td>
                                                                <td class="text-right">
                                                                    <a class="m-r-5 label label-info text-uppercase">
                                                                        <i class="fa fa-pencil"></i>
                                                                    </a>
                                                                    <a class="label label-danger text-uppercase">
                                                                        <i class="fa fa-trash"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr style="border-bottom:3px solid #eee">
                                                                <td colspan="6">
                                                                    <a href="#add-family" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Family</a>
                                                                </td>

                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>


                                            <div class="employment-info m-b-30">
                                                <div class="section-title">
                                                    <h3 class="box-title">Employment Background</h3>

                                                </div>
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                            <tr style="border-bottom:3px solid #eee">
                                                                <th>#</th>
                                                                <th>Company</th>
                                                                <th class="text-center">Position</th>
                                                                <th class="text-center">Contact</th>
                                                                <th class="text-center">Period</th>
                                                                <th class="text-right">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>Optima Innovations Sdn Bhd</td>
                                                                <td class="text-center">Web Designer</td>
                                                                <td class="text-center">038876113</td>
                                                                <td class="text-center">13 Aug 2012 to
                                                                    <br>25 June 2014</td>
                                                                <td class="text-right">
                                                                    <a class="m-r-5 label label-info text-uppercase">
                                                                        <i class="fa fa-pencil"></i>
                                                                    </a>
                                                                    <a class="label label-danger text-uppercase">
                                                                        <i class="fa fa-trash"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr style="border-bottom:3px solid #eee">
                                                                <td colspan="6">
                                                                    <a href="#add-employment" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Employment</a>
                                                                </td>

                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="education-info m-b-30">
                                                <div class="section-title">
                                                    <h3 class="box-title">Education Background</h3>

                                                </div>
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                            <tr style="border-bottom:3px solid #eee">
                                                                <th>#</th>
                                                                <th>Field of Study</th>
                                                                <th class="text-center">University</th>
                                                                <th class="text-center">Period</th>
                                                                <th class="text-right">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>BSc (Hons)
                                                                    <br>Computer Science</td>
                                                                <td class="text-center">University of Nottingham</td>
                                                                <td class="text-center">13 Aug 2008 to
                                                                    <br>25 June 2011</td>
                                                                <td class="text-right">
                                                                    <a class="m-r-5 label label-info text-uppercase">
                                                                        <i class="fa fa-pencil"></i>
                                                                    </a>
                                                                    <a class="label label-danger text-uppercase">
                                                                        <i class="fa fa-trash"></i>
                                                                    </a>
                                                                </td>

                                                            </tr>

                                                            <tr style="border-bottom:3px solid #eee">
                                                                <td colspan="6">
                                                                    <a href="#add-educational" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Education</a>
                                                                </td>

                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <div class="family-info  m-b-30">
                                                <div class="section-title">
                                                    <h3 class="box-title">Professional Certificate</h3>

                                                </div>
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                            <tr style="border-bottom:3px solid #eee">
                                                                <th>#</th>
                                                                <th class="text-left">Certificate Field & Name</th>
                                                                <th class="text-center">Expiry</th>
                                                                <th class="text-center">Issued By</th>
                                                                <th class="text-center">Ref No.</th>
                                                                <th class="text-right">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td>1</td>
                                                                <td>Telco Project Management
                                                                    <br>Field : Management</td>
                                                                <td class="text-center">20 Oct 2020</td>
                                                                <td class="text-center">Microsoft</td>
                                                                <td class="text-center">ABC1234</td>
                                                                <td class="text-right">
                                                                    <a class="m-r-5 label label-warning">
                                                                        <i class="fa fa-download"></i>
                                                                    </a>
                                                                    <a class="m-r-5 label label-info text-uppercase">
                                                                        <i class="fa fa-pencil"></i>
                                                                    </a>
                                                                    <a class="label label-danger text-uppercase">
                                                                        <i class="fa fa-trash"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>2</td>
                                                                <td>Basic in Digital Illustration
                                                                    <br> Field : Design</td>
                                                                <td class="text-center">25 Jan 2020</td>
                                                                <td class="text-center">Adobe System</td>
                                                                <td class="text-center">ABC1234</td>
                                                                <td class="text-right">
                                                                    <a class="m-r-5 label label-warning">
                                                                        <i class="fa fa-download"></i>
                                                                    </a>
                                                                    <a class="m-r-5 label label-info text-uppercase">
                                                                        <i class="fa fa-pencil"></i>
                                                                    </a>
                                                                    <a class="label label-danger text-uppercase">
                                                                        <i class="fa fa-trash"></i>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <tr style="border-bottom:3px solid #eee">
                                                                <td colspan="6">
                                                                    <a href="#add-cert" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Certificate</a>
                                                                </td>

                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>

                                            <button style="width:200px" class="btn-lg btn-rounded btn-info text-uppercase">Save</button>
                                        </div>
                                    </div>

                                </section>
                                <section id="employmentinfo">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <h3 class="box-title">Account Details</h3>
                                            <div class="form-group row">
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" placeholder="Username">
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" placeholder="Email Address">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-md-6">
                                                    <input type="password" class="form-control" placeholder="Enter Password">
                                                </div>

                                            </div>
                                            <hr>
                                            <h3 class="box-title">Employee Info</h3>

                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <select class="selectpicker" data-style="form-control">
                                                        <option selected disabled>Select Company</option>
                                                        <option>Company 1</option>
                                                        <option>Company 2</option>
                                                        <option>Company 3</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <select class="selectpicker" data-style="form-control">
                                                        <option selected disabled>Select Department</option>
                                                        <option>Dept 1</option>
                                                        <option>Dept 2</option>
                                                        <option>Dept 3</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <select class="selectpicker" data-style="form-control">
                                                        <option selected disabled>Select Designation</option>
                                                        <option>Position 1</option>
                                                        <option>Position 2</option>
                                                        <option>Position 3</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" placeholder="Date Joined">
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" placeholder="Employee Code">
                                                </div>
                                                <div class="col-md-4">
                                                    <select class="selectpicker" data-style="form-control">
                                                        <option selected disabled>Employment Status</option>
                                                        <option>Permanent</option>
                                                        <option>Contract</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-md-4">
                                                    <select class="selectpicker" data-style="form-control">
                                                        <option selected disabled>In Probation?</option>
                                                        <option>Yes</option>
                                                        <option>No</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" placeholder="Date of Confirmation">
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="text" class="form-control" placeholder="Contract End Date">
                                                </div>
                                            </div>

                                            <hr>
                                            <h3 class="box-title">Position Details</h3>

                                            <div class="form-group row m-b-40">
                                                <div class="col-md-4">
                                                    <select class="selectpicker" data-style="form-control">
                                                        <option selected disabled>Hierarchy</option>
                                                        <option>Management</option>
                                                        <option>Human Resource</option>
                                                        <option>Staff</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <select class="selectpicker" data-style="form-control">
                                                        <option selected disabled>Reports To</option>
                                                        <option>Staff 1</option>
                                                        <option>Staff 2</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <select class="selectpicker" data-style="form-control">
                                                        <option selected disabled>Employment Role</option>
                                                        <option>Employer</option>
                                                        <option>Employee</option>
                                                        <option>Finance</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <h3 class="box-title">Working Days</h3>
                                            <div class="form-inline row m-b-40">
                                                <div class="checkbox checkbox-info">
                                                    <input id="monday" type="checkbox">
                                                    <label for="monday">Monday</label>
                                                </div>
                                                <div class="checkbox checkbox-info">
                                                    <input id="tuesday" type="checkbox">
                                                    <label for="tuesday">Tuesday</label>
                                                </div>
                                                <div class="checkbox checkbox-info">
                                                    <input id="wednesday" type="checkbox">
                                                    <label for="wednesday">Wednesday</label>
                                                </div>
                                                <div class="checkbox checkbox-info">
                                                    <input id="thursday" type="checkbox">
                                                    <label for="thursday">Thursday</label>
                                                </div>
                                                <div class="checkbox checkbox-info">
                                                    <input id="friday" type="checkbox">
                                                    <label for="friday">Friday</label>
                                                </div>
                                                <div class="checkbox checkbox-info">
                                                    <input id="saturday" type="checkbox">
                                                    <label for="saturday">Saturday</label>
                                                </div>
                                                <div class="checkbox checkbox-info">
                                                    <input id="sunday" type="checkbox">
                                                    <label for="sunday">Sunday</label>

                                                </div>


                                            </div>
                                            <button style="width:200px" class=" btn-lg btn-rounded btn-info text-uppercase">Save</button>
                                        </div>

                                    </div>

                                </section>
                                <section id="medicalinfo">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <h3 class="box-title">Medical Info</h3>
                                            <div class="table-responsive m-b-30">
                                                <table class="table">
                                                    <thead>
                                                        <tr style="border-bottom:3px solid #eee">
                                                            <th>#</th>
                                                            <th>Insurance Name</th>
                                                            <th class="text-center">Policy No.</th>
                                                            <th class="text-center">Start Date</th>
                                                            <th class="text-center">Expiry Date</th>
                                                            <th class="text-center">Value (RM)</th>
                                                            <th class="text-right">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>PruBSN Takaful Life</td>
                                                            <td class="text-center">1234567</td>
                                                            <td class="text-center">13 Aug 2018</td>
                                                            <td class="text-center">13 Aug 2020</td>
                                                            <td class="text-center">100,00.00</td>
                                                            <td class="text-right">
                                                                <a class="m-r-5 label label-info text-uppercase">
                                                                    <i class="fa fa-pencil"></i>
                                                                </a>
                                                                <a class="label label-danger text-uppercase">
                                                                    <i class="fa fa-trash"></i>
                                                                </a>
                                                            </td>
                                                        </tr>


                                                        <tr style="border-bottom:3px solid #eee">
                                                            <td colspan="8">
                                                                <a href="#add-medical" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Medical Info</a>
                                                            </td>

                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <button style="width:200px" class=" btn-lg btn-rounded btn-info text-uppercase">Save</button>
                                        </div>
                                    </div>
                                </section>

                                <section id="leaveallocation">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <h3 class="box-title">Leave Allocation</h3>
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr style="border-bottom:3px solid #eee">
                                                            <th>Leave Type</th>
                                                            <th width="20%" class="text-right">Allocation</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Annual Leave</td>
                                                            <td>
                                                                <input type="number" class="form-control">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Medical Leave</td>
                                                            <td>
                                                                <input type="number" class="form-control">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Compassionate Leave</td>
                                                            <td>
                                                                <input type="number" class="form-control">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Maternity Leave</td>
                                                            <td>
                                                                <input type="number" class="form-control">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Paternity Leave</td>
                                                            <td>
                                                                <input type="number" class="form-control">
                                                            </td>
                                                        </tr>


                                                    </tbody>
                                                </table>
                                            </div>
                                            <button style="width:200px" class="m-b-20 btn-lg btn-rounded btn-info text-uppercase">Save</button>
                                        </div>
                                    </div>
                                </section>

                                <section id="approver">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <h3 class="box-title">Approval Layer</h3>
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr style="border-bottom:3px solid #eee">
                                                            <th>Leave Application</th>
                                                            <th width="40%" class="text-right">Approver</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Approver Level 1</td>
                                                            <td>
                                                                <select class="selectpicker" data-style="form-control">
                                                                    <option selected disabled>Employee Name</option>
                                                                    <option>Employee 1</option>
                                                                    <option>Employee 2</option>
                                                                    <option>Employee 3</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Approver Level 2</td>
                                                            <td>
                                                                <select class="selectpicker" data-style="form-control">
                                                                    <option selected disabled>Employee Name</option>
                                                                    <option>Employee 1</option>
                                                                    <option>Employee 2</option>
                                                                    <option>Employee 3</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr style="border-bottom:3px solid #eee">
                                                            <th>Claim Application</th>
                                                            <th width="40%" class="text-right">Approver</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Approver Level 1</td>
                                                            <td>
                                                                <select class="selectpicker" data-style="form-control">
                                                                    <option selected disabled>Employee Name</option>
                                                                    <option>Employee 1</option>
                                                                    <option>Employee 2</option>
                                                                    <option>Employee 3</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Approver Level 2</td>
                                                            <td>
                                                                <select class="selectpicker" data-style="form-control">
                                                                    <option selected disabled>Employee Name</option>
                                                                    <option>Employee 1</option>
                                                                    <option>Employee 2</option>
                                                                    <option>Employee 3</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr style="border-bottom:3px solid #eee">
                                                            <th>Leave In Lieu Application</th>
                                                            <th width="40%" class="text-right">Approver</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Approver Level 1</td>
                                                            <td>
                                                                <select class="selectpicker" data-style="form-control">
                                                                    <option selected disabled>Employee Name</option>
                                                                    <option>Employee 1</option>
                                                                    <option>Employee 2</option>
                                                                    <option>Employee 3</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Approver Level 2</td>
                                                            <td>
                                                                <select class="selectpicker" data-style="form-control">
                                                                    <option selected disabled>Employee Name</option>
                                                                    <option>Employee 1</option>
                                                                    <option>Employee 2</option>
                                                                    <option>Employee 3</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr style="border-bottom:3px solid #eee">
                                                            <th>Advanced Request</th>
                                                            <th width="40%" class="text-right">Approver</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Approver Level 1</td>
                                                            <td>
                                                                <select class="selectpicker" data-style="form-control">
                                                                    <option selected disabled>Employee Name</option>
                                                                    <option>Employee 1</option>
                                                                    <option>Employee 2</option>
                                                                    <option>Employee 3</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Approver Level 2</td>
                                                            <td>
                                                                <select class="selectpicker" data-style="form-control">
                                                                    <option selected disabled>Employee Name</option>
                                                                    <option>Employee 1</option>
                                                                    <option>Employee 2</option>
                                                                    <option>Employee 3</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <button style="width:200px" class="m-b-20 btn-lg btn-rounded btn-info text-uppercase">Save</button>
                                        </div>

                                    </div>
                                </section>


                                <section id="supportingdoc">
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <h3 class="box-title">Supporting Documents</h3>
                                            <div class="table-responsive m-b-30">
                                                <table class="table">
                                                    <thead>
                                                        <tr style="border-bottom:3px solid #eee">
                                                            <th>#</th>
                                                            <th>Document Name</th>
                                                            <th class="text-right">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>1</td>
                                                            <td>Employee Handbook</td>

                                                            <td class="text-right">
                                                                <a class="m-r-5 label label-info text-uppercase">
                                                                    <i class="fa fa-download"></i>
                                                                </a>
                                                                <a class="label label-danger text-uppercase">
                                                                    <i class="fa fa-trash"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>2</td>
                                                            <td>Document Name 1</td>

                                                            <td class="text-right">
                                                                <a class="m-r-5 label label-info text-uppercase">
                                                                    <i class="fa fa-download"></i>
                                                                </a>
                                                                <a class="label label-danger text-uppercase">
                                                                    <i class="fa fa-trash"></i>
                                                                </a>
                                                            </td>
                                                        </tr>


                                                        <tr style="border-bottom:3px solid #eee">
                                                            <td colspan="8">
                                                                <a href="#add-document" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Document</a>
                                                            </td>

                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <button style="width:200px" class=" btn-lg btn-rounded btn-info text-uppercase">Save</button>
                                        </div>
                                    </div>
                                </section>

                            </div>
                            <!-- /content -->
                    </div>
                    </form>
                </div>
            </div>
        </div>


        <div class="modal fade" id="add-family" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                        <h1>Add Family</h1>
                        <form>
                            <div class="form-group  ">

                                <input type="text" class="form-control" placeholder="Full Name">

                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Relationship">
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="MyKAD No./Passport No.">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Phone No.">
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker-autoclose" placeholder="Date of Birth">
                                        <span class="input-group-addon">
                                            <i class="icon-calender"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Add Family</button>
                        <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="add-cert" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                        <h1>Add Professional Certificate</h1>
                        <form>
                            <div class="form-group row ">
                                <div class="col-md-6">
                                    <select class="selectpicker" data-style="form-control">
                                        <option selected disabled>Field</option>
                                        <option>Engineering</option>
                                        <option>Marketing</option>
                                        <option>Social</option>
                                        <option>Accounting</option>
                                        <option>Finance</option>
                                        <option>Management</option>
                                        <option>Others</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="If others, please specify field...">
                                </div>

                            </div>
                            <div class="form-group ">

                                <input type="text" class="form-control" placeholder="Certificate Name">

                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Issued By">
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker-autoclose" placeholder="Expiry Date">
                                        <span class="input-group-addon">
                                            <i class="icon-calender"></i>
                                        </span>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Reference No.">
                                </div>
                                <div class="col-md-6">


                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                        <div class="form-control" data-trigger="fileinput">
                                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                            <span class="fileinput-filename"></span>
                                        </div>
                                        <span class="input-group-addon btn btn-default btn-file">
                                            <span class="fileinput-new">Select file</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="..."> </span>
                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>
                            </div>

                        </form>
                        <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Add Certificate</button>
                        <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="add-employment" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                        <h1>Add Employment</h1>
                        <form>
                            <div class="form-group  ">

                                <input type="text" class="form-control" placeholder="Company Name">

                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Position">
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Contact No.">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker-autoclose" placeholder="Date from">
                                        <span class="input-group-addon">
                                            <i class="icon-calender"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker-autoclose" placeholder="Date to">
                                        <span class="input-group-addon">
                                            <i class="icon-calender"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Add Employment</button>
                        <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="add-educational" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                        <h1>Add Education</h1>
                        <form>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Qualification">
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Field of Study">
                                </div>
                            </div>
                            <div class="form-group">

                                <input type="text" class="form-control" placeholder="University/Institution">

                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker-autoclose" placeholder="Date from">
                                        <span class="input-group-addon">
                                            <i class="icon-calender"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker-autoclose" placeholder="Date to">
                                        <span class="input-group-addon">
                                            <i class="icon-calender"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Add Education</button>
                        <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="add-document" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                        <h1>Add Document</h1>
                        <form>

                            <div class="form-group">

                                <input type="text" class="form-control" placeholder="Document Name">

                            </div>

                            <div class="form-group">
                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput">
                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                        <span class="fileinput-filename"></span>
                                    </div>
                                    <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">Select file</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" name="..."> </span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>

                            <div class="form-group">

                                <input type="text" class="form-control" placeholder="Paste URL Link">

                            </div>
                        </form>
                        <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Add Document</button>
                        <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="add-medical" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                        <h1>Add Medical Info</h1>
                        <form>


                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Insurance Name">
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Policy No.">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker-autoclose" placeholder="Start Date">
                                        <span class="input-group-addon">
                                            <i class="icon-calender"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker-autoclose" placeholder="Expiry Date">
                                        <span class="input-group-addon">
                                            <i class="icon-calender"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Value (RM)">
                                </div>
                            </div>
                        </form>
                        <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Add Medical</button>
                        <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

                    </div>
                </div>
            </div>
        </div>

        <!-- /.container-fluid -->
        <footer class="footer text-center"> 2018 &copy; IX HRMS. All Rights Reserved. </footer>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
