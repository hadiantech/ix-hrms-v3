<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use App\Admin;
use App\Employee;
use App\Role;
use App\Department;
use App\Designation;
use App\Organisation;
use App\Hierarchy;
use Illuminate\Http\Request;
use Auth;
use View;

class EmployeeProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $employee = Auth::user();

        return view('employees.profiles.index',compact('employee'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $organisations = Organisation::find($id);
        // $departments = Department::find($id);
        // $designations = Designation::find($id);
        $employees = Employee::find($id);
        return view('employees.profiles.show',compact('employees','adminroles','organisations','departments','designations','hierarchies'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $organisations = Organisation::find($id);
        $departments = Department::find($id);
        $designations = Designation::find($id);
        $adminroles = AdminRole::find($id);
        $employees = Employee::find($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
