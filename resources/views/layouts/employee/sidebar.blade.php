<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> <span class="hide-menu">Navigation</span></h3>
        </div>
        <ul class="nav" id="side-menu">
                <li class="user-pro">
                <a href="{{ url('/profiles') }}"><img src="../plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span class="hide-menu"> Steve Gection</span>
                </a>

            </li>

            <li><a href="{{ url('/leaves') }}" class="waves-effect"><i class="mdi mdi-calendar fa-fw"></i> <span class="hide-menu">Leaves</span></a></li>
            <li><a href="{{ url('/claims') }}" class="waves-effect"><i class="mdi mdi-coin fa-fw"></i> <span class="hide-menu">Claim</span></a></li>
            <li><a href="{{ url('/notices') }}" class="waves-effect"><i class="mdi mdi-message-bulleted fa-fw"></i> <span class="hide-menu">Notice</span></a></li>
            <li><a href="{{ url('/payslips') }}" class="waves-effect"><i class="mdi mdi-currency-usd fa-fw"></i> <span class="hide-menu">Payslip</span></a></li>
            <!--<li><a href="login.html" class="waves-effect"><i class="mdi mdi-file-outline fa-fw"></i> <span class="hide-menu">Claims</span></a></li>
            <li><a href="login.html" class="waves-effect"><i class="mdi mdi-coin fa-fw"></i> <span class="hide-menu">Payslips</span></a></li>
            <li><a href="login.html" class="waves-effect"><i class="mdi mdi-comment-alert-outline fa-fw"></i> <span class="hide-menu">Notice</span></a></li>
            <li><a href="login.html" class="waves-effect"><i class="mdi mdi-comment-processing-outline fa-fw"></i> <span class="hide-menu">Feedback</span></a></li>
            <li><a href="admin-setting.html" class="waves-effect"><i class="mdi mdi-settings fa-fw"></i> <span class="hide-menu">Settings</span></a></li>-->

        </ul>
    </div>
</div>
