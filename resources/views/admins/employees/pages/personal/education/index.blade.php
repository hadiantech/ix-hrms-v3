<div class="education-info m-b-30">
    <div class="section-title">
        <h3 class="box-title">Education Background</h3>
    </div>

    <a href="#add-educational" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Education</a>
    <br />
    <br />

    <div class="table-responsive">
        <table id="education_table" class="table">
            <thead>
                <tr style="border-bottom:3px solid #eee">
                    <th>#</th>
                    <th>Qualification</th>
                    <th>Field of Study</th>
                    <th class="text-center">University</th>
                    <th class="text-center">Period</th>
                    <th class="text-right">Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>


@include('admins.employees.pages.personal.education.create')
@include('admins.employees.pages.personal.education.edit')


@section('js')
    @parent

    <script>
        $(function () {
            $('#education_table').DataTable({
                serverSide: true,
                processing: true,
                ajax: '{{ route("employees.educations.index", $employee->id) }}',
                columns: [
                    {data: 'DT_Row_Index', orderable: false, searchable: false},
                    {data: 'qualification'},
                    {data: 'field'},
                    {data: 'university'},
                    {data: 'period', orderable: false, searchable: false},
                    {data: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@endsection
