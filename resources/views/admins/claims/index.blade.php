@extends('layouts.default.master')

@section('content')

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Claim Dashboard</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li class="active">Claim Dasboard</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="white-box" style="background: #ff7676">
                    <ul class="col-in">
                        <li>
                            <span class="circle circle-md bg-white"><i style="color:#ff7676" class="ti-timer"></i></span>
                        </li>
                        <li class="col-last">
                            <h3 class="counter text-right m-t-15 white">{{ $claim_month_count }}</h3>
                        </li>
                        <li class="col-middle" style="width:50%">
                            <h4 class="white">New Monthly Claims</h4>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-4 col-sm-12">
                <div class="white-box" style="background: #c59d31">
                    <ul class="col-in">
                        <li>
                            <span class="circle circle-md bg-white"><i style="color:#c59d31" class="ti-announcement"></i></span>
                        </li>
                        <li class="col-last">
                            <h3 class="counter text-right m-t-15 white">{{ $claim_advance_count }}</h3>
                        </li>
                        <li class="col-middle" style="width:50%">
                            <h4 class="white">New Advanced Claims</h4>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="white-box" style="background: #6d4d8d">
                    <ul class="col-in">
                        <li>
                            <span class="circle circle-md bg-white"><i style="color:#6d4d8d" class="ti-announcement"></i></span>
                        </li>
                        <li class="col-last">
                            <h3 class="counter text-right m-t-15 white">{{ $claim_request_count }}</h3>
                        </li>
                        <li class="col-middle" style="width:50%">
                            <h4 class="white">New Advance Requests</h4>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        {{-- Normal Claim --}}
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="box-title">Employee Claim Summary</h3>
                    <!-- Nav tabs -->
                    <ul class="nav customtab nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#unprocessed" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs"><i class="ti-home"></i></span>
                                <span class="hidden-xs"> Unprocessed </span>
                            </a>
                        </li>
                        <li role="presentation" class=""><a href="#processed" aria-controls="profile" role="tab"
                                data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span>
                                <span class="hidden-xs">Processed</span></a></li>

                        <li role="presentation" class=""><a href="#finished" aria-controls="profile" role="tab"
                                data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span>
                                <span class="hidden-xs">Finished</span></a>
                        </li>

                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="unprocessed">
                            <div class="table-responsive">
                                <table id="claim_unproc" class="table display">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Employee</th>
                                            <th class="text-center">Claim Desc</th>
                                            <th class="text-center">Applied Date</th>
                                            <th class="text-center">Amount</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-right">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="processed">
                            <div class="table-responsive">
                                <table id="claim_proc" class="table display">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Employee</th>
                                            <th class="text-center">Claim Desc</th>
                                            <th class="text-center">Applied Date</th>
                                            <th class="text-center">Amount</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-right">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="finished">
                            <div class="table-responsive">
                                <table id="claim_approved" class="table display">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Employee</th>
                                            <th class="text-center">Claim Desc</th>
                                            <th class="text-center">Applied Date</th>
                                            <th class="text-center">Amount</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-right">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Claim  Request--}}
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="box-title">Advanced Request Summary</h3>
                    <!-- Nav tabs -->
                    <ul class="nav customtab nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#unprocessed-ad" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-home"></i></span>
                                <span lass="hidden-xs"> Unprocessed</span>
                            </a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#processed-ad" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span>
                                <span class="hidden-xs">Processed</span>
                            </a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#finished-ad" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span>
                                <span class="hidden-xs">Finished</span>
                            </a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="unprocessed-ad">
                            <div class="table-responsive">
                                <table id="claim_request_unproc" class="table display">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Employee</th>
                                            <th class="text-center">Claim Desc</th>
                                            <th class="text-center">Applied Date</th>
                                            <th class="text-center">Amount</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-right">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="processed-ad">
                            <div class="table-responsive">
                                <table id="claim_request_proc" class="table display">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Employee</th>
                                            <th class="text-center">Claim Desc</th>
                                            <th class="text-center">Applied Date</th>
                                            <th class="text-center">Amount</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-right">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="finished-ad">
                            <div class="table-responsive">
                                <table id="claim_request_approved" class="table display">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Employee</th>
                                            <th class="text-center">Claim Desc</th>
                                            <th class="text-center">Applied Date</th>
                                            <th class="text-center">Amount</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-right">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Advance Claim --}}
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <h3 class="box-title">Advance Claim Summary</h3>

                    <!-- Nav tabs -->
                    <ul class="nav customtab nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#unprocessed-adv" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-home"></i></span>
                                <span class="hidden-xs"> Unprocessed</span>
                            </a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#processed-adv" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span>
                                <span class="hidden-xs">Processed</span>
                            </a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#finished-adv" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span>
                                <span class="hidden-xs">Finished</span>
                            </a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="unprocessed-adv">
                            <div class="table-responsive">
                                <table id="claim_advance_unproc" class="table display">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Employee</th>
                                            <th class="text-center">Claim Desc</th>
                                            <th class="text-center">Applied Date</th>
                                            <th class="text-center">Amount</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-right">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="processed-adv">
                            <div class="table-responsive">
                                <table id="claim_advance_proc" class="table display">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Employee</th>
                                            <th class="text-center">Claim Desc</th>
                                            <th class="text-center">Applied Date</th>
                                            <th class="text-center">Amount</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-right">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="finished-adv">
                            <div class="table-responsive">
                                <table id="claim_advance_approved" class="table display">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Employee</th>
                                            <th class="text-center">Claim Desc</th>
                                            <th class="text-center">Applied Date</th>
                                            <th class="text-center">Amount</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-right">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /.row -->


<!-- /.container-fluid -->
<footer class="footer text-center"> 2018 &copy; IX HRMS. All Rights Reserved. </footer>
</div>

@endsection

@section('js')
@parent

<script>
    $(function () {
        $('#claim_unproc').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/adminclaims/list/unproc',
            columns: [{
                    data: 'DT_Row_Index',
                    orderable: false,
                    searchable: false,
                    class: 'text-left'
                },
                {
                    data: 'employee'
                },
                {
                    data: 'desc',
                    class: 'text-center'
                },
                {
                    data: 'claim_applied_date',
                    class: 'text-center'
                },
                {
                    data: 'claim_total',
                    class: 'text-center'
                },
                {
                    data: 'status',
                    name: 'claim_status',
                    class: 'text-center'
                },
                {
                    data: 'action',
                    orderable: false,
                    searchable: false,
                    class: 'text-right'
                }
            ]
        });
    });
</script>
<script>
    $(function () {
        $('#claim_proc').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/adminclaims/list/proc',
            columns: [{
                    data: 'DT_Row_Index',
                    orderable: false,
                    searchable: false,
                    class: 'text-left'
                },
                {
                    data: 'employee'
                },
                {
                    data: 'desc',
                    class: 'text-center'
                },
                {
                    data: 'claim_applied_date',
                    class: 'text-center'
                },
                {
                    data: 'claim_total',
                    class: 'text-center'
                },
                {
                    data: 'status',
                    name: 'claim_status',
                    class: 'text-center'
                },
                {
                    data: 'action',
                    orderable: false,
                    searchable: false,
                    class: 'text-right'
                }
            ]
        });
    });
</script>
<script>
    $(function () {
        $('#claim_approved').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/adminclaims/list/approved',
            columns: [{
                    data: 'DT_Row_Index',
                    orderable: false,
                    searchable: false,
                    class: 'text-left'
                },
                {
                    data: 'employee'
                },
                {
                    data: 'desc',
                    class: 'text-center'
                },
                {
                    data: 'claim_applied_date',
                    class: 'text-center'
                },
                {
                    data: 'claim_total',
                    class: 'text-center'
                },
                {
                    data: 'status',
                    name: 'claim_status',
                    class: 'text-center'
                },
                {
                    data: 'action',
                    orderable: false,
                    searchable: false,
                    class: 'text-right'
                }
            ]
        });
    });
</script>

<script>
    $(function () {
        $('#claim_request_unproc').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/adminclaims/list_request/unproc',
            columns: [{
                    data: 'DT_Row_Index',
                    orderable: false,
                    searchable: false,
                    class: 'text-left'
                },
                {
                    data: 'employee'
                },
                {
                    data: 'desc',
                    class: 'text-center'
                },
                {
                    data: 'claim_applied_date',
                    class: 'text-center'
                },
                {
                    data: 'claim_total_advance',
                    class: 'text-center'
                },
                {
                    data: 'status',
                    name: 'claim_status',
                    class: 'text-center'
                },
                {
                    data: 'action',
                    orderable: false,
                    searchable: false,
                    class: 'text-right'
                }
            ]
        });
    });
</script>
<script>
    $(function () {
        $('#claim_request_proc').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/adminclaims/list_request/proc',
            columns: [{
                    data: 'DT_Row_Index',
                    orderable: false,
                    searchable: false,
                    class: 'text-left'
                },
                {
                    data: 'employee'
                },
                {
                    data: 'desc',
                    class: 'text-center'
                },
                {
                    data: 'claim_applied_date',
                    class: 'text-center'
                },
                {
                    data: 'claim_total_advance',
                    class: 'text-center'
                },
                {
                    data: 'status',
                    name: 'claim_status',
                    class: 'text-center'
                },
                {
                    data: 'action',
                    orderable: false,
                    searchable: false,
                    class: 'text-right'
                }
            ]
        });
    });
</script>
<script>
    $(function () {
        $('#claim_request_approved').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/adminclaims/list_request/approved_advance',
            columns: [{
                    data: 'DT_Row_Index',
                    orderable: false,
                    searchable: false,
                    class: 'text-left'
                },
                {
                    data: 'employee'
                },
                {
                    data: 'desc',
                    class: 'text-center'
                },
                {
                    data: 'claim_applied_date',
                    class: 'text-center'
                },
                {
                    data: 'claim_total_advance',
                    class: 'text-center'
                },
                {
                    data: 'status',
                    name: 'claim_status',
                    class: 'text-center'
                },
                {
                    data: 'action',
                    orderable: false,
                    searchable: false,
                    class: 'text-right'
                }
            ]
        });
    });
</script>

<script>
    $(function () {
        $('#claim_advance_unproc').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/adminclaims/list_advance/unproc_advance',
            columns: [{
                    data: 'DT_Row_Index',
                    orderable: false,
                    searchable: false,
                    class: 'text-left'
                },
                {
                    data: 'employee'
                },
                {
                    data: 'desc',
                    class: 'text-center'
                },
                {
                    data: 'claim_applied_date',
                    class: 'text-center'
                },
                {
                    data: 'claim_total',
                    class: 'text-center'
                },
                {
                    data: 'status',
                    name: 'claim_status',
                    class: 'text-center'
                },
                {
                    data: 'action',
                    orderable: false,
                    searchable: false,
                    class: 'text-right'
                }
            ]
        });
    });
</script>
<script>
    $(function () {
        $('#claim_advance_proc').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/adminclaims/list_advance/proc_advance',
            columns: [{
                    data: 'DT_Row_Index',
                    orderable: false,
                    searchable: false,
                    class: 'text-left'
                },
                {
                    data: 'employee'
                },
                {
                    data: 'desc',
                    class: 'text-center'
                },
                {
                    data: 'claim_applied_date',
                    class: 'text-center'
                },
                {
                    data: 'claim_total',
                    class: 'text-center'
                },
                {
                    data: 'status',
                    name: 'claim_status',
                    class: 'text-center'
                },
                {
                    data: 'action',
                    orderable: false,
                    searchable: false,
                    class: 'text-right'
                }
            ]
        });
    });
</script>
<script>
    $(function () {
        $('#claim_advance_approved').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/adminclaims/list_advance/approved_advance',
            columns: [{
                    data: 'DT_Row_Index',
                    orderable: false,
                    searchable: false,
                    class: 'text-left'
                },
                {
                    data: 'employee'
                },
                {
                    data: 'desc',
                    class: 'text-center'
                },
                {
                    data: 'claim_applied_date',
                    class: 'text-center'
                },
                {
                    data: 'claim_total',
                    class: 'text-center'
                },
                {
                    data: 'status',
                    name: 'claim_status',
                    class: 'text-center'
                },
                {
                    data: 'action',
                    orderable: false,
                    searchable: false,
                    class: 'text-right'
                }
            ]
        });
    });
</script>

@endsection
