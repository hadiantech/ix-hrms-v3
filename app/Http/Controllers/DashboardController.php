<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DataTables;
use App\Notice;
use App\Approval;
use App\EmployeeLeave;
use App\Claim;
use Carbon\Carbon;
use App\Employee;

class DashboardController extends Controller
{

    function view(){
        if(Auth::user()->hasRole('Admin')){
           // leave
        $leaves = EmployeeLeave::count();
        // $employees = Employee::where('probation_status','Yes')->count();
        $employees = Employee::where('probation_status','Yes')->whereNotNull('contract_ended_at')->count();

        // claim
        $claims = Claim::count();

        return view('admins.home',compact('leaves','claims','employees'));
        }

        $notices = Notice::all();
        $leaves = EmployeeLeave::where('employee_id', Auth::user()->id)->count();


        // claim
        $claims = Claim::where('emp_id', Auth::user()->id)->count();

        return view('employees.home',compact('leaves','claims','notices'));
    }
    public function summary()
    {
        $employees = Employee::where('probation_status','Yes')->count();
        // leave
        $leaves = EmployeeLeave::all();
        // claim
        $claims = Claim::where(['claim_proc_status' => 'unproc','claim_status' => 'submit', 'claim_type' => 1])->count();

        return view('admins.home',compact('leaves','claims','employees'));

    }
    public function leavelist($type)
    {
       $emp_ids = Approval::where('approver_id', Auth::id())->pluck('employee_id');

        if($type == 'unproc'){
            $query = EmployeeLeave::with(['employee'])->where(['leave_proc_status' => 'unproc']);
        }
        else if($type == 'proc'){
            $query = EmployeeLeave::with(['employee'])->where(['leave_proc_status' => 'proc']);
        }
        else if($type == 'approve'){
            $query = EmployeeLeave::with(['employee'])->where(['leave_proc_status' => 'approve']);
        }
        else{
            return 'No leave applied';
        }

        return Datatables::eloquent(EmployeeLeave::query())
        ->addColumn('employee', function($model){
            return $model->employee->fname." ".$model->employee->lname;
        })
        ->addColumn('status', function($model){
            $status = $model->leave_proc_status;

            if($status == 'unproc'){
                $status = 'Pending';
                $class = 'default';
            }
            elseif($status == 'proc'){
                $status = 'Reviewed';
                $class = 'warning';
            }
            elseif($status == 'approve'){
                $status = 'Approved';
                $class = 'success';
            }
            elseif($status == 'returned'){
                $status = 'Returned';
                $class = 'danger';
            }
            else{
                $class = 'primary';
            }
            return view('shared._button_color', compact('status', 'class'))->render();
        })
        ->addColumn('action', function($model){
            $url = 'adminleaves';
            $custom_permission = 'admin_leaves';
            $delete_hide = true;
            $edit_hide = true;
            // $modal_view_id = 'leaves_modal_view';
            $modal_edit_id = 'leaves_modal_edit';
            if($model->getMedia('leaves')->first()){
                $modal_data_customs = array('leaves' => $model->getMedia('leaves')->first()->getUrl());
            }
            // $edit_hide = true;
            return view('shared._table_action', compact('model','edit_hide','modal_data_customs','url','modal_view_id','modal_edit_id','custom_permission','delete_hide'))->render();
        })
        ->escapeColumns([])
        ->addIndexColumn()
        ->make(true);
    }

    public function claimlist($type){

        //get approval emp ids
        $emp_ids = Approval::where('approver_id', Auth::id())->type('claim')->pluck('employee_id');

        if($type == 'unproc'){
            $query = Claim::with(['employee'])->where(['claim_proc_status' => $type, 'claim_status' => 'submit', 'claim_type' => '1']);
        }
        else if($type == 'proc'){
            $query = Claim::with(['employee'])->where(['claim_proc_status' => $type, 'claim_status' => 'submit', 'claim_type' => '1']);
        }
        else if($type == 'approved'){
            $query = Claim::with(['employee'])->where(['claim_proc_status' => $type, 'claim_status' => 'submit', 'claim_type' => '1']);
        }
        else{
            return 'No claim type defined';
        }

        return Datatables::eloquent(Claim::query())
            ->addColumn('employee', function($model){
                return $model->employee->fname." ".$model->employee->lname;
            })
            ->addColumn('desc', function($model){
                return 'Normal Claim - '.Carbon::create($model->claim_month)->format('F');
            })
            ->addColumn('status', function($model){
                $status = $model->claim_proc_status;

                if($status == 'unproc'){
                    $status = 'Pending';
                    $class = 'default';
                }
                elseif($status == 'proc'){
                    $status = 'Reviewed';
                    $class = 'warning';
                }
                elseif($status == 'approved'){
                    $status = 'Approved';
                    $class = 'success';
                }
                elseif($status == 'returned'){
                    $status = 'Returned';
                    $class = 'danger';
                }
                else{
                    $class = 'primary';
                }
                return view('shared._button_color', compact('status', 'class'))->render();
            })
            ->addColumn('action', function($model){
                $url = 'adminclaims';
                $delete_hide = true;
                $edit_hide = true;
                return view('shared._table_action', compact('model', 'url','delete_hide','edit_hide'))->render();
            })
            ->escapeColumns([])
            ->addIndexColumn()
            ->make(true);

        }

        public function probationlist()
        {




            $query = Employee::where('probation_status','Yes');

            return Datatables::eloquent($query)
            ->addColumn('months', function($model){
                // return $model->date;
                $date = 'N/a';
                // $date = Employee::where('probation_status','Yes')->whereNotNull('contract_ended_at')->get(['contract_ended_at']);
                $mytime = Carbon::now();
                // foreach($date as $dat){
                    //$dat->contract_ended_at = 'haha';
                    $diff = abs(strtotime($model->contract_ended_at) - strtotime($mytime));
                    //$years = floor($diff / (365*60*60*24));
                    //$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                    //$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
                    $days = $diff/(60*60*24);
                    $format = number_format((float)$days, 0);
                    // $model->remaining = $format;
                return $format;
            })
            ->addColumn('action', function($model){
                $url = 'employees';

                $delete_datatable_id_name_refresh = 'probation_table';
                return view('shared._table_action', compact('model', 'url', 'modal_view_id', 'modal_edit_id', 'delete_datatable_id_name_refresh'))->render();
            })

        ->addIndexColumn()
        ->make(true);
        }


        public function noticelist(){

            return Datatables::eloquent(Notice::with('employee'))
        ->addColumn('createdby', function($model){
            return $model->employee->fname." ".$model->employee->lname;
        })
            ->addColumn('action', function($model){
                $url = 'notices';
                $modal_data_customs = array('employee' => $model->employee);
                $delete_datatable_id_name_refresh = 'notice_table';
                return view('shared._table_action', compact('model','employee', 'url','modal_data_customs', 'modal_view_id', 'modal_edit_id', 'delete_datatable_id_name_refresh'))->render();
            })
            ->addIndexColumn()
            ->make(true);
        }

}
