<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddress2ColumnEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {

            $table->string('per_phone_number2')->nullable()->after('pre_phone_number2');
            $table->string('per_phone_number1')->nullable()->after('pre_phone_number2');
            $table->string('per_country')->nullable()->after('pre_phone_number2');
            $table->string('per_postcode')->nullable()->after('pre_phone_number2');
            $table->string('per_state')->nullable()->after('pre_phone_number2');
            $table->string('per_city')->nullable()->after('pre_phone_number2');
            $table->string('per_address_line2')->nullable()->after('pre_phone_number2');
            $table->string('per_address_line1')->nullable()->after('pre_phone_number2');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            //
        });
    }
}
