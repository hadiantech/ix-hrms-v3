@extends('layouts.default.master')

@section('content')

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Process Employee Claim</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="dashboard-adminview.html">Dashboard</a></li>
                    <li><a href="{{ route('adminclaims.index') }}">Claim Dashboard</a></li>
                    <li class="active">Employee Claim</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="m-b-20 box-title text-uppercase">Employee Monthly Claim
                        @if($claim->claim_proc_status == 'approved')
                        <span class="m-l-5 label label-success">APPROVED</span>
                        @elseif($claim->claim_proc_status == 'proc')
                        <span class="m-l-5 label label-info">REVIEWED</span>
                        @elseif($claim->claim_proc_status == 'unproc')
                        <span class="m-l-5 label label-info">PENDING</span>
                        @elseif($claim->claim_proc_status == 'return')
                        <span class="m-l-5 label label-info">RETURNED</span>
                        @endif()
                    </h3>
                    <div class="employee-claim-details">
                        <div class="row">
                            <div class="col-md-4">
                                <blockquote>
                                    <h4>Employee Details</h4>
                                    <label>Name</label> <span class="m-l-5">{{ $claim->employee->fname }} {{ $claim->employee->lname }}</span><br>
                                    <label>ID</label> <span class="m-l-5">{{ $claim->employee->employee_uid }}</span><br>
                                    <label>Department</label> <span class="m-l-5">{{ $claim->employee->department->department_name }}, {{ $claim->employee->organisation->company_name }}</span><br>
                                    <label>Applied Date</label> <span class="m-l-5">{{ Carbon\Carbon::parse($claim->claim_applied_date)->format('d F Y') }}</span><br>
                                    <label>Claim</label> <span class="m-l-5">{{ Carbon\Carbon::create($claim->claim_month)->format('F') }}</span>

                                </blockquote>
                            </div>

                            {!! Form::open(['url' => '', 'id' => 'claim_update']) !!}
                            <input type="hidden" name="_method" value="PUT">
                            <input id="claim_id" name="claim_id" type="hidden" value="" />

                            {{-- {!! Form::model($claim, ['route' => ['claim.update.admin', $claim->id], 'id' => 'claim_update', 'method' => 'PUT']) !!} --}}

                            <div class="col-md-4">
                                <div class="form-group">
                                    <select name="claim_proc_status" class="selectpicker" data-style="form-control">
                                        <option value="unproc" @if($claim->claim_proc_status == 'unproc') selected @endif>Pending</option>
                                        <option value="return" @if($claim->claim_proc_status == 'return') selected @endif>Return</option>
                                        <option value="proc" @if($claim->claim_proc_status == 'proc') selected @endif>Reviewed</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <textarea name="claim_remark" rows="5" class="form-control" placeholder="Add overall remarks...">{{ $claim->claim_remark }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table id="expenses_table" class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Type</th>
                                    <th>Item Name & Desc</th>
                                    <th class="text-center">Project Code</th>
                                    <th class="text-center">Amount ({{ settings('currency_code') }})</th>
                                    <th class="text-center">Item Date</th>
                                    <th class="text-center">Receipt No.</th>
                                    <th class="text-center">Ref No.</th>
                                    <th class="text-right">Remarks</th>
                                    <th class="text-right">Action</th>
                                </tr>
                            </thead>
                            <tfoot class="claim-amount">
                                <tr>
                                    <th colspan="7"></th>
                                    <th class="text-right">TOTAL AMOUNT</th>
                                    <th colspan="2" class="text-right"><span class="amount">{{ settings('currency_code') }} {{ $claim->claim_total }}</span></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <button onclick="getsubmit('return')" type="submit" style="width:100px" class="m-r-10 btn btn-rounded btn-info text-uppercase">Return</button>
                    <button onclick="getsubmit('submit')" type="submit" style="width:100px" class=" m-r-10 btn btn-rounded btn-success text-uppercase">Submit</button>

                    @if($approver_level->level == '2')
                    <button onclick="getsubmit('approved')" type="submit" style="width:100px" class=" m-r-10 btn btn-rounded btn-success text-uppercase">Approve</button>
                    @endif
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

{!! Form::open(['url' => '', 'id' => 'expenses_edit']) !!}

<div id="expenses_modal_edit" class="modal fade" id="add-remarks">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                {{-- <input type="hidden" name="_method" value="POST"> --}}
                <input type="hidden" id="expenses_id" name="expenses_id"  value="" />
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h1>Add Remarks</h1>
                <p>Enter remarks:</p>
                <div class="form-group">
                    <textarea id="remark" name="remark" class="form-control" rows="4"></textarea>
                </div>
                <button type="submit" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">
                    Save Remarks</button>
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
<footer class="footer text-center"> 2018 &copy; IX HRMS. All Rights Reserved. </footer>


@endsection

@section('js')
@parent
<script>
    $(function () {
        $('#expenses_table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/expenses/list_admin/{{ $claim->id }}',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false, class: 'text-left'},
                {data: 'expense_type.expense', name: 'item_type'},
                {data: 'item_name_desc', name: 'item_name'},
                {data: 'project_code'},
                {data: 'amount'},
                {data: 'purchase_date'},
                {data: 'receipt_no'},
                {data: 'reference_no'},
                {data: 'remark'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
<script>
    $(document).off('click', '.open-modal').on("click", ".open-modal", function () {
        console.log('run');

        var data = $(this).data('modal_data');

         $('#expenses_modal_edit').on('shown.bs.modal', function(e){
            $(".modal-body #expenses_id").val(data.id);
            $(".modal-body #remark").val(data.remark);
            $(this).off('shown.bs.modal');
        });

        $('#expenses_modal_edit').on('hide.bs.modal', function(e){
            $(this).off('hide.bs.modal');
        });
    });
</script>
<script>
    $("#expenses_edit").on("submit", function (event) {

        event.preventDefault();
        swal({
            title: "Update",
            text: "Are you sure you want to update?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function(resolve, reject){
                    var form = document.getElementById('expenses_edit');
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url: '/expenses/update_remark/' + form.expenses_id.value,
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            resolve(data);
                        },
                        error: function(){
                            swal.showValidationMessage('server error');
                            swal.hideLoading();
                        }
                    });
                });

            }
        }).then((result) => {
            $('#expenses_modal_edit').modal('toggle');
            if (result.value) {
                $('#expenses_table').DataTable().ajax.reload();
                swal('Updated!', '--', 'success');
            }

        });
    });
</script>

<script>
    var submit_val;

    function getsubmit(e){
        submit_val = e;
    }


    $( "#claim_update" ).on( "submit", function( event ) {
        event.preventDefault();
        swal({
            title: "Submit",
            text: "Are you sure you want to submit?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            allowOutsideClick: () => !swal.isLoading(),
            preConfirm: function() {
                return new Promise(function(resolve, reject) {
                    var form = document.getElementById('claim_update');
                    var formData = new FormData(form);
                    formData.append('submit_val',submit_val);
                    console.log(submit_val);
                    $.ajax({
                        type: "POST",
                        url: '{{ route('claim.update.request', $claim->id) }}',
                        data: formData,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            resolve(data);
                        },
                        error: function(data) {
                            errormsg = '';
                            if(data.status == 422 ){
                                $.each(data.responseJSON.errors, function (key, value) {
                                    errormsg = errormsg + value[0] + "<br />";
                                });
                            }else{
                                console.log(data);
                                errormsg = "Server error. Please try again";
                            }
                            swal.showValidationMessage(errormsg);
                            swal.hideLoading();
                        },
                    });
                });

            }
        }).then((result) => {
            if(result.value){
                {{-- swal('Claim Updated!', '--', 'success'); --}}
                location.reload();
            }

        });
    });


</script>


@endsection
