<div id="view-leave-status" class="modal fade in" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body view-leave-status">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h1>Leave Status</h1>
                <div class="form-group">
                    <label>Status</label><span class="label label-success">Approved</span>
                </div>
                <div class="form-group">
                    <label>Applied Date</label><span class="text-muted">13 Aug 2018</span>
                </div>
                <div class="form-group">
                    <label>Leave Type</label><span class="text-muted">Medical Leave</span>
                </div>

                <div class="form-group">
                    <label>Start Date</label><span class="text-muted">13 Aug 2018</span>
                </div>
                <div class="form-group">
                    <label>End Date</label><span class="text-muted">16 Aug 2018</span>
                </div>
                <div class="form-group">
                    <label>Halfday</label><span class="text-muted">Halfday AM End Date</span>
                </div>
                <div class="form-group">
                    <label>Leave Reason</label><span class="text-muted">Maternity Checkup @ Putrajaya Hospital.</span>
                </div>
                <div class="form-group m-b-20">
                    <label>Total Days</label><span class="text-muted">3.5 Days</span>
                </div>



                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

            </div>

        </div>
    </div>
</div>
