@extends('layouts.default.master')

@section('content')
<div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>

<div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Assets</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    <ol class="breadcrumb">
                        <li><a href="dashboard-adminview.html">Dashboard</a></li>
                        <li class="active">Assets</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!-- ============================================================== -->
            <!-- Different data widgets -->
            <!-- ============================================================== -->

            {{-- Asset List --}}
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title">Assets Listings
                            <a href="#add-asset" data-toggle="modal" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Add Asset</a></h3>
                        <div class="table-responsive">
                            <table  id="asset_table" class="">
                                <thead>
                                    <tr>
                                        <th style="width: 5%">#</th>
                                        <th>Name</th>
                                        <th>Serial Number</th>
                                        <th>Warranty</th>
                                        <th>Status</th>
                                        <th style="width: 20%">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Type List --}}
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title">Configure Asset Type
                            <a href="#add-type" data-toggle="modal" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Add Asset Type</a></h3>
                        <div class="table-responsive">
                            <table  id="type_table" class="">
                                <thead>
                                    <tr>
                                        <th style="width: 5%">#</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                        <th style="width: 20%">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Brand List --}}
            {{-- <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title">Brands & Manufacturer
                                <a href="#add-brand" data-toggle="modal" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Add Brands</a></h3>
                            <div class="table-responsive">
                                <table  id="brand_table" class="">
                                    <thead>
                                        <tr>
                                            <th style="width: 5%">#</th>
                                            <th>Brands</th>
                                            <th>Status</th>
                                            <th>Type</th>
                                            <th style="width: 20%">Action</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> --}}

            {{-- Assign List --}}
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title">Assign Listings
                            <a href="#add-assign" data-toggle="modal" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Assign User</a></h3>
                        <div class="table-responsive">
                            <table  id="assign_table" class="">
                                <thead>
                                    <tr>
                                        <th style="width: 5%">#</th>
                                        <th>Employee</th>
                                        <th>Asset Type</th>
                                        <th>Brands</th>
                                        <th>Status</th>
                                        <th style="width: 20%">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


            <!--row -->
            {{-- modal assets --}}
            @include('assets.show')
            @include('assets.create')
            @include('assets.edit')

            @include('assets.types.show')
            @include('assets.types.create')
            @include('assets.types.edit')

            {{-- @include('assets.brands.show')
            @include('assets.brands.create')
            @include('assets.brands.edit') --}}


            @include('assets.assigns.show')
            @include('assets.assigns.create')
            @include('assets.assigns.edit')

    </div>
        <!-- /.container-fluid -->
    </div>

    @endsection

    @section('js')
    @parent

    <script>
    $(function () {

        $('#asset_table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/assets/list',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false},
                {data: 'name'},
                {data: 'serialnum'},
                {data: 'warranty'},
                {data: 'status'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });

         $('#type_table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/types/list',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false},
                {data: 'type_name'},
                {data: 'status'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });

        $('#brand_table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/brands/list',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false},
                {data: 'assign_type'},
                {data: 'assign_brand'},
                {data: 'status'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });

        $('#assign_table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/assigns/list',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false},
                {data: 'employee', name: 'employee.username'},
                {data: 'assign_type'},
                {data: 'assign_brand'},
                {data: 'assign_status'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });

    });

    $(document).off('click', '.open-modal').on("click", ".open-modal", function () {

        var data = $(this).data('modal_data');

        if($(this).data('type') ){
            var type = $(this).data('type');
        }

        $('#modal_asset_show').on('shown.bs.modal', function(e){
            $(".modal-body #modal_asset_name").text(data.name);
            $(".modal-body #modal_asset_serialnum").text(data.serialnum);
            $(".modal-body #modal_asset_warranty").text(data.warranty);
            $(".modal-body #modal_asset_status").text(data.status);
            $(this).off('shown.bs.modal');
        });

        $('#modal_asset_show').on('hide.bs.modal', function(e){
            $(".modal-body #modal_asset_name").text('');
            $(".modal-body #modal_asset_serialnum").text('');
            $(".modal-body #modal_asset_warranty").text('');
            $(".modal-body #modal_asset_status").text('');
            $(this).off('hide.bs.modal');
        });

        $('#modal_asset_edit').on('shown.bs.modal', function(e){
            $(".modal-body #asset_name_edit").val(data.name);
            $(".modal-body #asset_serialnum_edit").val(data.serialnum);
            $(".modal-body #asset_warranty_edit").val(data.warranty);
            $(".modal-body #id").val(data.id);
            $(this).off('shown.bs.modal');
        });

        $('#modal_asset_edit').on('hide.bs.modal', function(e){
            $(this).find('#asset_edit')[0].reset();
            $(this).off('hide.bs.modal');
        });

        // Types

        $('#modal_type_show').on('shown.bs.modal', function(e){
            $(".modal-body #modal_type_type").text(data.type_name);
            $(".modal-body #modal_type_status").text(data.status)
            $(this).off('shown.bs.modal');
        });

        $('#modal_type_show').on('hide.bs.modal', function(e){
            $(".modal-body #modal_type_type").text('');
            $(".modal-body #modal_type_status").text('');
            $(this).off('hide.bs.modal');
        });

        $('#modal_type_edit').on('shown.bs.modal', function(e){
            $(".modal-body #type_name_edit").val(data.type_name);
            $(".modal-body #type_status_edit").val(data.status);
            $(".modal-body #id").val(data.id);
            $(this).off('shown.bs.modal');
        });

        $('#modal_type_edit').on('hide.bs.modal', function(e){
            $(this).find('#type_edit')[0].reset();
            $(this).off('hide.bs.modal');
        });


        // Brands

        $('#modal_brand_show').on('shown.bs.modal', function(e){
            $(".modal-body #modal_brand_name").text(data.brand_name);
            $(".modal-body #modal_brand_status").text(data.status)
            $(".modal-body #modal_brand_asset").text(data.brand_asset)
            $(".modal-body #modal_brand_asset_name").text(data.type.type_name)
            $(this).off('shown.bs.modal');
        });

        $('#modal_brand_show').on('hide.bs.modal', function(e){
            $(".modal-body #modal_brand_name").text('');
            $(".modal-body #modal_brand_status").text('')
            $(".modal-body #modal_brand_asset").text('')
            $(this).off('hide.bs.modal');
        });

        $('#modal_brand_edit').on('shown.bs.modal', function(e){
            $(".modal-body #brand_name_edit").val(data.brand_name);
            $(".modal-body #brand_status_edit").val(data.status);
            $(".modal-body #id").val(data.id);
            $(this).off('shown.bs.modal');
        });

        $('#modal_brand_edit').on('hide.bs.modal', function(e){
            $(this).find('#brand_edit')[0].reset();
            $(this).off('hide.bs.modal');
        });

        // Assigns

        $('#modal_assign_show').on('shown.bs.modal', function(e){
            $(".modal-body #modal_assign_name").text(data.employee_id);
            $(".modal-body #modal_assign_status").text(data.assign_status);
            $(".modal-body #modal_assign_started").text(data.started_at);
            $(".modal-body #modal_assign_ended").text(data.ended_at);
            $(".modal-body #modal_assign_type").text(data.assign_type);
            $(".modal-body #modal_assign_brand").text(data.assign_brand);
            $(".modal-body #modal_assign_username").text(data.employee.username);
            $(".modal-body #modal_assign_type_name").text(data.type.type_name);
            $(".modal-body #modal_assign_brand_name").text(data.brands.brand_name);
            $(this).off('shown.bs.modal');
        });

        $('#modal_assign_show').on('hide.bs.modal', function(e){
            $(".modal-body #modal_assign_name").text('');
            $(".modal-body #modal_assign_status").text('');
            $(".modal-body #modal_assign_started").text('');
            $(".modal-body #modal_assign_ended").text('');
            $(".modal-body #modal_assign_type").text('');
            $(".modal-body #modal_assign_brand").text('');
            $(this).off('hide.bs.modal');
        });

        $('#modal_assign_edit').on('shown.bs.modal', function(e){
            $(".modal-body #assign_name_edit").val(data.employee_id);
            $(".modal-body #assign_username_edit").val(data.employee.username);
            $(".modal-body #assign_status_edit").val(data.assign_status);
            $(".modal-body #assign_started_edit").val(data.started_at);
            $(".modal-body #assign_ended_edit").val(data.ended_at);
            $(".modal-body #assign_type_edit").val(data.assign_type);
            $(".modal-body #assign_brand_edit").val(data.assign_brand);
            $(".modal-body #id").val(data.id);
            $(this).off('shown.bs.modal');
        });

        $('#modal_assign_edit').on('hide.bs.modal', function(e){
            $(this).find('#assign_edit')[0].reset();
            $(this).off('hide.bs.modal');
        });

    });
    </script>

    @endsection
