<div id="add-brand" class="modal fade in" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    <h1>Create New Category</h1>
                    {!! Form::open(['url' => 'brands', "id" => "brand_create", 'autocomplete' => "off"]) !!}
                    <div class="form-group row">
                        <div class="col-md-6">
                                {!! Form::label('brand name') !!}
                                {{Form::text('brand_name', null , ['required','class' => 'form-control','placeholder' => 'insert brand name'])}}
                        </div>
                    </div>
                    <div class="col-md-4">
                            {!! Form::label('Asset Type') !!}
                            <select class="form-control" name="brand_asset" id="types" data-parsley-required="true">
                            @foreach ($types as $type)
                                <option value="{{ $type->id }}">{{ $type->type_name}}</option>
                            @endforeach
                            </select>
                        </div>
                    <div class="form-group row">
                            <div class="col-md-6">
                                    {!! Form::label('status') !!}
                                    {{Form::select('status', ['Active' => 'Active', 'Inactive' => 'Inactive'], null, ['class' => 'form-control', 'placeholder' => 'Select Status'])}}
                                </div>
                        </div>
                    {{Form::submit('Add New Brand',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                    <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
     </div>

    @section('js')
    @parent
    <script>

    $('#add-brand').on('hidden.bs.modal', function(e) {
        $(this).find('#brand_create')[0].reset();
    });

    $( "#brand_create" ).on( "submit", function( event ) {
        event.preventDefault();
        swal({
            title: "Add Asset Brand",
            text: "Confirm to add new brand to inventory?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                var form = document.getElementById('brand_create');
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: '{{ route('brands.store') }}',
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(msg) {
                        return msg;
                    }
                });
            }
        }).then((result) => {
            $('#add-brand').modal('toggle');
            if(result.value){
                $('#brand_table').DataTable().ajax.reload(function(json){
                    var len = json.data.length;
                    $("#brands").empty();
                    $("#brands").append("<option value=''>Please Select</option>");
                    for( var i = 0; i<len; i++){
                        var id = json.data[i]['id'];
                        var name = json.data[i]['title'];
                        $("#brands").append("<option value='"+id+"'>"+name+"</option>");

                    }
                });

                //reload select designation


                swal('New Brand/Manufacturer Added!', '--', 'success');
            }
        });
    });
    </script>
    @endsection
