<?php

namespace App\Http\Controllers;

// use App\Medical;
use Illuminate\Http\Request;
// use App\Employee;
use App\LeaveType;
// use App\Annual;
use App\EmployeeLeave;
use Auth;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Postmark\PostmarkClient;

class MedicalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $employee = Employee::lists(['id']);
        $leavetype = LeaveType::lists(['id']);
        return view('employees.leaves.medicals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
        $client = new PostmarkClient("73a69c7d-8c56-4f59-877c-74d05cd5e247");

        $leaves = EmployeeLeave::where('employee_id', Auth::user()->id)->get();
        $approval = Approval::where('employee_id', Auth::user()->id)->first();
        //return $approval->approvedBy;

        $aid = Auth::user()->id;
        $employees = Employee::find($aid);

        $medical_leave_quota = Auth::user()->leave_allocs()->type('Medical Leave')->first()->quota;
        $medical_leave_taken = Auth::user()->leaves()->type('Medical Leave')->sum('total_days');
        $balance = $medical_leave_quota - $medical_leave_taken;

        $a = Carbon::parse($request->started_at);
        $b = Carbon::parse($request->ended_at);
        $days = $b->diffInDays($a);
        $days = $days + 1;

        $unpaid = $balance - $days;

        $medic = new EmployeeLeave;
        $medic->employee_id = Auth::user()->id;
        $medic->leave_type = 'Medical Leave';
        $medic->started_at = $request->started_at;
        $medic->ended_at = $request->ended_at;
        $medic->reason = $request->reason;
        $medic->total_days = $days;
        $medic->as_unpaid = $unpaid;
        $medic->leave_proc_status = 'unproc';
        // $emergency->as_lieu = $request->as_lieu;
        // $emergency->remark = $request->remark;
        $medic->status = 'Pending';

        $medic->save();
        if ($request->hasFile('file')) {
            $medic->addMediaFromRequest('file')->toMediaCollection('leaves');
        }
        $leave = $medic->id;
        $sendResult = $client->sendEmailWithTemplate(

            "hassanul@boneybone.com",
            $approval_lv1->approvedBy->email,
            10030123,
                [
                    "system_name" => "[IX HRMS] Leave Application Approval",
                    "fname" => $employees->fname,
                    "lname" => $employees->lname,
                    "uid" => $employees->employee_uid,
                    "approve" =>  $approval_lv1->approvedBy->fname,
                    "approver" =>  $approval_lv1->approvedBy->lname,
                    "leave_type" => 'Medical Leave',
                    "started_at" => $request->started_at,
                    "ended_at" => $request->ended_at,
                    "reason" => $request->reason,
                    "accept" => route('adminleaves.update_email',$leave),
                    "reject" => route('adminleaves.reject_email',$leave),
                    "link" => url('http://hrms.ixtelecom.net')
                ]);

            $sendResult = $client->sendEmailWithTemplate(

                    "hassanul@boneybone.com",
                    $approval_lv2->approvedBy->email,
                    10030123,
                        [
                            "system_name" => "[IX HRMS] Leave Application Approval",
                            "fname" => $employees->fname,
                            "lname" => $employees->lname,
                            "uid" => $employees->employee_uid,
                            "approve" =>  $approval_lv2->approvedBy->fname,
                            "approver" =>  $approval_lv2->approvedBy->lname,
                            "leave_type" => 'Medical Leave',
                            "started_at" => $request->started_at,
                            "ended_at" => $request->ended_at,
                            "reason" => $request->reason,
                            "accept" => route('adminleaves.approve_email',$leave),
                            "reject" => route('adminleaves.reject_email',$leave),
                            "link" => url('http://hrms.ixtelecom.net')
                        ]);
        return response()->json($medic, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Medical  $medical
     * @return \Illuminate\Http\Response
     */
    public function show(Medical $medical)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Medical  $medical
     * @return \Illuminate\Http\Response
     */
    public function edit(Medical $medical)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Medical  $medical
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Medical $medical)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Medical  $medical
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeLeave $employeeLeave, $id)
    {
        $employeeLeave = EmployeeLeave::find($id);
        $employeeLeave->delete();
        return redirect()->back();
    }
}
