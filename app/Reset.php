<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reset extends Model
{
    public $table = "reset_passwords";

    public function employee(){
        return $this->belongsTo(Employee::class,'email');
    }
}
