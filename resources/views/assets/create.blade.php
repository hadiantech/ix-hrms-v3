<div id="add-asset" class="modal fade in" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Asset Listing New Entry</h1>
                {!! Form::open(['url' => 'assets', "id" => "asset_create", 'autocomplete' => "off"]) !!}
                <div class="form-group row">
                    <div class="col-md-6">
                        {!! Form::label('Brand Name') !!}
                        {{Form::text('name', null , ['required','class' => 'form-control','placeholder' => 'insert brand name'])}}
                    </div>
                    <div class="col-md-6">
                            {!! Form::label('Manufacturer Name') !!}
                            {{Form::text('manufact_name', null , ['required','class' => 'form-control','placeholder' => 'insert manufacturer name'])}}
                    </div>
                </div>
                <div class="form-group row ">
                    <div class="col-md-6">
                        {!! Form::label('serial number') !!}
                        {{Form::text('serialnum', null , ['required','class' => 'form-control','placeholder' => 'insert serial number'])}}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('warranty') !!}
                        {{ Form::date('warranty',null,['required','class'=>'form-control','placeholder'=>'start warranty date']) }}
                    </div>
                </div>

                <div class="form-group row">
                        <div class="col-md-6">
                            {!! Form::label('price') !!}
                            {{ Form::text('price',null,['required','class'=>'form-control','placeholder'=>'brand price in RM']) }}
                        </div>
                        <div class="col-md-6">
                        {!! Form::label('Asset Type') !!}
                        <select class="form-control" name="asset_type" id="types" data-parsley-required="true">
                        @foreach ($types as $type)
                            <option value="{{ $type->id }}">{{ $type->type_name}}</option>
                        @endforeach
                        </select>
                    </div>
                    </div>
                <div class="form-group row">

                    <div class="col-md-6">
                        {!! Form::label('status') !!}
                        {{Form::select('status', ['Active' => 'Active', 'Inactive' => 'Inactive'], null, ['class' => 'form-control', 'placeholder' => 'Select Status'])}}
                    </div>
                </div>
                {{Form::submit('Add Asset',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
 </div>

@section('js')
@parent
<script>

$('#add-asset').on('hidden.bs.modal', function(e) {
    $(this).find('#asset_create')[0].reset();
});

$( "#asset_create" ).on( "submit", function( event ) {
    event.preventDefault();
    swal({
        title: "Add Asset",
        text: "Confirm to add asset?",
        type: "info",
        showCancelButton: true,
        showLoaderOnConfirm: true,
        preConfirm: function() {
            var form = document.getElementById('asset_create');
            var formData = new FormData(form);
            $.ajax({
                type: "POST",
                url: '{{ route('assets.store') }}',
                data: formData,
                cache:false,
                contentType: false,
                processData: false,
                success: function(msg) {
                    return msg;
                }
            });
        }
    }).then((result) => {
        $('#add-asset').modal('toggle');
        if(result.value){
            $('#asset_table').DataTable().ajax.reload(function(json){
                var len = json.data.length;
                $("#assets").empty();
                $("#assets").append("<option value=''>Please Select</option>");
                for( var i = 0; i<len; i++){
                    var id = json.data[i]['id'];
                    var name = json.data[i]['title'];
                    $("#assets").append("<option value='"+id+"'>"+name+"</option>");

                }
            });

            //reload select designation


            swal('New Notice Added!', '--', 'success');
        }
    });
});
</script>
@endsection
