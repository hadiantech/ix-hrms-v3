<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header admin-portal ">
        <div class="top-left-part">
            <!-- Logo -->
            <a class="logo" href="{{ route('dashboard') }}">

                <img src="{{ URL::asset('img/logonavbar.png') }}" alt="home" class="img-fluid" style="max-width:190px" />
            </a>

        </div>
        <!-- /Logo -->
        <!-- Search input and Toggle icon -->
        {{--  <ul class="nav navbar-top-links navbar-left">
            <li><a href="javascript:void(0)" class="open-close waves-effect waves-light visible-xs"><i class="ti-close ti-menu"></i></a></li>
            <li class="dropdown">
                <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"> <i class="mdi mdi-gmail"></i>
                    <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                </a>
                <ul class="dropdown-menu mailbox animated bounceInDown">
                    <li>
                        <div class="drop-title">You have 4 new messages</div>
                    </li>
                    <li>
                        <div class="message-center">

                        </div>
                    </li>
                    <li>
                        <a class="text-center" href="javascript:void(0);"> <strong>See all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                    </li>
                </ul>
                <!-- /.dropdown-messages -->
            </li>


        </ul>  --}}
        <ul class="nav navbar-top-links navbar-right pull-right">
            @role('Admin')
            <li class="p-t-20 p-r-20"><span class="label label-danger">Admin View</span></li>
            @endrole
            <li class="dropdown">
                <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#">
                    @if(Auth::user()->getMedia('profile')->first())
                    <img src="{{ Auth::user()->getMedia('profile')->first()->getUrl() }}" alt="user-img" width="36" class="img-circle">
                    @else
                    <img src="{{ asset('images/Avatar.png') }}" alt="user-img" width="36" class="img-circle">
                    @endif

                    <b class="hidden-xs">{{ Auth::user()->fname }}</b><span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-user animated bounceInDown">
                    <li>
                        <div class="dw-user-box">
                            <div class="u-img">
                                @if(Auth::user()->getMedia('profile')->first())
                                <img src="{{ Auth::user()->getMedia('profile')->first()->getUrl() }}" alt="user" />
                                @else
                                <img src="{{ asset('images/Avatar.png') }}" alt="user" />
                                @endif
                            </div>
                            <div class="u-text">
                                <h4>{{ Auth::user()->fname }} {{ Auth::user()->lname }}</h4>
                                <p class="text-muted">{{ Auth::user()->email }}</p>
                                {{-- @foreach(Auth::user()->roles as $role)
                                <a class="btn btn-rounded btn-danger btn-sm">{{ $role->name }}</a>
                                @endforeach --}}
                            </div>
                        </div>
                    </li>

                    <li role="separator" class="divider"></li>
                    <li><a href="{{ route('profile.my') }}"><i class="ti-user"></i> My Profile</a></li>

                    <li><a href="{{ route('logout') }}"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
    </div>
    <!-- /.navbar-header -->
    <!-- /.navbar-top-links -->
    <!-- /.navbar-static-side -->
</nav>
