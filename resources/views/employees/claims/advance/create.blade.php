<div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Add Advanced Claim</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    <ol class="breadcrumb">
                        <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                        <li><a href="{{ route('claims.index') }}">Claim Dashboard</a></li>
                        <li class="active">Add Advanced Claim</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>


             <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title text-uppercase m-b-30">New Advanced Claim : Project #MYIX123
                            <a href="#add-expense" data-toggle="modal" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Add Expenses</a></h3>

                            <div class="alert alert-warning alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Your advanced claim amount is lower match the advance requested. Kindly return the balance to HR.
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Type</th>
                                            <th>Item Name & Desc</th>
                                            <th class="text-center">Project Code</th>
                                            <th class="text-center">Amount (RM)</th>
                                            <th class="text-center">Item Date</th>
                                            <th class="text-center">Receipt No.</th>
                                            <th class="text-center">Ref No.</th>
                                            <th class="text-right">Action</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Purchases</td>
                                            <td>Ninja Form Add-ons
                                                <p class="text-muted">Add-on plugin for website purposes</p>
                                            </td>
                                            <td class="text-center">MYIX1234</td>
                                            <td class="text-center">45.00</td>
                                            <td class="text-center">14 Mar 2018</td>
                                            <td class="text-center">324AD</td>
                                            <td class="text-center">1</td>
                                            <td class="text-right">
                                                <a href="" class="label label-info m-r-5"><i class="fa fa-pencil"></i></a>
                                                <a href="" class="label label-danger"><i class="fa fa-trash"></i></a>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Mileage</td>
                                            <td>Trip to Teraju
                                                <p class="text-muted">One way trip from IX to Teraju<br>
                                                Distance : 33.5KM</p>
                                            </td>
                                            <td class="text-center">MYIX1234</td>
                                            <td class="text-center">16.75</td>
                                            <td class="text-center">14 Mar 2018</td>
                                            <td class="text-center">324AD</td>
                                            <td class="text-center">2</td>
                                            <td class="text-right">
                                                <a href="" class="label label-info m-r-5"><i class="fa fa-pencil"></i></a>
                                                <a href="" class="label label-danger"><i class="fa fa-trash"></i></a>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Purchases</td>
                                            <td>Ninja Form Add-ons
                                                <p class="text-muted">Add-on plugin for website purposes</p>
                                            </td>
                                            <td class="text-center">MYIX1234</td>
                                            <td class="text-center">86.00</td>
                                            <td class="text-center">19 Mar 2018</td>
                                            <td class="text-center">324AD</td>
                                            <td class="text-center">2</td>
                                            <td class="text-right">
                                                <a href="" class="label label-info m-r-5"><i class="fa fa-pencil"></i></a>
                                                <a href="" class="label label-danger"><i class="fa fa-trash"></i></a>
                                            </th>
                                        </tr>

                                    </tbody>
                                    <tfoot class="claim-amount">
                                        <tr>
                                            <th colspan="6" rowspan="4"></th>
                                            <th class="text-right">TOTAL AMOUNT</th>
                                            <th colspan="2" class="text-right"><span class="amount">RM1000.00</span></th>
                                        </tr>
                                        <tr>

                                            <th class="text-right">APPROVED ADVANCE REQUEST</th>
                                            <th colspan="2" class="text-right"><span class="amount">RM1300.00</span></th>
                                        </tr>
                                        <tr>

                                            <th class="text-right">BALANCE</th>
                                            <th colspan="2" class="text-right"><span class="amount">+RM300.00</span></th>
                                        </tr>
                                    </tfoot>
                                </table>


                        </div>

                        <div class="payment-form row m-b-20">
                            <div class="col-md-6"><h3 class="box-title">BALANCE PAYMENT UPDATE</h3>
                                <div class="form-group row">

                                    <div class="col-md-6">

                                   <select   class="selectpicker" data-style="form-control">
                                    <option selected>Method of Payment</option>
                                    <option>Online Transfer</option>
                                    <option>Cash By Hand</option>
                                   </select>
                               </div>
                               <div class="col-md-6">
                                <select  disabled class="selectpicker" data-style="form-control">
                                    <option selected>Status : Unpaid</option>
                                    <option>Paid</option>
                                   </select>
                               </div>
                                </div>
                                <p>If you have balance to pay back to the company, kindly transfer the balance to the following bank details and upload the payment receipt for verification.</p>
                                    <p style="background:#eee;padding:20px;margin:0"><strong>BONEYBONE VENTURES SDN BHD</strong><br>
                                    CIMB BANK BERHAD<br>
                                    712345678
                                </p>

                            </div>
                            <div class="col-md-6"><h3 class="box-title">&nbsp;</h3>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Bank Name">
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Reference Number">
                                    </div>
                                    <div class="col-md-6">

                                    <input type="text" class="form-control" placeholder="Date & Time">
                                </div>
                                </div>
                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control"  placeholder="ds" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                                    <input  type="file" name="..."> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> </div>
                                </div>
                             </div>
                         </div>

                        <button style="width:200px" class="m-r-10 btn-lg btn-rounded btn-info text-uppercase">Save Claim</button>
                        <button style="width:200px" class=" btn-lg btn-rounded btn-success text-uppercase">Confirm & Submit</button>

                    </div>
               </div>

               <div class="modal fade" id="add-expense">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h1>Add Expense</h1>
                            <form>

                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <select class="selectpicker" data-style="form-control">
                                            <option selected disabled>Expense Type</option>
                                            <option>Hotel & Travelling</option>
                                            <option> Meal Allowance</option>
                                            <option>Refreshment & Entertainment</option>
                                            <option>Mileage</option>
                                            <option>Toll & Parking</option>
                                            <option>Claimable Allowances</option>
                                            <option>Office Supplies</option>
                                            <option>Miscellaneous</option>

                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" placeholder="Project Code">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Item Name">
                                </div>
                                <div class="form-group">
                                    <textarea rows="4" class="form-control">Item Description</textarea>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <input type="text" class="form-control datepicker-autoclose" placeholder="Item Date"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="number" class="form-control" placeholder="Amount">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" placeholder="Receipt No.">
                                    </div>
                                    <div class="col-md-6">
                                        <input type="number" class="form-control" placeholder="Reference No.">
                                    </div>
                                </div>
                                <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Add Expense</button>
                                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

                            </form>
                        </div>
                    </div>
                </div>
               </div>
            </div>


            </div>

        </div>
        <!-- /.row -->


    <!-- /.container-fluid -->
</div>
