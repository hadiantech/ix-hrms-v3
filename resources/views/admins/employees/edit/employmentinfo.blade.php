<section id="employmentinfo">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3 class="box-title">Account Details</h3>
            <div class="form-group row">
                <div class="col-md-6">
                    <input type="text" class="form-control" value="{{$employees->username}} ">
                </div>
                <div class="col-md-6">
                    <input type="text" class="form-control" value="{{$employees->email}} ">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <input type="password" class="form-control" value="{{$employees->password}} ">
                </div>

            </div>
            <hr>
            <h3 class="box-title">Employee Info</h3>

            <div class="form-group row">
                <div class="col-md-4">
                    {{-- <input type="password" class="form-control" placeholder="{{$organisations->company_name}} "> --}}
                </div>
                <div class="col-md-4">
                    {{-- <input type="password" class="form-control" placeholder="{{$departments->department_name}} "> --}}
                </div>
                <div class="col-md-4">
                    {{-- <input type="password" class="form-control" placeholder="{{$designations->designation_name}} "> --}}
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-4">
                    <input type="text" class="form-control" value="{{$employees->created_at}} ">
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" value="{{$employees->id}} ">
                </div>
                <div class="col-md-4">

                        {{Form::select('permanentstatus', ['A' => 'Permanent', 'I' => 'Contract'], null, ['class' => 'form-control', 'placeholder' => 'Select Status'])}}

                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-4">
                    {{Form::select('probation', ['A' => 'Yes', 'I' => 'No'], null, ['class' => 'form-control', 'placeholder' => 'Probation Status'])}}
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" placeholder="Date of Confirmation">
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" placeholder="Contract End Date">
                </div>
            </div>

            <hr>
            <h3 class="box-title">Position Details</h3>

            <div class="form-group row m-b-40">
                <div class="col-md-4">
                    <select class="selectpicker" data-style="form-control">
                        <option selected disabled>Hierarchy</option>
                        <option>Management</option>
                        <option>Human Resource</option>
                        <option>Staff</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <select class="selectpicker" data-style="form-control">
                        <option selected disabled>Reports To</option>
                        <option>Staff 1</option>
                        <option>Staff 2</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <select class="selectpicker" data-style="form-control">
                        <option selected disabled>Employment Role</option>
                        <option>Employer</option>
                        <option>Employee</option>
                        <option>Finance</option>
                    </select>
                </div>
            </div>

            <h3 class="box-title">Working Days</h3>
            <div class="form-inline row m-b-40">
                <div class="checkbox checkbox-info">
                    <input id="monday" type="checkbox">
                    <label for="monday">Monday</label>
                </div>
                <div class="checkbox checkbox-info">
                    <input id="tuesday" type="checkbox">
                    <label for="tuesday">Tuesday</label>
                </div>
                <div class="checkbox checkbox-info">
                    <input id="wednesday" type="checkbox">
                    <label for="wednesday">Wednesday</label>
                </div>
                <div class="checkbox checkbox-info">
                    <input id="thursday" type="checkbox">
                    <label for="thursday">Thursday</label>
                </div>
                <div class="checkbox checkbox-info">
                    <input id="friday" type="checkbox">
                    <label for="friday">Friday</label>
                </div>
                <div class="checkbox checkbox-info">
                    <input id="saturday" type="checkbox">
                    <label for="saturday">Saturday</label>
                </div>
                <div class="checkbox checkbox-info">
                    <input id="sunday" type="checkbox">
                    <label for="sunday">Sunday</label>

                </div>


            </div>
            <button style="width:200px" class=" btn-lg btn-rounded btn-info text-uppercase">Save</button>
        </div>

    </div>

</section>
