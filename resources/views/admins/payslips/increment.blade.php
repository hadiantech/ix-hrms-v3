<div class="modal fade" id="set-increment">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h1>Set Increment</h1>
               <div class="form-group">
                 <input type="text" class="form-control" disabled="disabled" value="Employee : Husna Bahrudin">
               </div>
               <div class="form-group row">
                <div class="col-md-6">
                 <input type="text" class="form-control" placeholder="Set increment. E.g 130.00">
                </div>
                <div class="col-md-6">
                 <select class="selectpicker" data-style="form-control">
                    <option>Select Type of Increment</option>
                    <option>Monthly</option>
                    <option>Hourly</option>
                 </select>
                </div>
               </div>


               <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Save Salary</button>
                            <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
