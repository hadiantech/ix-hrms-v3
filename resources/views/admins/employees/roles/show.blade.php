<div id="show-adminroles{{$role->id}}" class="modal fade in" tabindex="-1" role="dialog" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    <h1>Designations Details</h1>

                    {!! Form::open(['url' => 'adminroles']) !!}

                <div class="  form-group ">
                        {!! Form::label('Role Name') !!}
                    {{Form::text('adminroles',$role->name,array('class' => 'form-control')) }}
                </div>
                <div class="  form-group ">
                        {!! Form::label('Status') !!}
                        {{Form::text('adminroles',$role->status,array('class' => 'form-control')) }}
                    </div>
                {{-- {{Form::submit('Close',['class' => 'btn btn-lg btn-info pull-right'])}} --}}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    </div>
{{--
<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title text-uppercase">Role Details
                <a href="#add-company" data-toggle="modal" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Edit Department</a></h3>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>

                            <th class="text-left">Role Name</th>
                            <th class="text-left">Status</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                                <tr>
                                <td> {{$adminroles->role_name}} </td>
                                <td> {{$adminroles->status}}</td>
                                </tr>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div> --}}
