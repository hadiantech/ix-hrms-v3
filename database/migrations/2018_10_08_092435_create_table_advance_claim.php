<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAdvanceClaim extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advance_claims', function (Blueprint $table) {
            $table->increments('id');
            $table->text('type')->nullable();
            $table->text('project_code')->nullable();
            $table->double('amount',10,2)->nullable();
            $table->integer('company_id')->nullable();
            $table->longtext('location')->nullable();
            $table->longtext('purpose')->nullable();
            $table->date('date_travel_from')->nullable();
            $table->date('date_travel_to')->nullable();
            $table->timestamps();
        });


        Schema::create('traveller_informations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('advance_claims_id')->nullable();
            $table->text('name')->nullable();
            $table->text('email')->nullable();
            $table->text('phone')->nullable();
            $table->text('mykad')->nullable();
            $table->text('passport')->nullable();
            $table->text('passport_expired')->nullable();
            $table->timestamps();
        });

        Schema::create('visa_informations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('advance_claims_id')->nullable();
            $table->text('visa_owner')->nullable();
            $table->text('country')->nullable();
            $table->text('type')->nullable();
            $table->date('entry_date')->nullable();
            $table->text('nationality')->nullable();
            $table->timestamps();
        });

        Schema::create('flight_informations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('advance_claims_id')->nullable();
            $table->text('flight_type')->nullable();
            $table->date('travel_date')->nullable();
            $table->text('depart')->nullable();
            $table->text('arrive')->nullable();
            $table->longtext('detail')->nullable();
            $table->timestamps();
        });

        Schema::create('hotel_informations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('advance_claims_id')->nullable();
            $table->text('name')->nullable();
            $table->text('location')->nullable();
            $table->text('room')->nullable();
            $table->date('check_in')->nullable();
            $table->date('check_out')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advance_claims');
        Schema::dropIfExists('traveller_informations');
        Schema::dropIfExists('visa_informations');
        Schema::dropIfExists('flight_informations');
        Schema::dropIfExists('hotel_informations');
    }
}
