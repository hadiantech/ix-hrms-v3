<?php

namespace App\Http\Controllers;

use App\EmployeeLieu;
use Illuminate\Http\Request;
use DataTables;
use App\Approval;
use Auth;
use App\Employee;
use Carbon\Carbon;

class LieuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Datatables::eloquent(EmployeeLieu::where('employee_id', Auth::id()))
        ->addColumn('action', function($model){
            $url = 'lieus';
            $show_hide = true;
            $edit_hide = true;
            $delete_hide = true;
            if($model->status == 'Pending'){
                $custom_btn = [
                    '<a class="btn btn-xs btn-danger" data-method="delete" data-confirm="confirm" href="'. route($url.'.destroy', $model->id) .'">Cancel</a>'
                ];
            }
            return view('shared._table_action', compact('model', 'url', 'show_hide', 'edit_hide', 'delete_hide', 'custom_btn'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $a = Carbon::parse($request->started_at);
        $b = Carbon::parse($request->ended_at);
        $days = $b->diffInDays($a);
        $days = $days + 1;
        $expired_at = Carbon::parse($request->ended_at)->addMonths(3);

        $lieu = new EmployeeLieu;
        $lieu->employee_id = Auth::id();
        $lieu->total_days = $days;
        $lieu->total_used_days = '0';
        $lieu->event_name = $request->event_name;
        $lieu->remark = $request->remark;
        $lieu->status = 'Pending';
        $lieu->started_at = $request->started_at;
        $lieu->ended_at = $request->ended_at;
        $lieu->expired_at = $expired_at;
        $lieu->lieu_proc_status = 'unproc';

        $lieu->save();
        return response()->json($lieu, 201);
    }

    public function list()
    {
        return Datatables::eloquent(EmployeeLeave::where('employee_id', Auth::id()))
        ->addColumn('action', function($model){
            return $model->employee->fname." ".$model->employee->lname;
        })

        ->addColumn('action', function($model){
            $url = 'lieus';
            if($model->status == 'Pending'){
                $custom_btn = [
                    '<a class="btn btn-xs btn-danger" data-method="delete" data-confirm="confirm" href="'. route($url.'.destroy', $model->id) .'">Cancel</a>'
                ];
            }
            return view('shared._table_action', compact('model', 'url', 'custom_btn'))->render();
        })
        ->addIndexColumn()
        ->make(true);
        // return Datatables::eloquent($query)
        // ->addColumn('employee', function($model){
        //     return $model->employee->fname." ".$model->employee->lname;
        // })
        // ->addColumn('action', function($model){
        //     $url = 'adminlieus';
        //     $custom_permission = 'admin_lieus';
        //     $modal_view_id = 'leaves_modal_view';
        //     $modal_edit_id = 'leaves_modal_edit';
        //     $delete_hide = true;
        //     $delete_datatable_id_name_refresh = 'lieu_table';
            // $modal_data_customs = array('employee' => $model->employee);

            // if($model->status == 'Pending'){
            //     $custom_btn = [
            //         '<a class="btn btn-xs btn-danger" data-method="delete" data-confirm="confirm" href="'. route($url.'.destroy', $model->id) .'">Cancel</a>'
            //     ];
            // }
        //     return view('shared._table_action', compact('model','url','delete_hide','custom_btn','custom_permission','modal_view_id','modal_edit_id' ))->render();
        // })
        // ->escapeColumns([])
        // ->addIndexColumn()
        // ->make(true);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $lieus = EmployeeLieu::find($id);

        return view('admins.leaves.lieus.edit',compact('lieus'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lieu = EmployeeLieu::find($id);
        $lieu->status = $request->status;

        $lieu->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lieu = EmployeeLieu::find($id);
        $lieu->delete();
        return redirect()->back();
    }
}
