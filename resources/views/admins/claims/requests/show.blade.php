@extends('layouts.admin.master')

@section('content')

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add Advance Request</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                <ol class="breadcrumb">
                    <li><a href="dashboard.html">Dashboard</a></li>
                    <li><a href="dashboard-claim.html">Claim Dashboard</a></li>
                    <li class="active">Add Advance Request</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>


         <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title text-uppercase">New Advance Request</h3>
                        <div class="employee-claim-details">



                                <div class="row">
                                    <div class="col-md-6">
                                        <blockquote>
                                        <h4>Employee Details</h4>
                                           <label>Name</label> <span class="m-l-5">Donalvean Angew</span><br>
                                           <label>ID</label> <span class="m-l-5">MYIX123</span><br>

                                           <label>Department</label> <span class="m-l-5">Engineering, IX Telecom</span><br>

                                           <label>Applied Date</label> <span class="m-l-5">13 Sept 2018</span><br>

                                       </blockquote>
                                    </div>
                                    <div class="col-md-6">

                                          <div class="form-group">
                                            <select class="selectpicker" data-style="form-control">
                                                <option selected>Pending</option>
                                                <option>Return</option>
                                                <option>Reviewed</option>
                                            </select>
                                          </div>
                                          <div class="form-group">
                                            <textarea rows="5" class="form-control">Add overall remarks...</textarea>
                                          </div>
                                    </div>
                                </div>


                        </div>
                        <h3 class="box-title">Cash Advance Details</h3>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Project Code</th>
                                        <th>Amount ({{ settings('currency_code') }})</th>
                                        <th>Company</th>
                                        <th>Location/ Destination</th>
                                        <th>Purpose</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>MYP3022</td>
                                        <td>1,000.00</td>
                                        <td>IX Telecom Sdn Bhd</td>
                                        <td>Proton City</td>
                                        <td>Purchases of item for Supersix Final Game<br>
                                            - Banner and Bunting<br>
                                            - Food and drinks for team<br>
                                            - Tshirt printing<br>
                                            - Petrol and tolls</td>
                                    </tr>
                                    <tr>
                                        <td colspan="10"></td>
                                    </tr>
                                </tbody>


                            </table>
                        </div>


                        <button style="width:100px" class="m-r-10 btn btn-rounded btn-info text-uppercase">Return</button>
                    <button style="width:100px" class=" m-r-10 btn btn-rounded btn-success text-uppercase">Approve</button>
                    <button style="width:100px" class=" btn btn-rounded btn-danger text-uppercase">Close</button>







                    </div>



                </div>
           </div>

           <div class="modal fade" id="add-expense">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h1>Add Expense</h1>
                        <form>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <select class="selectpicker" data-style="form-control">
                                        <option selected disabled>Expense Type</option>
                                        <option>Hotel & Travelling</option>
                                        <option> Meal Allowance</option>
                                        <option>Refreshment & Entertainment</option>
                                        <option>Mileage</option>
                                        <option>Toll & Parking</option>
                                        <option>Claimable Allowances</option>
                                        <option>Office Supplies</option>
                                        <option>Miscellaneous</option>

                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Project Code">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Item Name">
                            </div>
                            <div class="form-group">
                                <textarea rows="4" class="form-control">Item Description</textarea>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker-autoclose" placeholder="Item Date"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <input type="number" class="form-control" placeholder="Amount">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Receipt No.">
                                </div>
                                <div class="col-md-6">
                                    <input type="number" class="form-control" placeholder="Reference No.">
                                </div>
                            </div>
                            <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Add Expense</button>
                            <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

                        </form>
                    </div>
                </div>
            </div>
           </div>
        </div>


        </div>

    </div>
    <!-- /.row -->


<!-- /.container-fluid -->
<footer class="footer text-center"> 2018 &copy; IX HRMS. All Rights Reserved. </footer>
</div>
@endsection
