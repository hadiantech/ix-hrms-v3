<div id="add-type" class="modal fade in" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    <h1>Create New Category</h1>
                    {!! Form::open(['url' => 'types', "id" => "type_create", 'autocomplete' => "off"]) !!}
                    <div class="form-group row">
                        <div class="col-md-6">
                                {!! Form::label('type & category') !!}
                                {{Form::text('type_name', null , ['required','class' => 'form-control','placeholder' => 'insert new  type category'])}}
                        </div>
                        <div class="col-md-6">
                                    {!! Form::label('status') !!}
                                    {{Form::select('status', ['Active' => 'Active', 'Inactive' => 'Inactive'], null, ['class' => 'form-control', 'placeholder' => 'Select Status'])}}
                                </div>
                    </div>
                     
                    {{Form::submit('Add New Type',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                    <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
     </div>

    @section('js')
    @parent
    <script>

    $('#add-type').on('hidden.bs.modal', function(e) {
        $(this).find('#type_create')[0].reset();
    });

    $( "#type_create" ).on( "submit", function( event ) {
        event.preventDefault();
        swal({
            title: "Add Asset Type",
            text: "Confirm to add asset type?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                var form = document.getElementById('type_create');
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: '{{ route('types.store') }}',
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(msg) {
                        return msg;
                    }
                });
            }
        }).then((result) => {
            $('#add-type').modal('toggle');
            if(result.value){
                $('#type_table').DataTable().ajax.reload(function(json){
                    var len = json.data.length;
                    $("#types").empty();
                    $("#types").append("<option value=''>Please Select</option>");
                    for( var i = 0; i<len; i++){
                        var id = json.data[i]['id'];
                        var name = json.data[i]['title'];
                        $("#types").append("<option value='"+id+"'>"+name+"</option>");

                    }
                });

                //reload select designation


                swal('New Type of Inventory Added!', '--', 'success');
            }
        });
    });
    </script>
    @endsection
