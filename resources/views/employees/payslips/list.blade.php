@extends('layouts.default.master')

@section('content')

<div class="preloader">
   <svg class="circular" viewBox="25 25 50 50">
       <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
   </svg>
</div>

<div id="page-wrapper">
   <div class="container-fluid">
       <div class="row bg-title">
           <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
               <h4 class="page-title">Payslip</h4>
           </div>
           <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

               <ol class="breadcrumb">
                   <li><a href="dashboard-adminview.html">Dashboard</a></li>
                   <li><a href="admin-payslip.html">Payroll</a></li>
                   <li class="active">Generate Salary</li>
               </ol>
           </div>
           <!-- /.col-lg-12 -->
       </div>

        <div class="row">
           <div class="col-sm-3">
                <div class="white-box">

                    {{ $years }}

                    {{ $month }}

                </div>
            </div>

        </div>
        <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h3 class="box-title">Employee Payslip Summary</h3>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="generated">
                                <div class="table-responsive">
                                    <table id="generated_table" class="table display">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Employee ID</th>
                                                <th width="190px" class="text-center">Years</th>
                                                <th width="190px" class="text-center">Month</th>
                                                <th width="120px" class="text-right">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


   </div>
</div>
    @endsection

    @section('js')
    @parent
    <script>
            $(function () {
                $('#generated_table').DataTable({
                    serverSide: true,
                    processing: true,
                    ajax: '/empgenerates/{{ $years }}/{{ $month }}/list',
                    columns: [
                        {data: 'DT_Row_Index', orderable: false, searchable: false},
                        {data: 'employee_id'},
                        {data: 'year'},
                        {data: 'month'},
                        {data: 'action', orderable: false, searchable: false}
                    ]
                });
            });
</script>

    @endsection
