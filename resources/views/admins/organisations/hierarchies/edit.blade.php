<div id="modal_hierarchies_edit" class="modal fade in" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Add New Hierarchy</h1>
                {!! Form::open(['url' => 'hierarchies', 'id' => 'hierarchy_edit']) !!}
                    {{ Form::hidden('hierarchy_id', null , ['id' => 'hierarchy_id']) }}
                    <input type="hidden" name="_method" value="PUT">


                    <div class="form-group row">
                        <div class="col-md-9">
                        {{Form::text('hierarchy_name', null , ['required','class' => 'form-control','placeholder' => 'Hierarchy Name', 'id' => 'hierarchy_name'])}}
                    </div>
                    <div class="col-md-3">
                        {!! Form::select('hierarchy_ranking',
                        ['10' => '10', '9' => '9', '8' => '8', '7' => '7', '6' => '6', '5' => '5', '4' => '4', '3' => '3', '2' => '2', '1' => '1'], null,
                        ['class' => 'form-control', 'placeholder' => 'Select Rank', 'id' => 'hierarchy_ranking'] ); !!}
                    </div>
                </div>
                {{Form::submit('Save Hierarchy',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('js')
    @parent
    <script>

    $( "#hierarchy_edit" ).on( "submit", function( event ) {
        event.preventDefault();
        swal({
            title: "Update Hierarchy",
            text: "Are you sure you want to update this hierarchy?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                var form = document.getElementById('hierarchy_edit');
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: '/hierarchies/'+form.hierarchy_id.value,
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(msg) {
                        return msg;
                    }
                });
            }
        }).then((result) => {
            $('#modal_hierarchies_edit').modal('toggle');
            if(result.value){
                $('#hierarchy_table').DataTable().ajax.reload();
                swal('Hierarchy Updated!', '--', 'success');
            }
        });
    });
    </script>
@endsection
