{{-- Modal --}}
<div class="modal fade" id="modal_traveller_edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h1>Edit Traveller</h1>

                {!! Form::open(['url' => '', 'id' => 'traveller_edit']) !!}
                <input type="hidden" name="_method" value="PUT">
                <input id="traveller_id" name="id" type="hidden">
                <div class="form-group row">
                    <div class="col-md-6">
                        {{ Form::text('name',null,['class' => 'form-control', 'placeholder' => 'Traveller Name'])}}
                    </div>
                    <div class="col-md-6">
                        {{ Form::text('phone',null,['class' => 'form-control', 'placeholder' => 'Phone'])}}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        {{ Form::text('email',null,['class' => 'form-control', 'placeholder' => 'Email'])}}
                    </div>
                    <div class="col-md-6">
                        {{ Form::text('mykad',null,['class' => 'form-control', 'placeholder' => 'Mykad'])}}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        {{ Form::text('passport',null,['class' => 'form-control', 'placeholder' => 'Passport'])}}
                    </div>
                    <div class="col-md-6">
                        {{ Form::date('passport_expired',null,['class' => 'form-control', 'placeholder' => 'Passport
                        Expiration Date'])}}
                    </div>
                </div>
                <button type="submit" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">
                    Save Traveller
                </button>
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close
                </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('js')
@parent
<script>
    $("#traveller_edit").on("submit", function (event) {
        //console.log('mew');
        event.preventDefault();
        swal({
            title: "Update",
            text: "Are you sure you want to update?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    var form = document.getElementById('traveller_edit');
                    var formData = new FormData(form);
                    //console.log(form.traveller_id.value);
                    $.ajax({
                        type: "POST",
                        url: "/travellers/" + form.traveller_id.value,
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            resolve(data);
                        },
                        error: function () {
                            swal.showValidationMessage('server error');
                            swal.hideLoading();
                        }
                    });
                });
            }
        }).then((result) => {
            if (result.value) {
                $('#modal_traveller_edit').modal('toggle');
                $('#traveller_table').DataTable().ajax.reload();
                swal('Updated!', '--', 'success');

            }

        });
    });
</script>
@endsection
