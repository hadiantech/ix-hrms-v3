<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notice;
use App\Employee;
use DataTables;
use Auth;
use App\Role;

class NoticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notices = Notice::all();
        $roles = Role::all();
        $employee = Employee::all();
        return view('notices.index', compact('notices','roles','employee'));
    }

    public function list(){
        return Datatables::eloquent(Notice::with('employee'))
        ->addColumn('createdby', function($model){
            return $model->employee->fname." ".$model->employee->lname;
        })
        ->addColumn('action', function($model){
            $url = 'notices';
            $delete_datatable_id_name_refresh = 'notice_table';
            $modal_data_customs = array('employee' => $model->employee);
            return view('shared._table_action', compact('model','employee', 'url','modal_data_customs', 'modal_view_id', 'modal_edit_id', 'delete_datatable_id_name_refresh'))->render();
        })
        ->escapeColumns([])
        ->addIndexColumn()
        ->make(true);
    }

    public function emplist(){
        return Datatables::eloquent(Notice::with('employee'))
        ->addColumn('createdby', function($model){
            return $model->employee->fname." ".$model->employee->lname;
        })
        ->addColumn('action', function($model){
            $url = 'notices';
            $delete_datatable_id_name_refresh = 'notice_table';
            $modal_data_customs = array('employee' => $model->employee);
            return view('shared._table_action', compact('model','employee', 'url','modal_data_customs', 'modal_view_id', 'modal_edit_id', 'delete_datatable_id_name_refresh'))->render();
        })
        ->escapeColumns([])
        ->addIndexColumn()
        ->make(true);
    }

    public function list_based_employee($id){
        return Notice::where('id', $id)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employee = Employee::all();
        return view('notices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $notices = new Notice;
        $notices->createdby = Auth::user()->id;
        $notices->title = $request->title;
        $notices->status = $request->status;
        $notices->description = $request->editor;
        $notices->date = $request->date;
        $notices->save();
        return response()->json($notices, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notices = Notice::find($id);
        $employee = Employee::all();
        $description = Notice::where('description',$id)->get();
        return view('notices.show',compact('notices','description','employee'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notices = Notice::find($id);
        $employee = Employee::all();
        $description = Notice::where('description',$id)->get();
        return view('notices.edit',compact('notices','description','employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $notice = Notice::find($id);
        // $notice->update($request->all());

        $notice->title = $request->title;
        $notice->description = $request->editor;
        $notice->save();
        return redirect('notices');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notice $notice)
    {
        $notice->delete();
        return redirect()->back();
    }
}
