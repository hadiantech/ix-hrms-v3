<?php

use Illuminate\Database\Seeder;
use App\ExpensesType;

class ExpensesTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('name'=>'Normal'),
            array('name'=>'Advance')
        );

        ClaimType::insert($data);
    }
}
