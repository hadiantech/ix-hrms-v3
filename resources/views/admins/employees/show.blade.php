@extends('layouts.default.master')

@section('content')

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<!-- ============================================================== -->
<!-- Page Content-->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add New Employee</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                <ol class="breadcrumb">
                    <li>
                        <a href="dashboard-adminview.html">Dashboard</a>
                    </li>
                    <li>
                        <a href="admin-employee-dashboard.html">Employee Dashboard</a>
                    </li>
                    <li class="active">Edit Employee</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <!--employee -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box p-0">
                    <div class="sttabs tabs-style-iconbox">

                            <nav>
                                <ul>
                                    <li>
                                        <a href="#personalinfo" class="sticon ti-user">
                                            <span>Personal Details</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#accountinfo" class="sticon ti-key">
                                            <span>Account Details</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#employementinfo" class="sticon ti-list">
                                            <span>Employment Details</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#medicalinfo" class="sticon ti-info-alt">
                                            <span>Medical Info</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#leaveallocation" class="sticon ti-calendar">
                                            <span>Leave Allocation</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#assetallocation" class="sticon ti-calendar">
                                            <span>Asset Allocation</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#approver" class="sticon ti-layers-alt">
                                            <span>Approval Layer</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#supportingdoc" class="sticon ti-files">
                                            <span>Supporting Documents</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                            <div class="content-wrap p-t-20">
                                <section id="personalinfo">
                                    @include('admins.employees.pages.personal')
                                </section>
                                <section id="accountinfo">
                                    @include('admins.employees.pages.account')
                                </section>
                                <section id="employmentinfo">
                                    @include('admins.employees.pages.employee_info')
                                </section>
                                <section id="medicalinfo">
                                    @include('admins.employees.pages.medical')
                                </section>
                                <section id="leaveallocation">
                                    @include('admins.employees.pages.leave')
                                </section>
                                <section id="assetallocation">
                                    @include('admins.employees.pages.asset')
                                    </section>
                                <section id="approver">
                                    @include('admins.employees.pages.approver')
                                </section>
                                <section id="supportingdoc">
                                    @include('admins.employees.pages.document')
                                </section>

                            </div>
                            <!-- /content -->
                    </div>

                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
</div>
@endsection


@section('js')
    @parent
    <script>
        function bootstrap_modal_form(modal_id, form_id ,data){
            $('#'+modal_id).on('shown.bs.modal', function(e){
                fillFormJson(form_id, data);
                $(this).off('shown.bs.modal');
            });

            $('#'+modal_id).on('hide.bs.modal', function(e){
                $(this).find('#'+form_id)[0].reset();
                $(this).off('hide.bs.modal');
            });
        }

        $(document).off('click', '.open-modal').on("click", ".open-modal", function () {

            var data = $(this).data('modal_data');
            bootstrap_modal_form('modal_contacts_edit', 'contact_edit', data);
            bootstrap_modal_form('modal_history_edit', 'history_edit', data);
            bootstrap_modal_form('modal_education_edit', 'education_edit', data);
            bootstrap_modal_form('modal_cert_edit', 'cert_edit', data);
            bootstrap_modal_form('modal_medical_edit', 'medical_edit', data);
        });

    </script>

    @include('shared._fill_form')
@endsection
