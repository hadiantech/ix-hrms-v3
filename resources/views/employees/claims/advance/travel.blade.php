<div class="extra-form" style="display: none">
    <div class="form-group row m-b-30">
        <div class="col-md-6">
            {{ Form::label('','Travel Date From') }}
            {{ Form::date('travel_date_from',null,['class' => 'form-control', 'placeholder' => 'Travel Date From'])}}

        </div>
        <div class="col-md-6">
            {{ Form::label('','Travel Date To') }}
            {{ Form::date('travel_date_to',null,['class' => 'form-control', 'placeholder' => 'Travel Date To'])}}
        </div>
    </div>
    {{-- Travellers --}}
    <div class="form-group">
        <h3 class="box-title">Travellers Information</h3>
        <div class="table-responsive">
            <table id="traveller_table" class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th width="40%">Travellers Details</th>
                        <th class="text-center">MyKad No.</th>
                        <th class="text-center">Passport No.</th>
                        <th class="text-center">Passport Expiration date</th>
                        <th class="text-right">Action</th>
                    </tr>
                </thead>

            </table>
            @if($claim->claim_status == 'draft' or $claim->claim_proc_status == 'return')
            <a href="#modal_traveller_create" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">
                Add Traveller
            </a>
            @endif
        </div>
    </div>
    {{-- Visas --}}
    <div class="form-group">
        <h3 class="box-title">Visa Information</h3>
        <div class="table-responsive">
            <table id="visa_table" class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th width="30%">Visa Owner</th>
                        <th class="text-center">Country Origin</th>
                        <th class="text-center">Visa Type</th>
                        <th class="text-center">Entry</th>
                        <th class="text-center">Nationality</th>
                        <th class="text-right">Action</th>
                    </tr>
                </thead>
            </table>
            @if($claim->claim_status == 'draft' or $claim->claim_proc_status == 'return')
            <a href="#modal_visa_create" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">
                Add Visa
            </a>
            @endif
        </div>
    </div>
    {{-- Flights --}}
    <div class="form-group">
        <h3 class="box-title">Flight Information</h3>
        <div class="table-responsive">
            <table id="flight_table" class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Flight type</th>
                        <th class="text-center">Travel date</th>
                        <th class="text-center">Depart from</th>
                        <th class="text-center">Arrive to</th>
                        <th width="30%" class="text-center">Flight Detail</th>
                        <th class="text-right">Action</th>
                    </tr>
                </thead>
            </table>
            @if($claim->claim_status == 'draft' or $claim->claim_proc_status == 'return')
            <a href="#modal_flight_create" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">
                Add Flight
            </a>
            @endif
        </div>
    </div>
    {{-- Hotels --}}
    <div class="form-group">
        <h3 class="box-title">Hotel Information</h3>
        <div class="table-responsive">
            <table id="hotel_table" class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Hotel Name</th>
                        <th class="text-center">Location</th>
                        <th class="text-center">No. of room</th>
                        <th class="text-center">Check In</th>
                        <th width="30%" class="text-center">Check Out</th>
                        <th class="text-right">Action</th>
                    </tr>
                </thead>
            </table>
            @if($claim->claim_status == 'draft' or $claim->claim_proc_status == 'return')
            <a href="#modal_hotel_create" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">
                Add Hotel
            </a>
            @endif
        </div>
    </div>


</div>

@section('js')
@parent
<script>
    $(function () {
        $('#traveller_table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/travellers/list/{{ $claim->id }}',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false, class: 'text-left'},
                {data: 'details'},
                {data: 'mykad'},
                {data: 'passport'},
                {data: 'passport_expired'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
<script>
    $(function () {
        $('#visa_table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/visas/list/{{ $claim->id }}',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false, class: 'text-left'},
                {data: 'visa_owner'},
                {data: 'country'},
                {data: 'type'},
                {data: 'entry_date'},
                {data: 'nationality'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
<script>
    $(function () {
        $('#flight_table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/flights/list/{{ $claim->id }}',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false, class: 'text-left'},
                {data: 'flight_type'},
                {data: 'travel_date'},
                {data: 'depart'},
                {data: 'arrive'},
                {data: 'detail'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
<script>
    $(function () {
        $('#hotel_table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/hotels/list/{{ $claim->id }}',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false, class: 'text-left'},
                {data: 'name'},
                {data: 'location'},
                {data: 'room'},
                {data: 'check_in'},
                {data: 'check_out'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>
@endsection
