@extends('layouts.default.master')
@extends('layouts.default.calendar')

@section('content')
<div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Calendar & Events</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    <ol class="breadcrumb">
                        <li><a href="dashboard-adminview.html">Dashboard</a></li>
                        <li class="active">Calendar & Events</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!-- ============================================================== -->
            <!-- Different data widgets -->
            <!-- ============================================================== -->


        {{-- <div class="container"> --}}
    <div class="row">
        <div class="col-md-4"><div class="panel panel-primary">

<div class="panel-heading">IX HRMS Calendar & Event</div>

 <div class="panel-body">

      {!! Form::open(array('route' => 'events.add','method'=>'POST','files'=>'true')) !!}
       <div class="row">
          <div class="col-xs-12  ">
             @if (Session::has('success'))
                <div class="alert alert-success">{{ Session::get('success') }}</div>
             @elseif (Session::has('warnning'))
                 <div class="alert alert-danger">{{ Session::get('warnning') }}</div>
             @endif

         </div>

         <div class="col-xs-12">
           <div class="form-group">
               {!! Form::label('event_name','Event Name:') !!}
               <div class="">
               {!! Form::text('event_name', null, ['class' => 'form-control']) !!}
               {!! $errors->first('event_name', '<p class="alert alert-danger">:message</p>') !!}
               </div>
           </div>
         </div>

         <div class="col-xs-12">
           <div class="form-group">
             {!! Form::label('start_date','Start Date:') !!}
             <div class="">
             {!! Form::date('start_date', null, ['class' => 'form-control']) !!}
             {!! $errors->first('start_date', '<p class="alert alert-danger">:message</p>') !!}
             </div>
           </div>
         </div>

         <div class="col-xs-12">
           <div class="form-group">
             {!! Form::label('end_date','End Date:') !!}
             <div class="">
             {!! Form::date('end_date', null, ['class' => 'form-control']) !!}
             {!! $errors->first('end_date', '<p class="alert alert-danger">:message</p>') !!}
             </div>
           </div>
         </div>

         <div class="col-xs-12 text-left"> &nbsp;<br/>
         {!! Form::submit('Add Event',['class'=>'btn btn-primary']) !!}
         </div>
       </div>
      {!! Form::close() !!}

</div></div>

    </div>
    <div class="col-md-8">
            <div class="panel panel-primary">
              <div class="panel-heading">IX Event Details</div>
              <div class="panel-body" >
                  {!! $calendar_details->calendar() !!}
              </div>
            </div>
        </div>


            </div>



            </div>
@endsection
