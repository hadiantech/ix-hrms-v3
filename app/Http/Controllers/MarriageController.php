<?php

namespace App\Http\Controllers;

use App\Emergency;
use App\Employee;
use App\EmployeeLieu;
use App\Http\Controllers\Controller;
use App\EmployeeLeave;
use Illuminate\Support\Facades\Input;
use App\LeaveType;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Event;
use DataTables;
use JavaScript;
use App\Approval;
use Illuminate\Notifications\Notification;
use App\Notifications\LeaveRequest;
use Mail;
use Auth;
use Exception;
use Postmark\PostmarkClient;

class MarriageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $employee = Employee::lists(['id']);
        $leavetype = LeaveType::lists(['id']);
        return view('employees.leaves.marriages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $approval_lv1 = Approval::where('employee_id', Auth::user()->id)->where('level',1)->first();
        $approval_lv2 = Approval::where('employee_id', Auth::user()->id)->where('level',2)->first();

        $client = new PostmarkClient("73a69c7d-8c56-4f59-877c-74d05cd5e247");

        $leaves = EmployeeLeave::where('employee_id', Auth::user()->id)->get();
        $approval = Approval::where('employee_id', Auth::user()->id)->first();
        //return $approval->approvedBy;

        $aid = Auth::user()->id;
        $employees = Employee::find($aid);

        $marriage_leave_quota = Auth::user()->leave_allocs()->type('Marriage Leave')->first()->quota;
        $marriage_leave_taken = Auth::user()->leaves()->type('Marriage Leave')->sum('total_days');
        $balance = $marriage_leave_quota - $marriage_leave_taken;

        $a = Carbon::parse($request->started_at);
        $b = Carbon::parse($request->ended_at);
        $days = $b->diffInDays($a);
        $days = $days + 1;

        $unpaid = $balance - $days;

        $marriage = new EmployeeLeave;
        $marriage->employee_id = Auth::user()->id;
        $marriage->leave_type = 'Marriage Leave';
        $marriage->started_at = $request->started_at;
        $marriage->ended_at = $request->ended_at;
        $marriage->reason = $request->reason;
        $marriage->total_days = $days;
        $marriage->as_unpaid = $unpaid;

        $marriage->status = 'Pending';
        $marriage->leave_proc_status = 'unproc';

        $marriage->save();

        $leave = $marriage->id;

        $sendResult = $client->sendEmailWithTemplate(

            "hassanul@boneybone.com",
            $approval_lv1->approvedBy->email,
            10030123,
                [
                    "system_name" => "[IX HRMS] Leave Application Approval",
                    "fname" => $employees->fname,
                    "lname" => $employees->lname,
                    "uid" => $employees->employee_uid,
                    "approve" =>  $approval_lv1->approvedBy->fname,
                    "approver" =>  $approval_lv1->approvedBy->lname,
                    "leave_type" => 'Emergency Leave',
                    "started_at" => $request->started_at,
                    "ended_at" => $request->ended_at,
                    "reason" => $request->reason,
                    "accept" => route('adminleaves.update_email',$leave),
                    "reject" => route('adminleaves.reject_email',$leave),
                    "link" => url('http://hrms.ixtelecom.net')
                ]);

            $sendResult = $client->sendEmailWithTemplate(

                    "hassanul@boneybone.com",
                    $approval_lv2->approvedBy->email,
                    10030123,
                        [
                            "system_name" => "[IX HRMS] Leave Application Approval",
                            "fname" => $employees->fname,
                            "lname" => $employees->lname,
                            "uid" => $employees->employee_uid,
                            "approve" =>  $approval_lv2->approvedBy->fname,
                            "approver" =>  $approval_lv2->approvedBy->lname,
                            "leave_type" => 'Emergency Leave',
                            "started_at" => $request->started_at,
                            "ended_at" => $request->ended_at,
                            "reason" => $request->reason,
                            "accept" => route('adminleaves.approve_email',$leave),
                            "reject" => route('adminleaves.reject_email',$leave),
                            "link" => url('http://hrms.ixtelecom.net')
                        ]);
        return response()->json($marriage, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Emergency  $emergency
     * @return \Illuminate\Http\Response
     */
    public function show(MarriageLeave $marriage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Emergency  $emergency
     * @return \Illuminate\Http\Response
     */
    public function edit(MarriageLeave $marriage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Emergency  $emergency
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MarriageLeave $marriage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Emergency  $emergency
     * @return \Illuminate\Http\Response
     */
    public function destroy(MarriageLeave $marriage)
    {
        //
    }
}
