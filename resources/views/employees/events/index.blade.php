@extends('layouts.default.master')
@extends('layouts.default.calendar')

@section('content')
<div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Calendar & Events</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    <ol class="breadcrumb">
                        <li><a href="dashboard-adminview.html">Dashboard</a></li>
                        <li class="active">Calendar & Events</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <!-- ============================================================== -->
            <!-- Different data widgets -->
            <!-- ============================================================== -->

        {{-- <div class="container"> --}}
    <div class="row">

    <div class="col-md-12">
            <div class="panel panel-primary">
              <div class="panel-heading">IX Event Details</div>
              <div class="panel-body" >
                  {!! $calendar_details->calendar() !!}
              </div>
            </div>
    </div>
    </div>

            </div>
@endsection
