<?php

namespace App\Http\Controllers;

use App\Operand;
use Illuminate\Http\Request;

class OperandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Operand  $operand
     * @return \Illuminate\Http\Response
     */
    public function show(Operand $operand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Operand  $operand
     * @return \Illuminate\Http\Response
     */
    public function edit(Operand $operand)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Operand  $operand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Operand $operand)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Operand  $operand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Operand $operand)
    {
        //
    }
}
