@extends('layouts.default.master')

@section('content')

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Claim Dashboard</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li class="active">Claim Dasboard</li>
                </ol>
            </div>
        </div>
        <div class="row hide">
            <div class="col-md-4 col-sm-12">
                <div class="white-box" style="background: #ff7676">
                    <ul class="col-in">
                        <li>
                            <span class="circle circle-md bg-white"><i style="color:#ff7676" class="ti-timer"></i></span>
                        </li>
                        <li class="col-last">
                            <h3 class="counter text-right m-t-15 white">23</h3>
                        </li>
                        <li class="col-middle" style="width:50%">
                            <h4 class="white">Days Left to Submit May 2018 Claim</h4>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-4 col-sm-12">
                <div class="white-box" style="background: #6d4d8d">
                    <ul class="col-in">
                        <li>
                            <span class="circle circle-md bg-white"><i style="color:#6d4d8d" class="ti-announcement"></i></span>
                        </li>
                        <li class="col-last">
                            <h3 class="counter text-right m-t-15 white">3</h3>
                        </li>
                        <li class="col-middle" style="width:50%">
                            <h4 class="white">Pending Advanced Claim To Be Submitted.</h4>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="white-box p-b-3">
                    <h3 class="box-title text-uppercase">Claim Summary
                        <a href="{{route('claims.monthly.create')}}" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">
                            Add Monthly Claim
                        </a>
                    </h3>
                    <div class="table-responsive">
                        <table id="claim_table" class="table display">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Claim Desc</th>
                                    <th class="text-center">Applied Date</th>
                                    <th class="text-center">Amount</th>
                                    <th class="text-center">Processing Status</th>
                                    <th class="text-right">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box p-b-3">
                    <h3 class="box-title text-uppercase">Advanced Request Summary
                        <a href="{{route('claims.advance.create')}}" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">
                            Add advanced request
                        </a>
                    </h3>
                    <div class="table-responsive">
                        <table id="claim_advance_table" class="table display">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Project Name</th>
                                    <th class="text-center">Claim Desc</th>
                                    <th class="text-center">Applied Date</th>
                                    <th class="text-center">Amount</th>
                                    <th class="text-center">Processing Status</th>
                                    <th class="text-right">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('js')
@parent

<script>
    $(function () {
        $('#claim_table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/claims/list',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false, class: 'text-left'},
                {data: 'desc'},
                {data: 'claim_applied_date', class: 'text-center'},
                {data: 'claim_total', class: 'text-center'},
                {data: 'status', name: 'claim_status', class: 'text-center', class: 'text-center'},
                {data: 'action', orderable: false, searchable: false, class: 'text-center'}
            ]
        });
    });
</script>

<script>
    $(function () {
        $('#claim_advance_table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/claims/list_request',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false, class: 'text-left'},
                {data: 'project_id'},
                {data: 'desc'},
                {data: 'claim_applied_date', class: 'text-center'},
                {data: 'claim_total_advance', class: 'text-center'},
                {data: 'status', name: 'claim_status', class: 'text-center', class: 'text-center'},
                {data: 'action', orderable: false, searchable: false, class: 'text-center'}
            ]
        });
    });
</script>

@endsection
