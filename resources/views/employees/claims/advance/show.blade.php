@extends('layouts.default.master')
@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Advance Request</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{ route('dashboard') }}">Dashboard</a></li>
                    <li><a href="{{ route('claims.index') }}">Claim Dashboard</a></li>
                    <li class="active">Advance Request</li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title text-uppercase">Advance Request</h3>

                    @if($claim->claim_proc_status == 'return')
                    <p style="text-align: right;margin-top: -30px;margin-bottom: 30px;"><span class="label label-danger">Returned</span></p>
                    @else
                        @if($claim->claim_status == 'draft')
                        <p  style="text-align: right;margin-top: -30px;margin-bottom: 30px;"><span class="label label-warning">{{ $claim->claim_status }}</span></p>
                        @elseif($claim->claim_status == 'submit')
                        <p  style="text-align: right;margin-top: -30px;margin-bottom: 30px;"><span class="label label-success">Submitted</span></p>
                        @endif
                    @endif

                    @if($claim->claim_remark)
                    <div class="alert alert-info">
                        REMARK: {{ $claim->claim_remark }} <br/>
                    </div>
                    @endif

                    {!! Form::model($claim, ['url' => '','id'=>'update_advance']) !!}
                    <input type="hidden" name="_method" value="PUT">
                    <input id="claim_id" name="claim_id" type="hidden" value="{{ $claim->id }}" />
                    <div class="form-group row m-t-30">
                        <div class="col-md-4">
                            <select name="claim_type_advance" id="advance-req-type" class="selectpicker" data-style="form-control">
                                <option @if($claim->claim_type_advance == 'cash') selected @endif value="cash">Cash Advance</option>
                                <option @if($claim->claim_type_advance == 'travel') selected @endif value="travel">Travel Advance</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            {{ Form::text('project_id', null, ['class' => 'form-control','placeholder'=> 'Project Code']) }}
                        </div>
                        <div class="col-md-4">
                            {{ Form::text('claim_total_advance', null, ['class' => 'form-control','placeholder'=> 'Amount']) }}
                        </div>
                    </div>
                    <div class="form-group row m-t-30">
                        <div class="col-md-6">
                            <select name="organisation_id" class="selectpicker" data-style="form-control">
                                <option selected disabled>Select Company</option>
                                @foreach ($organisation as $o )
                                <option @if($o->id == $claim->organisation_id) selected @endif value="{{ $o->id }}">{{ $o->company_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    <div class="col-md-6">
                            {{ Form::text('claim_location', null, ['class' => 'form-control','placeholder'=> 'Location/Destination']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::textarea('claim_purpose', null, ['class' => 'form-control','placeholder'=> 'Purpose...']) }}
                    </div>
                    @include('employees.claims.advance.travel')

                    {{ Form::textarea('claim_remark', null, ['class' => 'form-control hide','placeholder'=> 'Remark...']) }}

                    @if($claim->claim_status == 'draft' and $claim->claim_proc_status == 'approved_advance')
                    @else
                        @if($claim->claim_status == 'draft' or $claim->claim_proc_status == 'return')
                            @if(Auth::user()->id == $claim->emp_id)
                            <button type="submit" onclick="status('draft')" style="width:200px" class="m-r-10 btn-lg btn-rounded btn-info text-uppercase">Save</button>
                            <button type="submit" onclick="status('submit')" style="width:200px" class=" btn-lg btn-rounded btn-success text-uppercase">Confirm & Submit</button>
                            @endif
                        @endif
                    @endif



                    @isset($approver)
                    @if($approver == 'yes')
                        <br/>
                        <br/>
                        <div class="form-group">
                            {{ Form::label('','Approver Remark') }}
                            {{ Form::textarea('claim_remark', null, ['class' => 'form-control','placeholder'=> 'Remark...']) }}
                        </div>

                        <button type="submit" onclick="proc_status('return')" style="width:200px" class="m-r-10 btn-lg btn-rounded btn-warning text-uppercase">Return</button>
                        <button type="submit" onclick="proc_status('proc')" style="width:200px" class="m-r-10 btn-lg btn-rounded btn-success text-uppercase">Reviewed</button>

                        @if($approver_level->level == '2')
                        <button type="submit" onclick="proc_status('approved_advance')" style="width:200px" class="m-r-10 btn-lg btn-rounded btn-success text-uppercase">Approve</button>
                        @endif
                    @endif
                    @endisset

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@include('employees.claims.advance.modal.create_traveller')
@include('employees.claims.advance.modal.edit_traveller')
@include('employees.claims.advance.modal.create_visa')
@include('employees.claims.advance.modal.edit_visa')
@include('employees.claims.advance.modal.create_flight')
@include('employees.claims.advance.modal.edit_flight')
@include('employees.claims.advance.modal.create_hotel')
@include('employees.claims.advance.modal.edit_hotel')
<footer class="footer text-center"> 2018 &copy; IX HRMS. All Rights Reserved. </footer>

@endsection
@section('js')
@parent
<script>
    var sel = $( "#advance-req-type option:selected" ).attr("value");
    if(sel == "cash"){
        $('.extra-form').hide();
        //console.log(sel);
    }
    else{
        $('.extra-form').show();
        //console.log(sel);
    }

    document.getElementById('advance-req-type').addEventListener('change', function (e) {
        if (e.target.value === "cash") {
            $('.extra-form').hide();
        }
        else if (e.target.value === "travel") {
            $('.extra-form').show();
        }
        else{
            $('.extra-form').hide();
        }
    });
</script>

<script>

    var claim_status;
    var approve_type;


    function status(e){
        approve_type = 'unproc';
        claim_status = e;


    }

    function proc_status(el){
        if(el == 'return'){
            claim_status = 'submit';
            approve_type = el;
        }
        else if(el == 'approved'){
            approve_type = el;
            claim_status = 'submit';
        }
        else if(el == 'approved_advance'){
             approve_type = 'approved_advance';
             claim_status = 'draft';
        }
        else{
            if(el == null){
                approve_type = 'unproc';
            }
            else{
                approve_type = el;
                claim_status = 'submit';
            }

        }

    }

    $( "#update_advance" ).on( "submit", function( event ) {
        event.preventDefault();
        swal({
            title: "Confirm submission",
            text: "Are you sure you want to submit?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                return new Promise(function(resolve, reject){
                    var form = document.getElementById('update_advance');
                    var formData = new FormData(form);
                    //console.log();
                    formData.append('claim_status', claim_status);
                    formData.append('claim_proc_status', approve_type);
                    //console.log(form.project_id.value);
                    $.ajax({
                        type: "POST",
                        url: "/claimadvances/update/"+form.claim_id.value,
                        data: formData,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            resolve(data);
                        },
                        error: function(){
                            swal.showValidationMessage('server error');
                            swal.hideLoading();
                        }
                    });
                });
            }
        }).then((result) => {
            if(result.value){
                //swal('Update Success!', '--', 'success');
                location.reload(true);
            }
        });
    });
</script>
<script>
    $(document).off('click', '.open-modal').on("click", ".open-modal", function () {

        var data = $(this).data('modal_data');
        console.log('run');

        // Traveller
        $('#modal_traveller_create').on('hide.bs.modal', function(e){
            $('#traveller_create').trigger("reset");
            $(this).off('hide.bs.modal');
        });

        $('#modal_traveller_edit').on('shown.bs.modal', function (e) {
            fillFormJson('traveller_edit', data);
            $(this).off('shown.bs.modal');
        });

        $('#modal_traveller_edit').on('hide.bs.modal', function (e) {
            $('#traveller_edit').trigger("reset");
            $(this).off('hide.bs.modal');
        });


        // Visa
        $('#modal_visa_create').on('hide.bs.modal', function(e){
            $('#traveller_create').trigger("reset");
            $(this).off('hide.bs.modal');
        });

        $('#modal_visa_edit').on('shown.bs.modal', function (e) {
            fillFormJson('visa_edit', data);
            $(this).off('shown.bs.modal');
        });

        $('#modal_visa_edit').on('hide.bs.modal', function (e) {
            $('#visa_edit').trigger("reset");
            $(this).off('hide.bs.modal');
        });

        // Flights
        $('#modal_flight_create').on('hide.bs.modal', function(e){
            $('#traveller_create').trigger("reset");
            $(this).off('hide.bs.modal');
        });

        $('#modal_flight_edit').on('shown.bs.modal', function (e) {
            fillFormJson('flight_edit', data);
            $(this).off('shown.bs.modal');
        });

        $('#modal_flight_edit').on('hide.bs.modal', function (e) {
            $('#flight_edit').trigger("reset");
            $(this).off('hide.bs.modal');
        });

         // Hotels
         $('#modal_hotel_create').on('hide.bs.modal', function(e){
            $('#hotel_create').trigger("reset");
            $(this).off('hide.bs.modal');
        });

        $('#modal_hotel_edit').on('shown.bs.modal', function (e) {
            fillFormJson('hotel_edit', data);
            $(this).off('shown.bs.modal');
        });

        $('#modal_hotel_edit').on('hide.bs.modal', function (e) {
            $('#hotel_edit').trigger("reset");
            $(this).off('hide.bs.modal');
        });
    });
</script>

@include('shared._fill_form')
@endsection
