<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivateToken extends Model
{
    //
    public $table = "activate_tokens";

    public function employee(){
        return $this->hasOne(Employee::class,'user_id');
    }
}
