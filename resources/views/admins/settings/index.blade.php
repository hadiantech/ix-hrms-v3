@extends('layouts.default.master')

@section('content')

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Settings</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                <ol class="breadcrumb">
                    <li><a href="dashboard-adminview.html">Dashboard</a></li>
                    <li class="active">Settings</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->



        <!--employee -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box p-0">
                <div class="sttabs tabs-style-iconbox">
                    <nav>
                        <ul>
                            <li><a href="#leave-setting" class="sticon ti-settings"><span>Leave Settings</span></a></li>
                            <li><a href="#employee-setting" class="sticon ti-settings"><span>Employee Settings</span></a></li>
                            <li><a href="#global-setting" class="sticon ti-settings"><span>System Settings</span></a></li>
                            </li>

                        </ul>
                    </nav>
                    <div class="content-wrap p-t-20">
                        @include('admins.settings.pages.leave')
                        @include('admins.settings.pages.employee')
                        @include('admins.settings.pages.system')
                    </div>
                    <!-- /content -->

                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</div>

@endsection
