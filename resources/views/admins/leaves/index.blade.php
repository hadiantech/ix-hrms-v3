@extends('layouts.default.master')

@section('content')
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Admin Leave Dashboard</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="dashboard-adminview.html">Dashboard</a></li>
                    <li class="active">Leave Dashboard</li>
                </ol>
            </div>
        </div>

        <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h3 class="box-title">Employee Lieus Summary</h3>
                        <!-- Nav tabs -->
                        <ul class="nav customtab nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#lieu" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">
                                    <span class="visible-xs"><i class="ti-home"></i></span>
                                    <span class="hidden-xs"> Unprocessed </span>
                                </a>
                            </li>
                            <li role="presentation" class=""><a href="#lieu2" aria-controls="profile" role="tab"
                                    data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span>
                                    <span class="hidden-xs">Processed</span></a></li>

                            <li role="presentation" class=""><a href="#lieu3" aria-controls="profile" role="tab"
                                    data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span>
                                    <span class="hidden-xs">Finished</span></a>
                            </li>

                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="lieu">
                                <div class="table-responsive">
                                    <table id="lieu_unproc" class="table display">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Employee</th>
                                                <th>Reason</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-right">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="lieu2">
                                <div class="table-responsive">
                                    <table id="lieu_proc" class="table display">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Employee</th>
                                                <th>Reason</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-right">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="lieu3">
                                <div class="table-responsive">
                                    <table id="lieu_approved" class="table display">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Employee</th>
                                                <th>Reason</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-right">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {{-- <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title text-uppercase">Leave in Lieu</h3>
                    <div class="table-responsive">
                        <table id="adminlieu_table" class="">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Employee</th>
                                    <th>Event Name</th>
                                    <th>Leave From</th>
                                    <th>Leave To</th>
                                    <th>Total Days</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div> --}}

        <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h3 class="box-title">Employee Leave Summary</h3>
                        <!-- Nav tabs -->
                        <ul class="nav customtab nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#unprocessed" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="false">
                                    <span class="visible-xs"><i class="ti-home"></i></span>
                                    <span class="hidden-xs"> Unprocessed </span>
                                </a>
                            </li>
                            <li role="presentation" class=""><a href="#processed" aria-controls="profile" role="tab"
                                    data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span>
                                    <span class="hidden-xs">Processed</span></a></li>

                            <li role="presentation" class=""><a href="#finished" aria-controls="profile" role="tab"
                                    data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="ti-user"></i></span>
                                    <span class="hidden-xs">Finished</span></a>
                            </li>

                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="unprocessed">
                                <div class="table-responsive">
                                    <table id="leave_unproc" class="table display">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Employee</th>
                                                <th>Reason</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                                <th>Created at</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-right">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="processed">
                                <div class="table-responsive">
                                    <table id="leave_proc" class="table display">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Employee</th>
                                                <th>Reason</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                                <th>Created at</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-right">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="finished">
                                <div class="table-responsive">
                                    <table id="leave_approved" class="table display">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Employee</th>
                                                <th>Reason</th>
                                                <th>Start Date</th>
                                                <th>End Date</th>
                                                <th>Created at</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-right">Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        {{-- <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title text-uppercase">Leave Status</h3>
                    <div class="table-responsive">
                        <table id="leave_status_table" class="">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Employee</th>
                                    <th>Reason</th>
                                    <th>Leave From</th>
                                    <th>Leave To</th>
                                    <th>Status</th>
                                    <th style="width: 20%">Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> --}}

{{-- <div class="modal fade" id="lieus_modal_view" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Leave in Lieu</h1>
                <hr />
                <p>
                    Request by: <b><span id="employee_name">Nazar</span></b>
                    <br />
                    Event Name: <b><span id="event_name">HRMS</span></b>
                    <br />
                    Total Days <b><span id="total_days">HRMS</span></b>
                    <br />
                    Date : <b><span id="started_at">2019/01/01</span> - <span id="ended_at">2019/01/01</span></b>
                    <br />
                    Expired At: <b><span id="expired_at">2019/01/01</span></b>
                    <br />
                    Remark:
                    <br />
                    <span id="remark">lorem ipsum</span>
                    <br />
                    <hr />
                    <a class="btn btn-success" onclick="change_status('Accept')">Accept</a> <a class="btn btn-danger" onclick="change_status('Reject')">Reject</a>
                </p>

            </div>
        </div>
    </div>
</div> --}}
{{-- @include('admins.leaves.lieus.show') --}}
{{-- @include('admins.leaves.show') --}}

@endsection

@section('js')
    @parent
    <script>
            $(function () {
                $('#leave_unproc').DataTable({
                    serverSide: true,
                    processing: true,
                    order: [[6,'desc']],
                    ajax: '/adminleaves/list/unproc',
                    columns: [{
                            data: 'DT_Row_Index',
                            orderable: false,
                            searchable: false,
                            class: 'text-left'
                        },
                        {data: 'employee'},
                        {data: 'reason'},
                        {data: 'started_at'},
                        {data: 'ended_at'},
                        {
                            data: 'status',
                            name: 'status',
                            class: 'text-center'
                        },
                        {data: 'created_at'},
                        {
                            data: 'action',
                            orderable: false,
                            searchable: false,
                            class: 'text-right'
                        }
                    ]
                });
            });
        </script>
        <script>
                $(function () {
                    $('#leave_proc').DataTable({
                        serverSide: true,
                        processing: true,
                        order: [[5,'desc']],
                        ajax: '/adminleaves/list/proc',
                        columns: [
                            {
                                data: 'DT_Row_Index',
                                orderable: false,
                                searchable: false,
                                class: 'text-left'
                            },
                            {data: 'employee'},
                            {data: 'reason'},
                            {data: 'started_at'},
                            {data: 'ended_at'},
                            {data: 'created_at'},
                            {data: 'status', name: 'status', class: 'text-center' },
                            {data: 'action', orderable: false,searchable: false,class: 'text-right'}
                        ]
                    });
                });
            </script>
            <script>
                    $(function () {
                        $('#leave_approved').DataTable({
                            serverSide: true,
                            processing: true,
                            order: [[5,'desc']],
                            ajax: '/adminleaves/list/approve',
                            columns: [{
                                    data: 'DT_Row_Index',
                                    orderable: false,
                                    searchable: false,
                                    class: 'text-left'
                                },
                                {data: 'employee'},
                            {data: 'reason'},
                            {data: 'started_at'},
                            {data: 'ended_at'},
                            {data: 'created_at'},
                            {data: 'status', name: 'status', class: 'text-center' },
                                {
                                    data: 'action',
                                    orderable: false,
                                    searchable: false,
                                    class: 'text-right'
                                }
                            ]
                        });
                    });
                </script>
                <script>
                        $(function () {
                            $('#lieu_unproc').DataTable({
                                serverSide: true,
                                processing: true,
                                ajax: '/adminlieus/list/unproc',
                                columns: [{
                                        data: 'DT_Row_Index',
                                        orderable: false,
                                        searchable: false,
                                        class: 'text-left'
                                    },
                                    {data: 'employee'},
                                    {data: 'remark'},
                                    {data: 'started_at'},
                                    {data: 'ended_at'},
                                    {
                                        data: 'status',
                                        name: 'status',
                                        class: 'text-center'
                                    },
                                    {
                                        data: 'action',
                                        orderable: false,
                                        searchable: false,
                                        class: 'text-right'
                                    }
                                ]
                            });
                        });
                    </script>
                    <script>
                            $(function () {
                                $('#lieu_proc').DataTable({
                                    serverSide: true,
                                    processing: true,
                                    ajax: '/adminlieus/list/proc',
                                    columns: [
                                        {
                                            data: 'DT_Row_Index',
                                            orderable: false,
                                            searchable: false,
                                            class: 'text-left'
                                        },
                                        {data: 'employee'},
                                        {data: 'remark'},
                                        {data: 'started_at'},
                                        {data: 'ended_at'},
                                        {data: 'status', name: 'status', class: 'text-center' },
                                        {data: 'action', orderable: false,searchable: false,class: 'text-right'}
                                    ]
                                });
                            });
                        </script>
                        <script>
                                $(function () {
                                    $('#lieu_approved').DataTable({
                                        serverSide: true,
                                        processing: true,
                                        ajax: '/adminlieus/list/approve',
                                        columns: [{
                                                data: 'DT_Row_Index',
                                                orderable: false,
                                                searchable: false,
                                                class: 'text-left'
                                            },
                                            {data: 'employee'},
                                        {data: 'remark'},
                                        {data: 'started_at'},
                                        {data: 'ended_at'},
                                        {data: 'status', name: 'status', class: 'text-center' },
                                            {
                                                data: 'action',
                                                orderable: false,
                                                searchable: false,
                                                class: 'text-right'
                                            }
                                        ]
                                    });
                                });
                            </script>

@endsection

