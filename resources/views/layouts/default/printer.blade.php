<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" type="image/png" sizes="16x16" href="img/favicon.png'">

        <title>IX - HRMS</title>

        <link href=" {{ URL::asset('bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href=" {{ URL::asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet">
        <link href=" {{ URL::asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') }}" rel="stylesheet">
        <link href=" {{ URL::asset('plugins/bower_components/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
        <link href=" {{ URL::asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css">
        <link href=" {{ URL::asset('plugins/bower_components/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
        <link href=" {{ URL::asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
        <link href=" {{ URL::asset('plugins/bower_components/calendar/dist/fullcalendar.css') }}" rel="stylesheet">
        <link href=" {{ URL::asset('css/animate.css') }}" rel="stylesheet">
        <link href=" {{ URL::asset('css/style.css') }}" rel="stylesheet">
        <link href=" {{ URL::asset('css/colors/blue-dark.css') }}" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset('plugins/bower_others/datatables.net-dt/css/jquery.dataTables.min.css') }}">
        <link href="{{ URL::asset('plugins/bower_others/select2/dist/css/select2.min.css') }}" rel="stylesheet" />
        <link href="{{ URL::asset('plugins/bower_others/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}" rel="stylesheet" />

    </head>

    <body>



        @yield('content')

        @include('shared._delete_confirm')

        <script src="{{ URL::asset('plugins/bower_components/jquery/dist/jquery.min.js')}}"></script>
        <script src="{{ URL::asset('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>
        <script src="{{ URL::asset('plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js')}}"></script>
        <script src="{{ URL::asset('js/jquery.slimscroll.js')}}"></script>
        <script src="{{ URL::asset('js/waves.js')}}"></script>
        <script src="{{ URL::asset('bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <script src="{{ URL::asset('plugins/bower_components/waypoints/lib/jquery.waypoints.js')}}"></script>
        <script src="{{ URL::asset('plugins/bower_components/counterup/jquery.counterup.min.js')}}"></script>
        <script src="{{ URL::asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{ URL::asset('plugins/bower_components/moment/moment.js')}}"></script>
        <script src="{{ URL::asset('plugins/bower_components/calendar/dist/fullcalendar.min.js')}}"></script>
        <script src="{{ URL::asset('plugins/bower_components/calendar/dist/cal-init.js')}}"></script>
        <script src="{{ URL::asset('js/custom.min.js')}}"></script>
        <script src="{{ URL::asset('js/cbpFWTabs.js')}}"></script>
        <script src="{{ URL::asset('plugins/bower_components/toast-master/js/jquery.toast.js')}}"></script>
        <script src="{{ URL::asset('plugins/bower_others/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
        <script type="text/javascript" charset="utf8" src="{{ URL::asset('plugins/bower_others/datatables.net/js/jquery.dataTables.js') }}"></script>
        <script src="{{ URL::asset('plugins/bower_others/select2/dist/js/select2.min.js') }}"></script>
        <script src="{{ URL::asset('plugins/bower_others/memoment/moment.js') }}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-range/4.0.1/moment-range.js"></script>

        {{-- <script>
        window['moment-range'].extendMoment(moment);
        </script> --}}

        <!-- Main Quill library -->
        <script src="//cdn.quilljs.com/1.3.6/quill.js"></script>
        <script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>

        <!-- Theme included stylesheets -->
        <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
        <link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">

        <!-- Core build with no theme, formatting, non-essential modules -->
        <link href="//cdn.quilljs.com/1.3.6/quill.core.css" rel="stylesheet">
        <script src="//cdn.quilljs.com/1.3.6/quill.core.js"></script>

        @yield('js')

        <script>
                (function() {
                [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
                    new CBPFWTabs(el);
                });
            })();
            $('.user-pro').removeClass('active');
            // Date Picker
            var defaults = $.fn.datepicker.defaults = {

            orientation: "auto",

            };
            jQuery('.mydatepicker, #datepicker').datepicker();
            jQuery('.datepicker-autoclose').datepicker({
                autoclose: true,
                todayHighlight: true
            });
        </script>

    </body>
</html>
