<section id="employee-setting">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3 class="box-title">Default Supporting Document</h3>

            <a href="#add-document" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Document</a>
            <br />
            <br />
            <div class="form-group">
                <div class="table-responsive m-b-30">
                    <table id="document_table"class="table">
                        <thead>
                                <tr style="border-bottom:3px solid #eee">
                                    <th>#</th>
                                    <th>Document Name</th>
                                    <th>URL</th>
                                    <th class="text-right">Action</th>
                                </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="add-document" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Add Document</h1>

                {!! Form::open(['id' => 'doc_saved', 'url' => '', 'files' => true]) !!}
                {!! Form::hidden('shared', '1') !!}

                <div class="form-group">
                    {!! Form::label('Document Name') !!}
                    {!! Form::text('name', null, array('required', 'class' => 'form-control', 'placeholder' => 'Document Name')) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('Upload Document') !!}
                    {{ Form::file('document', null, array('class' => 'form-control', 'placeholder' => 'Document File')) }}
                </div>

                <div class="form-group">
                    {!! Form::label('URL Link') !!}
                    {!! Form::url('url', null, array('class' => 'form-control', 'placeholder' => 'Paste URL Link')) !!}
                </div>

                {{ Form::submit('Add Document',['class' =>'btn btn-rounded btn-info text-uppercase']) }}
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('js')
    @parent
    <script>

        $('#add-document').on('hidden.bs.modal', function(e) {
            $(this).find('#doc_saved')[0].reset();
        });

        $(function () {
            $('#document_table').DataTable({
                serverSide: true,
                processing: true,
                ajax: '/documents/list',
                columns: [
                    {data: 'DT_Row_Index', orderable: false, searchable: false},
                    {data: 'name'},
                    {data: 'url_view', orderable: false, searchable: false},
                    {data: 'action', orderable: false, searchable: false}
                ]
            });
        })

        $( "#doc_saved" ).on( "submit", function( event ) {
            event.preventDefault();

            swal({
            title: 'Saving',
            onOpen: () => {
                swal.showLoading()
                return new Promise(function(resolve, reject) {
                    var form = document.getElementById('doc_saved');
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url: "{{ route('settings.store_document') }}",
                        data: formData,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            swal.close();
                            $('#add-document').modal('toggle');
                            $('#document_table').DataTable().ajax.reload();
                        },
                        error: function(data) {
                            errormsg = '';
                            if(data.status == 422 ){
                                $.each(data.responseJSON.errors, function (key, value) {
                                    errormsg = errormsg + value[0] + "<br />";
                                });
                            }else{
                                errormsg = "Server error. Please try again";
                            }
                            swal.showValidationMessage(errormsg);
                            swal.hideLoading();
                        },
                    });
                });
            }})
        });
        </script>
@endsection
