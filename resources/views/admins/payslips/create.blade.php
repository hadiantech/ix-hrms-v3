<div class="modal fade" id="add-payroll-cat">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                {!! Form::open(['url' => 'adminpayslips', "id" => "payroll_create",]) !!}
                <h1>Add Payroll Category</h1>

               <div class="form-group">
                    {!! Form::label('Payroll Category Name') !!}
                    {{Form::text('payrollcreate', null , ['required','class' => 'form-control','placeholder' => 'Create Name'])}}
                </div>
               <div class="form-group row">
                <div class="col-md-6">
                {!! Form::label('Select Operand') !!}
                 <select class="selectpicker" data-style="form-control">
                    <option>Addition</option>
                    <option>Deduction</option>
                 </select>
               </div>

                <div class="col-md-6">
                    {!! Form::label('Payroll Category Name') !!}
                    {{Form::text('payrollcreate', null , ['required','class' => 'form-control','placeholder' => 'Create Name'])}}

                </div>
            </div>

           {{Form::submit('Print Payslip',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
           <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
           {!! Form::close() !!}


            </div>
        </div>
    </div>
</div>
