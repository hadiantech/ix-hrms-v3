<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
Use Redirect;
use App\Employee;
use App\Role;
use App\Department;
use App\Organisation;
use App\Designation;
use App\Hierarchy;
use App\EmployeeLeaveAllocation;
use App\ActivateToken;
use App\Reset;

use DataTables;
use Validator;
use Postmark\PostmarkClient;
use GuzzleHttp\Client;
use Carbon\Carbon;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all();
        $roles = Role::all();
        $departments = Department::all();
        $organisations = Organisation::all();
        $designations = Designation::get();
        $hierarchies = Hierarchy::all();
        return view('admins.employees.index',compact('employees','roles','organisations','departments','designations','hierarchies'));

    }

    public function list(){
        return Datatables::eloquent(Employee::with('designation', 'department', 'organisation')
        ->select('employees.*')
        ->whereHas('roles', function($q){
            $q->where("name",'!=' ,"Admin");
        }))
        ->addColumn('designation', function($model){
            return $model->designation->designation_name;
        })
        ->addColumn('department', function($model){
            return $model->department->department_name;
        })
        ->addColumn('organisation', function($model){
            return $model->organisation->company_name;
        })
        ->addColumn('action', function($model){
            $edit_hide = true;
            $url = 'employees';
            return view('shared._table_action', compact('model', 'url', 'edit_hide'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = new PostmarkClient("73a69c7d-8c56-4f59-877c-74d05cd5e247");
        $request->validate([
            'username' => 'required|unique:employees,username|max:255',
            'email' => 'required|unique:employees,email'
        ]);

        $employee = new Employee;
        $employee->username = $request->username ;
        $employee->email = $request->email ;
        $employee->password = Hash::make($request->password) ;
        $employee->fname = $request->fname ;
        $employee->lname = $request->lname ;
        $employee->ic = $request->ic ;
        $employee->company_id = $request->company_id ;
        $employee->department_id = $request->department_id ;
        $employee->designation_id = $request->designation_id ;
        $employee->joined_at = $request->joined_at ;
        $employee->employee_uid = $request->employee_uid ;
        $employee->status = $request->status ;
        $employee->hierarchy_id = $request->hierarchy_id ;
        $employee->report_to = $request->report_to ;
        $employee->statustoken = "inactive";

        $employee->save();
        $employee->assignRole($request->role_id);

        $usertoken = $employee->id;

        $activate = new ActivateToken;
        $activate->activate_token = Hash::make('activate_token');
        $activate->user_id = $usertoken;
        $activate->status = "unused";

        $activate->save();

        $active=$employee->id;

        //email link activation
        $sendResult = $client->sendEmailWithTemplate(

        "hassanul@boneybone.com",
        $employee->email,
        10229183,
        [
            "system_name" => "[IX HRMS] User Registration Activation Link",
            "fname" => $employee->fname,
            "lname" => $employee->lname,
            "uid" => $employee->employee_uid,
            "activate" => route('employees.activate_user',$active),
            "link" => url('http://hrms.ixtelecom.net')
        ]);

        return response()->json($employee, 201);

        // untuk api reading
        // return response()->json(['data'=>$employee]);
    }

    public function activate_user(Request $request, $id)
    {
        $employee = Employee::find($id);
        $activate = ActivateToken::where('user_id',$id)->first();

        return view('check', compact('activate','employee'));

    }

    public function passwords(Request $request, $id)
    {
        $request->validate([
            'password' => 'required|confirmed|min:6',
        ]);
        $employee = Employee::find($id);
        $employee->password = $request->password;
        if($request->password){
            $employee->password = Hash::make($request->password);
        }
        $employee->statustoken = "active";
        $employee->save();

        $usertoken = $employee->id;

        $activate = ActivateToken::where('user_id',$usertoken)->first();
        $activate->status = "used";

        $activate->save();

        $url = "http://hrms.ixtelecom.net";
        return Redirect::to($url);
    }

    public function resets(Request $request)
    {
        $client = new PostmarkClient("73a69c7d-8c56-4f59-877c-74d05cd5e247");
        $employee = Employee::where('email', $request->email)->first();
        $activate = new Reset;
        $activate->token = Hash::make('token');
        $activate->email = $request->email;
        $activate->status = "unused";

        $activate->save();
        $active=$activate->email;

        $sendResult = $client->sendEmailWithTemplate(

            "hassanul@boneybone.com",
            $activate->email,
            10229183,
            [
                "system_name" => "[IX HRMS] User First Time Activation Link",
                "fname" => $employee->fname,
                "lname" => $employee->lname,
                "uid" => $employee->employee_uid,
                "activate" => route('employees.activate_password',$active),
                "link" => url('http://hrms.ixtelecom.net')
            ]);
            return back();
    }

    public function activate_password(Request $request, $email)
    {

        $employee = Employee::where('email',$email)->first();
        $activate = Reset::where('email',$email)->orderBy('status','asc')->first();

        return view('checkpass', compact('activate','employee'));
    }

    public function resetpasswords(Request $request, $email)
    {
        $request->validate([
            'password' => 'required|confirmed|min:6',
        ]);
        $employee = Employee::where('email',$email)->first();
        $employee->password = $request->password;
        if($request->password){
            $employee->password = Hash::make($request->password);
        }
        $employee->statustoken = "active";
        $employee->save();

        $usertoken = $employee->email;

        $activate = Reset::where('email',$usertoken)->first();
        $activate->status = "used";

        $activate->save();
$url = "http://hrms.ixtelecom.net";
return Redirect::to($url);
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        $top_employees = Employee::get()->pluck('full_name', 'id');
        $hierarchies = Hierarchy::all()->pluck('hierarchy_name', 'id');
        $organisations = Organisation::all()->pluck('company_name', 'id');
        $departments = Department::all()->pluck('department_name', 'id');
        $designations = Designation::all()->pluck('designation_name', 'id');
        $roles = Role::all()->pluck('name','id');

        return view('admins.employees.show', compact('check','departments','organisations','employee', 'designations','top_employees', 'hierarchies', 'roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $employee->fname = $request->fname;
        $employee->lname = $request->lname;
        $employee->dob = $request->dob;
        $employee->gender = $request->gender;
        $employee->marriage_status = $request->marriage_status;
        $employee->race = $request->race;
        $employee->race_other = $request->race_other;
        $employee->religion = $request->religion;
        $employee->religion_other = $request->religion_other;
        $employee->nationality = $request->nationality;
        $employee->passport = $request->passport;
        $employee->passport_expired_at = $request->passport_expired_at;
        $employee->ic = $request->ic;
        $employee->pre_address_line1 = $request->pre_address_line1;
        $employee->pre_address_line2 = $request->pre_address_line2;
        $employee->pre_city = $request->pre_city;
        $employee->pre_state = $request->pre_state;
        $employee->pre_postcode = $request->pre_postcode;
        $employee->pre_country = $request->pre_country;
        $employee->pre_phone_number1 = $request->pre_phone_number1;
        $employee->pre_phone_number2 = $request->pre_phone_number2;
        $employee->per_address_line1 = $request->per_address_line1;
        $employee->per_address_line2 = $request->per_address_line2;
        $employee->per_city = $request->per_city;
        $employee->per_state = $request->per_state;
        $employee->per_postcode = $request->per_postcode;
        $employee->per_country = $request->per_country;
        $employee->per_phone_number1 = $request->per_phone_number1;
        $employee->per_phone_number2 = $request->per_phone_number2;
        $employee->bank_name = $request->bank_name;
        $employee->bank_account_no = $request->bank_account_no;

        $employee->save();
        return response()->json($employee, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();
        return redirect()->back();
    }

    public function upload_image(Request $request, Employee $employee){
        $validator = Validator::make($request->all(), [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($validator->passes()) {
            $employee->clearMediaCollection('profile');
            $employee->addMediaFromRequest('image')->toMediaCollection('profile');
            return response()->json(['success'=>'done']);
        }
        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function account_update(Request $request, Employee $employee){
        $request->validate([
            'username' => 'required|max:255|unique:employees,username,'.$employee->id,
            'email' => 'required|unique:employees,email,'.$employee->id
        ]);

        $employee->username = $request->username;
        $employee->email = $request->email;

        if($request->password){
            $employee->password = Hash::make($request->password);
        }

        $employee->save();
        return response()->json('success', 201);
    }

    public function employee_update(Request $request, Employee $employee){

        $employee->joined_at = $request->joined_at;
        $employee->employee_uid = $request->employee_uid;
        $employee->status = $request->status;
        $employee->probation_status = $request->probation_status;
        $employee->confirmed_at = $request->confirmed_at;
        $employee->contract_ended_at = $request->contract_ended_at;
        $employee->hierarchy_id = $request->hierarchy_id;
        $employee->company_id = $request->company_id;
        $employee->department_id = $request->department_id;
        $employee->designation_id = $request->designation_id;
        $employee->working_days = $request->days;
        // $employee->report_to = $request->report_to;
        // $employee = $request->get('roles');
        $employee->assignRole($request->roles);
        $employee->save();

        // $employee->syncRoles($request->rol);
        // foreach($employee as $emp) {
        //     if ($emp = Role::find($emp)) {
        //       $emp->syncRoles($roles->id);
        //     }
        //   }

        return response()->json('success', 201);
    }

    public function leave_update(Request $request, Employee $employee){

        EmployeeLeaveAllocation::updateOrCreate(
            ['employee_id' => $employee->id, 'leave_type' => 'Annual Leave'],
            ['quota' => $request->annual_leave]
        );
        EmployeeLeaveAllocation::updateOrCreate(
            ['employee_id' => $employee->id, 'leave_type' => 'Emergency Leave'],
            ['quota' => $request->emergency_leave]
        );
        EmployeeLeaveAllocation::updateOrCreate(
            ['employee_id' => $employee->id, 'leave_type' => 'Medical Leave'],
            ['quota' => $request->medical_leave]
        );
        EmployeeLeaveAllocation::updateOrCreate(
            ['employee_id' => $employee->id, 'leave_type' => 'Compassionate Leave'],
            ['quota' => $request->compassionate_leave]
        );
        EmployeeLeaveAllocation::updateOrCreate(
            ['employee_id' => $employee->id, 'leave_type' => 'Maternity Leave'],
            ['quota' => $request->maternity_leave]
        );
        EmployeeLeaveAllocation::updateOrCreate(
            ['employee_id' => $employee->id, 'leave_type' => 'Paternity Leave'],
            ['quota' => $request->paternity_leave]
        );
        EmployeeLeaveAllocation::updateOrCreate(
            ['employee_id' => $employee->id, 'leave_type' => 'Marriage Leave'],
            ['quota' => $request->marriage_leave]
        );

        return response()->json($request->medical_leave, 201);
    }
}
