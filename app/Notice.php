<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
    public $table = "notices";

    protected $fillable = ['title','status','description','date'];

    public function employee()
    {
        return $this->belongsTo(Employee::class,'createdby');
    }

}
