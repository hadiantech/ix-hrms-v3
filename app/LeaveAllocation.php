<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveAllocation extends Model
{
    public $table = "leave_allocations";
    public function employee() {

        return $this->belongsTo('App\Employee', 'employee_id');
    }
    public function leavetype() {

        return $this->belongsTo('App\LeaveType', 'leave_type_id');
    }
}
