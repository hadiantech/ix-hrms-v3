<?php

namespace App\Http\Controllers;

use App\Hotel;
use DataTables;
use Illuminate\Http\Request;
use Carbon\Carbon;

class HotelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function list($id){
        return Datatables::eloquent(Hotel::where(['advance_claims_id' => $id]))
            ->addColumn('action', function($model){

                if($model->claim->claim_status == "submit"){
                    $delete_hide = true;
                    $edit_hide = true;
                }
                else{
                    $delete_hide = false;
                    $edit_hide = false;
                }

                $show_hide = true;
                $modal_edit_id = 'modal_hotel_edit';
                $url = 'hotels';
                return view('shared._table_action', compact('model', 'url','show_hide','modal_edit_id','delete_hide','edit_hide'))->render();
            })
            ->escapeColumns([])
            ->addIndexColumn()
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hotel = Hotel::create($request->all());

        return response()->json($hotel, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function show(Hotel $hotel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function edit(Hotel $hotel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Hotel $hotel)
    {
        $hotel->update($request->all());

        return response()->json($hotel, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Hotel  $hotel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Hotel $hotel)
    {
        $hotel->delete();

        return redirect()->back();
    }
}
