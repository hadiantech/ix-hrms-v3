<section id="supportingdoc">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3 class="box-title">Supporting Documents</h3>
            <div class="table-responsive m-b-30">
                <table class="table">
                    <thead>
                        <tr style="border-bottom:3px solid #eee">
                            <th>#</th>
                            <th>Document Name</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Employee Handbook</td>

                            <td class="text-right">
                                <a class="m-r-5 label label-info text-uppercase">
                                    <i class="fa fa-download"></i>
                                </a>
                                <a class="label label-danger text-uppercase">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Document Name 1</td>

                            <td class="text-right">
                                <a class="m-r-5 label label-info text-uppercase">
                                    <i class="fa fa-download"></i>
                                </a>
                                <a class="label label-danger text-uppercase">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>


                        <tr style="border-bottom:3px solid #eee">
                            <td colspan="8">
                                <a href="#add-document" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Document</a>
                            </td>

                        </tr>
                    </tbody>
                </table>
            </div>
            <button style="width:200px" class=" btn-lg btn-rounded btn-info text-uppercase">Save</button>
        </div>
    </div>
</section>

{{-- Modals Here --}}
<div class="modal fade" id="add-document" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Add Document</h1>

                    <div class="form-group">

                        <input type="text" class="form-control" placeholder="Document Name">

                    </div>

                    <div class="form-group">
                        <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                            <div class="form-control" data-trigger="fileinput">
                                <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                <span class="fileinput-filename"></span>
                            </div>
                            <span class="input-group-addon btn btn-default btn-file">
                                <span class="fileinput-new">Select file</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="..."> </span>
                            <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>

                    <div class="form-group">

                        <input type="text" class="form-control" placeholder="Paste URL Link">

                    </div>

                <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Add Document</button>
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>

