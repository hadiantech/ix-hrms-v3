<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Traveller extends Model
{
    protected $table = 'traveller_informations';
    protected $guarded = [];

    public function claim(){
        return $this->belongsTo(Claim::class,'advance_claims_id');
    }

}
