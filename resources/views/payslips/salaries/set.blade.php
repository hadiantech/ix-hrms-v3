<div id="modal_salary_set" class="modal fade in" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <h1>Set Employee Salary</h1>

                {!! Form::open(['url' => '', 'id' => 'salary_create']) !!}
                <input type="hidden" name="employee_id" id="employee_id" value="">
                <div class="form-group">
                    {!! Form::label('employee name') !!}
                    <p><span id="modal_salary_username"> </span></p>
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                        {!! Form::label('basic salary') !!}
                        {{Form::text('basic_salary', null , ['required','class' => 'form-control','placeholder' => 'salary' ])}}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('salary type') !!}
                        {{Form::select('salary_type', ['Monthly' => 'Monthly', 'Hourly' => 'Hourly'], null, ['class' => 'form-control', 'placeholder' => 'Select Status'])}}
                    </div>
                </div>
                {{Form::submit('Set Salary',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('js')
@parent
<script>
    $('#modal_salary_set').on('hidden.bs.modal', function (e) {
        $(this).find('#salary_create')[0].reset();
    });

    $("#salary_create").on("submit", function (event) {
        event.preventDefault();
        swal({
            title: "Salary Set",
            text: "Confirm employee basic salary?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function () {
                var form = document.getElementById('salary_create');
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: '{{ route('salaries.set') }}',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (msg) {
                        return msg;
                    }
                });
            }
        }).then((result) => {
            $('#modal_salary_set').modal('toggle');
            if (result.value) {
                $('#salary_table').DataTable().ajax.reload(function (json) {
                    var len = json.data.length;
                    $("#salaries").empty();
                    $("#salaries").append("<option value=''>Please Select</option>");
                    for (var i = 0; i < len; i++) {
                        var id = json.data[i]['id'];
                        var name = json.data[i]['title'];
                        $("#salaries").append("<option value='" + id + "'>" + name + "</option>");

                    }
                });

                //reload select designation


                swal('Information Added!', '--', 'success');
            }
        });
    });
</script>
@endsection
