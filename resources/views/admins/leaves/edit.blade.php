<div id="leaves_modal_edit" class="modal fade in" tabindex="-1" role="dialog" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">

                    <h1 style="padding-bottom:20px;">Approve/Reject Leave Application</h1>
                        {!! Form::open(['url' => 'adminleaves', 'id' => 'leave_edit']) !!}
                        {{ Form::hidden('id', null , ['id' => 'id']) }}
                        <input type="hidden" name="_method" value="PUT">
                        <div style="margin-bottom:15px" class="form-group row">

                                <div class="col-md-6">

                                    <p class="text-muted">Leave Event<br><span style="color:#000" id="event_name"></span></p>

                                    <p class="text-muted">Employee Name <br><span style="color:#000" id="employee_name"></span></p>

                                    <p class="text-muted">Leave Type <br><span style="color:#000" id="leave_type"></span></p>

                                </div>

                                <div class="col-md-6">
                                <p class="text-muted">Date Start <br><span style="color:#000" id="started_at"></span></p>

                                    <p class="text-muted">Date End <br><span style="color:#000" id="ended_at"></span></p>

                                </div>
                        </div>
                        <p style="border-top:1px solid #f1f1f1;padding-top:20px" class="text-muted">Change Application Status</p>
                                    {{--  <div class="col-md-6">
                                            {{ Form::select('status', ['Approve' => 'Approve', 'Reject' => 'Reject'], null, ['placeholder' => 'Approve or Reject']) }}
                                    </div>  --}}
                                   <div class="form-group row"> <div class="col-md-6">
                                    <select class="form-control" name="status" id="status" data-parsley-required="true">
                                        <option value="Reject">Reject</option>
                                        <option value="Approve">Approve</option>
                                    </select>
                                    </div></div>

                        {{Form::submit('SAVE',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                        <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    @section('js')
        @parent
        <script>

        $( "#leave_edit" ).on( "submit", function( event ) {
            event.preventDefault();
            swal({
                title: "Update Status",
                text: "Change this user leave status?",
                type: "info",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                preConfirm: function() {
                    var form = document.getElementById('leave_edit');
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url: '/adminleaves/'+form.id.value,
                        data: formData,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: function(msg) {
                            return msg;
                        }
                    });
                }
            }).then((result) => {
                $('#leaves_modal_edit').modal('toggle');
                if(result.value){
                    $('#leave_table').DataTable().ajax.reload();
                    swal('Status Updated!', '--', 'success');
                }
            });
        });
        </script>
    @endsection
