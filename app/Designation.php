<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    protected $table = 'designations';
    protected $fillable = ['designation_name','department_id'];

    public function department(){
        return $this->belongsTo('App\Department', 'department_id');
    }

    public function employee(){
        return $this->hasMany('App\Employee');
    }

}
