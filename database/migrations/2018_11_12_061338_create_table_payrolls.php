<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePayrolls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_payrolls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('salary_id')->nullable();
            $table->double('ee_epf')->nullable();
            $table->double('ee_scs')->nullable();
            $table->double('ee_eis')->nullable();
            $table->double('er_epf')->nullable();
            $table->double('er_scs')->nullable();
            $table->double('er_eis')->nullable();
            $table->double('tax')->nullable();
            $table->double('net_salary')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_payrolls');
    }
}
