<div id="add-notice" class="modal fade in" tabindex="-1" role="dialog">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h1>Add Notice</h1>
                    {!! Form::open(['url' => 'notices', "id" => "notice_create", 'autocomplete' => "off"]) !!}
                    {{-- {{ Form::hidden('createdby', null , ['id' => 'createdby']) }} --}}
                    <div class="form-group row">
                        <div class="col-md-6">
                                {!! Form::label('status') !!}
                                {!! Form::select('status', ['Draft'=>'Draft', 'Publish'=>'Publish'], null,['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                            {!! Form::label('title') !!}
                            {{Form::text('title', null , ['required','class' => 'form-control','placeholder' => 'Title'])}}
                    </div>
                    <div class="form-group">
                            {!! Form::label('description') !!}

                            <form>
                                <textarea class="ckeditor" name="editor" id="editor" rows="10" cols="80">

                                </textarea>
                            </form>

                    {{Form::submit('Add Notice',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                    <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                    {!! Form::close() !!}
                </div>
                </div>
            </div>
        </div>
</div>
@section('js')
@parent
<script src="{{ URL::asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}" type="text/javascript"></script>
<script>

    CKEDITOR.replace('editor');
</script>
<script>
    $('#add-notice').on('hidden.bs.modal', function(e) {
        $(this).find('#notice_create')[0].reset();
    });

    $( "#notice_create" ).on( "submit", function( event ) {
        event.preventDefault();
        swal({
            title: "Submit Notice",
            text: "Are you sure you want to submit this draft?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                var form = document.getElementById('notice_create');
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: '{{ route('notices.store') }}',
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(msg) {
                        return msg;
                    }
                });
            }
        }).then((result) => {
            $('#add-notice').modal('toggle');
            if(result.value){
                $('#notice_table').DataTable().ajax.reload(function(json){
                    var len = json.data.length;
                    $("#notices").empty();
                    $("#notices").append("<option value=''>Please Select</option>");
                    for( var i = 0; i<len; i++){
                        var id = json.data[i]['id'];
                        var name = json.data[i]['title'];
                        $("#notices").append("<option value='"+id+"'>"+name+"</option>");

                    }
                });
                swal('New Notice Added!', '--', 'success');
            }
        });
    });
</script>

@endsection
