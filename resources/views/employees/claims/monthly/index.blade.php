@extends('layouts.default.master')

@section('content')

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add New Claim</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="dashboard.html">Dashboard</a></li>
                    <li><a href="dashboard-claim.html">Claim Dashboard</a></li>
                    <li class="active">Add New Claim</li>
                </ol>
            </div>
        </div>
         <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="box-title text-uppercase">New Claim
                        <a href="#add-expense" data-toggle="modal" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Add Expenses</a></h3>
                        <div class="form-group row m-t-20">
                            <div class="col-md-3">
                                <select class="selectpicker" data-style="form-control">
                                    @foreach ($months as $index => $m)
                                        <option value="{{ $m }}">{{ \Carbon\Carbon::createFromDate(null, $m, null)->format('M') }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Sorry, claim submission for May 2018 has ended. You are unable to proceed with the claim.
                        </div>
                        <div class="alert alert-warning alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Your claim needs revision. Please revise the returned line item below.
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Type</th>
                                        <th>Item Name & Desc</th>
                                        <th class="text-center">Project Code</th>
                                        <th class="text-center">Amount (RM)</th>
                                        <th class="text-center">Item Date</th>
                                        <th class="text-center">Receipt No.</th>
                                        <th class="text-center">Ref No.</th>
                                        <th class="text-right">Action</th>
                                </thead>
                                <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Purchases</td>
                                            <td>Ninja Form Add-ons
                                                <p class="text-muted">Add-on plugin for website purposes</p>
                                            </td>
                                            <td class="text-center">MYIX1234</td>
                                            <td class="text-center">45.00</td>
                                            <td class="text-center">14 Mar 2018</td>
                                            <td class="text-center">324AD</td>
                                            <td class="text-center">1</td>
                                            <td class="text-right">
                                                <a href="" class="label label-info m-r-5"><i class="fa fa-pencil"></i></a>
                                                <a href="" class="label label-danger"><i class="fa fa-trash"></i></a>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Mileage</td>
                                            <td>Trip to Teraju
                                                <p class="text-muted">One way trip from IX to Teraju<br>
                                                Distance : 33.5KM</p>
                                            </td>
                                            <td class="text-center">MYIX1234</td>
                                            <td class="text-center">16.75</td>
                                            <td class="text-center">14 Mar 2018</td>
                                            <td class="text-center">324AD</td>
                                            <td class="text-center">2</td>
                                            <td class="text-right">
                                                <a href="" class="label label-info m-r-5"><i class="fa fa-pencil"></i></a>
                                                <a href="" class="label label-danger"><i class="fa fa-trash"></i></a>
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Purchases</td>
                                            <td>Ninja Form Add-ons
                                                <p class="text-muted">Add-on plugin for website purposes</p>
                                            </td>
                                            <td class="text-center">MYIX1234</td>
                                            <td class="text-center">86.00</td>
                                            <td class="text-center">19 Mar 2018</td>
                                            <td class="text-center">324AD</td>
                                            <td class="text-center">2</td>
                                            <td class="text-right">
                                                <a href="" class="label label-info m-r-5"><i class="fa fa-pencil"></i></a>
                                                <a href="" class="label label-danger"><i class="fa fa-trash"></i></a>
                                            </th>
                                        </tr>

                                </tbody>
                                <tfoot class="claim-amount">
                                    <tr>
                                        <th colspan="6"></th>
                                        <th class="text-right">TOTAL AMOUNT</th>
                                        <th colspan="2" class="text-right"><span class="amount">RM 350.00</span></th>
                                    </tr>
                                </tfoot>
                            </table>

                    </div>

                    <button style="width:200px" class="m-r-10 btn-lg btn-rounded btn-info text-uppercase">Save Claim</button>
                    <button style="width:200px" class=" btn-lg btn-rounded btn-success text-uppercase">Confirm & Submit</button>

                </div>
           </div>

           <div class="modal fade" id="add-expense">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h1>Add Expense</h1>
                        <form>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <select class="selectpicker" data-style="form-control">
                                        <option selected disabled>Expense Type</option>
                                        <option>Hotel & Travelling</option>
                                        <option> Meal Allowance</option>
                                        <option>Refreshment & Entertainment</option>
                                        <option>Mileage</option>
                                        <option>Toll & Parking</option>
                                        <option>Claimable Allowances</option>
                                        <option>Office Supplies</option>
                                        <option>Miscellaneous</option>

                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Project Code">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Item Name">
                            </div>
                            <div class="form-group">
                                <textarea rows="4" class="form-control">Item Description</textarea>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control datepicker-autoclose" placeholder="Item Date"> <span class="input-group-addon"><i class="icon-calender"></i></span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <input type="number" class="form-control" placeholder="Amount">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" placeholder="Receipt No.">
                                </div>
                                <div class="col-md-6">
                                    <input type="number" class="form-control" placeholder="Reference No.">
                                </div>
                            </div>
                            <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Add Expense</button>
                            <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

                        </form>
                    </div>
                </div>
            </div>
           </div>
        </div>
        </div>
    </div>
</div>
@endsection

@section('js')
@endsection
