<?php

namespace App\Http\Controllers;

use App\Salary;
use App\Payroll;
use App\Employee;
use App\PayrollCategory;
use DataTables;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

class SalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    public function list(){


        return Datatables::eloquent(Employee::with('salary'))
        ->addColumn('salary', function($model){
            try{
                return decrypt($model->salary['basic_salary']);
            }
            catch(\Exception $e){
                return 'N/A';

            }
        })
        ->addColumn('action', function($model){
            $url = 'salaries';
            $show_hide = true;
            $delete_hide = true;
            $modal_view_id = 'modal_salary_show';
            $modal_edit_id = 'modal_salary_edit';
            $modal_data_customs = array('salary' => $model->salary);
            $custom_btn = [
                '<a class="btn btn-xs btn-success open-modal" data-modal_data=\''.$model.'\' href="#modal_salary_set" data-toggle="modal">Salary</a>',
                // '<a class="btn btn-xs btn-default open-modal" data-modal_data=\''.$model.'\' href="#modal_salary_increment" data-toggle="modal">Increment</a>',
            ];
            return view('shared._table_action', compact('model','modal_data_customs','show_hide','delete_hide','modal_view_id','custom_btn', 'url'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }


    public function salary($id)
    {
        $employees = Employee::all();
        $salaries = Salary::find($id);
        // return $employees;
        return view('payslips.salaries.set', compact('employees','salaries'));
    }

    public function set(Request $request)
    {
        Salary::updateOrCreate(
            ['employee_id' => $request->employee_id],
            ['basic_salary' => encrypt($request->basic_salary), 'salary_type'=>$request->salary_type]
        )->save();

        return back();
    }

    public function list_based_employee($id){

        return Salary::where('employee_id', $id)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function show(Salary $salary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function edit(Salary $salary)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Salary  $salary
     * @return \Illuminate\Http\Response
     */

    public function increment()
    {
        $salaries = Salary::all();
        $employees = Employee::all();
        return view('payslips.salaries.increment', compact('employees','salaries'));
    }

    public function update(Request $request, Salary $salary)
    {
        $salary->update($request->only('basic_salary_increment','increment_type'));
        return response()->json($salary, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function destroy(Salary $salary)
    {
        //
    }
}
