<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_salaries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_id')->nullable();;
            $table->string('basic_salary')->nullable();;
            $table->string('overtime_salary')->nullable();;
            $table->string('payment_type')->nullable();;
            $table->string('basic_salary_increment')->nullable();;
            $table->string('salary_type')->nullable();;
            $table->string('increment_type')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_salaries');
    }
}
