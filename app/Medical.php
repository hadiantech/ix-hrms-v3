<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medical extends Model
{
    //
    public $table = "leaves";

    protected $fillable = ['emp_id','ltype_id','leave_from','leave_to','leave_totalday','leave_reason'];
    protected $dates = ['date'];
    //
    public function leavetype() {

        return $this->belongsTo('App\LeaveType');
    }

}
