<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\Event;
use DataTables;
use Calendar;

class EventController extends Controller
{
    public function index(){
        $e = Event::get();
    	$events = Event::get();
    	$event_list = [];
    	foreach ($events as $key => $event) {
    		$event_list[] = Calendar::event(
                $event->event_name,
                true,
                new \DateTime($event->start_date),
                new \DateTime($event->end_date.' +1 day')
            );
    	}
    	$calendar_details = Calendar::addEvents($event_list);

        return view('events.index', compact('calendar_details','e') );
    }

    public function employeeindex(){
        $e = Event::get();
    	$events = Event::get();
    	$event_list = [];
    	foreach ($events as $key => $event) {
    		$event_list[] = Calendar::event(
                $event->event_name,
                true,
                new \DateTime($event->start_date),
                new \DateTime($event->end_date.' +1 day')
            );
    	}
    	$calendar_details = Calendar::addEvents($event_list);

        return view('employees.events.index', compact('calendar_details','e') );
    }

    public function addEvent(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'event_name' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);

        if ($validator->fails()) {
        	\Session::flash('warnning','Please enter the valid details');
            return Redirect::to('/events')->withInput()->withErrors($validator);
        }

        $event = new Event;
        $event->event_name = $request['event_name'];
        $event->start_date = $request['start_date'];
        $event->end_date = $request['end_date'];
        $event->save();

        \Session::flash('success','Event added successfully.');
        return Redirect::to('/events');
    }
    function isWeekend($date) {
        $date = strtotime($date);
        $date = date("l", $date);
        $date = strtolower($date);
        echo $date;
        if($date == "saturday" || $date == "sunday") {
            return "true";
        } else {
            return "false";
        }
    }

    public function eventlist()
    {
        return Datatables::eloquent(Event::query())
        ->addColumn('action', function($model){
            $url = 'events';
            $custom_btn = [
                '<a class="btn btn-xs btn-danger" data-method="delete" data-confirm="confirm" href="'. route($url.'.destroy', $model->id) .'">Cancel</a>'
            ];
            return view('shared._table_action', compact('model', 'url','custom_btn'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    public function delete(Request $request, $id)
    {
        $eve = Event::find($id);
        $eve->delete();
        return redirect()->back();
    }
}
