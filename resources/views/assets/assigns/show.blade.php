<div id="modal_assign_show" class="modal fade in" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <p class="text-muted">Asset assigned to</p>
                <H1><span id="modal_assign_username"> </span></H1>
                <hr>
                <p class="text-muted">Status</p>
                <H1><span id="modal_assign_status"> </span></H1>
                <hr>
                <p class="text-muted">Started from</p>
                <H1><span id="modal_assign_started"> </span></H1>
                <p class="text-muted">untill</p>
                <H1><span id="modal_assign_ended"> </span></H1>
                <hr>
                <p class="text-muted">Asset Type</p>
                <H1><span id="modal_assign_type_name"> </span></H1>
                <hr>
                <p class="text-muted">Asset Brand</p>
                <H1><span id="modal_assign_brand_name"> </span></H1>

        </div>
    </div>
</div>
</div>
