<?php

namespace App\Http\Controllers;

use App\Designation;
use App\Department;
use Illuminate\Http\Request;
use DataTables;

class DesignationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function list(){
        return Datatables::eloquent(Designation::with('department'))
        ->addColumn('department', function($model){
            return $model->department->department_name;
        })
        ->addColumn('action', function($model){
            $url = 'designations';
            $show_hide = true;
            $modal_view_id = 'modal_designations_show';
            $modal_edit_id = 'modal_designations_edit';
            $delete_datatable_id_name_refresh = 'dept_table';
            return view('shared._table_action', compact('model', 'url', 'show_hide','modal_view_id', 'modal_edit_id'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    public function list_based_department($id){
        return Designation::where('department_id', $id)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::lists(['department_name']);
        return View('admins.organisations.designations.show', compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $designations = Designation::create($request->only('designation_name','department_id'));
        return redirect()-> route('organisations.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $designations = Designation::where('id', $id)->first();
         $designations = Designation::find($id);
         $departments = Department::find($id);
        return View ('admins.organisations.designations.show',compact('designations','departments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function edit(Designation $designation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Designation $designation)
    {
        $designation->update($request->all());
        return response()->json($designation, 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Designation  $designation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Designation $designation)
    {
        $designation->delete();
        return redirect()->back();
    }
}
