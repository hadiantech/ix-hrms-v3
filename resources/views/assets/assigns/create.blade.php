<div id="add-assign" class="modal fade in" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    <h1>Assign Assets to User</h1>
                    {!! Form::open(['url' => 'assigns', "id" => "assign_create", 'autocomplete' => "off"]) !!}
                    <div class="form-group row">
                        <div class="col-md-6">
                            {!! Form::label('Select Employee') !!}
                            <select class="form-control" name="employee_id" id="employees" data-parsley-required="true">
                                @foreach ($employees as $e)
                                <option value="{{ $e->id }}">{{ $e->fname}} {{ $e->lname}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            {!! Form::label('Assign Asset Type') !!}
                            <select class="form-control" name="assign_type" id="assign_type" data-parsley-required="true">
                                @foreach ($types as $type)
                                <option value="{{ $type->id }}">{{ $type->type_name}}</option>
                                @endforeach
                            </select>
                        </div>
                         
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            {!! Form::label('Start Date') !!}
                                <div class="input-group">
                                    {!!Form::date('started_at','',array('id'=>'started_at','class'=>'form-control','placeholder'=>'Start Date'))!!}<span class="input-group-addon"><i class="icon-calender"></i></span>
                                </div>
                        </div>

                        <div class="col-md-6">
                            {!! Form::label('status') !!}
                            {{Form::select('assign_status', ['Active' => 'Active', 'Inactive' => 'Inactive'], null, ['class' => 'form-control', 'placeholder' => 'Select Status'])}}
                        </div>
                    </div>
                    {{Form::submit('Assign Asset',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                    <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
     </div>


    @section('js')
     @parent

     <script>
     $(document).ready(function(){

         $("#assign_type").change(function(){
             var deptid = $(this).val();

             $.ajax({
                 url: '/assets/brand/'+deptid,
                 type: 'GET',
                 dataType: 'json',
                 success:function(response){
                     var len = response.length;
                     $("#assign_brand").empty();
                     $("#assign_brand").append("<option value=''>Please Select</option>");
                     for( var i = 0; i<len; i++){
                         var id = response[i]['id'];
                         var name = response[i]['name'];
                         $("#assign_brand").append("<option value='"+id+"'>"+name+"</option>");

                     }
                 }
             });
         });

     });

     </script>


    <script>

    $('#add-assign').on('hidden.bs.modal', function(e) {
        $(this).find('#assign_create')[0].reset();
    });

    $( "#assign_create" ).on( "submit", function( event ) {
        event.preventDefault();
        swal({
            title: "Assign Asset to the User",
            text: "Confirm to assign this asset to the designated user?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                var form = document.getElementById('assign_create');
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: '{{ route('assigns.store') }}',
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(msg) {
                        return msg;
                    }
                });
            }
        }).then((result) => {
            $('#add-assign').modal('toggle');
            if(result.value){
                $('#assign_table').DataTable().ajax.reload(function(json){
                    var len = json.data.length;
                    $("#assigns").empty();
                    $("#assigns").append("<option value=''>Please Select</option>");
                    for( var i = 0; i<len; i++){
                        var id = json.data[i]['id'];
                        var name = json.data[i]['title'];
                        $("#assigns").append("<option value='"+id+"'>"+name+"</option>");

                    }
                });

                //reload select designation


                swal('Asset Assign Confirmed!', '--', 'success');
            }
        });
    });
    </script>
    @endsection
