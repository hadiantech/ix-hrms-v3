<div class="modal fade" id="modal_education_edit" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    <h1>Edit Education</h1>

                        {!! Form::open(['url' => '', 'id' => 'education_edit']) !!}

                        {{ Form::hidden('employee_id', null) }}
                        {{ Form::hidden('id', null) }}
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group row">
                            <div class="col-md-6">
                                {!! Form::label('Qualification') !!}
                                {!! Form::text('qualification', null, array('class' => 'form-control', 'placeholder' => 'Qualification', 'required')) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('Field of Study') !!}
                                {!! Form::text('field', null, array('class' => 'form-control', 'placeholder' => 'Field of Study', 'required')) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('University/Institution') !!}
                            {!! Form::text('university', null, array('class' => 'form-control', 'placeholder' => 'University/Institution', 'required')) !!}

                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                {!! Form::label('Started At') !!}
                                {!! Form::date('started_at', null, array('class' => 'form-control', 'placeholder' => 'Started At', 'required')) !!}
                            </div>
                            <div class="col-md-6">
                                {!! Form::label('Graduated At') !!}
                                {!! Form::date('graduated_at', null, array('class' => 'form-control', 'placeholder' => 'Graduated At', 'required')) !!}
                            </div>
                        </div>

                        {{ Form::submit('Save Education',['class' =>'btn btn-rounded btn-info text-uppercase']) }}
                        <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                        {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>



    @section('js')
    @parent
    <script>

        $( "#education_edit" ).on( "submit", function( event ) {
            event.preventDefault();
            swal({
                title: "Edit Education Background",
                text: "Are you sure you want to edit this education background?",
                type: "info",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                allowOutsideClick: () => !swal.isLoading(),
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        var form = document.getElementById('education_edit');
                        var formData = new FormData(form);
                        $.ajax({
                            type: "POST",
                            url: '/employees/'+form.employee_id.value+'/educations/'+form.id.value,
                            data: formData,
                            cache:false,
                            contentType: false,
                            processData: false,
                            success: function(data) {
                                resolve(data);
                            },
                            error: function(data) {
                                errormsg = '';
                                if(data.status == 422 ){
                                    $.each(data.responseJSON.errors, function (key, value) {
                                        errormsg = errormsg + value[0] + "<br />";
                                    });
                                }else{
                                    console.log(data);
                                    errormsg = "Server error. Please try again";
                                }
                                swal.showValidationMessage(errormsg);
                                swal.hideLoading();
                            },
                        });
                    });

                }
            }).then((result) => {
                if(result.value){
                    $('#modal_education_edit').modal('toggle');
                    swal('Education Updated!', '--', 'success');
                    $('#education_table').DataTable().ajax.reload();
                }
            });
        });


    </script>
@endsection
