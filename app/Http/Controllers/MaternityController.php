<?php

namespace App\Http\Controllers;

use App\Maternity;
use Illuminate\Http\Request;
use App\Employee;
use App\LeaveType;
use App\Annual;
use App\EmployeeLeave;
use Auth;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Postmark\PostmarkClient;
use App\Approval;

class MaternityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $employee = Employee::lists(['id']);
        $leavetype = LeaveType::lists(['id']);
        return view('employees.leaves.maternity.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = new PostmarkClient("73a69c7d-8c56-4f59-877c-74d05cd5e247");

        $leaves = EmployeeLeave::where('employee_id', Auth::user()->id)->get();
        $approval_lv1 = Approval::where('employee_id', Auth::user()->id)->where('level',1)->first();
        $approval_lv2 = Approval::where('employee_id', Auth::user()->id)->where('level',2)->first();
        //return $approval->approvedBy;

        $aid = Auth::user()->id;
        $employees = Employee::find($aid);
        //

        $maternity_leave_quota = Auth::user()->leave_allocs()->type('Maternity Leave')->first()->quota;
        $maternity_leave_taken = Auth::user()->leaves()->type('Maternity Leave')->sum('total_days');
        $balance = $maternity_leave_quota - $maternity_leave_taken;

        $a = Carbon::parse($request->started_at);
        $b = Carbon::parse($request->ended_at);
        $days = $b->diffInDays($a);
        $days = $days + 1;

        $unpaid = $balance - $days;
        $leave = new EmployeeLeave;
        $leave->employee_id = Auth::user()->id;
        $leave->leave_type = 'Maternity Leave';
        $leave->started_at = $request->started_at;
        $leave->ended_at = $request->ended_at;

        $leave->status = 'Pending';
        $leave->leave_proc_status = 'unproc';


        $leave->save();
        $leaves = $leave->id;
        $sendResult = $client->sendEmailWithTemplate(

            "hassanul@boneybone.com",
            $approval_lv1->approvedBy->email,
            10030123,
                [
                    "system_name" => "[IX HRMS] Leave Application Approval",
                    "fname" => $employees->fname,
                    "lname" => $employees->lname,
                    "uid" => $employees->employee_uid,
                    "approve" =>  $approval_lv1->approvedBy->fname,
                    "approver" =>  $approval_lv1->approvedBy->lname,
                    "leave_type" => 'Maternity Leave',
                    "started_at" => $request->started_at,
                    "ended_at" => $request->ended_at,
                    "reason" => $request->reason,
                    "accept" => route('adminleaves.update_email',$leaves),
                    "reject" => route('adminleaves.reject_email',$leaves),
                    "link" => url('http://hrms.ixtelecom.net')
                ]);

            $sendResult = $client->sendEmailWithTemplate(

                    "hassanul@boneybone.com",
                    $approval_lv2->approvedBy->email,
                    10030123,
                        [
                            "system_name" => "[IX HRMS] Leave Application Approval",
                            "fname" => $employees->fname,
                            "lname" => $employees->lname,
                            "uid" => $employees->employee_uid,
                            "approve" =>  $approval_lv2->approvedBy->fname,
                            "approver" =>  $approval_lv2->approvedBy->lname,
                            "leave_type" => 'Maternity Leave',
                            "started_at" => $request->started_at,
                            "ended_at" => $request->ended_at,
                            "reason" => $request->reason,
                            "accept" => route('adminleaves.approve_email',$leaves),
                            "reject" => route('adminleaves.reject_email',$leaves),
                            "link" => url('http://hrms.ixtelecom.net')
                        ]);

        return redirect()-> route('leaves.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Maternity  $maternity
     * @return \Illuminate\Http\Response
     */
    public function show(Maternity $maternity)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Maternity  $maternity
     * @return \Illuminate\Http\Response
     */
    public function edit(Maternity $maternity)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Maternity  $maternity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Maternity $maternity)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Maternity  $maternity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Maternity $maternity)
    {
        //
    }
}
