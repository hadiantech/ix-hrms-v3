<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = ['department_name'];
    // protected $table = ['organisations'];


    public function designations()
    {
        return $this->hasMany('App\Designation');
    //     return $this->hasMany(Designation::class);
    }


}
