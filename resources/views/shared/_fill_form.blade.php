<script>

function fillFormJson(form_id, data){
    // reset form values from json object
    $.each(data, function(name, val){
        var $el = $('#'+form_id+' [name="'+name+'"]'),
            type = $el.attr('type');
        switch(type){
            case 'checkbox':
                $el.attr('checked', 'checked');
                break;
            case 'radio':
                $el.filter('[value="'+val+'"]').attr('checked', 'checked');
                break;
            default:
                $el.val(val);
        }
    });
}

</script>
