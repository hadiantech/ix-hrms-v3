<?php

namespace App\Http\Controllers;

use App\Expense;
use Illuminate\Http\Request;
use App\Claim;
use DataTables;

class ExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    //Datatable to list normal claim expenses (Employee)
    public function list_expense_month($id){
        return Datatables::eloquent(Expense::with('expenseType')->where('claim_id',$id))
            ->addColumn('item_name_desc', function($model){

                if($model->remark){
                    $text ="<br/><p class='remark-box'>".$model->remark."</p>";
                }
                else{
                    $text = '';
                }
                return $model->item_name."<br/>".$model->description.$text;
            })
            ->addColumn('item_type', function($model){
                return $model->expense_type ? $model->expense_type->expense : "";
            })
            ->addColumn('action', function($model){
                $status = $model->claim->claim_status;
                $proc_status = $model->claim->claim_proc_status;

                if($proc_status != 'return'){
                    if($status == 'submit'){
                        return '<span class="text-right">No Action Available</span>';
                    }
                }
                
                
                
                $modal_view_id = 'add-expense';
                $modal_edit_id = 'edit_expense';
                $show_hide = true;
                $url = 'expenses';

                return view('shared._table_action', compact('model', 'url','modal_view_id','modal_edit_id','show_hide'))->render();
            })
            ->escapeColumns([])
            ->addIndexColumn()
            ->make(true);
    }

    //Datatable to list normal claim expenses (Admin)
    public function list_expense_month_admin($id){
        return Datatables::eloquent(Expense::with('expenseType')->where('claim_id',$id))
            ->addColumn('item_name_desc', function($model){
                return $model->item_name."<br/>".$model->description;
            })
            ->addColumn('item_type', function($model){
                return $model->expense_type ? $model->expense_type->expense : "";
            })
            ->addColumn('action', function($model){
                $modal_view_id = '';
                $modal_edit_id = 'expenses_modal_edit';
                $url = 'expenses';
                $show_hide = 'true';
                $delete_hide = 'true';
                return view('shared._table_action', compact('model', 'url','modal_edit_id','show_hide','delete_hide'))->render();
            })
            ->escapeColumns([])
            ->addIndexColumn()
            ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $expenses = Expense::create($request->only(
            'claim_id',
            'project_code',
            'item_type',
            'item_name',
            'description',
            'purchase_date',
            'amount',
            'receipt_no',
            'reference_no'
        ));

        $expenses->save();

        $claim = Claim::find($request->claim_id);
        $claim->claim_total = $claim->claim_total + $request->amount;

        $claim->save();

        return response()->json($claim, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function show(Expense $expense)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function edit(Expense $expense)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Expense $expense)
    {
        $expense->project_code = $request->project_code;
        $expense->item_name = $request->item_name;
        $expense->item_type = $request->item_type;
        $expense->description = $request->description;
        $expense->purchase_date = $request->purchase_date;
        $expense->amount = $request->amount;
        $expense->receipt_no = $request->receipt_no;
        $expense->reference_no = $request->reference_no;
        $expense->remark = $request->remark;
        $expense->save();

        //update claim total amount
        $claim = Claim::find($request->claim_id);
        $claim->claim_total = $claim->claim_total - $request->amount_old;
        $claim->claim_total = $claim->claim_total + $request->amount;

        $claim->save();
        // return $claim;
        return response()->json($claim, 201);
    }

    public function update_remark(Request $request, Expense $expense)
    {
        $e = Expense::find($request->expenses_id);
        $e->remark = $request->remark;

        $e->save();
        return response()->json($expense, 201);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expense $expense)
    {
        //get claim_id
        //get amount for that expense
        //tolak expense dari claim

        $claim = Claim::find($expense->claim_id);
        $claim->claim_total = $claim->claim_total - $expense->amount;
        $claim->save();

        $expense->delete();
        return redirect()->back();
    }
}
