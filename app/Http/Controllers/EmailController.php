<?php

namespace App\Http\Controllers;

use App\Notifications\LeaveRequest;
use Illuminate\Http\Request;
use App\Employee;
use App\EmployeeLeave;

class EmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LeaveRequest  $leaveRequest
     * @return \Illuminate\Http\Response
     */
    public function show(LeaveRequest $leaveRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LeaveRequest  $leaveRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(LeaveRequest $leaveRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LeaveRequest  $leaveRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LeaveRequest $leaveRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LeaveRequest  $leaveRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeaveRequest $leaveRequest)
    {
        //
    }
    public function customEmail(Request $request)
    {
        $leaves = EmployeeLeave::find($id);
        $employees = Employee::find($id);

    }
}



// contoh yg hantar custom email

// if ($request->isMethod('get'))
//             return view('custom_email');
//         else {
//             $rules = [
//                 'to_email' => 'required|email',
//                 'subject' => 'required',
//                 'message' => 'required',
//             ];
//             $this->validate($request, $rules);
//             $user = new User();
//             $user->email = $request->to_email;
//             $user->notify(new CustomEmail($request->subject, $request->message));
//             $request->session()->put('status', true);
//             return back();
//              }
