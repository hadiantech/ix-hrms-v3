@extends('layouts.default.master')

@section('content')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Add New Employee</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li>
                        <a href="dashboard-adminview.html">Dashboard</a>
                    </li>
                    <li>
                        <a href="admin-employee-dashboard.html">Employee Dashboard</a>
                    </li>
                    <li class="active">Edit Employee</li>
                </ol>
            </div>
        </div>



        <!--employee -->
        <div class="row">
            <div class="col-sm-12">
                <div class="white-box p-0">
                    <div class="sttabs tabs-style-iconbox">
                        <nav>
                            <ul>
                                <li>
                                    <a href="#personalinfo" class="sticon ti-user">
                                        <span>Personal Details</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#employementinfo" class="sticon ti-list">
                                        <span>Employment Details</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="#medicalinfo" class="sticon ti-info-alt">
                                        <span>Medical Info</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#leaveallocation" class="sticon ti-calendar">
                                        <span>Leave Allocation</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#approver" class="sticon ti-layers-alt">
                                        <span>Approval Layer</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#supportingdoc" class="sticon ti-files">
                                        <span>Supporting Documents</span>
                                    </a>
                                </li>
                                </li>
                            </ul>
                        </nav>
                        <div class="content-wrap p-t-20">
                            @include('admins.employees.edit.personalinfo')
                            @include('admins.employees.edit.employmentinfo')
                            @include('admins.employees.edit.medicalinfo')
                            @include('admins.employees.edit.leaveallocation')
                            @include('admins.employees.edit.approver')
                            @include('admins.employees.edit.supportingdoc')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Page Content -->
    <!-- ============================================================== -->
</div>
@endsection
