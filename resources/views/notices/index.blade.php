@extends('layouts.default.master')

@section('content')
<!-- Include stylesheet -->
<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
<div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>

<div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Notice</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    <ol class="breadcrumb">
                        <li><a href="dashboard-adminview.html">Dashboard</a></li>
                        <li class="active">Notice</li>
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            {{-- Notice List --}}
            <div class="row">
                <div class="col-sm-12">
                    <div class="white-box">
                        <h3 class="box-title">Notice Listings</h3>
                             {{-- if condition utk admin --}}
                             {{-- admin je yg boleh add notice --}}
                             {{-- aku pun taktahu bagaimana --}}
                             {{-- <a href="{{ route('roles.create') }}" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Add Role</a> --}}
                             @can('add_notices')
                             <td><a href="#add-notice" data-toggle="modal" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Add Notice</a></td>
                             @endcan


                        <div class="table-responsive">
                            <table  id="notice_table" class="">
                                <thead>
                                    <tr>
                                        <th style="width: 5%">#</th>
                                        <th>Notice Title</th>
                                        <th>Created by</th>
                                        <th>Published Date</th>
                                        <th style="width: 20%">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
            <!--row -->
            {{-- modal notice --}}
            {{-- @include('notices.show') --}}
            @include('notices.create')
            {{-- @include('notices.edit') --}}

    </div>
        <!-- /.container-fluid -->
    </div>

    @endsection

    @section('js')
    @parent

    <script>
    $(function () {

        $('#notice_table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/notices/list',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false},
                {data: 'title'},
                {data: 'created_at'},
                {data: 'createdby'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });
    });

    $(document).off('click', '.open-modal').on("click", ".open-modal", function () {

        var data = $(this).data('modal_data');
        if($(this).data('logo') ){
        var logo = $(this).data('logo');

        }
    });
    </script>

    @endsection
