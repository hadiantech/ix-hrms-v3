<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Employee;
use App\LeaveType;
use App\Annual;
use App\EmployeeLeave;
use App\Event;

use Auth;

use Carbon\Carbon;

class AnnualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $aid = Auth::user()->id;
        $employees = Employee::find($aid);
        $approver = Approval::where('approver_id');
        $leavetype = LeaveType::lists(['id']);
        $dates = Event::pluck('start_date');
        return view('employees.leaves.annuals.create',compact('approver','aid','employees','dates')->with('mula'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $annual_leave_quota = Auth::user()->leave_allocs()->type('Annual Leave')->first()->quota;
        $annual_leave_taken = Auth::user()->leaves()->type('Annual Leave')->sum('total_days');
        $balance = $annual_leave_quota - $annual_leave_taken;

        $a = Carbon::parse($request->started_at);
        $b = Carbon::parse($request->ended_at);
        $days = $b->diffInDays($a);
        $days = $days + 1;

        $unpaid = $balance - $days;

        $annual = new EmployeeLeave;
        $annual->employee_id = Auth::user()->id;
        $annual->leave_type = 'Annual Leave';
        $annual->started_at = $request->started_at;
        $annual->ended_at = $request->ended_at;
        $annual->reason = $request->reason;
        $annual->total_days = $days;
        $annual->as_unpaid = $unpaid;
        // $emergency->as_lieu = $request->as_lieu;
        // $emergency->remark = $request->remark;
        $annual->status = 'Pending';
        $annual->leave_proc_status = 'unproc';

        $annual->save();

        return response()->json($annual, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeLeave $employeeLeave, $id)
    {
        $employeeLeave = EmployeeLeave::find($id);
        $employeeLeave->delete();
        return redirect()->back();
    }
}
