<section id="medicalinfo">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h3 class="box-title">Medical Info</h3>
            <div class="table-responsive m-b-30">
                <table class="table">
                    <thead>
                        <tr style="border-bottom:3px solid #eee">
                            <th>#</th>
                            <th>Insurance Name</th>
                            <th class="text-center">Policy No.</th>
                            <th class="text-center">Start Date</th>
                            <th class="text-center">Expiry Date</th>
                            <th class="text-center">Value (RM)</th>
                            <th class="text-right">Action</th>
                        </tr>
                    </thead>
                    <tbody>



                        <tr style="border-bottom:3px solid #eee">
                            <td colspan="8">
                                <a href="#add-medical" data-toggle="modal" class="m-auto btn btn-rounded btn-info text-uppercase">Add Medical Info</a>
                            </td>

                        </tr>
                    </tbody>
                </table>
            </div>
            <button style="width:200px" class=" btn-lg btn-rounded btn-info text-uppercase">Save</button>
        </div>
    </div>
</section>

{{-- Modal Here --}}
<div class="modal fade" id="add-medical" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Add Medical Info</h1>

                    <div class="form-group row">
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="Insurance Name">
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="Policy No.">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" class="form-control datepicker-autoclose" placeholder="Start Date">
                                <span class="input-group-addon">
                                    <i class="icon-calender"></i>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input type="text" class="form-control datepicker-autoclose" placeholder="Expiry Date">
                                <span class="input-group-addon">
                                    <i class="icon-calender"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <input type="text" class="form-control" placeholder="Value (RM)">
                        </div>
                    </div>

                <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Add Medical</button>
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
