@extends('layouts.default.master')

@section('content')

<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
    </svg>
</div>
<div id="page-wrapper">
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <h4 class="page-title">Generate Pay</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                    <ol class="breadcrumb">
                        <li><a href="dashboard-adminview.html">Dashboard</a></li>
                        <li><a href="admin-payslip.html">Payroll</a></li>
                        <li class="active">Generate Salary</li>
                    </ol>
                </div>
            </div>
            {{-- {!! Form::open(['url' => 'generates']) !!} --}}

            {{-- {{Form::text('basic',$generates->category_id,array('class' => 'form-control')) }} --}}
            {{-- {{Form::text('basic',$employees->id,array('class' => 'form-control')) }} --}}
            {{-- {{Form::text('employee',$salaries->basic_salary,array('class' => 'form-control')) }} --}}
            {{-- {{Form::text('category',$categories->name,array('class' => 'form-control')) }} --}}
            {{-- {{Form::text('operand',$operands->id,array('class' => 'form-control')) }} --}}

            {{-- {!! Form::close !!} --}}




            {{-- <div class="form-group  ">
                {!! Form::hidden('Payroll Category') !!}
                    @foreach ($categories as $categorie)
                        <option value="{{ $categorie->id }}">{{ $categorie->name }}</option>
                    @endforeach

            </div> --}}

            <div class="row">
                <div class="" style="display:flex; justify-content:space-between; margin-bottom:10px">
                        <button data-target="#add-item-pay" data-toggle="modal" class="pull-right btn btn-rounded btn-info"><i class="fa fa-plus"></i> Add Earnings</button>
                </div>
                <div class="" style="display:flex; justify-content:space-between; margin-bottom:10px">
                        <button data-target="#deductitem" data-toggle="modal" class="pull-right btn btn-rounded btn-info"><i class="fa fa-plus"></i> Add Deduction</button>
                </div>
            </div>

            <div class="row">


                <div class="col-sm-4">
                    <div class="white-box">
                        <h3 class="box-title">Earning</h3><br>
                        {!! Form::open(['url' => 'generates', "id" => "gen_create"]) !!}
                        <div class="  form-group ">
                            <div style="width:90px !important">
                            {{Form::hidden('basic',$a,array('class' => 'form-control')) }}
                            </div>
                    </div>
                    <div class="  form-group ">
                        <div style="width:90px !important">
                        {{Form::hidden('employee_name',$employees->username,array('class' => 'form-control')) }}
                        </div>
                    </div>
                    <div class="  form-group ">
                        <div style="width:90px !important">
                        {{Form::hidden('employee_id',$employees->id,array('class' => 'form-control')) }}
                        </div>
                    </div>
                        <table class="table" width="100%">
                                <tr>
                                    <td>
                                            <div class="" style="display:flex; justify-content:space-between; margin-bottom:-2px">
                                                <div class="">
                                                    {!! Form::label('Earning Name') !!}
                                                </div>
                                                <div class="">
                                                    {!! Form::label('Total (MYR)') !!}
                                                </div>
                                            </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                            <div class="" style="display:flex; justify-content:space-between; margin-bottom:-2px">
                                                <div class="">
                                                    {!! Form::label('Basic Earning') !!}
                                                </div>
                                                <div class="">
                                                    {{ $a }}
                                                </div>
                                            </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                            <div class="" style="display:flex; justify-content:space-between; margin-bottom:-2px">
                                                <div class="">
                                                    {!! Form::label('Additional Earning') !!}
                                                </div>
                                                <div class="">
                                                    {!! Form::label('Total (MYR)') !!}
                                                </div>
                                            </div>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                            @foreach ($generates as $gen)
                                                @if ($gen->operand_id == null)
                                                    {{ "" }}
                                                @elseif ($gen->operand_id == 1)
                                                    <div class="" style="display:flex; justify-content:space-between; margin-bottom:5px">
                                                        <div class="">
                                                            {{ $gen->paycategory->name }}
                                                        </div>
                                                        <div class="">
                                                                <a style="color: #e74c3c !important;" href= "/generates/generate/{{ $gen->employee_id }}/{{ $gen->category_id }}/delete">Delete</a>
                                                            </div>
                                                        <div style="text-align:right">
                                                            {{ $gen->value }}
                                                        </div>
                                                    </div>
                                                @else ($gen->operand_id == 2)
                                                    {{ "" }}
                                                @endif
                                            @endforeach
                                        </tr>
                                    </td>
                                </tr>

                                <tr>
                                    <td>
                                            <div class="" style="display:flex; justify-content:space-between; margin-bottom:-2px">
                                                <div class="">
                                                    {!! Form::label('Total Earning') !!}
                                                </div>
                                                <div class="">
                                                    {{ $totaladd + $a }}</td>
                                                </div>
                                            </div>
                                    </td>
                                </tr>
                        </table>
                    </div>
                </div>


                <div class="col-sm-4">
                        <div class="white-box">
                            <h3 class="box-title">Deductions</h3><br>
                            {!! Form::open(['url' => 'generates', "id" => "gen_create"]) !!}
                            <table class="table" width="100%">
                                    <tr>
                                        <td>
                                                <div class="" style="display:flex; justify-content:space-between; margin-bottom:-2px">
                                                    <div class="">
                                                        {!! Form::label('Deduction Name') !!}
                                                    </div>
                                                    <div class="">
                                                        {!! Form::label('Total (MYR)') !!}
                                                    </div>
                                                </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                                <div class="" style="display:flex; justify-content:space-between; margin-bottom:-2px">
                                                    <div class="">
                                                        {!! Form::label('Unpaid Leave') !!}
                                                    </div>
                                                    <div class="">
                                                        {{--  {{ $salaries->basic_salary }}  --}}
                                                    </div>
                                                </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                                <div class="" style="display:flex; justify-content:space-between; margin-bottom:-2px">
                                                    <div class="">
                                                        {!! Form::label('Additional Deduction') !!}
                                                    </div>
                                                    <div class="">
                                                        {!! Form::label('Total (MYR)') !!}
                                                    </div>
                                                </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                                @foreach ($generates as $gen)
                                                    @if ($gen->operand_id == null)
                                                        {{ "" }}
                                                    @elseif ($gen->operand_id == 2)
                                                        <div class="" style="display:flex; justify-content:space-between; margin-bottom:5px">
                                                            <div class="">
                                                                {{ $gen->paycategory->name }}
                                                            </div>
                                                            <div class="">
                                                                    <a style="color: #e74c3c !important;" href= "/generates/generate/{{ $gen->employee_id }}/{{ $gen->category_id }}/delete">Delete</a>
                                                                </div>
                                                            <div style="text-align:right">
                                                                {{ $gen->value }}
                                                            </div>
                                                        </div>
                                                    @else ($gen->operand_id == 1)
                                                        {{ "" }}
                                                    @endif
                                                @endforeach
                                            </tr>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                                <div class="" style="display:flex; justify-content:space-between; margin-bottom:-2px">
                                                    <div class="">
                                                        {!! Form::label('Total Deduction') !!}
                                                    </div>
                                                    <div class="">
                                                        {{ $totalded }}</td>
                                                    </div>
                                                </div>
                                        </td>
                                    </tr>
                            </table>
                        </div>
                    </div>


                    <div class="col-sm-4">
                            <div class="white-box">
                                <h3 class="box-title">Additional Item</h3><br>
                                {!! Form::open(['url' => 'generates', "id" => "payroll_create"]) !!}
                                <table class="table" width="100%">
                                        <tr>
                                            <td>
                                                    <div class="" style="display:flex; justify-content:space-between; margin-bottom:-2px">
                                                        <div class="">
                                                            {!! Form::label('Item Name') !!}
                                                        </div>
                                                        <div class="">
                                                            {!! Form::label('Total (MYR)') !!}
                                                        </div>
                                                    </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                    <div class="" style="display:flex; justify-content:space-between; margin-bottom:-2px">
                                                        <div class="">
                                                            {!! Form::label('Earning-Deduction') !!}
                                                        </div>
                                                        <div class="">
                                                            {{ $earns }}
                                                        </div>
                                                    </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                    <div class="" style="display:flex; justify-content:space-between; margin-bottom:-2px">
                                                        <div class="">
                                                            {!! Form::label('PSMB') !!}
                                                        </div>
                                                        <div class="">
                                                            {{ $tax }}
                                                        </div>
                                                    </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                    <div class="" style="display:flex; justify-content:space-between; margin-bottom:-2px">
                                                            <div class="">
                                                                {!! Form::label('EPF') !!}
                                                            </div>
                                                            <div class="">
                                                                {{ $epf }}
                                                            </div>
                                                    </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                    <div class="" style="display:flex; justify-content:space-between; margin-bottom:-2px">
                                                            <div class="">
                                                                {!! Form::label('SOCSO') !!}
                                                            </div>
                                                            <div class="">
                                                                {{ $epf }}
                                                            </div>
                                                    </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                    <div class="" style="display:flex; justify-content:space-between; margin-bottom:-2px">
                                                            <div class="">
                                                                {!! Form::label('EIS') !!}
                                                            </div>
                                                            <div class="">
                                                                {{ $epf }}
                                                            </div>
                                                    </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                    <div class="" style="display:flex; justify-content:space-between; margin-bottom:-2px">
                                                        <div class="">
                                                            {!! Form::label('Total Addition Item') !!}
                                                        </div>
                                                        <div class="">
                                                            {{ "$earns" }}</td>
                                                        </div>
                                                    </div>
                                            </td>
                                        </tr>

                                </table>
                            </div>
                        </div>

            {{Form::submit('GENERATE',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
            <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
            {!! Form::close() !!}
            {{-- Add Item Modal --}}
            @include('payslips.salaries.generates.additem')
            @include('payslips.salaries.generates.deduction')

            {{-- <div class="modal fade" id="confirm-pay">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h1>Confirm Generate Pay</h1>

                           <div class="form-group">
                             <p>Are you sure?</p>

                           </div>

                           <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Yes</button>
                                        <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div> --}}

        <!-- /.container-fluid -->

    </div>

    @endsection

    @section('js')
    @parent
    <script>

        $( "#gen_create" ).on( "submit", function( event ) {
            event.preventDefault();
            swal({
                title: "Submit Payslip",
                text: "Confirm to submit employee payslip? This action is not editable",
                type: "info",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                preConfirm: function() {
                    var form = document.getElementById('gen_create');
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url: "{{ route('generates.store_gen',['years'=> $years, 'month'=> $month]) }}",
                        data: formData,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: function(msg) {
                            return msg;
                        }
                    });
                }
            }).then((result) => {
                $('#gen_create').modal('toggle');
                if(result.value){
                    $('#payroll_table').DataTable().ajax.reload(function(json){
                        var len = json.data.length;
                        $("#generates").empty();
                        $("#generates").append("<option value=''>Please Select</option>");
                        for( var i = 0; i<len; i++){
                            var id = json.data[i]['id'];
                            var name = json.data[i]['title'];
                            $("#generated_table").append("<option value='"+id+"'>"+name+"</option>");

                        }
                    });

                    //reload select designation


                    swal('Payslip Generated!', '--', 'success');
                }
            });
        });
        </script>
        @endsection

