<div class="row">
    <div class="col-sm-12">
        <div class="white-box">
            <h3 class="box-title text-uppercase">Leave in Lieu
                <a href="#lieu-leave-form" data-toggle="modal" class="pull-right m-t--5 btn btn-rounded btn btn-info text-uppercase">Apply
                    Leave in Lieu</a></h3>
            <div class="table-responsive">
                <table id="lieu_table" class="table display">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Event Name</th>
                            <th>Leave From</th>
                            <th>Leave To</th>
                            <th>Total Days</th>
                            <th>Expired At</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

@section('js')
    @parent
    <script>
        $(function () {
            $('#lieu_table').DataTable({
                serverSide: true,
                processing: true,
                ajax: '{{ route("lieus.index") }}',
                columns: [
                    {data: 'DT_Row_Index', orderable: false, searchable: false},
                    {data: 'event_name'},
                    {data: 'started_at'},
                    {data: 'ended_at'},
                    {data: 'total_days'},
                    {data: 'expired_at'},
                    {data: 'status'},
                    {data: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@endsection
