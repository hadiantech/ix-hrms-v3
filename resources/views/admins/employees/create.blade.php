<div class="modal fade" id="add-employee" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                {!! Form::open(['url' => 'adminemployees', "id" => "employee_create", 'autocomplete' => "off"]) !!}
                <h1>Add Employee</h1>
                <h3 class="box-title">Account Details</h3>
                <hr/>

                <div class="form-group row">
                    <div class="col-md-6">
                        {!! Form::label('Username') !!}
                        {{Form::text('username', null , ['required','class' => 'form-control','placeholder' => 'Username'])}}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('Email') !!}
                        {{Form::email('email', null , ['required','class' => 'form-control','placeholder' => 'Email'])}}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        {!! Form::label('Password') !!}
                        {{Form::password('password', ['required','class' => 'form-control','placeholder' => 'Password'])}}
                    </div>
                </div>
                <hr/>

                <h3 class="box-title">Personal Info</h3>
                <div class="form-group row">
                    <div class="col-md-6">
                        {!! Form::label('First Name') !!}
                        {{ Form::text('fname',null,['required','class'=>'form-control','placeholder'=>'FirstName']) }}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('Last Name') !!}
                        {{Form::text('lname', null , ['required','class' => 'form-control','placeholder' => 'Last Name'])}}
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                        {!! Form::label('MyKAD / IC Number / Passport Number') !!}
                        {{Form::text('ic', null , ['required','class' => 'form-control','placeholder' => 'MyKAD / IC / Passport'])}}
                    </div>
                </div>
                <hr/>

                <h3 class="box-title">Employee Info</h3>
                <div class="form-group row">
                    <div class="col-md-4">
                        {!! Form::label('Company') !!}
                        <select class="form-control" name="company_id" id="company_id" required data-parsley-required="true">
                            <option value="">Please  Select</option>
                            @foreach ($organisations as $organisation)
                            <option value="{{ $organisation->id }}">{{ $organisation->company_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-4">
                        {!! Form::label('Department') !!}
                        <select class="form-control" name="department_id" id="department_id" required data-parsley-required="true">
                            <option value="">Please  Select</option>
                            @foreach ($departments as $department)
                            <option value="{{ $department->id }}">{{ $department->department_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-4">
                        {!! Form::label('Designation') !!}
                        <select class="form-control" name="designation_id" id="designation_id" required data-parsley-required="true">
                            <option value="">Please  Select</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-4">
                        {!! Form::label('Date Joined') !!}
                        {{ Form::date('joined_at',null,['required','class'=>'form-control','placeholder'=>'Date Joined']) }}
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('Employee ID') !!}
                        {{Form::text('employee_uid', null , ['required','class' => 'form-control','placeholder' => 'Employee ID'])}}
                    </div>
                    <div class="col-md-4">
                        {!! Form::label('Employment Status') !!}
                        <select name="status" class="form-control" required>
                            <option value="">Please  Select</option>
                            <option value="Permanent">Permanent</option>
                            <option value="Contract">Contract</option>
                            <option value="Intern">Intern</option>
                        </select>
                    </div>
                </div>
                <hr/>

                <h3 class="box-title">Position Details</h3>
                <div class="form-group row m-b-40">
                    <div class="col-md-4">
                        {!! Form::label('Hierarchy') !!}
                        <select class="form-control" name="hierarchy_id" id="hierarchies" required data-parsley-required="true">
                            <option value="">Please  Select</option>
                            @foreach ($hierarchies as $hierarchy)
                            <option value="{{ $hierarchy->id }}">{{ $hierarchy->hierarchy_name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-4">
                        {!! Form::label('Deployment Role') !!}
                        <select class="form-control" name="role_id" id="role_id" required data-parsley-required="true">
                            <option value="">Please  Select</option>
                            @foreach ($roles as $role)
                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>

                </div>

                {{Form::submit('Add Employee',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>

@section('js')
    @parent

    <script>
    $(document).ready(function(){

        $("#department_id").change(function(){
            var deptid = $(this).val();

            $.ajax({
                url: '/designations/department/'+deptid,
                type: 'GET',
                dataType: 'json',
                success:function(response){
                    var len = response.length;
                    $("#designation_id").empty();
                    $("#designation_id").append("<option value=''>Please Select</option>");
                    for( var i = 0; i<len; i++){
                        var id = response[i]['id'];
                        var name = response[i]['designation_name'];
                        $("#designation_id").append("<option value='"+id+"'>"+name+"</option>");

                    }
                }
            });
        });

    });
    </script>


    <script>

        $('#add-employee').on('hidden.bs.modal', function(e) {
            $(this).find('#employee_create')[0].reset();
        });

        $( "#employee_create" ).on( "submit", function( event ) {
            event.preventDefault();
            swal({
                title: "Submit Employee",
                text: "Are you sure you want to submit this employee?",
                type: "info",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                allowOutsideClick: () => !swal.isLoading(),
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        var form = document.getElementById('employee_create');
                        var formData = new FormData(form);
                        $.ajax({
                            type: "POST",
                            url: '{{ route('employees.store') }}',
                            data: formData,
                            cache:false,
                            contentType: false,
                            processData: false,
                            success: function(data) {
                                resolve(data);
                            },
                            error: function(data) {
                                errormsg = '';
                                if(data.status == 422 ){
                                    $.each(data.responseJSON.errors, function (key, value) {
                                        errormsg = errormsg + value[0] + "<br />";
                                    });
                                }else{
                                    errormsg = "Server error. Please try again";
                                }
                                swal.showValidationMessage(errormsg);
                                swal.hideLoading();
                            },
                        });
                    });

                }
            }).then((result) => {
                if(result.value){
                    $('#add-employee').modal('toggle');
                    $('#employee_table').DataTable().ajax.reload();
                    swal('New Employee Added!', '--', 'success');
                }
            });
        });
        </script>

@endsection
