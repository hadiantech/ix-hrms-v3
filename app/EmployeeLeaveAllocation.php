<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeLeaveAllocation extends Model
{
    protected $guarded = [];

    public function scopeType($query, $type){
        return $query->where('leave_type', $type);
    }
}
