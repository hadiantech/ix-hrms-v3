{{-- Modal --}}
<div class="modal fade" id="modal_flight_edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h1>Edit Flight</h1>

                {!! Form::open(['url' => '', 'id' => 'flight_edit']) !!}
                <input type="hidden" name="_method" value="PUT">
                <input id="flight_id" name="id" type="hidden">
                <div class="form-group row">
                    <div class="col-md-12">
                        <select name="flight_type" class="form-control" >
                            <option selected disabled>Flight Type</option>
                            <option value="First Class">First Class</option>
                            <option value="Business Class">Business Class</option>
                            <option value="Economy Class">Economy Class</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        {{ Form::date('travel_date',null,['class' => 'form-control', 'placeholder' => 'Travel Date'])}}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        {{ Form::text('arrive',null,['class' => 'form-control', 'placeholder' => 'Arrive To'])}}
                    </div>
                    <div class="col-md-6">
                        {{ Form::text('depart',null,['class' => 'form-control', 'placeholder' => 'Depart From'])}}
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        {{ Form::textarea('detail',null,['class' => 'form-control', 'placeholder' => 'Flight Details...'])}}
                    </div>
                </div>
                <button type="submit" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">
                    Save Flight
                </button>
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">
                    Close
                </button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@section('js')
@parent
<script>
    $("#traveller_edit").on("submit", function (event) {
        //console.log('mew');
        event.preventDefault();
        swal({
            title: "Update",
            text: "Are you sure you want to update?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function (resolve, reject) {
                    var form = document.getElementById('traveller_edit');
                    var formData = new FormData(form);
                    //console.log(form.traveller_id.value);
                    $.ajax({
                        type: "POST",
                        url: "/travellers/" + form.flight_id.value,
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            resolve(data);
                        },
                        error: function () {
                            swal.showValidationMessage('server error');
                            swal.hideLoading();
                        }
                    });
                });
            }
        }).then((result) => {
            if (result.value) {
                $('#modal_traveller_edit').modal('toggle');
                $('#traveller_table').DataTable().ajax.reload();
                swal('Updated!', '--', 'success');

            }

        });
    });
</script>
@endsection
