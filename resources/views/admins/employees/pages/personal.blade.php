<div class="row">

    <div class="col-md-4 text-center m-b-40">
        @if($employee->getMedia('profile')->first())
        <img src="{{ $employee->getMedia('profile')->first()->getUrl() }}" class="profile-photo">
        @else
        <img src="{{ asset('images/Avatar.png') }}" class="profile-photo">
        @endif
        <br>
        <a onclick="upload_image()" class="m-t-30 btn btn-rounded btn-info text-uppercase">Upload Photo</a>
    </div>

    <div class="col-md-8">

        {!! Form::model($employee, ['route' => ['profiles.update', $employee->id], 'id' => 'employee_update', 'method' => 'PUT']) !!}
        <div class="form-group row">
            <div class="col-md-6">
                {!! Form::label('First Name') !!}
                {!! Form::text('fname', null, array('class' => 'form-control', 'placeholder' => 'First Name')) !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('Last Name') !!}
                {!! Form::text('lname', null, array('class' => 'form-control', 'placeholder' => 'Last Name')) !!}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-4">
                {!! Form::label('Date of Birth') !!}
                {{ Form::date('dob', null,  array('class' => 'form-control', 'placeholder' => 'Date of Birth')) }}
            </div>
            <div class="col-md-4">
                {!! Form::label('Gender') !!}
                {{Form::select('gender', ['Male' => 'Male', 'Female' => 'Female'], null, ['class' => 'form-control', 'placeholder' => 'Please Select'])}}
            </div>
            <div class="col-md-4">
                {!! Form::label('Marriage Status') !!}
                {{Form::select('marriage_status', ['Single' => 'Single', 'Married' => 'Married' , 'Widowed' => 'Widowed'], null, ['class' => 'form-control', 'placeholder' => 'Please Select'])}}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                {!! Form::label('Race') !!}
                {{Form::select('race', ['Melayu' => 'Melayu', 'Chinese' => 'Chinese' , 'India' => 'India', 'Others' => 'Others'], null, ['class' => 'form-control', 'placeholder' => 'Please Select'])}}
            </div>
            <div class="col-md-6">
                {!! Form::label('Race: If Others') !!}
                {!! Form::text('race_other', null, array('class' => 'form-control', 'placeholder' => 'Race: Others')) !!}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                {!! Form::label('Religion') !!}
                {{Form::select('religion', ['Islam' => 'Islam', 'Others' => 'Others' ], null, ['class' => 'form-control', 'placeholder' => 'Please Select'])}}
            </div>
            <div class="col-md-6">
                {!! Form::label('Religion: If Others') !!}
                {!! Form::text('religion_other', null, array('class' => 'form-control', 'placeholder' => 'Religion: Others')) !!}
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-6">
                {!! Form::label('MyKAD / IC Number') !!}
                {!! Form::text('ic', null, array('class' => 'form-control', 'placeholder' => 'IC')) !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('Nationality') !!}
                @include('shared._countries', ['c_name' => 'nationality', 'default_value' => $employee->nationality])
                {{-- {{Form::select('nationality', ['Malaysia' => 'Malaysia', 'Singapore' => 'Singapore' , 'Thailand' => 'Thailand', 'Others' => 'Others'], null, ['class' => 'form-control', 'placeholder' => 'Please Select'])}} --}}
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-6">
                {!! Form::label('Passport') !!}
                {!! Form::text('passport', null, array('class' => 'form-control', 'placeholder' => 'Passport')) !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('Passport Expired') !!}
                {{ Form::date('passport_expired_at', null,  array('class' => 'form-control', 'placeholder' => 'Passport Expired')) }}
            </div>
        </div>

        <div class="employee-addresses m-b-30">
            <div class="form-group row">
                <div class="col-md-12">
                    <h3 class="box-title">Permanent Address</h3>
                </div>
                <div class="col-md-6">
                        {!! Form::text('pre_address_line1', null, ['class' => 'form-control', 'placeholder'=>'Address Line 1']) !!}
                </div>
                <div class="col-md-6">
                        {!! Form::text('pre_address_line2', null, ['class' => 'form-control', 'placeholder'=>'Address Line 2']) !!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                        {!! Form::text('pre_city', null, ['class' => 'form-control', 'placeholder'=>'City']) !!}
                </div>
                <div class="col-md-6">
                        {!! Form::text('pre_state', null, ['class' => 'form-control', 'placeholder'=>'State']) !!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                        {!! Form::text('pre_postcode', null, ['class' => 'form-control', 'placeholder'=>'Postcode']) !!}
                </div>
                <div class="col-md-6">
                        {!! Form::text('pre_country', null, ['class' => 'form-control', 'placeholder'=>'Country']) !!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    {!! Form::text('pre_phone_number1', null, ['class' => 'form-control', 'placeholder'=>'Phone No.']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::text('pre_phone_number2', null, ['class' => 'form-control', 'placeholder'=>'Mobile No.']) !!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <h3 class="box-title">Correspondance Address</h3>
                    <div class="m-b-15 checkbox checkbox-info">
                        <input id="checkbox4" type="checkbox">
                        <label for="checkbox4">Same as the above?</label>
                    </div>
                </div>
                <div class="col-md-6">
                    {!! Form::text('per_address_line1', null, ['class' => 'form-control', 'placeholder'=>'Address Line 1']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::text('per_address_line2', null, ['class' => 'form-control', 'placeholder'=>'Address Line 2']) !!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    {!! Form::text('per_city', null, ['class' => 'form-control', 'placeholder'=>'City']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::text('per_state', null, ['class' => 'form-control', 'placeholder'=>'State']) !!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    {!! Form::text('per_postcode', null, ['class' => 'form-control', 'placeholder'=>'Postcode']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::text('per_country', null, ['class' => 'form-control', 'placeholder'=>'Country']) !!}
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    {!! Form::text('per_phone_number1', null, ['class' => 'form-control', 'placeholder'=>'Phone No.']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::text('per_phone_number2', null, ['class' => 'form-control', 'placeholder'=>'Mobile No.']) !!}
                </div>
            </div>
        </div>


        <div class="bank-info   m-b-30">
            <div class="section-title">
                <h3 class="box-title">Banking Info</h3>

            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    {!! Form::text('bank_name', null, ['class' => 'form-control', 'placeholder'=>'Bank Name']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::text('bank_account_no', null, ['class' => 'form-control', 'placeholder'=>'Account No']) !!}
                </div>
            </div>
        </div>


        <button style="width:200px" class=" btn-lg btn-rounded btn-info text-uppercase">Save</button>
        {!! Form::close() !!}

        <hr />

        {{-- Contact start --}}
        @include('admins.employees.pages.personal.contact.index')
        {{-- Contact end --}}

        {{-- History start --}}
        @include('admins.employees.pages.personal.history.index')
        {{-- History end --}}

        {{-- Education start --}}
        @include('admins.employees.pages.personal.education.index')
        {{-- Education end --}}

        {{-- Certificate start --}}
        @include('admins.employees.pages.personal.professional.index')
        {{-- Certificate end --}}

    </div>


</div>


@section('js')
    @parent

    <script>

        function upload_image(){
            swal({
                title: 'Upload your profile picture',
                input: 'file',
                inputAttributes: {
                    accept: 'image/*'
                },
                showCancelButton: true,
                confirmButtonText: 'Upload',
                showLoaderOnConfirm: true,
                allowOutsideClick: () => !swal.isLoading(),
                preConfirm: function(i) {
                    return new Promise(function() {

                        var form_data = new FormData();
                        form_data.append('image', i);

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax({
                            type:'POST',
                            url: '{{ route('employees.image.upload', $employee->id) }}',
                            data: form_data,
                            cache:false,
                            contentType: false,
                            processData: false,
                            dataType: "json",
                            success:function(data){
                                swal({
                                    title: 'Success!',
                                    text: 'Your profile image has been updated.',
                                    type: 'success',
                                    onClose: () => {
                                        location.reload();
                                    }
                                })
                            },
                            error: function(data){

                            }
                        });
                    });
                }
            })

        }

    </script>


    <script>

        $( "#employee_update" ).on( "submit", function( event ) {
            event.preventDefault();
            swal({
                title: "Submit Employee",
                text: "Are you sure you want to submit this employee?",
                type: "info",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                allowOutsideClick: () => !swal.isLoading(),
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        var form = document.getElementById('employee_update');
                        var formData = new FormData(form);
                        $.ajax({
                            type: "POST",
                            url: '{{ route('employees.update', $employee->id) }}',
                            data: formData,
                            cache:false,
                            contentType: false,
                            processData: false,
                            success: function(data) {
                                resolve(data);
                            },
                            error: function(data) {
                                errormsg = '';
                                if(data.status == 422 ){
                                    $.each(data.responseJSON.errors, function (key, value) {
                                        errormsg = errormsg + value[0] + "<br />";
                                    });
                                }else{
                                    console.log(data);
                                    errormsg = "Server error. Please try again";
                                }
                                swal.showValidationMessage(errormsg);
                                swal.hideLoading();
                            },
                        });
                    });

                }
            }).then((result) => {
                if(result.value){
                    swal('Employee Updated!', '--', 'success');
                }
            });
        });
        </script>

@endsection
