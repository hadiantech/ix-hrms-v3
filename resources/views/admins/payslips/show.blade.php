<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Process Employee Claim</h4>
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                <ol class="breadcrumb">
                    <li><a href="dashboard-adminview.html">Dashboard</a></li>
                    <li><a href="admin-claim-dashboard.html">Claim Dashboard</a></li>
                    <li class="active">Employee Claim</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>


         <div class="row">
            <div class="col-sm-12">
                <div class="white-box">
                    <h3 class="m-b-20 box-title text-uppercase">Employee Monthly Claim</h3>

                        <div class="employee-claim-details">



                                <div class="row">
                                    <div class="col-md-4">
                                        <blockquote>
                                        <h4>Employee Details</h4>
                                           <label>Name</label> <span class="m-l-5">Donalvean Angew</span><br>
                                           <label>ID</label> <span class="m-l-5">MYIX123</span><br>

                                           <label>Department</label> <span class="m-l-5">Engineering, IX Telecom</span><br>

                                           <label>Applied Date</label> <span class="m-l-5">13 Sept 2018</span><br>
                                           <label>Claim</label> <span class="m-l-5">March 2018</span>

                                       </blockquote>
                                    </div>
                                    <div class="col-md-4">

                                          <div class="form-group">
                                            <select class="selectpicker" data-style="form-control">
                                                <option selected>Pending</option>
                                                <option>Return</option>
                                                <option>Reviewed</option>
                                            </select>
                                          </div>
                                          <div class="form-group">
                                            <textarea rows="5" class="form-control">Add overall remarks...</textarea>
                                          </div>
                                    </div>
                                </div>

                            </blockquote>
                        </div>

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr><th class="hasCheck">
                                        <div class="checkbox checkbox-info">
                                            <input onClick="selectAll(this)" type="checkbox">
                                            <label for="checkbox4"></label>
                                        </div>
                                        </th>
                                        <th>#</th>
                                        <th>Type</th>
                                        <th>Item Name & Desc</th>
                                        <th class="text-center">Project Code</th>
                                        <th class="text-center">Amount (RM)</th>
                                        <th class="text-center">Item Date</th>
                                        <th class="text-center">Receipt No.</th>
                                        <th class="text-center">Ref No.</th>
                                        <th class="text-right">Remarks</th>
                                </thead>
                                <tbody>
                                    <tr><td>
                                        <div class="checkbox checkbox-info">
                                            <input name="select" type="checkbox">
                                            <label for="checkbox4"></label>
                                        </div>
                                        </td>
                                        <td>1</td>
                                        <td>Purchases</td>
                                        <td>Ninja Form Add-ons
                                            <p class="text-muted">Add-on plugin for website purposes</p>
                                        </td>
                                        <td class="text-center">MYIX1234</td>
                                        <td class="text-center">45.00</td>
                                        <td class="text-center">14 Mar 2018</td>
                                        <td class="text-center">324AD</td>
                                        <td class="text-center">1</td>
                                        <td class="text-right">
                                            <a href="#add-remarks" data-toggle="modal" class="label label-info m-r-5"><i class="fa fa-pencil"></i></a>
                                            </th>
                                    </tr>
                                    <tr>
                                        <td>
                                        <div class="checkbox checkbox-info">
                                            <input name="select" type="checkbox">
                                            <label for="checkbox4"></label>
                                        </div>
                                        </td>
                                        <td>2</td>
                                        <td>Mileage</td>
                                        <td>Trip to Teraju
                                            <p class="text-muted">One way trip from IX to Teraju<br>
                                            Distance : 33.5KM</p>
                                        </td>
                                        <td class="text-center">MYIX1234</td>
                                        <td class="text-center">16.75</td>
                                        <td class="text-center">14 Mar 2018</td>
                                        <td class="text-center">324AD</td>
                                        <td class="text-center">2</td>
                                        <td class="text-right">
                                            <a href="" class="label label-info m-r-5"><i class="fa fa-pencil"></i></a>
                                           </th>
                                    </tr>
                                    <tr>
                                        <td>
                                        <div class="checkbox checkbox-info">
                                            <input name="select" type="checkbox">
                                            <label for="checkbox4"></label>
                                        </div>
                                        </td>
                                        <td>3</td>
                                        <td>Purchases</td>
                                        <td>Ninja Form Add-ons
                                            <p class="text-muted">Add-on plugin for website purposes</p>
                                        </td>
                                        <td class="text-center">MYIX1234</td>
                                        <td class="text-center">86.00</td>
                                        <td class="text-center">19 Mar 2018</td>
                                        <td class="text-center">324AD</td>
                                        <td class="text-center">2</td>
                                        <td class="text-right">
                                            <a href="" class="label label-info m-r-5"><i class="fa fa-pencil"></i></a>

                                        </th>
                                    </tr>

                                </tbody>
                                <tfoot class="claim-amount">
                                    <tr>
                                        <th colspan="7"></th>
                                        <th class="text-right">TOTAL AMOUNT</th>
                                        <th colspan="2" class="text-right"><span class="amount">RM 350.00</span></th>
                                    </tr>
                                </tfoot>
                            </table>

                    </div>

                    <button style="width:100px" class="m-r-10 btn btn-rounded btn-info text-uppercase">Return</button>
                    <button style="width:100px" class=" m-r-10 btn btn-rounded btn-success text-uppercase">Approve</button>
                    <button style="width:100px" class=" btn btn-rounded btn-danger text-uppercase">Close</button>

                </div>
           </div>


        </div>


        </div>

    </div>
    <!-- /.row -->

    <div class="modal fade" id="add-remarks">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Add Remarks</h1>
                <p>Remark for item #1</p>
                <div class="form-group">
                    <textarea class="form-control" rows="4"></textarea>
                </div>
                <button type="button" class="m-r-5 text-uppercase btn btn-info btn-rounded waves-effect waves-light">Add Remarks</button>
        <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!-- /.container-fluid -->
</div>
