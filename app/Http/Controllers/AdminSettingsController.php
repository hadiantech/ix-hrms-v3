<?php

namespace App\Http\Controllers;

use App\EmployeeDocument;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use DataTables;

class AdminSettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:view_system_setting']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $date_format = [
            'd/m/Y' => '01/01/2018',
            'd-m-Y' => '01-01-2018',
            'd M Y' => '01 Jan 2018',
            'M d Y' => 'Jan 01 2018',
        ];
        return view('admins.settings.index', compact('date_format'));
    }

    public function list_document(){
        return Datatables::eloquent(EmployeeDocument::where('shared', 1))
        ->addColumn('url_view', function($model){
            return view('admins.settings.pages.btn_url_view', compact('model'))->render();
        })
        ->addColumn('action', function($model){
            $show_hide = true;
            $edit_hide = true;
            $url = 'default_documents';
            $show_hide = true;
            return view('shared._table_action', compact('model', 'model_2','url', 'show_hide', 'edit_hide'))->render();
        })
        ->addIndexColumn()
        ->escapeColumns([])
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    public function store_leave(Request $request){
        settings()->set('Annual Leave', $request->annual_leave);
        settings()->set('Emergency Leave', $request->emergency_leave);
        settings()->set('Medical Leave', $request->medical_leave);
        settings()->set('Compassionate Leave', $request->compassionate_leave);
        settings()->set('Maternity Leave', $request->maternity_leave);
        settings()->set('Paternity Leave', $request->paternity_leave);
        settings()->set('Marriage Leave', $request->marriage_leave);
        settings()->set('working_days_to_apply_annual_leave', $request->working_days_to_apply_annual_leave);
        settings()->set('max_calendar_to_apply_annual_leave', $request->max_calendar_to_apply_annual_leave);
        settings()->set('working_days_to_apply_lieu_days', $request->working_days_to_apply_lieu_days);
        return response()->json('success', 201);
    }

    public function store_system(Request $request){
        settings()->set('date_format', $request->date_format);
        settings()->set('currency_code', $request->currency_code);
        return response()->json('success', 201);
    }

    public function store_document(Request $request){

        $request->validate([
            'name' => 'required',
            'url' => 'nullable|url',
            'document' => 'max:2048',
        ]);

        if(!$request->url  && !$request->hasFile('document') ){
            $returnData = array(
                'document' => array('Please upload document or insert url link')
            );
            return response()->json(['errors' => $returnData], 422);
        }

        $document = EmployeeDocument::create($request->except('document'));

        if ($request->hasFile('document')) {
            $document->addMediaFromRequest('document')->toMediaCollection('document');
        }

        return response()->json('success', 201);
    }

    public function delete_document($id){
        $doc = EmployeeDocument::find($id);
        $doc->delete();
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
