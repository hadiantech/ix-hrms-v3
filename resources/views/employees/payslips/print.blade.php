@extends('layouts.default.printer')
@section('content')

<body style="padding:50px; background: #f6f6f6">

    <div style="width:700px;margin:0 auto;background:#fff; color:#333; padding:25px">
        <div style="float:right;overflow:auto;margin-bottom:30px" class="logo-print">
           <img src="../plugins/images/admin-text-dark.png">
        </div>

        <div class="payslip-header">
            <table border="0" width="100%">
                <tr>
                    <td style="vertical-align: top">
                        <h3 style="margin-top:0;">PAYSLIP NO: #Payslip[*]</h3>
                        <strong>Date</strong>: ***** <br>
                    </td>
                    <td style="text-align:right;"><div class="payslip-address">
                        <p>{{ $employees->organisation->company_name }}<br>{{ $employees->organisation->address1 }}<br>{{ $employees->organisation->address2 }}<br>{{ $employees->organisation->city }}<br>{{ $employees->organisation->state }} </p>
                    </div></td>
                </tr>
                <tr>
                    <td>
                        <table border="0">
                            <tr>
                                <td style="font-weight:500">Employee ID</td><td style="padding-left:5px">{{ $employees->id }}</td>
                            </tr>
                            <tr>
                                <td style="font-weight:500">Employee Name</td><td style="padding-left:5px">{{ $employees->fname }}</td>
                            </tr>
                            <tr>
                                <td style="font-weight:500">Contact No</td><td style="padding-left:5px">{{ $employees->pre_phone_number1 }}</td>
                            </tr>
                            <tr>
                                <td style="font-weight:500">Payment Method</td><td style="padding-left:5px">{{ $employees->salary->payment_type }}</td>
                            </tr>
                            <tr>
                                <td style="font-weight:500">Department</td><td style="padding-left:5px">{{ $employees->department->department_name }}</td>
                            </tr>
                            <tr>
                                <td style="font-weight:500">Designation</td><td style="padding-left:5px">{{ $employees->designation->designation_name }}</td>
                            </tr>
                        </table>
                    </td>
                    <td style="text-align:right; vertical-align: bottom">
                        <table bordercolor="#bbb" width="100%" class="table-payslip" border="1">
                            <tr>
                                <th>Payment No</th>
                                <td> ***** </td>
                            </tr>
                            <tr>
                                <th>Payment Month</th>
                                <td> ***** </td>
                            </tr>
                            <tr>
                                <th>Basic Salary</th>
                                <td>RM {{ $a }}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <table bordercolor="#bbb" width="100%" border="1" style="margin-top:30px" class="earnings-deduction">
                <tr>
                    <th style="width:50%; text-align:center; background:#eee">Earnings</th>
                    <th style="width:50%; text-align:center; background:#eee">Deductions</th>
                </tr>
                <tr>
                    <td style="vertical-align: top">
                        <table width="100%">
                            {!! Form::open(['url' => 'generates', "id" => "payroll_create"]) !!}
                        <table class="table" width="100%">
                            <tr>
                                <td>
                                    <div class="row" style="display:flex; justify-content:space-between;">
                                        <div class="">
                                            {!! Form::label('Basic Earning') !!}
                                        </div>
                                        <div class="">
                                            {{ $a }}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                        @foreach ($generates as $gen)
                                        @if ($gen->operand_id == null)
                                            {{ "" }}
                                        @elseif ($gen->operand_id == 1)
                                    <div class="row" style="display:flex; justify-content:space-between; margin-bottom:5px">
                                        <div class="">
                                            {{ $gen->paycategory->name }}
                                        </div>
                                        <div style="text-align:right">
                                            {{ $gen->value }}
                                        </div>
                                    </div>
                                    @else ($gen->operand_id == 2)
                                        {{ "" }}
                                    @endif

                                    @endforeach
                                </div>
                                    </tr>
                                </td>
                            </tr>

                        </table>
                    </td>

                    <td style="vertical-align: top">
                        <table width="100%">
                            <tr>
                                {!! Form::open(['url' => 'payrolls', "id" => "payroll_create"]) !!}
                                <table class="table" width="100%">
                                        <tr>
                                                <td>
                                                    <div class="row" style="display:flex; justify-content:space-between; margin-bottom:-2px">
                                                        <div class="">
                                                            {!! Form::label('Unpaid Leave') !!}
                                                        </div>
                                                        <div class="">
                                                            {{ '***refer leave' }}
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                    <td>
                                                        @foreach ($generates as $gen)
                                                        @if ($gen->operand_id == null)
                                                        {{ "" }}
                                                        @elseif ($gen->operand_id == 2)
                                                        <div class="row" style="display:flex; justify-content:space-between; margin-bottom:5px">
                                                            <div class="">
                                                                {{ $gen->paycategory->name }}
                                                            </div>
                                                            <div style="text-align:right">
                                                                {{ $gen->value }}
                                                            </div>
                                                        </div>
                                                        @else ($gen->operand_id == 1)
                                                            {{ "" }}
                                                        @endif

                                                        @endforeach
                                                        </div>
                                                    </td>
                                                </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table border="0" width="100%">
                            <tr>
                                <td>Total</td>
                                <td style="text-align:right">RM {{ $totaladd + $a }}</td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table border="0" width="100%">
                            <tr>
                                <td>Total</td>
                                <td style="text-align:right">RM {{ $totalded }}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td><b>TOTAL</b></td>
                    <td style="text-align:right"><b> RM {{ $totaladd + $a - $totalded }}</b></td>
                </tr>
            </table>
        </div>

         <div style="margin-top:30px" class="net-salary">
            <table class="earnings-deduction" width="100%" border="1" bordercolor="#bbb">
                <tr>
                    <th style="background:#eee" width="50%">Employer Contribution ***** </th>
                    <th style="background:#eee;text-align:right" width="50%" >Year To Date ***** </th>
                </tr>
                <tr>
                    <td style="vertical-align: top">
                        <table class="employer-cont" width="100%">
                            <tr>
                                <td>Employer EPF</td><td>RM 600.00</td>
                            </tr>
                            <tr>
                                <td>Employer Socso</td><td>RM 25.00</td>
                            </tr>
                            <tr>
                                <td>Employer EIS</td><td>RM 1.00</td>
                            </tr>
                            <tr>
                                <td>Bank</td><td>Affin Bank</td>
                            </tr>
                        </table>
                    </td>
                    <td  style="vertical-align: top">
                        <table class="employer-cont" width="100%">
                            <tr>
                                <td>Gross Pay</td><td>RM 600.00</td>
                            </tr>
                            <tr>
                                <td>Nett Pay</td><td>RM 25.00</td>
                            </tr>
                            <tr>
                                <td>Employer EPF</td><td>RM 1.00</td>
                            </tr>
                            <tr>
                                <td>Employer Socso</td><td>RM 1.00</td>
                            </tr>
                            <tr>
                                <td>Employer EIS</td><td>RM 1.00</td>
                            </tr>
                            <tr>
                                <td>Employee EPF</td><td>RM 1.00</td>
                            </tr>
                            <tr>
                                <td>Employee Socso</td><td>RM 1.00</td>
                            </tr>
                            <tr>
                                <td>Employee EIS</td><td>RM 1.00</td>
                            </tr>
                            <tr>
                                <td>Employee Tax</td><td>RM 1.00</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <br><br>
        <p style="font-size:12px;text-align:center">payslip generated via IX HRMS</p>
    </div>

</body>

</html>

@endsection
