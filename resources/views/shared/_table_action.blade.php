@php
    $permission_name = $url;

    if (strpos($url, ".") !== false) {
        if(isset($model_2)){
            $doublemodel = true;
            $permission_name = str_replace(".", "_", $url);
        }else{
            //escape error
            $permission_name = 'not_allow';
        }
    }

    if(isset($custom_permission)){
        $permission_name = $custom_permission;
    }

    if(!isset($show_hide)){
        $show_hide = false;
    }

    if(!isset($edit_hide)){
        $edit_hide = false;
    }

    if(!isset($delete_hide)){
        $delete_hide = false;
    }
@endphp

@isset($custom_btn)
    @foreach ($custom_btn as $btn)
        {!! $btn !!}
    @endforeach
@endisset

@can('view_'.$permission_name)
@if($show_hide == false)

@if(isset($modal_view_id))
<a class="btn btn-xs btn-success open-modal" data-toggle="modal" data-modal_data="{{ $model }}"

@isset($modal_data_customs)
    @foreach ($modal_data_customs as $key => $data)
        data-{{ $key }}="{{ $data }}"
    @endforeach
@endisset

href="#{{ $modal_view_id }}">Show</a>
@else

@if(isset($doublemodel))
<a class="btn btn-xs btn-success" href="{{route($url.'.show', [$model_2->id, $model->id])}}">Show</a>
@else
<a class="btn btn-xs btn-success" href="{{route($url.'.show', $model->id)}}">Show</a>
@endif

@endif

@endif
@endcan

@can('edit_'.$permission_name)
@if($edit_hide == false)

@if(isset($modal_edit_id))
<a class="btn btn-xs btn-warning open-modal" data-toggle="modal" data-modal_data="{{ $model }}"

@isset($modal_data_customs)
    @foreach ($modal_data_customs as $key => $data)
        data-{{ $key }}="{{ $data }}"
    @endforeach
@endisset

href="#{{ $modal_edit_id }}">Edit</a>
@else

@if(isset($doublemodel))
<a class="btn btn-xs btn-warning" href="{{route($url.'.edit', [$model_2->id, $model->id])}}">Edit</a>
@else
<a class="btn btn-xs btn-warning" href="{{route($url.'.edit', $model->id)}}">Edit</a>
@endif

@endif

@endif
@endcan

@can('delete_'.$permission_name)
@if($delete_hide == false)

@if(isset($delete_datatable_id_name_refresh))
<a class="btn btn-xs btn-danger" data-method="delete" data-table-id="{{ $delete_datatable_id_name_refresh }}" data-confirm="confirm" href="{{ route($url.'.destroy', $model->id) }}">Delete</a>
@else

@if(isset($doublemodel))
<a class="btn btn-xs btn-danger" data-method="delete" data-confirm="confirm" href="{{ route($url.'.destroy', [$model_2->id, $model->id]) }}">Delete</a>
@else
<a class="btn btn-xs btn-danger" data-method="delete" data-confirm="confirm" href="{{ route($url.'.destroy', $model->id) }}">Delete</a>
@endif

@endif

@endif
@endcan
