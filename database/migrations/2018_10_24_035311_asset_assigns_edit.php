<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AssetAssignsEdit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset_assigns', function (Blueprint $table) {
            $table->string('assign_type')->after('employee_id')->nullable();
            $table->string('assign_brand')->after('ended_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('asset_assigns', function (Blueprint $table) {
            //
        });
    }
}
