<div id="add-designation" class="modal fade in" tabindex="-1" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Add New Designation</h1>
                {!! Form::open(['url' => 'designations', 'id' => 'designation_create']) !!}
                <div class="row form-group">
                    <div class="col-md-8">
                        {{Form::text('designation_name', null , ['required','class' => 'form-control','placeholder' => 'Designation Name'])}}
                    </div>
                    <div class="col-md-4">
                        <select class="form-control" name="department_id" id="departments" data-parsley-required="true">
                        @foreach ($departments as $department)
                            <option value="{{ $department->id }}">{{ $department->department_name }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                {{Form::submit('Add Designation',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

                {!! Form::close() !!}
            </div>

        </div>
    </div>
</div>

@section('js')
    @parent
    <script>

    $('#add-designation').on('hidden.bs.modal', function(e) {
        $(this).find('#designation_create')[0].reset();
    });

    $( "#designation_create" ).on( "submit", function( event ) {
        event.preventDefault();
        swal({
            title: "Submit Designation",
            text: "Are you sure you want to submit this designation?",
            type: "info",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            preConfirm: function() {
                var form = document.getElementById('designation_create');
                var formData = new FormData(form);
                $.ajax({
                    type: "POST",
                    url: '{{ route('designations.store') }}',
                    data: formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    success: function(msg) {
                        return msg;
                    }
                });
            }
        }).then((result) => {
            $('#add-designation').modal('toggle');
            if(result.value){
                $('#designation_table').DataTable().ajax.reload();
                swal('New Designation Added!', '--', 'success');
            }
        });
    });
    </script>
@endsection
