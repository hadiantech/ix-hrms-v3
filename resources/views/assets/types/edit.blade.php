<div id="modal_type_edit" class="modal fade in" tabindex="-1" role="dialog" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    <h1>Edit Category Details </h1>
                        {!! Form::open(['url' => 'types', 'id' => 'type_edit']) !!}
                        {{ Form::hidden('id', null , ['id' => 'id']) }}
                        <input type="hidden" name="_method" value="PUT">
                        <div class="form-group row ">
                            <div class="col-md-6">{{Form::text('type_name', null , ['id' => 'type_name_edit', 'required','class' => 'form-control','placeholder' => 'edit asset type'])}}</div>
                            <div class="col-md-6"> {{Form::select('status', ['Active' => 'Active', 'Inactive' => 'Inactive'], null, ['class' => 'form-control', 'placeholder' => 'Select Status'])}}</div>
                            
                        </div>
                        
                        {{Form::submit('Update Details',['class' =>'btn btn-rounded btn-info text-uppercase'])}}
                        <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>

                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    @section('js')
        @parent
        <script>

        $( "#type_edit" ).on( "submit", function( event ) {
            event.preventDefault();
            swal({
                title: "Update Assets",
                text: "Are you sure you want to update this asset?",
                type: "info",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                preConfirm: function() {
                    var form = document.getElementById('type_edit');
                    var formData = new FormData(form);
                    $.ajax({
                        type: "POST",
                        url: '/types/'+form.id.value,
                        data: formData,
                        cache:false,
                        contentType: false,
                        processData: false,
                        success: function(msg) {
                            return msg;
                        }
                    });
                }
            }).then((result) => {
                $('#modal_type_edit').modal('toggle');
                if(result.value){
                    $('#type_table').DataTable().ajax.reload();
                    swal('Asset Updated!', '--', 'success');
                }
            });
        });
        </script>
    @endsection
