<div class="modal fade" id="add-cert" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                <h1>Add Professional Certificate</h1>
                {!! Form::open(['url' => '', 'id' => 'cert_create', 'files' => true]) !!}

                <div class="form-group row ">
                    <div class="col-md-6">
                        {!! Form::label('Company Name') !!}
                        <select name="field" class="form-control" required>
                            <option selected disabled>Field</option>
                            <option>Engineering</option>
                            <option>Marketing</option>
                            <option>Social</option>
                            <option>Accounting</option>
                            <option>Finance</option>
                            <option>Management</option>
                            <option>Others</option>
                        </select>
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('Field: Other') !!}
                        {!! Form::text('field_other', null, array('class' => 'form-control', 'placeholder' => 'If others, please specify field...')) !!}
                    </div>

                </div>
                <div class="form-group ">
                        {!! Form::label('Certificate Name') !!}
                        {!! Form::text('title', null, array('class' => 'form-control', 'placeholder' => 'Certificate Name', 'required')) !!}

                </div>
                <div class="form-group row">
                    <div class="col-md-6">
                        {!! Form::label('Issued By') !!}
                        {!! Form::text('issued_by', null, array('class' => 'form-control', 'placeholder' => 'Issued By', 'required')) !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('Expiry Date') !!}
                        {!! Form::date('expired_at', null, array('class' => 'form-control', 'placeholder' => 'Expiry Date', 'required')) !!}
                    </div>

                </div>

                <div class="form-group row">
                    <div class="col-md-6">
                        {!! Form::label('Reference No.') !!}
                        {!! Form::text('ref_number', null, array('class' => 'form-control', 'placeholder' => 'Reference No.', 'required')) !!}
                    </div>
                    <div class="col-md-6">
                        {!! Form::label('Attach Certificate') !!}
                        {!! Form::file('cert', null, array('class' => 'form-control', 'placeholder' => 'Attach Certificate', 'required')) !!}
                    </div>
                </div>

                {{ Form::submit('Add Certificate',['class' =>'btn btn-rounded btn-info text-uppercase']) }}
                <button type="button" class="text-uppercase btn btn-danger btn-rounded waves-effect" data-dismiss="modal">Close</button>
                {!! Form::close() !!}


            </div>
        </div>
    </div>
</div>

@section('js')
    @parent

    <script>

        $('#add-cert').on('hidden.bs.modal', function(e) {
            $(this).find('#cert_create')[0].reset();
        });

        $( "#cert_create" ).on( "submit", function( event ) {
            event.preventDefault();
            swal({
                title: "Submit Professional Certificate",
                text: "Are you sure you want to submit this certificate?",
                type: "info",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                allowOutsideClick: () => !swal.isLoading(),
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        var form = document.getElementById('cert_create');
                        var formData = new FormData(form);
                        $.ajax({
                            type: "POST",
                            url: '{{ route('employees.certificates.store', $employee->id) }}',
                            data: formData,
                            cache:false,
                            contentType: false,
                            processData: false,
                            success: function(data) {
                                resolve(data);
                            },
                            error: function(data) {
                                errormsg = '';
                                if(data.status == 422 ){
                                    $.each(data.responseJSON.errors, function (key, value) {
                                        errormsg = errormsg + value[0] + "<br />";
                                    });
                                }else{
                                    console.log(data);
                                    errormsg = "Server error. Please try again";
                                }
                                swal.showValidationMessage(errormsg);
                                swal.hideLoading();
                            },
                        });
                    });

                }
            }).then((result) => {
                if(result.value){
                    $('#add-cert').modal('toggle');
                    swal('Certificate Saved!', '--', 'success');
                    $('#cert_table').DataTable().ajax.reload();
                }
            });
        });

    </script>

@endsection
