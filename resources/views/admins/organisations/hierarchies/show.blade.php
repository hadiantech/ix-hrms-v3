<div id="show-hierarchy{{$hierarchy->id}}" class="modal fade in" tabindex="-1" role="dialog" >
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

                    <h1>Hierarchy Details</h1>

                    {!! Form::open(['url' => 'hierarchies']) !!}
                <div class="  form-group ">
                        {!! Form::label('Hierarchy Name') !!}
                    {{Form::text('hierarchies',$hierarchy->hierarchy_name,array('class' => 'form-control')) }}
                </div>
                <div class="  form-group ">
                        {!! Form::label('Rank') !!}
                        {{Form::text('hierarchies',$hierarchy->hierarchy_ranking,array('class' => 'form-control')) }}
                    </div>
                {{-- {{Form::submit('Close',['class' => 'btn btn-lg btn-info pull-right'])}} --}}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
