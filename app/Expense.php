<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $guarded = [];

    public function claim(){
        return $this->belongsTo(Claim::class,'claim_id');
    }

    public function expenseType(){
        return $this->belongsTo(ExpenseType::class,'item_type');
    }

    public function claimAdvance(){
        return $this->belongsTo(ExpenseType::class,'item_type');
    }

}
